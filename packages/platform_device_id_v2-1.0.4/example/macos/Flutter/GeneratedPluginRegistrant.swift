//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import platform_device_id_macos
import platform_device_id_v2

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  PlatformDeviceIdMacosPlugin.register(with: registry.registrar(forPlugin: "PlatformDeviceIdMacosPlugin"))
  PlatformDeviceIdMacosPlugin.register(with: registry.registrar(forPlugin: "PlatformDeviceIdMacosPlugin"))
}
