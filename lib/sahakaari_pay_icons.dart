/// Flutter icons SahakaariPay
/// Copyright (C) 2022 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  SahakaariPay
///      fonts:
///       - asset: fonts/SahakaariPay.ttf
///
/// 
/// * Elusive, Copyright (C) 2013 by Aristeides Stathopoulos
///         Author:    Aristeides Stathopoulos
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://aristeides.com/
/// * Font Awesome 5, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL (https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
///
import 'package:flutter/widgets.dart';

class SahakaariPay {
  SahakaariPay._();

  static const _kFontFam = 'SahakaariPay';
  static const String? _kFontPkg = null;

  static const IconData video_circled = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData cable_car_svgrepo_com = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData tap_svgrepo_com__1_ = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData government_14_svgrepo_com = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData electricity_svgrepo_com_svg = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData card_svgrepo_com_svg = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData umbrella = IconData(0xf0e9, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData bus = IconData(0xf207, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
