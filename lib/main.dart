import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:http/http.dart' as http;
import 'package:in_app_update/in_app_update.dart';
import 'package:platform_device_id_v2/platform_device_id_v2.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_login.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/result_page.dart';
import 'package:sahakari_pay/pmc/sharedPreferences.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:sahakari_pay/themes.dart';
import 'package:shared_preferences/shared_preferences.dart';

/*Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}*/

/*const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'SahakaariPay',
  'SahakaariPay Notification',
  description: 'SahakaariPay Channel for Push Notification.',
  importance: Importance.high,
);*/

<<<<<<< HEAD
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
=======
/*final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();*/
>>>>>>> sudeep

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await CooperativeSharedPreferences.init();

  /*await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );*/

  SharedPreferences prefService = await SharedPreferences.getInstance();
  //Begin Default Locale
  if (prefService.getString("default_locale") != null) {
    DEFAULT_LOCALE = prefService.getString("default_locale")!;
    DEFAULT_LANGUAGE = prefService.getString("default_language")!;
  }
  //End Default Locale
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  static const MaterialColor white = MaterialColor(
    0xFFFFFFFF,
    <int, Color>{
      50: Color(0xFFFFFFFF),
      100: Color(0xFFFFFFFF),
      200: Color(0xFFFFFFFF),
      300: Color(0xFFFFFFFF),
      400: Color(0xFFFFFFFF),
      500: Color(0xFFFFFFFF),
      600: Color(0xFFFFFFFF),
      700: Color(0xFFFFFFFF),
      800: Color(0xFFFFFFFF),
      900: Color(0xFFFFFFFF),
    },
  );

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.

  AppUpdateInfo? _updateInfo;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      checkForUpdate();
    }
  }

  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
        print("this is the update info:${_updateInfo}");
      });
      if (_updateInfo?.updateAvailability ==
          UpdateAvailability.updateAvailable) {
        InAppUpdate.performImmediateUpdate()
            .catchError((e) => showSnack(e.toString()));
      }
    }).catchError((e) {
      showSnack(e.toString());
    });
  }

  void showSnack(String text) {
    if (_scaffoldKey.currentContext != null) {
      ScaffoldMessenger.of(_scaffoldKey.currentContext!)
          .showSnackBar(SnackBar(content: Text(text)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      locale: _getLocale(),
      key: _scaffoldKey,
      localizationsDelegates: const <LocalizationsDelegate>[
        AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: _getLocalesFromLocaleCodes(),
      theme: ThemeData(
        // This is the theme of your application.
        //
        primarySwatch: Colors.green,
        useMaterial3: false,
      ),
      home: const SplashScreenPage(),
      debugShowCheckedModeBanner: false,
    );
  }

  Locale _getLocale() {
    Locale locale = Locale(DEFAULT_LOCALE);

    return locale;
  }

  List<Locale> _getLocalesFromLocaleCodes() {
    List<Locale> locales = [];

    locales.add(const Locale('en'));
    locales.add(const Locale('ne'));

    return locales;
  }
}

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  // bool _isLoading = false;
  void initState() {
    super.initState();
    checkCooperative().then((value) {
      if (value == true) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => PMCLoginPage()));
      } else {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => SahakariScreenPage()));
      }
    });

    // setState(() {
    //   _isLoading = true;
    // });
  }

  Future<bool> checkCooperative() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.getInt("coopIndex");
    print("this is the value${sharedPreferences.getInt("coopIndex")}");
    if (sharedPreferences.getInt("coopIndex") == null) {
      return false;
    }
    ProjectIndex = sharedPreferences.getInt("coopIndex");
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Loading(),
    );
  }
}

class SahakariScreenPage extends StatefulWidget {
  const SahakariScreenPage({Key? key}) : super(key: key);

  @override
  _SahakariScreenPageState createState() => _SahakariScreenPageState();
}

class _SahakariScreenPageState extends State<SahakariScreenPage> {
  // var _connectionStatus = "unknown";
  // bool hasInternet = false;
  // late StreamSubscription internetSubscription;
  // late Connectivity connectivity;
  //
  late StreamSubscription<ConnectivityResult> subscription;

  Future<bool> checkConnection() async {
    ConnectivityResult results = await (Connectivity().checkConnectivity());
    if (results == ConnectivityResult.none) {
      return true;
      // getJsonDataOffline();
      // ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      //   content: Text("You do not have Internet Connection."),
      // ));
    }
    return false;
    // getJsonData();
  }

  // String? smsCode;
  // List? Cooperatives;
  //
  // getCooperativesList() async {
  //   SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   setState(() {
  //     smsCode = sharedPreferences.getString("Cooperatives");
  //     print(smsCode);
  //   });
  // }

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    checkConnection().then((value) {
      setState(() {
        noInternet = value;
      });
      if (noInternet) {
        getJsonDataOffline();
      } else {
        getJsonData();
      }
    });
    subscription = Connectivity().onConnectivityChanged.listen((result) {
      setState(() {
        // _isLoading = false;
        // this.result = result;
        if (result != ConnectivityResult.none) {
          getJsonData();
          noInternet = false;
          String? message;
          if (result == ConnectivityResult.wifi) {
            message = "You have Wifi Connection";
          } else if (result == ConnectivityResult.mobile) {
            message = "You have Mobile Data Connection";
          }
          // final color = Colors.green;
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: Duration(seconds: 2),
            content: Text(message!),
          ));
          // Utils.showTopSnackBar(context, message, color);
        } else {
          setState(() {
            // _isLoading = false;
            noInternet = true;
            getJsonDataOffline();
            try {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                duration: Duration(seconds: 2),
                content: const Text("Check Internet Connection."),
              ));
            } catch (e, s) {
              print(s);
            }
          });
        }
      });
    });
    // _initPushNotifications();
    dropdownValue = DEFAULT_LANGUAGE;
    getDeviceId();
  }

  /*static FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;*/

  List? coopList;

  Future<String> getJsonData() async {
    String url = "https://merchant.sahakaari.com/merchant/api/v1/cooplist";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final response = await http.get(Uri.parse(url), headers: {
      "Token": "q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP"
    });
    var convertDataToJson;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        convertDataToJson = json.decode(response.body);
        if (convertDataToJson != null) {
          setState(() {
            _isLoading = false;
            coopList = convertDataToJson["payload"];
            print(coopList);
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      if (convertDataToJson != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${convertDataToJson["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${convertDataToJson["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  Future<bool> getJsonDataOffline() async {
    var convertDataToJson;
    String response = await rootBundle.loadString("assets/cooplist.json");
    convertDataToJson = json.decode(response);
    setState(() {
      _isLoading = true;
      if (convertDataToJson != null) {
        setState(() {
          _isLoading = false;
          coopList = convertDataToJson["payload"];

          print(coopList);
        });
      }
    });
    return true;
  }

  @override
  void dispose() {
    subscription.cancel();
    // internetSubscription.cancel();
    super.dispose();
  }

  bool _isLoading = false;

  String dropdownValue = "Nepali";

  /*Future<void> _initPushNotifications() async {
    //onMessage
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channelDescription: channel.description,
              icon: 'fcm_icon',
            ),
          ),
        );
      }

      if (message.data != null) {
        _processPushNotification(message);
      }
    });

    //onResume
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      _processPushNotification(message);
    });

    //onLaunch
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage?.data != null) {
      _processPushNotification(initialMessage?.data);
    }

    if (Platform.isIOS) {
      iOSPermission();
    }

    firebaseMessaging.getToken().then((token) {
      print("Firebase: " + token!);
      FCM_TOKEN = token;
    });
    //FirebaseMessaging.instance.subscribeToTopic('himsikhar');
  }*/

  Future<void> getDeviceId() async {
    deviceId = await PlatformDeviceId.getDeviceId;
  }

  /*void _processPushNotification(payload) async {
    print(payload);
  }

  void iOSPermission() async {
    await firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }*/

  exit() {
    SystemNavigator.pop();
  }

  @override
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return exit();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: 100,
                      decoration: BoxDecoration(
                        color: selectedColor,
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 210,
                            height: 160,
                            child: TextButton(
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("service_info"),
                                        ),
                                        content: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString(
                                                  "service_not_available"),
                                        ),
                                        actions: [
                                          TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString("okay"),
                                                style: const TextStyle(
                                                    color: Colors.black87),
                                              ))
                                        ],
                                      );
                                    });
                              },
                              child: Row(
                                children: [
                                  const Icon(
                                    Icons.update,
                                    size: 30.0,
                                    color: Colors.green,
                                  ),
                                  const SizedBox(
                                    width: 10.0,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("check_for_update"),
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        fontStyle: FontStyle.italic),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Stack(
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                          child: SizedBox(
                            height: 450,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                color: Colors.green[100],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 15.0),
                                child: Column(
                                  children: [
                                    Container(
                                      height: 60,
                                      width: 160,
                                      decoration: const BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                "assets/images/spay.png"),
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 25.0,
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    ResultPage(
                                                        coopList: coopList)));
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          const Icon(
                                            Icons.warning,
                                            size: 15.0,
                                          ),
                                          const SizedBox(
                                            width: 10.0,
                                          ),
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                    "select_cooperative"),
                                            style: const TextStyle(
                                                color: Colors.black87,
                                                fontSize: 12,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          const SizedBox(
                                            width: 15.0,
                                          ),
                                          Icon(
                                            Icons.search,
                                            size: 30.0,
                                            color: Colors.black87,
                                          )
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 15.0,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 10.0, right: 10),
                                      child: Container(
                                        height: 200,
                                        width: 600,
                                        decoration: BoxDecoration(
                                            // color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: coopList == null
                                                ? 0
                                                : coopList?.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return InkWell(
                                                onTap: () async {
                                                  ProjectIndex = index;
                                                  banktransferType =
                                                      coopList?[index]
                                                          ["getWayName"];
                                                  // coopList?[index]
                                                  //     ["gateWayName"];
                                                  coopShortName =
                                                      coopList?[index]
                                                          ["coopShortName"];
                                                  HasPhonePay = coopList?[index]
                                                      ["hasPhonePay"];
                                                  SharedPreferences
                                                      sharedPreferences =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  sharedPreferences.setInt(
                                                      "coopIndex",
                                                      ProjectIndex!);
                                                  Navigator.pushReplacement(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            PMCLoginPage(),
                                                      ));
                                                  // (name: coopList?[index]["coopName"],logoUrl: coopList?[index]["logoUrl"],baseUrl: coopList?[index]["baseUrl"]);
                                                },
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 20.0,
                                                          top: 20,
                                                          right: 20,
                                                          bottom: 30),
                                                  child: Center(
                                                      child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        height: 100,
                                                        width: 100,
                                                        decoration: BoxDecoration(
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: Colors
                                                                    .grey
                                                                    .withOpacity(
                                                                        0.5),
                                                                spreadRadius: 5,
                                                                blurRadius: 7,
                                                                offset:
                                                                    const Offset(
                                                                        0, 3),
                                                              )
                                                            ],
                                                            image: DecorationImage(
                                                                image: noInternet
                                                                    ? AssetImage(OfflineImagePath +
                                                                            coopList?[index]["logoUrl"])
                                                                        as ImageProvider
                                                                    : NetworkImage(
                                                                        coopList?[index]
                                                                            [
                                                                            "logoUrl"]),
                                                                fit: BoxFit
                                                                    .cover),
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        20)),
                                                      ),
                                                      Text(coopList?[index]
                                                          ["coopShortName"]),
                                                    ],
                                                  )),
                                                ),
                                              );
                                            }),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 5.0,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 5),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12.0, vertical: 10),
                                        child: Container(
                                          height: 50,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 12, vertical: 4),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButton<String>(
                                              isExpanded: true,
                                              value: dropdownValue,
                                              alignment:
                                                  AlignmentDirectional.center,
                                              //itemHeight: 90,
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                              icon: const Icon(
                                                Icons.arrow_drop_down,
                                                color: Colors.black87,
                                              ),
                                              iconSize: 34,
                                              elevation: 16,
                                              style: const TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold),
                                              onChanged: (String? newValue) {
                                                handleLanguage(newValue!);
                                                /*setState(() {
                                            dropdownValue = newValue!;
                                          });*/
                                              },
                                              items: <String>[
                                                'English',
                                                'Nepali'
                                              ].map<DropdownMenuItem<String>>(
                                                  (String? value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child: Text(
                                                    value!,
                                                    style: const TextStyle(
                                                        color: Colors.black87),
                                                  ),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 480.0, 20.0, 0.0),
                          child: Center(
                            child: Container(
                              height: 200,
                              width: 400,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0),
                                image: const DecorationImage(
                                    image: AssetImage(
                                        "assets/images/sahakaaripay.png"),
                                    fit: BoxFit.fill),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("powered_by"),
                              style: const TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                            Container(
                              height: 70,
                              width: 140,
                              decoration: const BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage("assets/images/spay.png"),
                                    fit: BoxFit.cover),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  void handleLanguage(String value) async {
    SharedPreferences prefService = await SharedPreferences.getInstance();

    switch (value) {
      case 'English':
        DEFAULT_LOCALE = "en";
        DEFAULT_LANGUAGE = "English";

        prefService.setString("default_locale", DEFAULT_LOCALE);
        prefService.setString("default_language", DEFAULT_LANGUAGE);

        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => new MyApp(),
          ),
          (Route<dynamic> route) => false,
        );

        break;
      case 'Nepali':
        DEFAULT_LOCALE = "ne";
        DEFAULT_LANGUAGE = "Nepali";

        prefService.setString("default_locale", DEFAULT_LOCALE);
        prefService.setString("default_language", DEFAULT_LANGUAGE);

        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => new MyApp(),
          ),
          (Route<dynamic> route) => false,
        );

        break;
    }
  }
}
