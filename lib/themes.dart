import 'package:flutter/material.dart';

TextStyle listTileDefaultTextStyle = const TextStyle(
    color: Colors.white70, fontSize: 20.0, fontWeight: FontWeight.bold);
TextStyle listTileSelectedTextStyle = const TextStyle(
    color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold);

Color selectedColor = Colors.indigo;
Color drawerBackgroundColor = const Color(0xFF272D34);
