import 'package:flutter/material.dart';
import 'package:sahakari_pay/main.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/faq.dart';

import '../../utils/localizations.dart';
import '../pmc_smspage.dart';

class OffLineAboutUsPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;

  OffLineAboutUsPage(
      {Key? key,
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode})
      : super(key: key);

  @override
  _OffLineAboutUsPageState createState() => _OffLineAboutUsPageState(
        coopList,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
        smsCode,
      );
}

class _OffLineAboutUsPageState extends State<OffLineAboutUsPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  _OffLineAboutUsPageState(
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode);

  moveToSMSPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PMCsmsPage(
                coopList: coopList,
                accesstoken: accesstoken,
                baseUrl: baseUrl,
                accountno: accountno,
                primaryColor: primaryColor,
                loginButtonTitleColor: loginButtonTitleColor,
                loginbuttonColor: loginbuttonColor,
                loginTextFieldColor: loginTextFieldColor,
                dasboardIconColor: dasboardIconColor,
                dashboardTopTitleColor: dashboardTopTitleColor,
                SecondaryColor: SecondaryColor,
                smsCode: smsCode)));
  }

  final TextEditingController mpincontroller = TextEditingController();
  DialogBox() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
                child: Text(
              AppLocalizations.of(context)!.localizedString("alert"),
            )),
            content: SizedBox(
              height: 150,
              width: 350,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("confirm_pin"),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("enter_transaction_pin"),
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: mpincontroller,
                    maxLength: 4,
                    keyboardType: TextInputType.number,
                    validator: (value) => value!.isEmpty
                        ? AppLocalizations.of(context)!
                            .localizedString("enter_transaction_pin")
                        : null,
                    onSaved: (value) => mpincontroller,
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: AppLocalizations.of(context)!
                          .localizedString("transaction_pin"),
                      labelStyle: TextStyle(color: Colors.green),
                      counterText: "",
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("cancel"),
                      )),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                      )),
                ],
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return moveToSMSPage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          title: Text(
            AppLocalizations.of(context)!.localizedString("about_us"),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                    image: AssetImage("assets/images/spb.jpg"),
                  ),
                ),
                child: null,
              ),
              ListTile(
                leading: const Icon(Icons.account_balance),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("account_balance"),
                ),
                onTap: () {
                  DialogBox();
                },
              ),
              // ListTile(
              //   leading: const Icon(Icons.settings),
              //   title: Text(
              //     AppLocalizations.of(context)!.localizedString("settings"),
              //   ),
              //   onTap: () {
              //     Navigator.push(
              //         context,
              //         MaterialPageRoute(
              //             builder: (context) => OffLineSettingsPage()));
              //   },
              // ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage(
                          "assets/images/menu/ic_menu_about_us.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("about_us"),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage("assets/images/menu/ic_menu_faq.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("faq"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FAQPage(
                              coopList: coopList,
                              accesstoken: accesstoken,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                              smsCode: smsCode)));
                },
              ),
              ListTile(
                leading: const Icon(Icons.logout),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("switch_internet"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SahakariScreenPage()));
                },
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                SizedBox(
                  height: 30.0,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 30.0, right: 25.0),
                  child: SizedBox(
                    height: 200,
                    width: 400,
                    child: Text(
                        "SahakaariPay Mobile Banking App is an easy and secure way to do banking and payment related transactions. Through this app you can connect with your bank account for most of the banking services along with the online payments via Internet or SMS channel. You can enquire balance of your account, view mini statement, transfer fund to another account, top up your mobile balance, and pay your landline bills, internet bills, cable operator's bills and electricity bills."),
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 30.0, right: 25.0),
                  child: SizedBox(
                    height: 100,
                    width: 400,
                    child: Text(
                        "If you have any suggestion please write to us at info@planetearthsolution.com"),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
