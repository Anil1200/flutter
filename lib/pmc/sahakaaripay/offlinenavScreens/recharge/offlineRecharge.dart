import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_smspage.dart';

import '../../../utils/localizations.dart';

class OffLineRechargePage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  OffLineRechargePage(
      {Key? key,
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode})
      : super(key: key);

  @override
  State<OffLineRechargePage> createState() => _OffLineRechargePageState(
      coopList,
      accesstoken,
      balance,
      baseUrl,
      accountno,
      primaryColor,
      loginButtonTitleColor,
      loginbuttonColor,
      loginTextFieldColor,
      dasboardIconColor,
      dashboardTopTitleColor,
      SecondaryColor,
      smsCode);
}

class _OffLineRechargePageState extends State<OffLineRechargePage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  _OffLineRechargePageState(
    this.coopList,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.smsCode,
  );

  bool _isLoading = false;
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();
  List<String> recipient = ["37001"];

  void initState() {
    super.initState();
    print("My SMS Code: ${smsCode}");
    // setState(() {
    //   CooperativeSharedPreferences().getsmsCode();
    //   print("this is the code:  ${smsCode}");
    // });
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  showMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
                child: Column(
              children: [
                Text(
                  AppLocalizations.of(context)!.localizedString("alert"),
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 60,
                  ),
                )
              ],
            )),
            content: SizedBox(
              height: 120,
              width: 200,
              child: Column(
                children: [
                  Center(
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message3"),
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(left: 8.0, top: 20),
                      child: Text(
                        AppLocalizations.of(context)!
                                .localizedString("transaction_message4") +
                            "${dropdownValue} " +
                            AppLocalizations.of(context)!
                                .localizedString("transaction_message5") +
                            " ${dropdownValueamount}",
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("cancel"),
                        style: TextStyle(color: Colors.green[900]),
                      )),
                  const Spacer(),
                  TextButton(
                      onPressed: () {
                        transactionPinMessage();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                        style: TextStyle(color: Colors.green[900]),
                      )),
                ],
              )
            ],
          );
        });
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${dropdownValueamount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message6") +
                          " ${dropdownValue.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message9a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OffLineRechargePage(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode,
                                      )));
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            if (dropdownValue == 'NTC') {
                              _sendSMS(
                                  "${smsCode} ${mpincontroller.text} NTG ${dropdownValueamount}",
                                  recipient);
                            } else if (dropdownValue == 'UTL') {
                              _sendSMS(
                                  "${smsCode} ${mpincontroller.text} UTLR ${dropdownValueamount}",
                                  recipient);
                            } else if (dropdownValue == 'Smart Cell') {
                              _sendSMS(
                                  "${smsCode} ${mpincontroller.text} DHR ${dropdownValueamount}",
                                  recipient);
                            } else if (dropdownValue == 'Dish Home') {
                              _sendSMS(
                                  "${smsCode} ${mpincontroller.text} BLR ${dropdownValueamount}",
                                  recipient);
                            } else if (dropdownValue == 'Broadlink') {
                              _sendSMS(
                                  "${smsCode} ${mpincontroller.text} SCR ${dropdownValueamount}",
                                  recipient);
                            } else if (dropdownValue == 'BroadTel') {
                              _sendSMS(
                                  "${smsCode} ${mpincontroller.text} BRT ${dropdownValueamount}",
                                  recipient);
                            }
                          } else {
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                          Navigator.pop(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OffLineRechargePage(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode,
                                      )));
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  void _sendSMS(String message, List<String> recipient) async {
    String _result = await sendSMS(message: message, recipients: recipient)
        .catchError((onError) {
      print(onError);
    });
    print(_result);
  }

  movetohomepage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PMCsmsPage(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                  smsCode: smsCode,
                )));
  }

  String? dropdownValue;
  String? dropdownValueamount;

  List<String?> amount = [];
  List<String?> services = [
    'NTC',
    'UTL',
    'Smart Cell',
    'Dish Home',
    'Broadlink',
    'BroadTel',
    'Net TV'
  ];
  List<String?> ntcamount = [
    'Select Amount',
    '100',
    '150',
    '200',
    '500',
    '1000'
  ];
  List<String?> utlamount = ['Select Amount', '100', '250', '500'];
  List<String?> smartCellamount = [
    'Select Amount',
    '20',
    '50',
    '100',
    '200',
    '500'
  ];
  List<String?> dishHomeamount = [
    'Select Amount',
    '1000',
    '2000',
    '3000',
    '4000',
    '5000',
    '6000',
    '7000'
  ];
  List<String?> broadlinkamount = ['Select Amount', '550', '1200', '1500'];
  List<String?> broadtelamount = ['Select Amount', '250', '500'];
  List<String?> netTvamount = ['Select Amount', '50', '100'];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(
                  color: Color(int.parse(primaryColor.toString())),
                  borderRadius: BorderRadius.circular(30),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: 10.0,
                      top: 40,
                      child: IconButton(
                          onPressed: () {
                            movetohomepage();
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            size: 30.0,
                            color: Colors.white,
                          )),
                    ),
                    Positioned(
                      left: 100,
                      top: 50,
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("sahakaari_pay"),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20,
                      top: 95,
                      child: Row(
                        children: [
                          const SizedBox(
                            height: 30,
                            width: 30,
                            child: Image(
                                image: AssetImage(
                                    "assets/images/wallet_icon.png")),
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          const Text(
                            "xxxx",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 220.0),
                            child: IconButton(
                              icon: const Icon(
                                Icons.update,
                                color: Colors.white,
                                size: 30.0,
                              ),
                              onPressed: () {},
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 30.0),
                        child: Center(
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("recharge_cards"),
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 40.0),
                        child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("select_vendor"),
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 14,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 30.0, top: 10.0),
                        child: Container(
                          width: 300,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 4),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                              border:
                                  Border.all(color: Colors.black87, width: 2),
                              borderRadius: BorderRadius.circular(20.0)),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              isExpanded: true,
                              hint: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("select_vendor"),
                              ),
                              value: dropdownValue,
                              alignment: AlignmentDirectional.center,
                              // itemHeight: 90,
                              borderRadius: BorderRadius.circular(20.0),
                              icon: const Icon(
                                Icons.arrow_drop_down,
                                color: Colors.black87,
                              ),
                              iconSize: 34,
                              elevation: 16,
                              style: const TextStyle(
                                  color: Colors.black87,
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                              onChanged: (services) {
                                if (services == 'NTC') {
                                  amount = ntcamount;
                                } else if (services == 'UTL') {
                                  amount = utlamount;
                                } else if (services == 'Smart Cell') {
                                  amount = smartCellamount;
                                } else if (services == 'Dish Home') {
                                  amount = dishHomeamount;
                                } else if (services == 'Broadlink') {
                                  amount = broadlinkamount;
                                } else if (services == 'BroadTel') {
                                  amount = broadtelamount;
                                } else if (services == 'Net TV') {
                                  amount = netTvamount;
                                } else {
                                  amount = [];
                                }
                                setState(() {
                                  dropdownValueamount = 'Select Amount';
                                  dropdownValue = services.toString();
                                });
                              },
                              items: services.map((String? newValue) {
                                return DropdownMenuItem<String>(
                                    value: newValue, child: Text(newValue!));
                              }).toList(),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 40.0, top: 10),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("select_amount"),
                          style: TextStyle(color: Colors.black87, fontSize: 16),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 30.0, top: 10.0),
                        child: Container(
                          width: 300,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 4),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                              border:
                                  Border.all(color: Colors.black87, width: 2),
                              borderRadius: BorderRadius.circular(20.0)),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                hint: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("select_amount"),
                                ),
                                value: dropdownValueamount,
                                onChanged: (amount) {
                                  setState(() {
                                    dropdownValueamount = amount;
                                  });
                                },
                                items: amount.map((String? newValue) {
                                  return DropdownMenuItem<String>(
                                      value: newValue,
                                      child: Text('${newValue}'));
                                }).toList()),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 220.0, top: 20),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color:
                                  Color(int.parse(loginbuttonColor.toString())),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          height: 50,
                          width: 100,
                          child: TextButton(
                            onPressed: () {
                              if (dropdownValueamount == 'Select Amount') {
                                setState(() {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Center(
                                              child: Column(
                                            children: [
                                              Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString("alert"),
                                              ),
                                              SizedBox(height: 10.0),
                                              Icon(
                                                Icons.add_alert,
                                                color: Colors.red,
                                                size: 40,
                                              )
                                            ],
                                          )),
                                          content: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                    "select_Recharge_amount"),
                                          ),
                                          actions: [
                                            const Spacer(),
                                            TextButton(
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                              child: Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString("okay"),
                                                style: TextStyle(
                                                    color: Colors.green[900]),
                                              ),
                                            )
                                          ],
                                        );
                                      });
                                });
                              } else {
                                setState(() {
                                  _isLoading = false;
                                });
                                showMessage();
                              }
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("submit"),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
