import 'package:flutter/material.dart';
import 'package:sahakari_pay/main.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/faq.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/olaboutUs.dart';
import 'package:sahakari_pay/themes.dart';

import '../../utils/localizations.dart';
import '../pmc_smspage.dart';

class OffLineSettingsPage extends StatefulWidget {
  const OffLineSettingsPage({Key? key}) : super(key: key);

  @override
  State<OffLineSettingsPage> createState() => _OffLineSettingsPageState();
}

class _OffLineSettingsPageState extends State<OffLineSettingsPage> {
  moveToSMSPage() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => PMCsmsPage()));
  }

  final TextEditingController mpincontroller = TextEditingController();

  DialogBox() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
                child: Text(
              AppLocalizations.of(context)!.localizedString("alert"),
            )),
            content: SizedBox(
              height: 150,
              width: 350,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("confirm_pin"),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("enter_transaction_pin"),
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: mpincontroller,
                    maxLength: 4,
                    keyboardType: TextInputType.number,
                    validator: (value) => value!.isEmpty
                        ? AppLocalizations.of(context)!
                            .localizedString("enter_transaction_pin")
                        : null,
                    onSaved: (value) => mpincontroller,
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: AppLocalizations.of(context)!
                          .localizedString("transaction_pin"),
                      labelStyle: TextStyle(color: Colors.green),
                      counterText: "",
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("cancel"),
                      )),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                      )),
                ],
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return moveToSMSPage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: selectedColor,
          title: Text(
            AppLocalizations.of(context)!.localizedString("settings"),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                    image: AssetImage("assets/images/spb.jpg"),
                  ),
                ),
                child: null,
              ),
              ListTile(
                leading: const Icon(Icons.account_balance),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("account_balance"),
                ),
                onTap: () {
                  DialogBox();
                },
              ),
              ListTile(
                leading: const Icon(Icons.settings),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("settings"),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage(
                          "assets/images/menu/ic_menu_about_us.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("about_us"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => OffLineAboutUsPage()));
                },
              ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage("assets/images/menu/ic_menu_faq.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("faq"),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => FAQPage()));
                },
              ),
              ListTile(
                leading: const Icon(Icons.logout),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("switch_internet"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SahakariScreenPage()));
                },
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
                  child: SizedBox(
                    height: 60,
                    width: 200,
                    child: InkWell(
                      onTap: () {},
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("change_mpin"),
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
