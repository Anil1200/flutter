import 'package:flutter/material.dart';
import 'package:sahakari_pay/main.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/olaboutUs.dart';

// import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/olsettingemes.dart';

import '../../utils/localizations.dart';
import '../pmc_smspage.dart';

class FAQPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;

  FAQPage(
      {Key? key,
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode})
      : super(key: key);

  @override
  _FAQPageState createState() => _FAQPageState(
      coopList,
      accesstoken,
      balance,
      baseUrl,
      accountno,
      primaryColor,
      loginButtonTitleColor,
      loginbuttonColor,
      loginTextFieldColor,
      dasboardIconColor,
      dashboardTopTitleColor,
      SecondaryColor,
      smsCode);
}

class _FAQPageState extends State<FAQPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  _FAQPageState(
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode);

  moveToSMSPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PMCsmsPage(
                coopList: coopList,
                accesstoken: accesstoken,
                baseUrl: baseUrl,
                accountno: accountno,
                primaryColor: primaryColor,
                loginButtonTitleColor: loginButtonTitleColor,
                loginbuttonColor: loginbuttonColor,
                loginTextFieldColor: loginTextFieldColor,
                dasboardIconColor: dasboardIconColor,
                dashboardTopTitleColor: dashboardTopTitleColor,
                SecondaryColor: SecondaryColor,
                smsCode: smsCode)));
  }

  final TextEditingController mpincontroller = TextEditingController();

  DialogBox() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
                child: Text(
              AppLocalizations.of(context)!.localizedString("alert"),
            )),
            content: SizedBox(
              height: 150,
              width: 350,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("confirm_pin"),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("enter_transaction_pin"),
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: mpincontroller,
                    maxLength: 4,
                    keyboardType: TextInputType.number,
                    validator: (value) => value!.isEmpty
                        ? AppLocalizations.of(context)!
                            .localizedString("enter_transaction_pin")
                        : null,
                    onSaved: (value) => mpincontroller,
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(),
                      labelText: AppLocalizations.of(context)!
                          .localizedString("transaction_pin"),
                      labelStyle: TextStyle(color: Colors.green),
                      counterText: "",
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("cancel"),
                      )),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                      )),
                ],
              )
            ],
          );
        });
  }

  //
  String dropdownValue = 'What is SahakaariPay Mobile Banking App?';
  String dropdownValue1 = 'How cna I use SahakaariPay Mobile Banking App?';
  String dropdownValue2 = 'Can I change my password and MPIN?';
  String dropdownValue3 = 'What is Switch to SMS Channel?';
  String dropdownValue4 = 'What is SahakaariPay?';
  String dropdownValue5 = 'Security Tips?';

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return moveToSMSPage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          title: Text(
            AppLocalizations.of(context)!.localizedString("faq"),
          ),
          centerTitle: true,
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                    image: AssetImage("assets/images/spb.jpg"),
                  ),
                ),
                child: null,
              ),
              ListTile(
                leading: const Icon(Icons.account_balance),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("account_balance"),
                ),
                onTap: () {
                  DialogBox();
                },
              ),
              // ListTile(
              //   leading: const Icon(Icons.settings),
              //   title: Text(
              //     AppLocalizations.of(context)!.localizedString("settings"),
              //   ),
              //   onTap: () {
              //     Navigator.push(
              //         context,
              //         MaterialPageRoute(
              //             builder: (context) => OffLineSettingsPage()));
              //   },
              // ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage(
                          "assets/images/menu/ic_menu_about_us.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("about_us"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => OffLineAboutUsPage(
                              coopList: coopList,
                              accesstoken: accesstoken,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                              smsCode: smsCode)));
                },
              ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage("assets/images/menu/ic_menu_faq.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("faq"),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: const Icon(Icons.logout),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("switch_internet"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SahakariScreenPage()));
                },
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 20, right: 20),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.red,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'What is SahakaariPay Mobile Banking App?',
                          'SahakaariPay Mobile Banking App is an easy and secure way to do banking and payment related transactions. Through this app you can connect with your bank account for most of the banking services along with the online payments via Internet or SMS channel.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue1,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.red,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'How cna I use SahakaariPay Mobile Banking App?',
                          'You need to have an account with one of the partner bank of PES. You need to request the bank to open mobile banking account with your mobile number, email, and account number to be linked. After verification, the bank will send you your username, password and a four digit MPIN. Now you can use SahakaariPay Mobile Banking.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue2,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.red,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'Can I change my password and MPIN?',
                          'Yes, please login with your existing username and password. Go to settings, there you will find two options - Change Password, Change Transaction PIN. You need to change password and MPIN time and again in order to secure your account,'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue3,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.red,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'What is Switch to SMS Channel?',
                          'If you do not have access to Internet then you can switch to SMS Channel to access Mobile Banking services using SMS. The app will automatically send SMS to get the desired services.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue4,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.red,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'What is SahakaariPay?',
                          'Through SahakaariPay you can top-up or get recharge coupons for your NT GSM/CDMA, NCELL, UTL, Smart Cell mobile phone balances. You can pay bills for NT Landline, ADSL, World Link, Subishu, Sim TV, Dish Home. You can also get recharge coupons for Broad Link Internet and NET TV.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue5,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.red,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'Security Tips?',
                          "In order to secure your Mobile App, please don't share your username, password, MPIN with anyone. Logout from the app once you have used the services."
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
