import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/adsl/offlineScreen1.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/adsl/offlineScreen2.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_smspage.dart';

import '../../../utils/localizations.dart';

class OffLineADSLPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  OffLineADSLPage(
      {Key? key,
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode})
      : super(key: key);

  @override
  _OffLineADSLPageState createState() => _OffLineADSLPageState(
      coopList,
      accesstoken,
      balance,
      baseUrl,
      accountno,
      primaryColor,
      loginButtonTitleColor,
      loginbuttonColor,
      loginTextFieldColor,
      dasboardIconColor,
      dashboardTopTitleColor,
      SecondaryColor,
      smsCode);
}

class _OffLineADSLPageState extends State<OffLineADSLPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  _OffLineADSLPageState(
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode);

  movetohomepage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => PMCsmsPage(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                  smsCode: smsCode,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                size: 30,
                color: Colors.white,
              ),
              onPressed: () {
                movetohomepage();
              },
            ),
            backgroundColor: Color(int.parse(primaryColor.toString())),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40.0)),
            toolbarHeight: 110,
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Text(
                    AppLocalizations.of(context)!
                        .localizedString("sahakaari_pay"),
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white),
                  ),
                ),
                Row(
                  children: [
                    const SizedBox(
                      height: 20,
                      width: 20,
                      child: Image(
                          image: AssetImage("assets/images/wallet_icon.png")),
                    ),
                    const SizedBox(
                      width: 10.0,
                    ),
                    const Text(
                      "xxxx",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 150.0),
                      child: IconButton(
                        icon: const Icon(
                          Icons.update,
                          color: Colors.white,
                          size: 22.0,
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ],
                )
              ],
            ),
            bottom: PreferredSize(
              preferredSize: const Size(60, 60),
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 10.0, left: 10.0, right: 10),
                child: Container(
                  height: 55,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ]),
                  child: const TabBar(
                      indicatorColor: Colors.red,
                      indicator: UnderlineTabIndicator(
                        borderSide: BorderSide(width: 3.0, color: Colors.red),
                        insets: EdgeInsets.symmetric(
                          horizontal: 30.0,
                        ),
                      ),
                      labelColor: Colors.black87,
                      tabs: [
                        Tab(
                          text: "Volume Based",
                          icon: Icon(
                            Icons.speed,
                            color: Colors.red,
                          ),
                        ),
                        Tab(
                          text: "Unlimited",
                          icon: Icon(
                            Icons.all_inclusive_rounded,
                            color: Colors.red,
                          ),
                        )
                      ]),
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              OffLineScreen1Page(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                  smsCode: smsCode),
              OffLineScreen2Page(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                  smsCode: smsCode),
            ],
          ),
        ),
      ),
    );
  }
}
