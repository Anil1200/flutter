import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/television/simtv/offlineSimTv.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_smspage.dart';

import '../../../../utils/localizations.dart';

class OffLineCasIDPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;

  OffLineCasIDPage(
      {Key? key,
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode})
      : super(key: key);

  @override
  _OffLineCasIDPageState createState() => _OffLineCasIDPageState(
      coopList,
      accesstoken,
      balance,
      baseUrl,
      accountno,
      primaryColor,
      loginButtonTitleColor,
      loginbuttonColor,
      loginTextFieldColor,
      dasboardIconColor,
      dashboardTopTitleColor,
      SecondaryColor,
      smsCode);
}

class _OffLineCasIDPageState extends State<OffLineCasIDPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  _OffLineCasIDPageState(
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode);

  bool _isLoading = false;
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  // final GlobalKey<FormState> globalFormKey3 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();
  List<String> recipient = ["37001"];

  final TextEditingController CasIDController = TextEditingController();
  final TextEditingController amountController = TextEditingController();

  void initState() {
    super.initState();
    print("My SMS Code: ${smsCode}");
    // setState(() {
    //   CooperativeSharedPreferences().getsmsCode();
    //   print("this is the code:  ${smsCode}");
    // });
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  showMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
                child: Column(
              children: [
                Text(
                  AppLocalizations.of(context)!.localizedString("alert"),
                ),
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 60,
                  ),
                )
              ],
            )),
            content: SizedBox(
              height: 120,
              width: 200,
              child: Column(
                children: [
                  Center(
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message3"),
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: EdgeInsets.only(left: 8.0, top: 20),
                      child: Text(
                        AppLocalizations.of(context)!
                                .localizedString("transaction_message6") +
                            " ${CasIDController.text} " +
                            AppLocalizations.of(context)!
                                .localizedString("rs") +
                            " ${amountController.text} " +
                            AppLocalizations.of(context)!
                                .localizedString("transaction_message2"),
                        style: TextStyle(fontSize: 14),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("cancel"),
                        style: TextStyle(color: Colors.green[900]),
                      )),
                  const Spacer(),
                  TextButton(
                      onPressed: () {
                        transactionPinMessage();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                        style: TextStyle(color: Colors.green[900]),
                      )),
                ],
              )
            ],
          );
        });
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${amountController.text} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${CasIDController.text.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OffLineSimTvPage(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode,
                                      )));
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            _sendSMS(
                                "${smsCode} ${mpincontroller.text} NTPP ${CasIDController.text} ${amountController.text}",
                                recipient);
                          } else {
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }

                          Navigator.pop(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OffLineSimTvPage(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode,
                                      )));
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  void _sendSMS(String message, List<String> recipient) async {
    String _result = await sendSMS(message: message, recipients: recipient)
        .catchError((onError) {
      print(onError);
    });
    print(_result);
  }

  movetohomepage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PMCsmsPage(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                  smsCode: smsCode,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cas_id"),
                          style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Form(
                      key: globalFormKey,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 50.0,
                            ),
                            child: Container(
                              width: 260,
                              child: TextFormField(
                                controller: CasIDController,
                                maxLength: 11,
                                keyboardType: TextInputType.number,
                                validator: (value) => value!.isEmpty
                                    ? AppLocalizations.of(context)!
                                        .localizedString("enter_cas_id")
                                    : null,
                                onSaved: (value) => CasIDController,
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(),
                                  labelText: AppLocalizations.of(context)!
                                      .localizedString("cas_id"),
                                  labelStyle: TextStyle(color: Colors.green),
                                  counterText: "",
                                  icon: Icon(
                                    Icons.confirmation_number,
                                    color: Colors.green,
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 20.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 50.0,
                            ),
                            child: Container(
                              width: 260,
                              child: TextFormField(
                                controller: amountController,
                                maxLength: 10,
                                keyboardType: TextInputType.number,
                                validator: (value) => value!.isEmpty
                                    ? AppLocalizations.of(context)!
                                        .localizedString("amount")
                                    : null,
                                onSaved: (value) => amountController,
                                decoration: InputDecoration(
                                  border: UnderlineInputBorder(),
                                  labelText: AppLocalizations.of(context)!
                                      .localizedString("amount"),
                                  labelStyle: TextStyle(color: Colors.green),
                                  counterText: "",
                                  icon: Icon(
                                    Icons.confirmation_number,
                                    color: Colors.green,
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 220.0, top: 20),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color:
                                Color(int.parse(loginbuttonColor.toString())),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ]),
                        height: 50,
                        width: 100,
                        child: TextButton(
                          onPressed: () async {
                            if (validateAndSave()) {
                              setState(() {
                                _isLoading = true;
                              });
                              showMessage();
                            } else {
                              setState(() {
                                _isLoading = false;
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Center(
                                          child: Column(
                                            children: [
                                              Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString("alert"),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Icon(
                                                Icons.add_alert,
                                                color: Colors.red,
                                                size: 50,
                                              )
                                            ],
                                          ),
                                        ),
                                        content: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("valid_mobile"),
                                        ),
                                        actions: [
                                          const Spacer(),
                                          TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("okay"),
                                              style: TextStyle(
                                                  color: Colors.green[900]),
                                            ),
                                          )
                                        ],
                                      );
                                    });
                              });
                            }
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("submit"),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        "Customer ID is 10 digits ID provided by"
                        " SIM TV while installing the SET TOP BOX whereas Cas ID is 11 digits.",
                        style: TextStyle(fontSize: 14, color: Colors.black87),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  // bool validateMobile(String value) {
  //   String pattern = r'(^(985)?[0-9]{7}$)';
  //   // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
  //   RegExp regExp = RegExp(pattern);
  //   if (!regExp.hasMatch(value)) {
  //     print("Does not match: ${value}");
  //     return false;
  //   }
  //   print("the value match: ${value}");
  //   return true;
  // }
}
