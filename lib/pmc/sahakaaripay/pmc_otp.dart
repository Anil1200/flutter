import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:sahakari_pay/main.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';

import '../utils/localizations.dart';

class PMCOTPPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? mobileno;

  PMCOTPPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.mobileno,
  }) : super(key: key);

  @override
  _PMCOTPPageState createState() => _PMCOTPPageState(
        coopList,
        userId,
        accesstoken,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
        mobileno,
      );
}

class _PMCOTPPageState extends State<PMCOTPPage> {
  final scaffoldKey = GlobalKey<State>();

  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? primaryColor;
  String? mobileno;

  _PMCOTPPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.mobileno,
  );

  TextEditingController textEditingController = TextEditingController();

  StreamController<ErrorAnimationType>? errorController;

  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    errorController = StreamController<ErrorAnimationType>();
    reset();
  }

  void reset() {
    if (countDown) {
      setState(() => duration = countdownDuration);
    } else {
      setState(() => duration = Duration());
    }
  }

  void startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (_) => addTime());
  }

  bool showtime = false;

  void addTime() {
    final addSeconds = countDown ? -1 : 1;
    setState(() {
      final seconds = duration.inSeconds + addSeconds;
      if (seconds < 0) {
        timer?.cancel();
        showtime = false;
        reset();
      } else {
        duration = Duration(seconds: seconds);
        showtime = true;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    errorController!.close();
  }

  bool _isLoading = false;
  static const countdownDuration = Duration(minutes: 2, seconds: 30);
  Duration duration = Duration();
  Timer? timer;

  bool countDown = true;

  postOTP() async {
    String url =
        "${baseUrl}api/v1/otpverify?userId=${userId.toString()}&OTP=${textEditingController.text.toString()}";
    // Map body = {"userId": userId, "OTP": textEditingController.text.toString()};
    // print(body);
    var jsonResponse;
    var res = await http.get(Uri.parse(url), headers: {
      // "userId": userId.toString(),
      // "OTP": textEditingController.text.toString(),
      'Authorization': "Bearer ${accesstoken}"
    });
    textEditingController.clear();
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Row(
              children: [
                const Icon(
                  Icons.verified,
                  color: Colors.green,
                  size: 40,
                ),
                const SizedBox(
                  width: 20,
                ),
                Text(
                  AppLocalizations.of(context)!
                      .localizedString("login_success"),
                  style: const TextStyle(color: Colors.white, fontSize: 16),
                ),
              ],
            )));
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (context) => HomePage(
                          coopList: coopList,
                          userId: userId,
                          accesstoken: accesstoken,
                          baseUrl: baseUrl,
                          accountno: accountno,
                          primaryColor: primaryColor,
                          loginButtonTitleColor: loginButtonTitleColor,
                          loginbuttonColor: loginbuttonColor,
                          loginTextFieldColor: loginTextFieldColor,
                          dasboardIconColor: dasboardIconColor,
                          dashboardTopTitleColor: dashboardTopTitleColor,
                          SecondaryColor: SecondaryColor,
                        )));
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16, color: Colors.black87),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14, color: Colors.black87),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.black87),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16, color: Colors.black87),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14, color: Colors.black87),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.black87),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
        jsonResponse = json.decode(res.body);
        print(jsonResponse);
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16, color: Colors.black87),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14, color: Colors.black87),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.black87),
                    ))
              ],
            );
          });
    }
  }

  ReSendOTP() async {
    String url =
        "${baseUrl}api/v1/resendotp?userId=${userId.toString()}&mobilenumber=${login_username.toString()}";
    // Map body = {"userId": userId, "OTP": textEditingController.text.toString()};
    // print(body);
    var jsonResponse;
    var res = await http.get(Uri.parse(url), headers: {
      // "userId": userId.toString(),
      // "OTP": textEditingController.text.toString(),
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            startTimer();
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                duration: Duration(milliseconds: 500),
                content: Row(
                  children: [
                    const Icon(
                      Icons.verified,
                      color: Colors.green,
                      size: 40,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("check_message"),
                      style: const TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )));
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16, color: Colors.black87),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14, color: Colors.black87),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.black87),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16, color: Colors.black87),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14, color: Colors.black87),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.black87),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
        jsonResponse = json.decode(res.body);
        print(jsonResponse);
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16, color: Colors.black87),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14, color: Colors.black87),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.black87),
                    ))
              ],
            );
          });
    }
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SahakariScreenPage()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.white,
          ),
          onPressed: () {
            movetoHomePage();
          },
        ),
        title: Text(
          AppLocalizations.of(context)!.localizedString("sahakaari_pay"),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Text(
                'Enter the OTP send to $mobileno',
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Form(
              key: formKey,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
                child: PinCodeTextField(
                  appContext: context,
                  autoFocus: true,
                  length: 4,
                  obscureText: false,
                  animationType: AnimationType.fade,
                  validator: (v) {},
                  pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    borderRadius: BorderRadius.circular(5),
                    fieldHeight: 85,
                    fieldWidth: 75,
                    selectedFillColor: Colors.white,
                    activeFillColor: Colors.white,
                    inactiveFillColor: Colors.white,
                  ),
                  cursorColor: Colors.black,
                  animationDuration: const Duration(milliseconds: 300),
                  enableActiveFill: true,
                  errorAnimationController: errorController,
                  controller: textEditingController,
                  keyboardType: TextInputType.number,
                  boxShadows: const [
                    BoxShadow(
                      offset: Offset(0, 1),
                      color: Colors.black12,
                      blurRadius: 10,
                    )
                  ],
                  onCompleted: (v) {},
                  onChanged: (value) {},
                  beforeTextPaste: (text) {
                    return true;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            _isLoading ? Loading() : Container(),
            const SizedBox(
              height: 40,
            ),
            Container(
              margin:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 30),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color(int.parse(loginbuttonColor.toString())),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: const Offset(0, 3), // changes position of shadow
                    ),
                  ]),
              height: 50,
              width: 100,
              child: TextButton(
                onPressed: () {
                  setState(() {
                    _isLoading = true;
                  });
                  formKey.currentState!.validate();
                  if (textEditingController.text.length != 4) {
                    errorController!.add(ErrorAnimationType.shake);
                  } else {
                    postOTP();
                  }
                },
                child: const Center(
                  child: Text(
                    "Verify",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Didn't get the Code ?",
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black87,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                TextButton(
                    onPressed: () {
                      if (showtime == false) {
                        ReSendOTP();
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            duration: Duration(milliseconds: 500),
                            content: Row(
                              children: [
                                const Icon(
                                  Icons.verified,
                                  color: Colors.green,
                                  size: 40,
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("loading"),
                                  style: const TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ],
                            )));
                      }
                    },
                    child: Text(
                      "Try Again",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.green[900],
                          fontSize: 18),
                    )),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            showtime ? buildTime() : Container(),
          ],
        ),
      ),
    );
  }

  Widget buildTime() {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    final minutes = twoDigits(duration.inMinutes.remainder(60));
    final seconds = twoDigits(duration.inSeconds.remainder(60));
    return Center(
        child: Text(
      "Please wailt for: ${minutes}:${seconds}",
      style: TextStyle(fontSize: 18),
    ));
  }
}
