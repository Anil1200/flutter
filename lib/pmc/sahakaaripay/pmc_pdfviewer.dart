import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/cooptransfer/cooptransferpdfbill/mobile.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';

class PdfViewerPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? pdfUrl;
  PdfViewerPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.pdfUrl,
  }) : super(key: key);

  @override
  _PdfViewerPageState createState() => _PdfViewerPageState(
        coopList,
        userId,
        accesstoken,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
        pdfUrl,
      );
}

class _PdfViewerPageState extends State<PdfViewerPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? pdfUrl;
  _PdfViewerPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.pdfUrl,
  );

  void initState() {
    super.initState();
  }

  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void dispose() {
    super.dispose();
  }

  movetoHistoryPage() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoHistoryPage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetoHistoryPage();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          title: Text(
            "SahakaariPay",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
          actions: [
            IconButton(
                onPressed: () async {
                  HttpClient httpClient = new HttpClient();
                  try {
                    var request = await httpClient.getUrl(Uri.parse(pdfUrl.toString()));
                    var response = await request.close();
                    if(response.statusCode == 200) {
                      var bytes = await consolidateHttpClientResponseBytes(response);
                      saveAndLaunchFile(bytes, 'Bill.pdf');
                    }
                  }
                  catch(ex){
                  }
                },
                icon: const Icon(Icons.download),
            ),
          ],
        ),
        body: ScrollConfiguration(
            behavior: CustomScrollBehavior(
            androidSdkVersion: androidSdkVersion,
          ),
          child: SfPdfViewer.network(
            pdfUrl.toString(),
            key: _pdfViewerKey,
          ),
        ),
      ),
    );
  }
}
