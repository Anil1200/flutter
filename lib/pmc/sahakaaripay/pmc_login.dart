import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:platform_device_id_v2/platform_device_id_v2.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/local_auth_api.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_mobilebanking.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_otp.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_set_fingerprint.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_smspage.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:sahakari_pay/pmc/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class PMCLoginPage extends StatefulWidget {
  const PMCLoginPage({Key? key}) : super(key: key);

  @override
  _PMCLoginPageState createState() => _PMCLoginPageState();
}

class _PMCLoginPageState extends State<PMCLoginPage> {
  // var _connectionStatus = "unknown";
  // bool hasInternet = false;
  // late StreamSubscription internetSubscription;
  // late Connectivity connectivity;
  //
  late StreamSubscription<ConnectivityResult> subscription;

  Future<bool> checkConnection() async {
    ConnectivityResult results = await (Connectivity().checkConnectivity());
    if (results == ConnectivityResult.none) {
      return true;
    }
    return false;
    // getJsonData();
  }

  List? Ticket;
  List flight = [];

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    checkConnection().then((value) {
      setState(() {
        noInternet = value;
      });
      if (noInternet) {
        getJsonDataOffline();
      } else {
        getJsonData();
      }
    });
    subscription = Connectivity().onConnectivityChanged.listen((result) {
      setState(() {
        // _isLoading = false;
        // this.result = result;
        if (result != ConnectivityResult.none) {
          getJsonData();
          noInternet = false;
          String? message;
          if (result == ConnectivityResult.wifi) {
            message = "You have Wifi Connection";
          } else if (result == ConnectivityResult.mobile) {
            message = "You have Mobile Data Connection";
          }
          // final color = Colors.green;
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: Duration(seconds: 2),
            content: Text(message!),
          ));
          // Utils.showTopSnackBar(context, message, color);
        } else {
          setState(() {
            // _isLoading = false;
            noInternet = true;
            getJsonDataOffline();
            try {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                duration: Duration(seconds: 2),
                content: const Text("Check Internet Connection."),
              ));
            } catch (e, s) {
              print(s);
            }
          });
        }
      });
    });
    dropdownValue = DEFAULT_LANGUAGE;
    getDeviceId();
    // hidebiometric();
  }

  // Future<bool> getindex() async {
  //   String data =
  //       await DefaultAssetBundle.of(context).loadString("assets/coopId.json");
  //   final jsonResult = jsonDecode(data);
  //   index = jsonResult["coopindex"];
  //   return true;
  // }

  static FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  List? coopList;
  String baseUrl = "";
  String logoUrl = "";
  String name = "";
  String smsCode = "";

  Future<String> getJsonData() async {
    String url = "https://merchant.sahakaari.com/merchant/api/v1/cooplist";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final response = await http.get(Uri.parse(url), headers: {
      "Token": "q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP"
    });
    var convertDataToJson;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        convertDataToJson = json.decode(response.body);
        if (convertDataToJson != null) {
          setState(() {
            coopList = convertDataToJson["payload"];
            baseUrl = coopList?[index]["baseUrl"];
            logoUrl = coopList?[index]["logoUrl"];
            name = coopList?[index]["coopName"];
            smsCode = coopList?[index]["smsCode"];
            coopShortName = coopList?[index]["coopShortName"];
            print("this is the coop short name: ${coopShortName}");
            color = coopList?[index]["primaryColor"];
            loginbutton = coopList?[index]["loginButtonBackground"];
            loginTextField = coopList?[index]["loginTextField"];
            loginButtonTitle = coopList?[index]["loginButtonTitle"];
            dasboardIcon = coopList?[index]["dashboardTopIcon"];
            dashboardTopTitle = coopList?[index]["dashboardTopTitle"];
            secondaryColor = coopList?[index]["secondaryColor"];
            SahakaarilogoUrl = coopList?[index]["logoUrl"];
            banktransferType = coopList?[index]["gateWayName"];
            utilitiesType = coopList?[index]["gatewayNameUtilities"];
            HasPhonePay = coopList?[index]["hasPhonePay"];
            AllowBankTransfer = coopList?[index]["allowBankTransfer"];
            AllowLoadKhalti = coopList?[index]["allowLoadKhalti"];
            AllowLoadEsewa = coopList?[index]["allowLoadEsewa"];
            HasATM = coopList?[index]["hasATM"];
            AllowOnePG = coopList?[index]["allowOnePG"];
            clientCode = coopList?[index]["clientCode"];
            clientID = coopList?[index]["clientID"];
            print(coopList);
            getColor();
            _isLoading = false;
          });
          rememberUsername().then((value) {
            if (value != null) {
              usernamecontroller.text = value.toString();
            }
          });
          hidebiometric().then((value) {
            print(
                "this is the value for the biometric hide and show : ${value}");
            if (value == true) {
              setState(() {
                showBiometricbutton = false;
              });
            } else {
              setState(() {
                showBiometricbutton = true;
              });
            }
          });
          hidebiometricTransaction().then((value) {
            print(
                "this is the value for the biometric hide and show transaction : ${value}");
            if (value == true) {
              setState(() {
                showBiometricbuttonTransaction = false;
              });
            } else {
              setState(() {
                showBiometricbuttonTransaction = true;
              });
            }
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      if (convertDataToJson != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${convertDataToJson["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${convertDataToJson["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  Future<bool> getJsonDataOffline() async {
    var convertDataToJson;
    String response = await rootBundle.loadString("assets/cooplist.json");
    convertDataToJson = json.decode(response);
    setState(() {
      _isLoading = true;
      if (convertDataToJson != null) {
        setState(() {
          coopList = convertDataToJson["payload"];
          baseUrl = coopList?[index]["baseUrl"];
          logoUrl = coopList?[index]["logoUrl"];
          name = coopList?[index]["coopName"];
          smsCode = coopList?[index]["smsCode"];
          color = coopList?[index]["primaryColor"];
          loginbutton = coopList?[index]["loginButtonBackground"];
          loginTextField = coopList?[index]["loginTextField"];
          loginButtonTitle = coopList?[index]["loginButtonTitle"];
          dasboardIcon = coopList?[index]["dashboardTopIcon"];
          dashboardTopTitle = coopList?[index]["dashboardTopTitle"];
          secondaryColor = coopList?[index]["secondaryColor"];
          SahakaarilogoUrl = coopList?[index]["logoUrl"];
          getColor();
          _isLoading = false;
          print(coopList);
        });
      }
    });
    return true;
  }

  String? Status;

  Future<void> getAds() async {
    String url = "${baseUrl}api/v1/adsnew";
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(res.body);
        print("Response status Ads : ${jsonResponse}");
        if (jsonResponse != null) {
          Status = jsonResponse["Status"];
          if (Status.toString().toLowerCase() ==
              "Success".toString().toLowerCase()) {
            AdsList = jsonResponse["Payload"];
          } else {
            _isLoading = false;
          }
        }
      });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("Response status Ads : ${jsonResponse}");
    }
  }

  Future<bool> hidebiometric() async {
    SharedPreferences logincredentials = await SharedPreferences.getInstance();
    String? loginUsername = logincredentials.getString("username${coopShortName}");
    String? loginPassword = logincredentials.getString("password${coopShortName}");
    print(
        "this is the value from the shared prefrences for biometric: ${logincredentials.getString("password${coopShortName}")}");
    if (loginUsername == null && loginPassword == null ||
        loginUsername == "" && loginPassword == "") {
      return true;
    }
    return false;
  }

  Future<bool> hidebiometricTransaction() async {
    SharedPreferences logincredentials = await SharedPreferences.getInstance();
    String? loginUsername = logincredentials.getString("transactionusername${coopShortName}");
    String? loginPassword = logincredentials.getString("transactionpassword${coopShortName}");
    print(
        "mpin: ${loginPassword}");
    try {
      transactionMPIN = Utils.decryptAES(loginPassword);
    } catch (e) {
      //
    }
    print(
        "mpin after decrypt: ${transactionMPIN}");
    if (loginUsername == null && loginPassword == null ||
        loginUsername == "" && loginPassword == "") {
      return true;
    }
    return false;
  }

  getColor() {
    primaryColor = color?.replaceAll("#", "0xFF");
    loginButtonTitleColor = loginButtonTitle?.replaceAll("#", "0xFF");
    loginbuttonColor = loginbutton?.replaceAll("#", "0xFF");
    print("this is login button color: ${loginbuttonColor}");
    loginTextFieldColor = loginTextField?.replaceAll("#", "0xff");
    print("this is text color: ${loginTextFieldColor}");
    dasboardIconColor = dasboardIcon?.replaceAll("#", "0xff");
    dashboardTopTitleColor = dashboardTopTitle?.replaceAll("#", "0xff");
    SecondaryColor = secondaryColor?.replaceAll("#", "0xff");
  }

  showMessage(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("no_internet"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      signIn(usernamecontroller.text, passwordcontroller.text);
    }
  }

  @override
  void dispose() {
    subscription.cancel();
    // internetSubscription.cancel();
    super.dispose();
  }

  bool _isLoading = false;
  bool _obscureText = true;
  String? accesstoken;
  double? balance;
  String? color;
  String? loginButtonTitle;
  String? loginbutton;
  String? loginTextField;
  String? dasboardIcon;
  String? dashboardTopTitle;
  String? secondaryColor;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  String dropdownValue = "Nepali";

  Future<void> getDeviceId() async {
    deviceId = await PlatformDeviceId.getDeviceId;
  }

  void _processPushNotification(payload) async {
    print(payload);
  }

  void iOSPermission() async {
    await firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
  }

  String? accountno;
  String? authUserName;
  bool? isOTPVerification;
  String? userId;
  bool checkRemember = false;

  void signIn(String username, String password) async {
    String url = "${baseUrl}api/v1/auth";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map body = {
      "username": username,
      "password": password,
      "DeviceId": deviceId,
      "FCMToken": FCM_TOKEN.toString(),
    };
    print("this is the auth body mapping: ${body}");
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body);
    print("this is the response: ${res.body}");
    //checking the status of api
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = true;
        print("Response status: ${res.statusCode}");
        print("Response status: ${jsonResponse}");
        if (jsonResponse != null) {
          setState(() {
            userId = jsonResponse["UserId"];
            print("this is the userId from auth: ${userId}");
            USERID = userId;
            accesstoken = jsonResponse["AccessToken"];
            accountno = jsonResponse["LinkedAccountNo"];
            authUserName = jsonResponse["UserName"];
            Address = jsonResponse["Address"];
            AccountNumber = accountno;
            if (Address == null) {
              Address = "KTM";
            }
            print("this is address: ${Address}");
            FCM_TOPIC = coopShortName!;
            isOTPVerification = jsonResponse["IsOTPVerification"];
            if (checkRemember == true) {
              sharedPreferences.setString(
                  "rememberusername${coopShortName}", username);
            } else {
              sharedPreferences.setString(
                  "rememberusername${coopShortName}", "");
            }
            login_username = username;
            print("this is the LOGIN USERNAME: ${login_username}");
            login_password = password;
            coopName = name;


            String mpinstring = sharedPreferences.getString("transactionusername${coopShortName}") ?? "";

            if (login_username != mpinstring) {
              sharedPreferences.remove("transactionusername${coopShortName}");
              sharedPreferences.remove("transactionpassword${coopShortName}");

              hidebiometric().then((value) {
                print(
                    "this is the value for the biometric hide and show : ${value}");
                if (value == true) {
                  setState(() {
                    showBiometricbutton = false;
                  });
                } else {
                  setState(() {
                    showBiometricbutton = true;
                  });
                }
              });
              hidebiometricTransaction().then((value) {
                print(
                    "this is the value for the biometric hide and show transaction : ${value}");
                if (value == true) {
                  setState(() {
                    showBiometricbuttonTransaction = false;
                  });
                } else {
                  setState(() {
                    showBiometricbuttonTransaction = true;
                  });
                }
              });

              print('mpin cleared as username is different');
            }


            //Utils.BalanceUpdate(baseUrl, accesstoken).then((value) {
              //getAds().then((value) {
                _isLoading = false;
                if (isOTPVerification == true) {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PMCOTPPage(
                                coopList: coopList,
                                userId: userId,
                                accesstoken: accesstoken,
                                baseUrl: baseUrl,
                                accountno: accountno,
                                primaryColor: primaryColor,
                                loginButtonTitleColor: loginButtonTitleColor,
                                loginbuttonColor: loginbuttonColor,
                                loginTextFieldColor: loginTextFieldColor,
                                dasboardIconColor: dasboardIconColor,
                                dashboardTopTitleColor: dashboardTopTitleColor,
                                SecondaryColor: SecondaryColor,
                                mobileno: authUserName,
                              )));
                } else {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => HomePage(
                                coopList: coopList,
                                userId: userId,
                                accesstoken: accesstoken,
                                baseUrl: baseUrl,
                                accountno: accountno,
                                primaryColor: primaryColor,
                                loginButtonTitleColor: loginButtonTitleColor,
                                loginbuttonColor: loginbuttonColor,
                                loginTextFieldColor: loginTextFieldColor,
                                dasboardIconColor: dasboardIconColor,
                                dashboardTopTitleColor: dashboardTopTitleColor,
                                SecondaryColor: SecondaryColor,
                              )));
                }
              //});
            //});
            // ShowMessage(context);
          });
        }
        print("Response status: ${res.body}");
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    ),
                  ],
                ),
              ),
              content: Text(
                jsonResponse.toString(),
                style: TextStyle(color: Colors.black87),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    ),
                  ],
                ),
              ),
              content: Text(jsonResponse["Message"]),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  showBiometric(ConnectivityResult result) async {
    if (result == ConnectivityResult.none) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
                style: TextStyle(color: Colors.black87),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("no_internet"),
                style: TextStyle(color: Colors.black87),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.black87),
                    ))
              ],
            );
          });
    } else {
      final isAuthenticated = await LocalAuthApi.authenticate();
      if (isAuthenticated) {
        setState(() {
          _isLoading = true;
        });
        SharedPreferences logincredentials =
            await SharedPreferences.getInstance();
        String? loginUsername =
            logincredentials.getString("username${coopShortName}");
        String? loginPassword =
            logincredentials.getString("password${coopShortName}");
        print("this is the required login username: ${loginUsername}");
        signIn(loginUsername!, loginPassword!);
      } else {
        setState(() {
          _isLoading = false;
        });
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("Fingerprint Not Used.")));
      }
    }
  }

  Future<String?> rememberUsername() async {
    SharedPreferences logincredentials = await SharedPreferences.getInstance();
    String? loginUsername =
        logincredentials.getString("rememberusername${coopShortName}");
    if (logincredentials.getBool("Rememberusername${coopShortName}") == false ||
        logincredentials.getBool("Rememberusername${coopShortName}") == null) {
      loginUsername = "";
    } else {
      checkRemember = true;
    }
    return loginUsername;
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final TextEditingController usernamecontroller = TextEditingController();
  final TextEditingController passwordcontroller = TextEditingController();
  final scaffoldKey = GlobalKey<State>();
  final GlobalKey<FormState> globalFormKey = GlobalKey();

  int index = ProjectIndex!;

  movetohome() async {
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // sharedPreferences.clear();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SahakariScreenPage()),
        (Route<dynamic> route) => false);
  }

  exit() {
    SystemNavigator.pop();
  }

  @override
  Widget build(BuildContext context) {
    print("this is from build: ${_isLoading}");
    print("this is primary color: ${primaryColor}");
    return WillPopScope(
      onWillPop: () {
        return exit();
      },
      child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.green[900],
            toolbarHeight: 90,
            centerTitle: true,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                size: 30,
                color: Colors.white,
              ),
              onPressed: () {
                movetohome();
              },
            ),
            title: Text(
                AppLocalizations.of(context)!.localizedString("sahakaari_pay")),
            actions: [
              PopupMenuButton(
                  // add icon, by default "3 dot" icon
                  // icon: Icon(Icons.book)
                  itemBuilder: (context) {
                return [
                  PopupMenuItem<int>(
                    value: 0,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10),
                            image: DecorationImage(
                                image: AssetImage("assets/images/english.png"),
                                fit: BoxFit.fill),
                          ),
                        ),
                        SizedBox(width: 10),
                        Padding(
                          padding: const EdgeInsets.only(top: 15.0),
                          child: Text(AppLocalizations.of(context)!
                              .localizedString("english")),
                        ),
                      ],
                    ),
                  ),
                  PopupMenuItem<int>(
                    value: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                    image:
                                        AssetImage("assets/images/nepali.png"),
                                    fit: BoxFit.fill)),
                          ),
                          SizedBox(width: 10),
                          Padding(
                            padding: const EdgeInsets.only(top: 15.0),
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("nepali"),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ];
              }, onSelected: (value) {
                if (value == 0) {
                  handleLanguage("English");
                } else if (value == 1) {
                  handleLanguage("Nepali");
                }
              }),
            ],
          ),
          body: _isLoading
              ? Center(
                  child: Loading(),
                )
              : SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Center(
                                  child: Text(
                                    name,
                                    style: const TextStyle(
                                        color: Colors.green,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                              Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 20.0, right: 20, top: 15),
                                  child: Container(
                                    height: 440,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                      color: Color(
                                          int.parse(primaryColor.toString())),
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: const Offset(0, 3),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 0.0),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 5.0,
                                            ),
                                            child: Center(
                                              child: Container(
                                                height: 80,
                                                width: 90,
                                                decoration: BoxDecoration(
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.grey
                                                            .withOpacity(0.5),
                                                        spreadRadius: 5,
                                                        blurRadius: 7,
                                                        offset:
                                                            const Offset(0, 3),
                                                      )
                                                    ],
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    color: Colors.white,
                                                    image: DecorationImage(
                                                      image: noInternet
                                                          ? AssetImage(
                                                                  OfflineImagePath +
                                                                      logoUrl)
                                                              as ImageProvider
                                                          : NetworkImage(
                                                              logoUrl),
                                                      fit: BoxFit.contain,
                                                    )),
                                              ),
                                            ),
                                          ),
                                          Form(
                                              key: globalFormKey,
                                              child: Column(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 20.0, top: 5),
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 12.0,
                                                          vertical: 10),
                                                      child: TextFormField(
                                                        controller:
                                                            usernamecontroller,
                                                        maxLength: 10,
                                                        keyboardType:
                                                            TextInputType
                                                                .number,
                                                        style: TextStyle(
                                                            color: Color(int.parse(
                                                                loginTextFieldColor
                                                                    .toString()))),
                                                        validator: (value) => value!
                                                                .isEmpty
                                                            ? AppLocalizations
                                                                    .of(
                                                                        context)!
                                                                .localizedString(
                                                                    "username_empty_message")
                                                            : null,
                                                        onSaved: (value) =>
                                                            usernamecontroller,
                                                        decoration:
                                                            InputDecoration(
                                                                border:
                                                                    const UnderlineInputBorder(),
                                                                labelText: AppLocalizations.of(
                                                                        context)!
                                                                    .localizedString(
                                                                        "username"),
                                                                labelStyle: const TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                                errorStyle: const TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                                counterText: "",
                                                                icon:
                                                                    const Icon(
                                                                  Icons
                                                                      .supervised_user_circle,
                                                                  size: 30.0,
                                                                  color: Colors
                                                                      .white,
                                                                )),
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                      left: 20.0,
                                                    ),
                                                    child: Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 12.0,
                                                          vertical: 10),
                                                      child: TextFormField(
                                                        controller:
                                                            passwordcontroller,
                                                        maxLength: 50,
                                                        keyboardType:
                                                            TextInputType.text,
                                                        style: TextStyle(
                                                            color: Color(int.parse(
                                                                loginTextFieldColor
                                                                    .toString()))),
                                                        obscureText:
                                                            _obscureText,
                                                        validator: (value) => value!
                                                                    .isEmpty ||
                                                                value.length < 3
                                                            ? AppLocalizations
                                                                    .of(
                                                                        context)!
                                                                .localizedString(
                                                                    "password_empty_message")
                                                            : null,
                                                        onSaved: (value) =>
                                                            passwordcontroller,
                                                        decoration:
                                                            InputDecoration(
                                                          border:
                                                              const UnderlineInputBorder(),
                                                          labelText: AppLocalizations
                                                                  .of(context)!
                                                              .localizedString(
                                                                  "password"),
                                                          labelStyle:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                          errorStyle:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                          counterText: "",
                                                          icon: const Icon(
                                                            Icons.lock,
                                                            size: 30.0,
                                                            color: Colors.white,
                                                          ),
                                                          suffixIcon:
                                                              IconButton(
                                                            onPressed: () {
                                                              _toggle();
                                                            },
                                                            icon: Icon(
                                                              _obscureText
                                                                  ? Icons
                                                                      .visibility_off
                                                                  : Icons
                                                                      .visibility,
                                                              color:
                                                                  Colors.white,
                                                              size: 30.0,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              left: 20.0,
                                            ),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Checkbox(
                                                    value: checkRemember,
                                                    onChanged: (value) async {
                                                      setState(() {
                                                        checkRemember = value!;
                                                      });
                                                      print(
                                                          "check box value:${checkRemember}");
                                                      SharedPreferences
                                                          RememberUsername =
                                                          await SharedPreferences
                                                              .getInstance();
                                                      RememberUsername.setBool(
                                                          "Rememberusername${coopShortName}",
                                                          checkRemember);
                                                    }),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                    left: 10.0,
                                                  ),
                                                  child: Text(
                                                    AppLocalizations.of(
                                                            context)!
                                                        .localizedString(
                                                            "remember_username"),
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 14),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                              top: 20.0,
                                            ),
                                            child: Center(
                                              child: InkWell(
                                                onTap: () async {
                                                  if (validateAndSave()) {
                                                    setState(() {
                                                      _isLoading = true;
                                                    });
                                                    final result =
                                                        await Connectivity()
                                                            .checkConnectivity();
                                                    showMessage(result);
                                                    // signIn(usernamecontroller.text,
                                                    //     passwordcontroller.text);
                                                  } else {
                                                    setState(() {
                                                      _isLoading = false;
                                                    });
                                                  }
                                                },
                                                child: Container(
                                                  height: 40,
                                                  width: 140,
                                                  decoration: BoxDecoration(
                                                    color: Color(int.parse(
                                                        loginbuttonColor
                                                            .toString())),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.white
                                                            .withOpacity(0.9),
                                                        spreadRadius: 5,
                                                        blurRadius: 7,
                                                        offset:
                                                            const Offset(0, 3),
                                                      )
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "login"),
                                                      style: TextStyle(
                                                        color: Color(int.parse(
                                                            loginButtonTitleColor
                                                                .toString())),
                                                        fontSize: 16.0,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ]),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Platform.isAndroid
                                  ? showBiometricbutton
                                      ? Padding(
                                          padding: const EdgeInsets.only(
                                              top: 10.0, left: 20, right: 20),
                                          child: Container(
                                              child: TextButton(
                                            onPressed: () async {
                                              // setState(() {
                                              //   _isLoading = true;
                                              // });
                                              final result =
                                                  await Connectivity()
                                                      .checkConnectivity();
                                              showBiometric(result);
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 30.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                // mainAxisAlignment:
                                                //     MainAxisAlignment.center,
                                                children: [
                                                  Icon(
                                                    Icons.fingerprint,
                                                    size: 50,
                                                    color: Color(int.parse(
                                                        loginbuttonColor
                                                            .toString())),
                                                  ),
                                                  SizedBox(width: 10),
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            3,
                                                    child: Text(
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "use_fingerprint"),
                                                      style: TextStyle(
                                                          color: Color(int.parse(
                                                              loginbuttonColor
                                                                  .toString()))),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )),
                                        )
                                      : Padding(
                                          padding: const EdgeInsets.only(
                                              top: 10.0, left: 20, right: 20),
                                          child: Container(
                                              child: TextButton(
                                            onPressed: () async {
                                              // setState(() {
                                              //   _isLoading = true;
                                              // });
                                              final result =
                                                  await Connectivity()
                                                      .checkConnectivity();
                                              //showBiometric(result);
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) => PMCSetFingerprintPage(
                                                          name: name,
                                                          logoUrl: logoUrl,
                                                          baseUrl: baseUrl,
                                                          primaryColor:
                                                              primaryColor,
                                                          loginButtonTitle:
                                                              loginButtonTitleColor,
                                                          loginButtonBackground:
                                                              loginbuttonColor,
                                                          loginTextField:
                                                              loginTextFieldColor,
                                                          dashboardTopIcon:
                                                              dasboardIconColor,
                                                          dashboardTopTitle:
                                                              dashboardTopTitleColor,
                                                          secondaryColor:
                                                              SecondaryColor,
                                                          smsCode: smsCode)));
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 30.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                // mainAxisAlignment:
                                                //     MainAxisAlignment.center,
                                                children: [
                                                  Icon(
                                                    Icons.fingerprint,
                                                    size: 40,
                                                    color: Color(int.parse(
                                                        loginbuttonColor
                                                            .toString())),
                                                  ),
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            3,
                                                    child: Text(
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "set_fingerprint"),
                                                      style: TextStyle(
                                                          color: Color(int.parse(
                                                              loginbuttonColor
                                                                  .toString()))),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )),
                                        )
                                  : Container(),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 20.0, left: 10, right: 10),
                                child: Center(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      PMCMobileBankingPage(
                                                        coopList: coopList,
                                                        accesstoken:
                                                            accesstoken,
                                                        baseUrl: baseUrl,
                                                        accountno: accountno,
                                                        name: name,
                                                        logoUrl: logoUrl,
                                                        primaryColor:
                                                            primaryColor,
                                                        loginButtonTitleColor:
                                                            loginButtonTitleColor,
                                                        loginbuttonColor:
                                                            loginbuttonColor,
                                                        loginTextFieldColor:
                                                            loginTextFieldColor,
                                                        dasboardIconColor:
                                                            dasboardIconColor,
                                                        dashboardTopTitleColor:
                                                            dashboardTopTitleColor,
                                                        SecondaryColor:
                                                            SecondaryColor,
                                                      )));
                                        },
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              3,
                                          height: 100,
                                          decoration: BoxDecoration(
                                            color: Color(int.parse(
                                                loginbuttonColor.toString())),
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.white
                                                    .withOpacity(0.9),
                                                spreadRadius: 5,
                                                blurRadius: 7,
                                                offset: const Offset(0, 3),
                                              )
                                            ],
                                          ),
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 4.0),
                                                child: Container(
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      image:
                                                          const DecorationImage(
                                                        image: AssetImage(
                                                            "assets/images/mobile_banking.png"),
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20.0),
                                                      color: Color(int.parse(
                                                          loginbuttonColor
                                                              .toString()))),
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "activate_mobile_banking"),
                                                style: TextStyle(
                                                    color: Color(int.parse(
                                                        loginButtonTitleColor
                                                            .toString())),
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                textAlign: TextAlign.center,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => PMCsmsPage(
                                                      coopList: coopList,
                                                      accesstoken: accesstoken,
                                                      baseUrl: baseUrl,
                                                      accountno: accountno,
                                                      primaryColor:
                                                          primaryColor,
                                                      loginButtonTitleColor:
                                                          loginButtonTitleColor,
                                                      loginbuttonColor:
                                                          loginbuttonColor,
                                                      loginTextFieldColor:
                                                          loginTextFieldColor,
                                                      dasboardIconColor:
                                                          dasboardIconColor,
                                                      dashboardTopTitleColor:
                                                          dashboardTopTitleColor,
                                                      SecondaryColor:
                                                          SecondaryColor,
                                                      smsCode: smsCode)));
                                        },
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              3,
                                          height: 100,
                                          decoration: BoxDecoration(
                                            color: Color(int.parse(
                                                loginbuttonColor.toString())),
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.white
                                                    .withOpacity(0.9),
                                                spreadRadius: 5,
                                                blurRadius: 7,
                                                offset: const Offset(0, 3),
                                              )
                                            ],
                                          ),
                                          child: Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8.0),
                                                child: Container(
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      image:
                                                          const DecorationImage(
                                                        image: AssetImage(
                                                            "assets/images/sms_banking.png"),
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20.0),
                                                      color: Color(int.parse(
                                                          loginbuttonColor
                                                              .toString()))),
                                                ),
                                              ),
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              Center(
                                                child: Text(
                                                  AppLocalizations.of(context)!
                                                      .localizedString(
                                                          "activate_sms_banking"),
                                                  style: TextStyle(
                                                      color: Color(int.parse(
                                                          loginButtonTitleColor
                                                              .toString())),
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("powered_by"),
                                  style: const TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Container(
                                  height: 70,
                                  width: 140,
                                  decoration: const BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/spay.png"),
                                        fit: BoxFit.cover),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ]),
                )),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void handleLanguage(String value) async {
    SharedPreferences prefService = await SharedPreferences.getInstance();

    switch (value) {
      case 'English':
        DEFAULT_LOCALE = "en";
        DEFAULT_LANGUAGE = "English";

        prefService.setString("default_locale", DEFAULT_LOCALE);
        prefService.setString("default_language", DEFAULT_LANGUAGE);

        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => new MyApp(),
          ),
          (Route<dynamic> route) => false,
        );

        break;
      case 'Nepali':
        DEFAULT_LOCALE = "ne";
        DEFAULT_LANGUAGE = "Nepali";

        prefService.setString("default_locale", DEFAULT_LOCALE);
        prefService.setString("default_language", DEFAULT_LANGUAGE);

        Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
            builder: (context) => new MyApp(),
          ),
          (Route<dynamic> route) => false,
        );

        break;
    }
  }
}
