import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';

class HelpPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  HelpPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _HelpPageState createState() => _HelpPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _HelpPageState extends State<HelpPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  _HelpPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  String dropdownValue = 'What is SahakaariPay Mobile Banking App?';
  String dropdownValue1 = 'How cna I use SahakaariPay Mobile Banking App?';
  String dropdownValue2 = 'Can I change my password and MPIN?';
  String dropdownValue3 = 'What is Switch to SMS Channel?';
  String dropdownValue4 = 'What is SahakaariPay?';
  String dropdownValue5 = 'Security Tips?';

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetohome();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          title: Text(
            "Help",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0, top: 20, right: 20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3),
                        ),
                      ],
                      border: Border.all(color: Colors.green, width: 2),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.green,
                        ), // changes position of shadow
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'What is SahakaariPay Mobile Banking App?',
                          'SahakaariPay Mobile Banking App is an easy and secure way to do banking and payment related transactions. Through this app you can connect with your bank account for most of the banking services along with the online payments via Internet or SMS channel.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue1,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.green,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'How cna I use SahakaariPay Mobile Banking App?',
                          'You need to have an account with one of the partner bank of PES. You need to request the bank to open mobile banking account with your mobile number, email, and account number to be linked. After verification, the bank will send you your username, password and a four digit MPIN. Now you can use SahakaariPay Mobile Banking.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue2,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.green,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'Can I change my password and MPIN?',
                          'Yes, please login with your existing username and password. Go to settings, there you will find two options - Change Password, Change Transaction PIN. You need to change password and MPIN time and again in order to secure your account,'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue3,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.green,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'What is Switch to SMS Channel?',
                          'If you do not have access to Internet then you can switch to SMS Channel to access Mobile Banking services using SMS. The app will automatically send SMS to get the desired services.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue4,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 90,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.green,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'What is SahakaariPay?',
                          'Through SahakaariPay you can top-up or get recharge coupons for your NT GSM/CDMA, NCELL, UTL, Smart Cell mobile phone balances. You can pay bills for NT Landline, ADSL, World Link, Subishu, Sim TV, Dish Home. You can also get recharge coupons for Broad Link Internet and NET TV.'
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    height: 65,
                    width: 400,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 18, vertical: 2),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.1),
                            spreadRadius: 5,
                            blurRadius: 7,
                            offset: const Offset(
                                0, 3), // changes position of shadow
                          ),
                        ],
                        border: Border.all(color: Colors.green, width: 2),
                        borderRadius: BorderRadius.circular(20.0)),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue5,
                        alignment: AlignmentDirectional.center,
                        itemHeight: 60,
                        borderRadius: BorderRadius.circular(20.0),
                        icon: const Icon(
                          Icons.arrow_drop_down,
                          color: Colors.green,
                        ),
                        iconSize: 60,
                        elevation: 16,
                        style: const TextStyle(
                            color: Colors.black87,
                            fontSize: 12,
                            fontWeight: FontWeight.bold),
                        onChanged: (String? newValue) {},
                        items: <String>[
                          'Security Tips?',
                          "In order to secure your Mobile App, please don't share your username, password, MPIN with anyone. Logout from the app once you have used the services."
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: SingleChildScrollView(child: Text(value)),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
