import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
<<<<<<< HEAD
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';

=======
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../main.dart';
import '../../../constants.dart';
import '../../../utils/localizations.dart';
>>>>>>> sudeep
import '../../models/loading.dart';

class ChangeMPINPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  ChangeMPINPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<ChangeMPINPage> createState() => _ChangeMPINPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _ChangeMPINPageState extends State<ChangeMPINPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  _ChangeMPINPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;
  bool _obscureText = true;

  final scaffoldKey = GlobalKey<State>();
  final GlobalKey<FormState> globalFormKey = GlobalKey();

  final TextEditingController oldmpincontroller = TextEditingController();
  final TextEditingController newmpincontroller = TextEditingController();
  final TextEditingController confirmpincontroller = TextEditingController();

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  changeMPIN() async {
    String url = "${baseUrl}api/v1/users/${userId}/changempin";
    Map body = {
      "userId": userId,
      "OldMPIN": oldmpincontroller.text.toString(),
      "ConfirmMPIN": confirmpincontroller.toString(),
      "NewPIN": newmpincontroller.text.toString()
    };
    var jsonResponse;
    var res = await http.put(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print("Response status : ${jsonResponse}");
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Icon(
                            Icons.verified,
                            size: 40,
                            color: Colors.green,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10.0),
                          child: Text(
                            "Alert",
                            style:
                                TextStyle(fontSize: 14, color: Colors.black87),
                          ),
                        ),
                      ],
                    ),
                  ),
                  content: Text(
                    jsonResponse["Message"],
                    style: const TextStyle(fontSize: 14, color: Colors.black87),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () async {
                          SharedPreferences logincredentials = await SharedPreferences.getInstance();
                          logincredentials.setString("transactionusername${coopShortName}", "");
                          logincredentials.setString("transactionpassword${coopShortName}", "");
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY",
                            style:
                                TextStyle(fontSize: 14, color: Colors.black87)))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: const [
                    Text("Alert"),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: const [
                    Text("Alert"),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  movetoSettingsPage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoSettingsPage();
      },
      child: Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          title: const Text("Chnage MPIN"),
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              movetoSettingsPage();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 40,
              color: Color(int.parse(dasboardIconColor.toString())),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 50.0, left: 30),
                child: Container(
                  height: 430,
                  width: 350,
                  decoration: BoxDecoration(
                      color: Color(int.parse(primaryColor.toString())),
                      borderRadius: BorderRadius.circular(20)),
                  child: _isLoading
                      ? Center(
                          child: Column(
                          children: const [
                            SizedBox(height: 20),
                            Loading(),
                          ],
                        ))
                      : Padding(
                          padding: const EdgeInsets.only(top: 30.0),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Form(
                                key: globalFormKey,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, right: 20.0),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12.0, vertical: 10),
                                        child: TextFormField(
                                          controller: oldmpincontroller,
                                          maxLength: 4,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                              color: Color(int.parse(
                                                  loginTextFieldColor
                                                      .toString()))),
                                          obscureText: _obscureText,
                                          validator: (value) => value!
                                                      .isEmpty ||
                                                  value.length < 3
                                              ? "MPIN should be more then 3 characters"
                                              : null,
                                          onSaved: (value) => oldmpincontroller,
                                          decoration: InputDecoration(
                                            border:
                                                const UnderlineInputBorder(),
                                            labelText: "Old MPIN",
                                            labelStyle: const TextStyle(
                                                color: Colors.white),
                                            errorStyle: const TextStyle(
                                                color: Colors.white),
                                            counterText: "",
                                            icon: Icon(
                                              Icons.lock,
                                              size: 30.0,
                                              color: Color(int.parse(
                                                  dasboardIconColor
                                                      .toString())),
                                            ),
                                            suffixIcon: IconButton(
                                              onPressed: () {
                                                _toggle();
                                              },
                                              icon: Icon(
                                                _obscureText
                                                    ? Icons.visibility
                                                    : Icons.visibility_off,
                                                color: Color(int.parse(
                                                    dasboardIconColor
                                                        .toString())),
                                                size: 30.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, right: 20),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12.0, vertical: 10),
                                        child: TextFormField(
                                          controller: newmpincontroller,
                                          maxLength: 4,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                              color: Color(int.parse(
                                                  loginTextFieldColor
                                                      .toString()))),
                                          obscureText: _obscureText,
                                          validator: (value) => value!
                                                      .isEmpty ||
                                                  value.length < 3
                                              ? "MPIN should be more then 3 characters"
                                              : null,
                                          onSaved: (value) => newmpincontroller,
                                          decoration: InputDecoration(
                                            border:
                                                const UnderlineInputBorder(),
                                            labelText: "New MPIN",
                                            labelStyle: const TextStyle(
                                                color: Colors.white),
                                            errorStyle: const TextStyle(
                                                color: Colors.white),
                                            counterText: "",
                                            icon: Icon(
                                              Icons.lock,
                                              size: 30.0,
                                              color: Color(int.parse(
                                                  dasboardIconColor
                                                      .toString())),
                                            ),
                                            suffixIcon: IconButton(
                                              onPressed: () {
                                                _toggle();
                                              },
                                              icon: Icon(
                                                _obscureText
                                                    ? Icons.visibility
                                                    : Icons.visibility_off,
                                                color: Color(int.parse(
                                                    dasboardIconColor
                                                        .toString())),
                                                size: 30.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, right: 20.0),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12.0, vertical: 10),
                                        child: TextFormField(
                                          controller: confirmpincontroller,
                                          maxLength: 4,
                                          keyboardType: TextInputType.text,
                                          style: TextStyle(
                                              color: Color(int.parse(
                                                  loginTextFieldColor
                                                      .toString()))),
                                          obscureText: _obscureText,
                                          validator: (value) => value!
                                                      .isEmpty ||
                                                  value.length < 3
                                              ? "MPIN should be more then 3 characters"
                                              : null,
                                          onSaved: (value) =>
                                              confirmpincontroller,
                                          decoration: InputDecoration(
                                            border:
                                                const UnderlineInputBorder(),
                                            labelText: "Confirm New MPIN",
                                            labelStyle: const TextStyle(
                                                color: Colors.white),
                                            errorStyle: const TextStyle(
                                                color: Colors.white),
                                            counterText: "",
                                            icon: Icon(
                                              Icons.lock,
                                              size: 30.0,
                                              color: Color(int.parse(
                                                  dasboardIconColor
                                                      .toString())),
                                            ),
                                            suffixIcon: IconButton(
                                              onPressed: () {
                                                _toggle();
                                              },
                                              icon: Icon(
                                                _obscureText
                                                    ? Icons.visibility
                                                    : Icons.visibility_off,
                                                color: Color(int.parse(
                                                    dasboardIconColor
                                                        .toString())),
                                                size: 30.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                        left: 200.0,
                                        top: 40.0,
                                      ),
                                      child: InkWell(
                                        onTap: () async {
                                          if (validateAndSave()) {
                                            setState(() {
                                              _isLoading = true;
                                            });
                                            changeMPIN();
                                          } else {
                                            _isLoading = false;
                                          }
                                        },
                                        child: Container(
                                          height: 50,
                                          width: 100,
                                          decoration: BoxDecoration(
                                              color: Color(int.parse(
                                                  loginbuttonColor.toString())),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Center(
                                            child: Text(
                                              "Submit",
                                              style: TextStyle(
                                                color: Color(int.parse(
                                                    loginTextFieldColor
                                                        .toString())),
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                )),
                          ),
                        ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
