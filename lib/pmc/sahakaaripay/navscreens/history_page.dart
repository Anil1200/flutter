import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/models/historymodel.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';

import '../../utils/localizations.dart';

class HistoryPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  HistoryPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _HistoryPageState createState() => _HistoryPageState(
        coopList,
        userId,
        accesstoken,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _HistoryPageState extends State<HistoryPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _HistoryPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    _isLoading = true;
    controller.addListener(() {
      if (controller.position.pixels >= controller.position.maxScrollExtent) {
        print("New data called");
        // page += 1;
        setState(() {
          HistoryDetailsData();
        });
      }
    });
    HistoryDetailsData();
  }

  // InfiniteLoading() {
  //   if (historylist.length == historylist[0].TotalCount) {
  //     isLoading = false;
  //   } else {
  //     setState(() {
  //       isLoading = true;
  //     });
  //   }
  // }

  // scrollcontroller() {
  //   var End = controller.offset >= controller.position.maxScrollExtent &&
  //       controller.position.outOfRange;
  //   if (End) {
  //     setState(() {
  //       page += 1;
  //       HistoryDetailsData();
  //       print("jklafhlkadfklajflka lkajfklaj dalkd");
  //     });
  //   }
  // }
  bool isLoading = false;
  bool hasmore = true;

  bool _isLoading = false;
  List<dynamic> historylist = [];

  String? balance;

  int page = 1;
  final ScrollController controller = ScrollController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  HistoryDetailsData() async {
    if (isLoading) return;
    isLoading = true;
    const itemPerPage = 10;
    print("this is user id: ${userId}");
    String url =
        "${baseUrl}api/v1/pes/users/${userId}/transaction-report-history?pageNumber=${page}&itemPerPage=$itemPerPage&userId=${userId}";
    var jsonResponse;
    print("Page Number: ${url.toString()}");
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("this is the jsonResponse of history: ${jsonResponse}");
      setState(() {
        _isLoading = false;
        page++;
        isLoading = false;
        historylist.addAll(json
            .decode(res.body)
            .map((m) => HistoryModel.fromJson(m))
            .toList());
        // InfiniteLoading();
        print("the required list: ${historylist}");
        if (historylist.length % itemPerPage != 0) {
          hasmore = false;
        }

        // if (historylist.length <= itemPerPage &&
        //     historylist.length == historylist[0].TotalCount) {
        //   hasmore = false;
        // }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
        hasmore = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the 400 error: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.black87),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
        hasmore = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the 401 error :${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.black87),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
        hasmore = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error message: ${jsonResponse}");
    }
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoHomePage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetoHomePage();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          title: Text(
            "SahakaariPay",
            style: TextStyle(color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: _isLoading
            ? Center(child: Loading())
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height - 100,
                      child: ListView.builder(
                          controller: controller,
                          itemCount:
                              historylist == null ? 0 : historylist.length + 1,
                          itemBuilder: (BuildContext context, int index) {
                            if (index < historylist.length) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                  top: 20,
                                  left: 20.0,
                                  right: 20,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 20, left: 20, right: 10),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                3.5,
                                            child: Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString(
                                                      "transaction_type"),
                                              style: TextStyle(
                                                color: Color(int.parse(
                                                    dashboardTopTitleColor
                                                        .toString())),
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                1.85,
                                            child: Text(
                                                "${historylist[index].TransactionType}",
                                                style: TextStyle(
                                                  color: Color(int.parse(
                                                      dashboardTopTitleColor
                                                          .toString())),
                                                  fontSize: 16.0,
                                                )),
                                          ),
                                        ],
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 10.0),
                                        child: Row(
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  3.5,
                                              child: Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "transaction_amount"),
                                                style: TextStyle(
                                                  color: Color(int.parse(
                                                      dashboardTopTitleColor
                                                          .toString())),
                                                  fontSize: 16.0,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.85,
                                              child: Text(
                                                  "${historylist[index].TransactionAmount}",
                                                  style: TextStyle(
                                                    color: Color(int.parse(
                                                        dashboardTopTitleColor
                                                            .toString())),
                                                    fontSize: 16.0,
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 10.0),
                                        child: Row(
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  3.5,
                                              child: Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "transaction_date"),
                                                style: TextStyle(
                                                  color: Color(int.parse(
                                                      dashboardTopTitleColor
                                                          .toString())),
                                                  fontSize: 16.0,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.85,
                                              child: Text(
                                                  "${historylist[index].CreatedDate.toString()}",
                                                  style: TextStyle(
                                                    color: Color(int.parse(
                                                        dashboardTopTitleColor
                                                            .toString())),
                                                    fontSize: 16.0,
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 10.0),
                                        child: Row(
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  3.5,
                                              child: Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "transaction_remark"),
                                                style: TextStyle(
                                                  color: Color(int.parse(
                                                      dashboardTopTitleColor
                                                          .toString())),
                                                  fontSize: 16.0,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.85,
                                              child: Text(
                                                  "${historylist[index].Remarks.toString()}",
                                                  style: TextStyle(
                                                    color: Color(int.parse(
                                                        dashboardTopTitleColor
                                                            .toString())),
                                                    fontSize: 16.0,
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 10.0),
                                        child: Row(
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  3.5,
                                              child: Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString("status"),
                                                style: TextStyle(
                                                  color: Color(int.parse(
                                                      dashboardTopTitleColor
                                                          .toString())),
                                                  fontSize: 16.0,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  1.85,
                                              child: Text(
                                                  "${historylist[index].Status.toString()}",
                                                  style: TextStyle(
                                                    color: Color(int.parse(
                                                        dashboardTopTitleColor
                                                            .toString())),
                                                    fontSize: 16.0,
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 20.0),
                                        child: Divider(
                                          thickness: 1,
                                          color: Colors.green[900],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            } else {
                              return Center(
                                  child: hasmore
                                      ? const CircularProgressIndicator(
                                          color: Colors.green,
                                        )
                                      : Text(AppLocalizations.of(context)!
                                          .localizedString("data_load")));
                            }
                          }),
                    ),

                    // isLoading
                    //     ? SpinKitChasingDots(
                    //         color: Colors.red,
                    //         size: 50.0,
                    //       )
                    //     : Container()
                  ],
                ),
              ),
      ),
    );
  }
}
