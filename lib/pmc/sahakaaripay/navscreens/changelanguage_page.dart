import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import '../pmc_homepage.dart';

class LanguagePage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  LanguagePage(
      {Key? key,
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor})
      : super(key: key);

  @override
  _LanguagePageState createState() => _LanguagePageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _LanguagePageState extends State<LanguagePage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _LanguagePageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor);

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Center(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Container(
                    height: 150,
                    width: 150,
                    child: TextButton(
                        style: DEFAULT_LOCALE == "ne"
                            ? TextButton.styleFrom(
                                side: BorderSide(color: Colors.green, width: 2),
                              )
                            : TextButton.styleFrom(side: BorderSide.none),
                        onPressed: () async {
                          SharedPreferences prefService =
                              await SharedPreferences.getInstance();
                          DEFAULT_LOCALE = "ne";
                          DEFAULT_LANGUAGE = "Nepali";
                          prefService.setString(
                              "default_locale", DEFAULT_LOCALE);
                          prefService.setString(
                              "default_language", DEFAULT_LANGUAGE);
                          Locale locale = Locale(DEFAULT_LOCALE);
                          AppLocalizations.load(locale);
                          movetohome();
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 80,
                              width: 100,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/nepali.png"))),
                            ),
                            Text(AppLocalizations.of(context)!
                                .localizedString("nepali")),
                          ],
                        )),
                  ),
                ),
                SizedBox(
                  width: 60,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Container(
                    height: 150,
                    width: 150,
                    child: TextButton(
                        style: DEFAULT_LOCALE == "en"
                            ? TextButton.styleFrom(
                                side: BorderSide(color: Colors.green, width: 2),
                              )
                            : TextButton.styleFrom(side: BorderSide.none),
                        onPressed: () async {
                          SharedPreferences prefService =
                              await SharedPreferences.getInstance();
                          DEFAULT_LOCALE = "en";
                          DEFAULT_LANGUAGE = "English";

                          prefService.setString(
                              "default_locale", DEFAULT_LOCALE);
                          prefService.setString(
                              "default_language", DEFAULT_LANGUAGE);
                          Locale locale = Locale(DEFAULT_LOCALE);
                          AppLocalizations.load(locale);
                          movetohome();
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 80,
                              width: 100,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/english.png"))),
                            ),
                            Text(AppLocalizations.of(context)!
                                .localizedString("english")),
                          ],
                        )),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
