import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';

import '../../utils/localizations.dart';
import '../models/loading.dart';

class ATMInfoPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  ATMInfoPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<ATMInfoPage> createState() => _ATMInfoPageState(
        coopList,
        userId,
        accesstoken,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _ATMInfoPageState extends State<ATMInfoPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _ATMInfoPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  String? accountNumber;
  String? accountName;
  String? cardNumber;
  bool? isActive;
  bool? isAutoBlocked;

  bool? requestAtm;

  @override
  void initState() {
    _isLoading = true;
    super.initState();
    getAtmData();
  }

  bool _isLoading = false;

  Future<String> getAtmData() async {
    String url = "${baseUrl}api/v1/pes/account/$accountno/atminfo";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer $accesstoken"});
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        var convertDataToJson = json.decode(response.body);
        if (convertDataToJson != null) {
          accountNumber = convertDataToJson["AccountNumber"];
          accountName = convertDataToJson["AccountName"];
          cardNumber = convertDataToJson["CardNumber"];
          isActive = convertDataToJson["IsAvtice"];
          isAutoBlocked = convertDataToJson["IsAutoBlocked"];

          requestAtm = false;
        } else {
          requestAtm = true;
        }
        print(convertDataToJson);
      });
      return "success";
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
    return "success";
  }

  Future<bool> getBlock() async {
    String url = "${baseUrl}api/v1/pes/account/$accountno/blockatm";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer $accesstoken"});
    print(response.body);
    if (response.statusCode == 200) {
      /*setState(() {
        _isLoading = false;
      });*/
      var convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      if (convertDataToJson != null) {
        if (convertDataToJson) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context)!.localizedString("atm_card_blocked"))));

          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      /*setState(() {
        _isLoading = false;
      });*/
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
      return false;
    }
  }

  Future<bool> getUnblock() async {
    String url = "${baseUrl}api/v1/pes/account/$accountno/unblockatm";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer $accesstoken"});
    print(response.body);
    if (response.statusCode == 200) {
      /*setState(() {
        _isLoading = false;
      });*/
      var convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      if (convertDataToJson != null) {
        if (convertDataToJson) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context)!.localizedString("atm_card_unblocked"))));

          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      /*setState(() {
        _isLoading = false;
      });*/
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
      return false;
    }
  }

  Future<bool> postRequest() async {
    String url = "${baseUrl}api/v1/pes/account/$accountno/requestatm";
    final response = await http.post(Uri.parse(url),
        headers: {'Authorization': "Bearer $accesstoken"});
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
      var convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      if (convertDataToJson != null) {
        if (convertDataToJson) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context)!.localizedString("atm_card_requested"))));

          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
      return false;
    }
  }

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  Widget card() {
    return Stack(
      children: <Widget>[
        Container(
          height: 220.0,
          width: double.infinity,
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Color(0xFF31A1C9), Color(0xFF3DB6D4)],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          ),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(25.0, 25.0, 25.0, 5.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 32.0,
                          width: 32.0,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                SahakaarilogoUrl!,
                              ),
                              fit: BoxFit.contain,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(
                            coopName!,
                            style: const TextStyle(
                                fontFamily: "Sans",
                                fontWeight: FontWeight.normal,
                                fontSize: 16.0,
                                letterSpacing: 1.5,
                                color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 25,
                ),
                Container(
                  height: 40.0,
                  width: 40.0,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                          "assets/images/emv_chip.png"
                      ),
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                SizedBox(
                  height: (isActive! && !isAutoBlocked!) ? 10 : 5,
                ),
                Row(
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          cardNumber!.replaceRange(9, 15, "******"),
                          style: const TextStyle(
                              fontFamily: "Sofia",
                              letterSpacing: 1.5,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              color: Colors.white),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          accountName!.trim(),
                          style: const TextStyle(
                              fontFamily: "Sofia",
                              letterSpacing: 1.5,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                if (!isActive! && isAutoBlocked!) ...[
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("atm_card_blocked"),
                        style: const TextStyle(
                            fontFamily: "Sofia",
                            letterSpacing: 1.5,
                            fontWeight: FontWeight.w500,
                            color: Colors.red),
                      ),
                    ),
                  ),
                ],
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            height: 170.0,
            width: 170.0,
            decoration: BoxDecoration(
                color: Colors.white10.withOpacity(0.1),
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(200.0),
                    topRight: Radius.circular(20.0))),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.localizedString("atm_info"),
          ),
          leading: IconButton(
            onPressed: () {
              movetohome();
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
        ),
        body: _isLoading
          ? const Center(child: Loading())
          : !requestAtm!
          ? Column(
            children: [
              Container(
                color: Colors.transparent,
                child: Padding(
                    padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10, bottom: 10),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: card(),
                    ),
                  ),
              ),
              Container(
                color: Colors.black38,
                alignment: Alignment.centerLeft,
                child: const Padding(
                  padding: EdgeInsets.only(top: 10.0, left: 10, right: 10, bottom: 10),
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text("Card Services",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10, bottom: 10),
                  child: ListView(
                    children: [
                      ListTile(
                        leading: const Icon(Icons.people),
                        trailing: const Icon(Icons.arrow_forward_ios),
                        title: Text(
                          AppLocalizations.of(context)!.localizedString("atm_card_block_unblock"),
                        ),
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text(
                                  AppLocalizations.of(context)!.localizedString("alert"),
                                ),
                                content: Text(
                                  (isActive! && !isAutoBlocked!)
                                    ? AppLocalizations.of(context)!.localizedString("atm_card_block")
                                    : AppLocalizations.of(context)!.localizedString("atm_card_unblock")
                                ),
                                actions: [
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text(AppLocalizations.of(context)!.localizedString("no")),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      setState(() {
                                        _isLoading = true;
                                      });
                                      Navigator.of(context).pop();

                                      if (isActive! && !isAutoBlocked!) {
                                        getBlock().then((value) {
                                          getAtmData();
                                        });
                                      } else {
                                        getUnblock().then((value) {
                                          getAtmData();
                                        });
                                      }
                                    },
                                    child: Text(AppLocalizations.of(context)!.localizedString("yes")),
                                  ),
                                ],
                              );
                            }
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
          : Center(
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.green[900],
                  borderRadius: BorderRadius.circular(5),
                ),
                child: TextButton(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text(
                          AppLocalizations.of(context)!.localizedString("alert"),
                        ),
                        content: Text(AppLocalizations.of(context)!.localizedString("atm_card_request_message")
                        ),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(AppLocalizations.of(context)!.localizedString("no")),
                          ),
                          TextButton(
                            onPressed: () {
                              setState(() {
                                _isLoading = true;
                              });
                              Navigator.of(context).pop();

                              postRequest();
                            },
                            child: Text(AppLocalizations.of(context)!.localizedString("yes")),
                          ),
                        ],
                      );
                    }
                  );
                },
                child: Text(
                  AppLocalizations.of(context)!.localizedString("atm_card_request"),
                  style: TextStyle(
                    color:
                    Color(int.parse(loginTextFieldColor.toString())),
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
      ),
    );
  }
}
