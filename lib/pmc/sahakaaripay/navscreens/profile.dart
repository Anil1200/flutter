import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';

class ProfilePage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  ProfilePage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState(
    coopList,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
  );
}

class _ProfilePageState extends State<ProfilePage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _ProfilePageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      );

  bool _isLoading = false;
  bool showBalance = false;

  final GlobalKey<FormState> globalFormKey = GlobalKey();

  String? AccountNo;
  String? FullName;
  String? MobileNo;
  String? Address;
  String? Email;
  String? ExpiryDate;
  String? PerTransactionAmount;
  String? DailyFundTransferLimit;
  String? MonthlyFundTransferLimit;
  String? UsedDailyFundTransfer;
  String? UsedMonthlyFundTransfer;

  void initState() {
    super.initState();
    _isLoading = true;
    getProfileData();
  }

  getProfileData() async {
    String url = "${baseUrl}api/v1/users/${userId}/getuserdetails";
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;

        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            AccountNo = jsonResponse[0]["AccountNo"];
            FullName = jsonResponse[0]["FullName"];
            MobileNo = jsonResponse[0]["MobileNo"];
            Address = jsonResponse[0]["Address"];
            Email = jsonResponse[0]["Email"];
            ExpiryDate = jsonResponse[0]["ExpiryDate"];
            PerTransactionAmount = jsonResponse[0]["PerTransactionAmount"];
            DailyFundTransferLimit = jsonResponse[0]["DailyFundTransferLimit"];
            MonthlyFundTransferLimit = jsonResponse[0]["MonthlyFundTransferLimit"];
            UsedDailyFundTransfer = jsonResponse[0]["UsedDailyFundTransfer"];
            UsedMonthlyFundTransfer = jsonResponse[0]["UsedMonthlyFundTransfer"];
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    AppLocalizations.of(context)!
                        .localizedString("something_went_wrong"),
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
              coopList: coopList,
              userId: userId,
              accesstoken: accesstoken,
              baseUrl: baseUrl,
              accountno: accountno,
              primaryColor: primaryColor,
              loginButtonTitleColor: loginButtonTitleColor,
              loginbuttonColor: loginbuttonColor,
              loginTextFieldColor: loginTextFieldColor,
              dasboardIconColor: dasboardIconColor,
              dashboardTopTitleColor: dashboardTopTitleColor,
              SecondaryColor: SecondaryColor,
            )),
            (Route<dynamic> route) => false);
  }

  final TextEditingController addressController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : ScrollConfiguration(
          behavior: CustomScrollBehavior(
            androidSdkVersion: androidSdkVersion,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    color: Color(int.parse(primaryColor.toString())),
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        left: 10.0,
                        top: 40,
                        child: IconButton(
                            onPressed: () {
                              movetohome();
                            },
                            icon: const Icon(
                              Icons.arrow_back,
                              size: 30.0,
                              color: Colors.white,
                            )),
                      ),
                      Positioned(
                        left: 100,
                        top: 50,
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("sahakaari_pay"),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                      Positioned(
                        left: 20,
                        top: 95,
                        child: Row(
                          children: [
                            const SizedBox(
                              height: 30,
                              width: 30,
                              child: Image(
                                  image: AssetImage(
                                      "assets/images/wallet_icon.png")),
                            ),
                            const SizedBox(
                              width: 10.0,
                            ),
                            Text(
                              showBalance
                                  ? AppLocalizations.of(context)!
                                  .localizedString("rs") +
                                  "${balance.toString()}"
                                  : "xxx.xx".toUpperCase(),
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 180.0),
                              child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      if (showBalance == false) {
                                        showBalance = true;
                                      } else {
                                        showBalance = false;
                                      }
                                    });
                                  },
                                  icon: Icon(
                                    showBalance
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    size: 25.0,
                                    color: Colors.white,
                                  )),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: Center(
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Center(
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString(
                                    "profile"),
                                style: const TextStyle(
                                  color: Colors.black87,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            width:
                            MediaQuery.of(context).size.width - 75,
                            child: Form(
                              key: globalFormKey,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                "account_number"),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left:10.0, top: 10.0),
                                          child: Text(
                                            AccountNo.toString(),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                "account_name"),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left:10.0, top: 10.0),
                                          child: Text(
                                            FullName.toString(),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                "mobile_number_title"),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left:10.0, top: 10.0),
                                          child: Text(
                                            MobileNo.toString(),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                "address"),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left:10.0, top: 10.0),
                                          child: Text(
                                            Address.toString(),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                "email"),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left:10.0, top: 10.0),
                                          child: Text(
                                            Email.toString(),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString(
                                                "expiry_date"),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left:10.0, top: 10.0),
                                          child: Text(
                                            ExpiryDate.toString(),
                                            style: const TextStyle(
                                              color: Colors.black87,
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 50.0),
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString(
                                          "transaction_details"),
                                      style: const TextStyle(
                                        color: Colors.black87,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(top: 10),
                                    child: Center(
                                      child: Container(
                                        height: 150,
                                        width: MediaQuery.of(context).size.width - 0,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            border: Border.all(color: Colors.black12),
                                            color: Colors.white),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                                                    child: Text(
                                                      AppLocalizations.of(context)!
                                                          .localizedString(
                                                          "per_transaction_amount"),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(left:10.0, top: 10.0),
                                                    child: Text(
                                                      PerTransactionAmount.toString(),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                                                    child: Text(
                                                      AppLocalizations.of(context)!
                                                          .localizedString(
                                                          "daily_fund_transfer_limit"),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(left:10.0, top: 10.0),
                                                    child: Text(
                                                      DailyFundTransferLimit.toString(),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                                                    child: Text(
                                                      AppLocalizations.of(context)!
                                                          .localizedString(
                                                          "monthly_fund_transfer_limit"),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(left:10.0, top: 10.0),
                                                    child: Text(
                                                      MonthlyFundTransferLimit.toString(),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                                                    child: Text(
                                                      AppLocalizations.of(context)!
                                                          .localizedString(
                                                          "used_daily_fund_transfer"),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(left:10.0, top: 10.0),
                                                    child: Text(
                                                      UsedDailyFundTransfer.toString(),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            SingleChildScrollView(
                                              scrollDirection: Axis.horizontal,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                                                    child: Text(
                                                      AppLocalizations.of(context)!
                                                          .localizedString(
                                                          "used_monthly_fund_transfer"),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(left:10.0, top: 10.0),
                                                    child: Text(
                                                      UsedMonthlyFundTransfer.toString(),
                                                      style: const TextStyle(
                                                        color: Colors.black87,
                                                        fontSize: 14.0,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  /*const SizedBox(
                                    height: 10,
                                  ),
                                  TextFormField(
                                    controller: addressController,
                                    keyboardType: TextInputType.text,
                                    validator: (value) => value!.isEmpty
                                        ? AppLocalizations.of(context)!
                                        .localizedString(
                                        "address")
                                        : null,
                                    onSaved: (value) =>
                                    addressController,
                                    decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText:
                                      AppLocalizations.of(context)!
                                          .localizedString(
                                          "address"),
                                      labelStyle: const TextStyle(
                                          color: Colors.green),
                                      counterText: "",
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  TextFormField(
                                    controller: emailController,
                                    keyboardType: TextInputType.emailAddress,
                                    validator: (value) => value!.isEmpty
                                        ? AppLocalizations.of(context)!
                                        .localizedString(
                                        "email")
                                        : null,
                                    onSaved: (value) =>
                                    emailController,
                                    decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText:
                                      AppLocalizations.of(context)!
                                          .localizedString(
                                          "email"),
                                      labelStyle: const TextStyle(
                                          color: Colors.green),
                                      counterText: "",
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),*/
                                ],
                              ),
                            ),
                          ),
                          /*Padding(
                            padding:
                            const EdgeInsets.only(left: 20, top: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(10.0),
                                      color: Color(int.parse(
                                          loginbuttonColor.toString())),
                                      boxShadow: [
                                        BoxShadow(
                                          color:
                                          Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: const Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  height: 50,
                                  width: MediaQuery.of(context).size.width -
                                      120,
                                  child: TextButton(
                                    onPressed: () async {
                                      if (validateAndSave()) {
                                        setState(() {
                                          _isLoading = true;
                                        });
                                        postProfileData();
                                      } else {
                                        setState(() {
                                          _isLoading = false;
                                        });
                                      }
                                    },
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("update"),
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),*/
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
