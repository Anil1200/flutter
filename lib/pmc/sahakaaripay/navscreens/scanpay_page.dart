import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_zxing/flutter_zxing.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:image/image.dart' as imglib;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/qrscan_page.dart';

import '../../utils/localizations.dart';
import '../pmc_homepage.dart';

class ScanPayPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  ScanPayPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _ScanPayPageState createState() => _ScanPayPageState(
    coopList,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
  );
}

class _ScanPayPageState extends State<ScanPayPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _ScanPayPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      );

  // double? balance;
  String? accountName;
  Uint8List? qrCodeData;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      // createBarcode(accountno.toString() + "#IFT#" + AccountName.toString(), 300, 300);

      Map<String, dynamic> qrData = {
        "sahakaariPay": accountno.toString() + "#IFT#" + AccountName.toString(),
        "NFSGK3TUNFTGSZLS": "aHR0cHM6Ly9lc2V3YS5jb20ubnA=",
        "code": "SAHAKARI_WITHDRAW",
        "properties": {
          "acc_no": accountno,
          "beneficiary_name": AccountName.toString(),
          "client_code": clientCode
        }
      };

      String jsonString = jsonEncode(qrData);

      createBarcode(jsonString, 300, 300);
    }
  }

  movetohome() {
    Navigator.of(context).pop();
    /*Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
              coopList: coopList,
              userId: userId,
              accesstoken: accesstoken,
              baseUrl: baseUrl,
              accountno: accountno,
              primaryColor: primaryColor,
              loginButtonTitleColor: loginButtonTitleColor,
              loginbuttonColor: loginbuttonColor,
              loginTextFieldColor: loginTextFieldColor,
              dasboardIconColor: dasboardIconColor,
              dashboardTopTitleColor: dashboardTopTitleColor,
              SecondaryColor: SecondaryColor,
            )),
            (Route<dynamic> route) => false);*/
  }

  String? result;

  createBarcode(String qrText, int qrWidth, int qrHeight) async {
    final result = zx.encodeBarcode(
      qrText,
      format: Format.qrCode,
      width: qrWidth,
      height: qrHeight,
      margin: 10,
      eccLevel: 0,
    );
    if (result.isValid && result.data != null) {
      try {
        final imglib.Image img = imglib.Image.fromBytes(
          qrWidth,
          qrHeight,
          result.data!,
        );
        final Uint8List encodedBytes = Uint8List.fromList(
          imglib.encodeJpg(img),
        );

        qrCodeData = encodedBytes;
      } catch (e) {
        qrCodeData = null;
      }
    } else {
      qrCodeData = null;
    }
  }

  String? toaccountno;
  String? toaccountName;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        /*appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.localizedString("sahakaari_scan_pay"),
          ),
          leading: IconButton(
            onPressed: () {
              movetohome();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
        ),*/
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 40.0, left: 00),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        movetohome();
                      },
                      icon: const Icon(
                        Icons.close,
                        size: 30,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
              /*Padding(
                padding: const EdgeInsets.only(left: 20, top: 20.0),
                child: Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Icon(
                        Icons.account_balance_wallet_outlined,
                        color: Colors.green,
                        size: 20,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("npr_balance"),
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 16,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        AppLocalizations.of(context)!.localizedString("rs") +
                            "${balance.toString()}",
                        style: const TextStyle(
                          color: Colors.black87,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
              ),*/
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/spay.png',
                        height: 125,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "My QR Code",
                        style: TextStyle(
                          color: Color(int.parse(loginbuttonColor.toString())),
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              if (Platform.isAndroid) ...[
                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Center(
                    child: Image.memory(qrCodeData!),
                  ),
                ),
              ] else ...[
                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Center(
                    child: QrImageView(
                      data: accountno.toString() + "#IFT#" + AccountName.toString(),
                      size: 300,
                      backgroundColor: Colors.white,
                    ),
                  ),
                ),
              ],
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        AccountName.toString(),
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        accountno.toString(),
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              /*Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Center(
                  child: Container(
                    height: 100,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Color(int.parse(loginbuttonColor.toString())),
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                        onPressed: () {
                          setState(() {
                            EtellerWithdraw = true;
                          });
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => QRScanPage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor: loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor: loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor: dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("qr_scanner"),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Center(
                              child: Icon(
                                Icons.qr_code_scanner,
                                size: 40,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        )),
                  ),
                ),
              ),*/
            ],
          ),
        ),
      ),
    );
  }
}
