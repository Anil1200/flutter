import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_zxing/flutter_zxing.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/banktransfer/banktransfer_eprabhu_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/banktransfer/banktransfer_esewa_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/banktransfer/banktransfer_khalti_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/scanpay_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/loadwallet/esewa/loadesewa_page.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:sahakari_pay/pmc/utils/qrscan_overlay.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/fonepay/phone_scanpay.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/fundtransferV2/fundtransferV2_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/qrfundtransfer/moneytransfer_page.dart';

class QRScanPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  QRScanPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _QRScanPageState createState() => _QRScanPageState(
    coopList,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
  );
}

class _QRScanPageState extends State<QRScanPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _QRScanPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      );

  String? accountName;
  Uint8List? qrCodeData;

  @override
  void initState() {
    super.initState();
  }

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
              coopList: coopList,
              userId: userId,
              accesstoken: accesstoken,
              baseUrl: baseUrl,
              accountno: accountno,
              primaryColor: primaryColor,
              loginButtonTitleColor: loginButtonTitleColor,
              loginbuttonColor: loginbuttonColor,
              loginTextFieldColor: loginTextFieldColor,
              dasboardIconColor: dasboardIconColor,
              dashboardTopTitleColor: dashboardTopTitleColor,
              SecondaryColor: SecondaryColor,
            )),
            (Route<dynamic> route) => false);
  }

  MobileScannerController controller = MobileScannerController(
    facing: CameraFacing.back,
    detectionSpeed: DetectionSpeed.noDuplicates,
    //detectionTimeoutMs: 1000,
    torchEnabled: false,
    formats: [BarcodeFormat.qrCode],
  );

  String? result;

  String? toaccountno;
  String? toaccountName;

  bool validQRCode = true;

  @override
  Widget build(BuildContext context) {
    final scanWindow = Rect.fromCenter(
      center: MediaQuery.of(context).size.center(Offset.zero),
      width: MediaQuery.of(context).size.width / 1.25,
      height: MediaQuery.of(context).size.width / 1.25,
    );
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Stack(
          children: [
            MobileScanner(
              controller: controller,
              fit: BoxFit.cover,
              scanWindow: scanWindow,
              onDetect: (BarcodeCapture barcode) {
                print("This is scanned qrcode : ${barcode.barcodes.first.rawValue}");

                controller.dispose();

                result = barcode.barcodes.first.rawValue;

                if (result == null) {
                  setState(() {
                    validQRCode = false;
                  });
                } else if (!result!.contains("#PES#") && !result!.contains("#IFT#") && !result!.contains("fonepay") && !result!.contains("bankCode") && !result!.contains("eSewa_id")) {
                  setState(() {
                    validQRCode = false;
                  });
                }

                if (EtellerDeposit!) {
                  if (!result!.contains("#PES#")) {
                    setState(() {
                      validQRCode = false;
                    });
                  }
                }

                if (validQRCode) {
                  if (result!.contains("#PES#")) {
                    toaccountName = result?.split("#PES#")[1];
                    toaccountno = result?.split("#PES#")[0];
                    setState(() {
                      ScannedRemarks = "QR Cash Withdrawal";
                    });
                  } else if (result!.contains("#IFT#")) {
                    toaccountName = result?.split("#IFT#")[1].split('","').first;
                    toaccountno = result?.split("#IFT#")[0].split('":"').last;
                    setState(() {
                      ScannedRemarks = "Internal Fund Transfer";
                    });
                  } else if (result!.contains("bankCode")) {
                    toaccountName = "";
                    toaccountno = "";
                    setState(() {
                      var jsonResponse = json.decode(result!);
                      ScannedBankDeposit = true;
                      ScannedBankCode = jsonResponse["bankCode"];
                      ScannedBankAccountNumber = jsonResponse["accountNumber"];
                      ScannedBankAccountName = jsonResponse["accountName"];
                    });
                  } else if (result!.contains("eSewa_id")) {
                    toaccountName = "";
                    toaccountno = "";
                    setState(() {
                      var jsonResponse = json.decode(result!);
                      ScannedEsewaLoad = true;
                      ScannedEsewaId = jsonResponse["eSewa_id"];
                    });
                  } else {
                    toaccountName = "";
                    toaccountno = "";
                  }
                  setState(() {
                    isQRTransaction = true;

                    if (toaccountName != "" && toaccountno != "") {
                      if (EtellerDeposit!) {
                        EtellerDepositFromAccount = toaccountno;
                        print("eteller deposit $EtellerDepositAccount");
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FundTransferV2Page(
                                  coopList: coopList,
                                  userId: userId,
                                  accesstoken: accesstoken,
                                  balance: 0,
                                  baseUrl: baseUrl,
                                  accountno: accountno,
                                  primaryColor: primaryColor,
                                  loginButtonTitleColor: loginButtonTitleColor,
                                  loginbuttonColor: loginbuttonColor,
                                  loginTextFieldColor: loginTextFieldColor,
                                  dasboardIconColor: dasboardIconColor,
                                  dashboardTopTitleColor: dashboardTopTitleColor,
                                  SecondaryColor: SecondaryColor,
                                  AccountNo: EtellerDepositAccount,
                                )));
                      } else {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    MoneyTransferPage(
                                      coopList: coopList,
                                      userId: userId,
                                      accesstoken: accesstoken,
                                      baseUrl: baseUrl,
                                      accountno: accountno,
                                      toaccountno: toaccountno,
                                      toaccountName: toaccountName,
                                      balance: balance,
                                      accountName: accountName,
                                      primaryColor: primaryColor,
                                      loginButtonTitleColor: loginButtonTitleColor,
                                      loginbuttonColor: loginbuttonColor,
                                      loginTextFieldColor: loginTextFieldColor,
                                      dasboardIconColor: dasboardIconColor,
                                      dashboardTopTitleColor: dashboardTopTitleColor,
                                      SecondaryColor: SecondaryColor,
                                    )));
                      }
                    } else {
                      if (!EtellerWithdraw! && !EtellerDeposit!) {
                        if (ScannedBankDeposit!) {
                          if (AllowBankTransfer == true) {
                            if (banktransferType?.toLowerCase() ==
                                "Khalti".toLowerCase()) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          KhaltiBankTransferPage(
                                            coopList: coopList,
                                            userId: userId,
                                            accesstoken:
                                            accesstoken,
                                            balance: balance,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            // BankList: BankList,
                                            primaryColor:
                                            primaryColor,
                                            loginButtonTitleColor:
                                            loginButtonTitleColor,
                                            loginbuttonColor:
                                            loginbuttonColor,
                                            loginTextFieldColor:
                                            loginTextFieldColor,
                                            dasboardIconColor:
                                            dasboardIconColor,
                                            dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                            SecondaryColor:
                                            SecondaryColor,
                                          )));
                            } else if (banktransferType
                                ?.toLowerCase() ==
                                "Eprabhu".toLowerCase()) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          EprabhuBankTransferPage(
                                            coopList: coopList,
                                            userId: userId,
                                            accesstoken:
                                            accesstoken,
                                            balance: balance,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            // BankList: BankList,
                                            primaryColor:
                                            primaryColor,
                                            loginButtonTitleColor:
                                            loginButtonTitleColor,
                                            loginbuttonColor:
                                            loginbuttonColor,
                                            loginTextFieldColor:
                                            loginTextFieldColor,
                                            dasboardIconColor:
                                            dasboardIconColor,
                                            dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                            SecondaryColor:
                                            SecondaryColor,
                                          )));
                            } else if (banktransferType
                                ?.toLowerCase() ==
                                "Esewa".toLowerCase()) {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          EsewaBankTransferPage(
                                            coopList: coopList,
                                            userId: userId,
                                            accesstoken:
                                            accesstoken,
                                            balance: balance,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            // BankList: BankList,
                                            primaryColor:
                                            primaryColor,
                                            loginButtonTitleColor:
                                            loginButtonTitleColor,
                                            loginbuttonColor:
                                            loginbuttonColor,
                                            loginTextFieldColor:
                                            loginTextFieldColor,
                                            dasboardIconColor:
                                            dasboardIconColor,
                                            dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                            SecondaryColor:
                                            SecondaryColor,
                                          )));
                            }
                          } else {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString(
                                      "service_not_available")),
                            ));
                          }
                        } else if (ScannedEsewaLoad!) {
                          if (AllowLoadEsewa == true) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        LoadEsewaPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                          loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                          loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                          dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          } else {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(
                              duration: Duration(milliseconds: 500),
                              content: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString(
                                      "service_not_available")),
                            ));
                            movetohome();
                          }
                      } else {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      PhoneScanPayPage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        balance: balance,
                                        result: result,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor: loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor: loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor: dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        }
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                              duration: Duration(seconds: 2),
                              content: Text("Invalid QR Code."),
                            ));

                        setState(() {
                          EtellerWithdraw = false;
                          EtellerDeposit = false;
                          EtellerDepositFromAccount = null;
                          EtellerDepositAccount = null;

                          ScannedBankDeposit = false;
                          ScannedBankCode = null;
                          ScannedBankAccountNumber = null;
                          ScannedBankAccountName = null;
                        });

                        movetohome();
                      }
                    }
                  });
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    duration: Duration(seconds: 2),
                    content: Text("Invalid QR Code."),
                  ));

                  setState(() {
                    EtellerWithdraw = false;
                    EtellerDeposit = false;
                    EtellerDepositFromAccount = null;
                    EtellerDepositAccount = null;

                    ScannedBankDeposit = false;
                    ScannedBankCode = null;
                    ScannedBankAccountNumber = null;
                    ScannedBankAccountName = null;
                  });

                  movetohome();
                }
              },
            ),
            Container(
              decoration: ShapeDecoration(
                shape: OverlayShape(
                  borderRadius: 0,
                  borderColor: Colors.white,
                  borderLength: 15,
                  borderWidth: 5,
                  cutOutWidth: scanWindow.width, // MediaQuery.of(context).size.height / 2,
                  cutOutHeight: scanWindow.height,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                alignment: Alignment.bottomCenter,
                height: 40,
                color: Colors.black.withOpacity(0.4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      color: Colors.white,
                      icon: ValueListenableBuilder(
                        valueListenable: controller.torchState,
                        builder: (context, state, child) {
                          if (state == null) {
                            return const Icon(
                              Icons.flash_off,
                              color: Colors.grey,
                            );
                          }
                          switch (state as TorchState) {
                            case TorchState.off:
                              return const Icon(
                                Icons.flash_off,
                                color: Colors.grey,
                              );
                            case TorchState.on:
                              return const Icon(
                                Icons.flash_on,
                                color: Colors.yellow,
                              );
                          }
                        },
                      ),
                      iconSize: 24.0,
                      onPressed: () => controller.toggleTorch(),
                    ),
                    IconButton(
                      color: Colors.white,
                      icon: ValueListenableBuilder(
                        valueListenable: controller.cameraFacingState,
                        builder: (context, state, child) {
                          if (state == null) {
                            return const Icon(Icons.camera_front);
                          }
                          switch (state as CameraFacing) {
                            case CameraFacing.front:
                              return const Icon(Icons.camera_front);
                            case CameraFacing.back:
                              return const Icon(Icons.camera_rear);
                          }
                        },
                      ),
                      iconSize: 24.0,
                      onPressed: () => controller.switchCamera(),
                    ),
                    IconButton(
                      color: Colors.white,
                      icon: const Icon(Icons.image),
                      iconSize: 24.0,
                      onPressed: () async {
                        ImagePicker picker = ImagePicker();

                        XFile? xFile = await picker.pickImage(source: ImageSource.gallery);

                        Code? resultFromXFile = await zx.readBarcodeImagePath(xFile!);

                        print("This is file qrcode : ${resultFromXFile!.text}");

                        result = resultFromXFile.text;

                        print("This is file qrcode result : ${result}");

                        if (result == null) {
                          setState(() {
                            validQRCode = false;
                          });
                        } else if (!result!.contains("#PES#") && !result!.contains("#IFT#") && !result!.contains("fonepay") && !result!.contains("bankCode") && !result!.contains("eSewa_id")) {
                          setState(() {
                            validQRCode = false;
                          });
                        }

                        if (EtellerDeposit!) {
                          if (!result!.contains("#PES#")) {
                            setState(() {
                              validQRCode = false;
                            });
                          }
                        }

                        if (validQRCode) {
                          if (result!.contains("#PES#")) {
                            toaccountName = result?.split("#PES#")[1];
                            toaccountno = result?.split("#PES#")[0];
                            setState(() {
                              ScannedRemarks = "QR Cash Withdrawal";
                            });
                          } else if (result!.contains("#IFT#")) {
                            toaccountName = result?.split("#IFT#")[1].split('","').first;
                            toaccountno = result?.split("#IFT#")[0].split('":"').last;
                            setState(() {
                              ScannedRemarks = "Internal Fund Transfer";
                            });
                          } else if (result!.contains("bankCode")) {
                            toaccountName = "";
                            toaccountno = "";
                            setState(() {
                              var jsonResponse = json.decode(result!);
                              ScannedBankDeposit = true;
                              ScannedBankCode = jsonResponse["bankCode"];
                              ScannedBankAccountNumber = jsonResponse["accountNumber"];
                              ScannedBankAccountName = jsonResponse["accountName"];
                            });
                          } else if (result!.contains("eSewa_id")) {
                            toaccountName = "";
                            toaccountno = "";
                            setState(() {
                              var jsonResponse = json.decode(result!);
                              ScannedEsewaLoad = true;
                              ScannedEsewaId = jsonResponse["eSewa_id"];
                            });
                          } else {
                            toaccountName = "";
                            toaccountno = "";
                          }
                          setState(() {
                            isQRTransaction = true;

                            if (toaccountName != "" && toaccountno != "") {
                              if (EtellerDeposit!) {
                                EtellerDepositFromAccount = toaccountno;
                                print("eteller deposit $EtellerDepositAccount");
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => FundTransferV2Page(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: 0,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor: loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor: loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor: dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                          AccountNo: EtellerDepositAccount,
                                        )));
                              } else {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            MoneyTransferPage(
                                              coopList: coopList,
                                              userId: userId,
                                              accesstoken: accesstoken,
                                              baseUrl: baseUrl,
                                              accountno: accountno,
                                              toaccountno: toaccountno,
                                              toaccountName: toaccountName,
                                              balance: balance,
                                              accountName: accountName,
                                              primaryColor: primaryColor,
                                              loginButtonTitleColor: loginButtonTitleColor,
                                              loginbuttonColor: loginbuttonColor,
                                              loginTextFieldColor: loginTextFieldColor,
                                              dasboardIconColor: dasboardIconColor,
                                              dashboardTopTitleColor: dashboardTopTitleColor,
                                              SecondaryColor: SecondaryColor,
                                            )));
                              }
                            } else {
                              if(!EtellerWithdraw! && !EtellerDeposit!) {
                                if (ScannedBankDeposit!) {
                                  if (AllowBankTransfer == true) {
                                    if (banktransferType?.toLowerCase() ==
                                        "Khalti".toLowerCase()) {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  KhaltiBankTransferPage(
                                                    coopList: coopList,
                                                    userId: userId,
                                                    accesstoken:
                                                    accesstoken,
                                                    balance: balance,
                                                    baseUrl: baseUrl,
                                                    accountno: accountno,
                                                    // BankList: BankList,
                                                    primaryColor:
                                                    primaryColor,
                                                    loginButtonTitleColor:
                                                    loginButtonTitleColor,
                                                    loginbuttonColor:
                                                    loginbuttonColor,
                                                    loginTextFieldColor:
                                                    loginTextFieldColor,
                                                    dasboardIconColor:
                                                    dasboardIconColor,
                                                    dashboardTopTitleColor:
                                                    dashboardTopTitleColor,
                                                    SecondaryColor:
                                                    SecondaryColor,
                                                  )));
                                    } else if (banktransferType
                                        ?.toLowerCase() ==
                                        "Eprabhu".toLowerCase()) {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EprabhuBankTransferPage(
                                                    coopList: coopList,
                                                    userId: userId,
                                                    accesstoken:
                                                    accesstoken,
                                                    balance: balance,
                                                    baseUrl: baseUrl,
                                                    accountno: accountno,
                                                    // BankList: BankList,
                                                    primaryColor:
                                                    primaryColor,
                                                    loginButtonTitleColor:
                                                    loginButtonTitleColor,
                                                    loginbuttonColor:
                                                    loginbuttonColor,
                                                    loginTextFieldColor:
                                                    loginTextFieldColor,
                                                    dasboardIconColor:
                                                    dasboardIconColor,
                                                    dashboardTopTitleColor:
                                                    dashboardTopTitleColor,
                                                    SecondaryColor:
                                                    SecondaryColor,
                                                  )));
                                    } else if (banktransferType
                                        ?.toLowerCase() ==
                                        "Esewa".toLowerCase()) {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EsewaBankTransferPage(
                                                    coopList: coopList,
                                                    userId: userId,
                                                    accesstoken:
                                                    accesstoken,
                                                    balance: balance,
                                                    baseUrl: baseUrl,
                                                    accountno: accountno,
                                                    // BankList: BankList,
                                                    primaryColor:
                                                    primaryColor,
                                                    loginButtonTitleColor:
                                                    loginButtonTitleColor,
                                                    loginbuttonColor:
                                                    loginbuttonColor,
                                                    loginTextFieldColor:
                                                    loginTextFieldColor,
                                                    dasboardIconColor:
                                                    dasboardIconColor,
                                                    dashboardTopTitleColor:
                                                    dashboardTopTitleColor,
                                                    SecondaryColor:
                                                    SecondaryColor,
                                                  )));
                                    }
                                  } else {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(SnackBar(
                                      duration: Duration(milliseconds: 500),
                                      content: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString(
                                              "service_not_available")),
                                    ));
                                  }
                                } else if (ScannedEsewaLoad!) {
                                  if (AllowLoadEsewa == true) {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                LoadEsewaPage(
                                                  coopList: coopList,
                                                  userId: userId,
                                                  accesstoken: accesstoken,
                                                  balance: balance,
                                                  baseUrl: baseUrl,
                                                  accountno: accountno,
                                                  primaryColor: primaryColor,
                                                  loginButtonTitleColor:
                                                  loginButtonTitleColor,
                                                  loginbuttonColor: loginbuttonColor,
                                                  loginTextFieldColor:
                                                  loginTextFieldColor,
                                                  dasboardIconColor: dasboardIconColor,
                                                  dashboardTopTitleColor:
                                                  dashboardTopTitleColor,
                                                  SecondaryColor: SecondaryColor,
                                                )));
                                  } else {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(SnackBar(
                                      duration: Duration(milliseconds: 500),
                                      content: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString(
                                              "service_not_available")),
                                    ));
                                    movetohome();
                                  }
                                } else {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              PhoneScanPayPage(
                                                coopList: coopList,
                                                userId: userId,
                                                accesstoken: accesstoken,
                                                baseUrl: baseUrl,
                                                accountno: accountno,
                                                balance: balance,
                                                result: result,
                                                primaryColor: primaryColor,
                                                loginButtonTitleColor: loginButtonTitleColor,
                                                loginbuttonColor: loginbuttonColor,
                                                loginTextFieldColor: loginTextFieldColor,
                                                dasboardIconColor: dasboardIconColor,
                                                dashboardTopTitleColor: dashboardTopTitleColor,
                                                SecondaryColor: SecondaryColor,
                                              )));
                                }
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      duration: Duration(seconds: 2),
                                      content: Text("Invalid QR Code."),
                                    ));

                                setState(() {
                                  EtellerWithdraw = false;
                                  EtellerDeposit = false;
                                  EtellerDepositFromAccount = null;
                                  EtellerDepositAccount = null;

                                  ScannedBankDeposit = false;
                                  ScannedBankCode = null;
                                  ScannedBankAccountNumber = null;
                                  ScannedBankAccountName = null;
                                });

                                movetohome();
                              }
                            }
                          });
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            duration: Duration(seconds: 2),
                            content: Text("Invalid QR Code."),
                          ));

                          setState(() {
                            EtellerWithdraw = false;
                            EtellerDeposit = false;
                            EtellerDepositFromAccount = null;
                            EtellerDepositAccount = null;

                            ScannedBankDeposit = false;
                            ScannedBankCode = null;
                            ScannedBankAccountNumber = null;
                            ScannedBankAccountName = null;
                          });

                          movetohome();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: 30,
              right: 5,
              child: ClipOval(
                child: Material(
                  color: Colors.black26,
                  child: InkWell(
                    onTap: () {
                      movetohome();
                    },
                    child: const SizedBox(
                      width: 56,
                      height: 56,
                      child: Icon(
                        Icons.close,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 50,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/spay.png',
                        height: 125,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height / 2 + 220,
              left: 0,
              right: 0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      onPressed: () {
                        checkScan = false;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScanPayPage(
                                  coopList: coopList,
                                  userId: userId,
                                  accesstoken: accesstoken,
                                  balance: balance,
                                  baseUrl: baseUrl,
                                  accountno: accountno,
                                  primaryColor: primaryColor,
                                  loginButtonTitleColor: loginButtonTitleColor,
                                  loginbuttonColor: loginbuttonColor,
                                  loginTextFieldColor: loginTextFieldColor,
                                  dasboardIconColor: dasboardIconColor,
                                  dashboardTopTitleColor: dashboardTopTitleColor,
                                  SecondaryColor: SecondaryColor,
                                )));
                      },
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        backgroundColor: Colors.white
                      ),
                      child: Row(
                        children: [
                          Icon(
                            Icons.qr_code,
                            color: Color(int.parse(loginbuttonColor.toString())),
                            size: 24,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "SHOW MY QR CODE",
                            style: TextStyle(
                              color: Color(int.parse(loginbuttonColor.toString())),
                              fontSize: 14,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              top: MediaQuery.of(context).size.height / 2 + 170,
              left: 10,
              right: 10,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/eteller.png',
                        width: 75,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Image.asset(
                        'assets/images/fonepay2.png',
                        width: 75,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}