import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/scanpay_page.dart';

import '../../utils/localizations.dart';
import '../models/loading.dart';

class MoneyTransferPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? toaccountno;
  String? toaccountName;
  double? balance;
  String? accountName;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  MoneyTransferPage({
    Key? key,
    this.coopList,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.toaccountno,
    this.toaccountName,
    this.balance,
    this.accountName,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _MoneyTransferPageState createState() => _MoneyTransferPageState(
        coopList,
        accesstoken,
        baseUrl,
        accountno,
        toaccountno,
        toaccountName,
        balance,
        accountName,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _MoneyTransferPageState extends State<MoneyTransferPage> {
  List? coopList;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? toaccountno;
  String? toaccountName;
  double? balance;
  String? accountName;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _MoneyTransferPageState(
    this.coopList,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.toaccountno,
    this.toaccountName,
    this.balance,
    this.accountName,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();
  final TextEditingController amountController = TextEditingController();
  final TextEditingController statementController = TextEditingController();

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Center(
                child: Text(
              AppLocalizations.of(context)!.localizedString("alert"),
              style: TextStyle(color: Colors.black87),
            )),
            content: Container(
              height: 200,
              width: 350,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${amountController.text} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${toaccountno.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style: const TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("confirm_pin"),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.black87),
                    ),
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("enter_transaction_pin"),
                      style: TextStyle(fontSize: 14, color: Colors.black87),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Form(
                    key: globalFormKey2,
                    child: TextFormField(
                      controller: mpincontroller,
                      maxLength: 4,
                      keyboardType: TextInputType.number,
                      obscureText: true,
                      validator: (value) => value!.isEmpty
                          ? AppLocalizations.of(context)!
                              .localizedString("enter_transaction_pin")
                          : null,
                      onSaved: (value) => mpincontroller,
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: AppLocalizations.of(context)!
                            .localizedString("transaction_pin"),
                        labelStyle: TextStyle(color: Colors.green),
                        counterText: "",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(30)),
                    child: TextButton(
                        onPressed: () {
                          setState(() {
                            _isLoading = false;
                          });
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.white),
                        )),
                  ),
                  const Spacer(),
                  Container(
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(30)),
                    child: TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            postTransferData();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.black87),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                      style: TextStyle(color: Colors.black87),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      Container(
                                        height: 40,
                                        width: 80,
                                        decoration: BoxDecoration(
                                            color: Colors.green,
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("okay"),
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.white),
                        )),
                  ),
                ],
              )
            ],
          );
        });
  }

  postTransferData() async {
    String url = "${baseUrl}api/v1/pes/account/fund-transfer";
    Map body = {
      "FromAccount": accountno.toString(),
      "ToAccount": toaccountno.toString(),
      "Amount": amountController.text.toString(),
      "Statement": statementController.text.toString(),
      "MPIN": mpincontroller.text,
    };
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("success"),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Icon(
                            Icons.verified,
                            color: Colors.green,
                            size: 50,
                          )
                        ],
                      ),
                    ),
                    content: Text("${jsonResponse["Message"]}"),
                    actions: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Spacer(),
                          Container(
                            height: 40,
                            width: 80,
                            decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius: BorderRadius.circular(30)),
                            child: TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              MoneyTransferPage(
                                                coopList: coopList,
                                                accesstoken: accesstoken,
                                                baseUrl: baseUrl,
                                                accountno: accountno,
                                                toaccountno: toaccountno,
                                                toaccountName: toaccountName,
                                                balance: balance,
                                                accountName: accountName,
                                                primaryColor: primaryColor,
                                                loginButtonTitleColor:
                                                    loginButtonTitleColor,
                                                loginbuttonColor:
                                                    loginbuttonColor,
                                                loginTextFieldColor:
                                                    loginTextFieldColor,
                                                dasboardIconColor:
                                                    dasboardIconColor,
                                                dashboardTopTitleColor:
                                                    dashboardTopTitleColor,
                                                SecondaryColor: SecondaryColor,
                                              )));
                                },
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("okay"),
                                  style: TextStyle(color: Colors.white),
                                )),
                          ),
                        ],
                      )
                    ],
                  );
                });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Center(
                  child: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              )),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  movetoScanPayPage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ScanPayPage(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoScanPayPage();
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 80.0, left: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ScanPayPage(
                                      coopList: coopList,
                                      accesstoken: accesstoken,
                                      baseUrl: baseUrl,
                                      accountno: accountno,
                                      primaryColor: primaryColor,
                                      loginButtonTitleColor:
                                          loginButtonTitleColor,
                                      loginbuttonColor: loginbuttonColor,
                                      loginTextFieldColor: loginTextFieldColor,
                                      dasboardIconColor: dasboardIconColor,
                                      dashboardTopTitleColor:
                                          dashboardTopTitleColor,
                                      SecondaryColor: SecondaryColor,
                                    )));
                      },
                      icon: const Icon(
                        Icons.arrow_back,
                        size: 30,
                        color: Colors.green,
                      ),
                    ),
                    const SizedBox(
                      width: 50,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("money_transfer_details"),
                        style: TextStyle(
                            color: Colors.green[900],
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              _isLoading
                  ? Center(
                      child: Column(
                      children: const [
                        SizedBox(
                          height: 40,
                        ),
                        Loading(),
                      ],
                    ))
                  : Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("receiver_name"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 50,
                          ),
                          Text("${toaccountName}"),
                        ],
                      ),
                    ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0, top: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("receiver_account"),
                      style: TextStyle(color: Colors.black87, fontSize: 14),
                    ),
                    const SizedBox(
                      width: 50,
                    ),
                    Text("${toaccountno}"),
                  ],
                ),
              ),
              Form(
                key: globalFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 30.0),
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("transfer_amount"),
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Container(
                        height: 60,
                        width: 320,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: TextFormField(
                          controller: amountController,
                          maxLength: 30,
                          keyboardType: TextInputType.number,
                          validator: (value) => value!.isEmpty
                              ? AppLocalizations.of(context)!
                                  .localizedString("amount")
                              : null,
                          onSaved: (value) => amountController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            labelText: AppLocalizations.of(context)!
                                .localizedString("amount"),
                            labelStyle: const TextStyle(color: Colors.green),
                            counterText: "",
                            icon: const Icon(
                              Icons.money,
                              color: Colors.green,
                              size: 20.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 30.0),
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("statement"),
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Container(
                        height: 60,
                        width: 320,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: TextFormField(
                          controller: statementController,
                          maxLength: 30,
                          keyboardType: TextInputType.text,
                          validator: (value) => value!.isEmpty
                              ? AppLocalizations.of(context)!
                                  .localizedString("amount")
                              : null,
                          onSaved: (value) => statementController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            labelText: AppLocalizations.of(context)!
                                .localizedString("statement"),
                            labelStyle: const TextStyle(color: Colors.green),
                            counterText: "",
                            icon: const Icon(
                              Icons.content_copy,
                              color: Colors.green,
                              size: 20.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 280.0, top: 40),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.green,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ]),
                  height: 50,
                  width: 100,
                  child: TextButton(
                    onPressed: () {
                      if (validateAndSave()) {
                        setState(() {
                          _isLoading = true;
                          transactionPinMessage();
                        });
                        // showMessage();
                      } else {
                        setState(() {
                          _isLoading = false;
                          // showDialog(
                          //     context: context,
                          //     builder: (BuildContext context) {
                          //       return AlertDialog(
                          //         title: Center(
                          //           child: Column(
                          //             children: [
                          //               Text(AppLocalizations.of(context)!.localizedString("alert"),),
                          //               SizedBox(
                          //                 height: 10,
                          //               ),
                          //               Icon(
                          //                 Icons.add_alert,
                          //                 color: Colors.red,
                          //                 size: 50,
                          //               )
                          //             ],
                          //           ),
                          //         ),
                          //         content: Text(
                          //             "AppLocalizations.of(context)!.localizedString("valid_mobile"),),
                          //         actions: [
                          //           const Spacer(),
                          //           Container(
                          //             height: 40,
                          //             width: 80,
                          //             decoration: BoxDecoration(
                          //                 color: Colors.green,
                          //                 borderRadius:
                          //                     BorderRadius
                          //                         .circular(30)),
                          //             child: TextButton(
                          //               onPressed: () {
                          //                 Navigator.of(context)
                          //                     .pop();
                          //               },
                          //               child: Text(
                          //                 AppLocalizations.of(context)!.localizedString("okay"),
                          //                 style: TextStyle(
                          //                     color:
                          //                         Colors.white),
                          //               ),
                          //             ),
                          //           )
                          //         ],
                          //       );
                          //     });
                        });
                      }
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("submit"),
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }
}
