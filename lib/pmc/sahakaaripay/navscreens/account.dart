import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';

import '../../utils/localizations.dart';
import '../models/loading.dart';

class AccountPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  AccountPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<AccountPage> createState() => _AccountPageState(
        coopList,
        userId,
        accesstoken,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _AccountPageState extends State<AccountPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _AccountPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  double? balance;
<<<<<<< HEAD
=======
  double? minimumbalance;
  double? actualbalance;
>>>>>>> sudeep
  double? interestRate;
  // String? BranchCode;
  String? accountNumber;
  // String? clientCode;
  String? product;
  String? accountName;
  // String? accountType;
  // String? CardNumber;

  @override
  void initState() {
    _isLoading = true;
    super.initState();
    this.getJsonData();
    // this.AccountDetailsData();
  }

  bool _isLoading = false;

  Future<String> getJsonData() async {
    String url = "${baseUrl}api/v1/pes/account/${accountno}/info";
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // Map authorization = {
    //   "Token":
    //       "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI3NWNiMGU4Yy1hZjZlLWVjMTEtODEzNS0wMDUwNTY4OWRlYjkiLCJlbWFpbElkIjoidGVzdEB0ZXN0LmNvbSIsImFjY291bnRObyI6IlNTLTAwMDAwMDYiLCJtUElOIjoiYTZhYzg2MDA0MDZhODViNDIwNTE1MGQ2YWRiZDljYjQiLCJpc1JlZnJlc2hUb2tlbiI6ZmFsc2UsImV4cCI6MTY0NTAzNTI3M30.Tvd0PcwlDyqxDAdCNnwXzrkJtSDEF0fTRx0dAYKjOBY"
    // };
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        var convertDataToJson = json.decode(response.body);
<<<<<<< HEAD
=======
        minimumbalance =
            double.parse(convertDataToJson["MinimumBalance"].toString());
        actualbalance =
            double.parse(convertDataToJson["ActualBalance"].toString());
>>>>>>> sudeep
        balance = double.parse(convertDataToJson["Balance"].toString());
        interestRate =
            double.parse(convertDataToJson["InterestRate"].toString());
        accountName = convertDataToJson["AccountName"];
        accountNumber = convertDataToJson["AccountNumber"];
        product = convertDataToJson["Product"];

        print(convertDataToJson);
        // sharedPreferences.setStringList(
        //     'Cooperatives', accountinfo!.map((e) => e.toString()).toList());
      });
      return "success";
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
    return "success";
  }

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.localizedString("my_account"),
          ),
          leading: IconButton(
            onPressed: () {
              movetohome();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
        ),
        body: _isLoading
            ? Center(child: Loading())
            : Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.account_circle_rounded,
                                  color: Colors.green[900],
                                  size: 20,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("account_number"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text(
                              "${accountNumber}",
                              style: const TextStyle(
                                color: Colors.black87,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Row(
                              children: [
                                Icon(
                                  Icons.account_box,
                                  color: Colors.green[900],
                                  size: 20,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("account_name"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text(
                              "${accountName.toString()}",
                              style: const TextStyle(
                                color: Colors.black87,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Row(
                              children: [
                                Icon(
                                  Icons.switch_account,
                                  color: Colors.green[900],
                                  size: 20,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("product_type"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text(
                              "${product}",
                              style: const TextStyle(
                                color: Colors.black87,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 20.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Row(
                              children: [
                                Icon(
                                  Icons.account_box,
                                  color: Colors.green[900],
                                  size: 20,
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("interest_rate"),
                                  style: const TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text(
                              "${interestRate.toString()}",
                              style: const TextStyle(
                                color: Colors.black87,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 20.0),
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2.5,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.account_balance_wallet_outlined,
                                    color: Colors.green[900],
                                    size: 20,
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("npr_balance"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.5,
                              child: Text(
                                AppLocalizations.of(context)!
                                        .localizedString("rs") +
                                    "${balance.toString()}",
                                style: const TextStyle(
                                  color: Colors.black87,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
<<<<<<< HEAD
=======
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 20.0),
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2.5,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.account_balance_wallet_outlined,
                                    color: Colors.green[900],
                                    size: 20,
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("minimum_balance"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.5,
                              child: Text(
                                AppLocalizations.of(context)!
                                        .localizedString("rs") +
                                    "${minimumbalance.toString()}",
                                style: const TextStyle(
                                  color: Colors.black87,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, top: 20.0),
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2.5,
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.account_balance_wallet_outlined,
                                    color: Colors.green[900],
                                    size: 20,
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("actual_balance"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.5,
                              child: Text(
                                AppLocalizations.of(context)!
                                        .localizedString("rs") +
                                    "${actualbalance.toString()}",
                                style: const TextStyle(
                                  color: Colors.black87,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
>>>>>>> sudeep
                  ],
                ),
              ),
      ),
    );
  }
}
