import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/notifications/notifications.dart';

import '../../../utils/localizations.dart';

class NotificationDetailsPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? NotificationTitle;
  String? NotificationMessage;
  String? CreatedDate;
  // double? amount;
  String? statement;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  // List? ministatement;

  NotificationDetailsPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.NotificationTitle,
    this.NotificationMessage,
    this.CreatedDate,
    // this.amount,
    this.statement,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _NotificationDetailsPageState createState() => _NotificationDetailsPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        NotificationTitle,
        NotificationMessage,
        CreatedDate,
        statement,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _NotificationDetailsPageState extends State<NotificationDetailsPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? NotificationTitle;
  String? NotificationMessage;
  String? CreatedDate;
  // double? amount;
  String? statement;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  // List? ministatement;
  _NotificationDetailsPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.NotificationTitle,
    this.NotificationMessage,
    this.CreatedDate,
    this.statement,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  // bool _isLoading = false;
  bool showBalance = false;

  movetoNotificationsPage() {
    Navigator.pop(
        context,
        MaterialPageRoute(
            builder: (context) => NotificationsPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoNotificationsPage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
            onPressed: () {
              return movetoNotificationsPage();
            },
          ),
          title: Text(
            AppLocalizations.of(context)!
                .localizedString("noticication_details"),
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 20),
                child: SingleChildScrollView(
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2.3,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("notification_title"),
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.3,
                              child: Text(
                                NotificationTitle.toString(),
                                style: const TextStyle(
                                  color: Colors.black87,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 20.0,
                          ),
                          child: Row(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 2.3,
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("notification_message"),
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.3,
                                child: Text(
                                  NotificationMessage.toString(),
                                  style: const TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 20.0,
                          ),
                          child: Row(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 2.3,
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("date"),
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.3,
                                child: Text(
                                  CreatedDate.toString(),
                                  style: const TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.only(
                        //     top: 20.0,
                        //   ),
                        //   child: Row(
                        //     children: [
                        //       Container(
                        //         width: MediaQuery.of(context).size.width / 2.3,
                        //         child: Text(
                        //           AppLocalizations.of(context)!
                        //               .localizedString("amount"),
                        //           style: TextStyle(
                        //               color: Colors.black87,
                        //               fontSize: 16,
                        //               fontWeight: FontWeight.bold),
                        //         ),
                        //       ),
                        //       Container(
                        //         width: MediaQuery.of(context).size.width / 2.3,
                        //         child: Text(
                        //           amount.toString(),
                        //           style: const TextStyle(
                        //             color: Colors.black87,
                        //             fontSize: 14,
                        //           ),
                        //         ),
                        //       ),
                        //     ],
                        //   ),
                        // ),
                        // Padding(
                        //   padding: const EdgeInsets.only(
                        //     top: 20.0,
                        //   ),
                        //   child: Row(
                        //     children: [
                        //       Container(
                        //         width: MediaQuery.of(context).size.width / 2.3,
                        //         child: Text(
                        //           AppLocalizations.of(context)!
                        //               .localizedString("statement"),
                        //           style: TextStyle(
                        //               color: Colors.black87,
                        //               fontSize: 16,
                        //               fontWeight: FontWeight.bold),
                        //         ),
                        //       ),
                        //       Container(
                        //         width: MediaQuery.of(context).size.width / 2.3,
                        //         child: Text(
                        //           statement == null
                        //               ? "No Statement for this Transaction"
                        //               : statement.toString(),
                        //           style: const TextStyle(
                        //             color: Colors.black87,
                        //             fontSize: 14,
                        //           ),
                        //         ),
                        //       ),
                        //     ],
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
