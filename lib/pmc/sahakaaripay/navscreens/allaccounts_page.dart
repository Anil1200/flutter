import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/qrscan_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
<<<<<<< HEAD
<<<<<<< HEAD
=======
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/fundtransferV2/fundtransferV2_page.dart';
=======
>>>>>>> sudeep
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';
>>>>>>> sudeep

import '../../utils/localizations.dart';
import '../services/loan/loaninformation/loaninformation_detailspage.dart';
import '../services/loan/loanschedule/loanschedule_detailspage.dart';
import '../services/loanstatement/loanstatement_page.dart';

class AllAccountsPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  double? balance;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  AllAccountsPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.balance,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _AllAccountsPageState createState() => _AllAccountsPageState(
        coopList,
        userId,
        accesstoken,
        baseUrl,
        accountno,
        balance,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _AllAccountsPageState extends State<AllAccountsPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  String? baseUrl;
  String? accountno;
  double? balance;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _AllAccountsPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.baseUrl,
    this.accountno,
    this.balance,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    _isLoading = true;
    AccountDetailsData();
    print("the account no:${accountno}");
  }

  bool _isLoading = false;
  List? allaccounts;

  //String? balance;
  // double? interestRate;
  String? BranchCode;
  String? accountNumber;
  String? clientCode;
  String? product;
  String? accountName;
  String? accountType;
  String? CardNumber;

  AccountDetailsData() async {
    String url = "${baseUrl}api/v1/pes/users/accountdetails";
    Map body = {
      "AccountNumber": accountno.toString(),
    };
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });
          allaccounts = jsonResponse;
          print("these are all the accounts: ${allaccounts}");
          accountNumber = jsonResponse[0]["AccountNumber"];
          //balance = double.parse(jsonResponse[0]["Balance"]);
          BranchCode = jsonResponse[0]["BranchCode"];
          CardNumber = jsonResponse[0]["CardNumber"];
          accountType = jsonResponse[0]["AccountType"];
          clientCode = jsonResponse[0]["ClientCode"];
          product = jsonResponse[0]["Product"];
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error for all acounts:${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  String? loanMessage;
  String? loanTotalAmount;
  String? loanFine;
  String? loanPenalty;
  String? loanInterest;
  String? loanDuePrincipal;

  List<dynamic>? loanSchedule;

  postLoanInformation(String loanAccountNumber) async {
    String url = "${baseUrl}api/v1/pes/account/get-loan-payment-information";
    Map body = {
      "AccountNumber": loanAccountNumber,
    };
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          loanMessage = jsonResponse["Message"];
          loanTotalAmount = jsonResponse["TotalAmount"].toString();
          loanFine = jsonResponse["Fine"].toString();
          loanPenalty = jsonResponse["Penalty"].toString();
          loanInterest = jsonResponse["Interest"].toString();
          loanDuePrincipal = jsonResponse["DuePrincipal"].toString();

          setState(() {
            _isLoading = false;
          });

          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => LoanInformationDetailsPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    loanMessage: loanMessage,
                    loanTotalAmount: loanTotalAmount,
                    loanFine: loanFine,
                    loanPenalty: loanPenalty,
                    loanInterest: loanInterest,
                    loanDuePrincipal: loanDuePrincipal,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                  )));
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error meesage: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  postLoanSchedule(String loanAccountNumber) async {
    String url = "${baseUrl}api/v1/pes/account/loan-schedule";
    Map body = {
      "AccountNumber": loanAccountNumber,
    };
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          loanSchedule = jsonResponse;

          setState(() {
            _isLoading = false;
          });

          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => LoanScheduleDetailsPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    loanSchedule: loanSchedule,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                  )));
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error meesage: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoHomePage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetoHomePage();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          title: Text("SahakaariPay",
              style: TextStyle(
                color: Colors.white,
              )),
          centerTitle: true,
        ),
        body: _isLoading
            ? Center(child: Loading())
            : ListView.builder(
                itemCount: allaccounts == null ? 0 : allaccounts?.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                      top: 20,
                      left: 20.0,
                      right: 20,
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 20, right: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("total_balance"),
                                  style: TextStyle(
                                    color: Color(int.parse(
                                        dashboardTopTitleColor.toString())),
                                    fontSize: 16.0,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text(
                                    AppLocalizations.of(context)!
                                            .localizedString("rs") +
                                        "${allaccounts?[index]["Balance"].toString()}",
                                    style: TextStyle(
                                      color: Color(int.parse(
                                          dashboardTopTitleColor.toString())),
                                      fontSize: 16.0,
                                    )),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("account_number"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    "${allaccounts?[index]["AccountNumber"]}",
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("branch_code"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    "${allaccounts?[index]["BranchCode"]}",
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("account_type"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    "${allaccounts?[index]["AccountType"].toString()}",
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("client_code"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    "${allaccounts?[index]["ClientCode"].toString()}",
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("product_type"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Text(
                                    "${allaccounts?[index]["Product"].toString()}",
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                    ),
                                  ),
<<<<<<< HEAD
                                ),
                              ],
=======
                                ],
                              ),
                            ),
                            if (allaccounts?[index]["AccountType"].toString() == "Saving") ...[
                              Padding(
                                padding: const EdgeInsets.only(left: 25.0, top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          //borderRadius: BorderRadius.circular(10.0),
                                          color: Color(int.parse(
                                              primaryColor.toString())),
                                          /*boxShadow: [
                                            BoxShadow(
                                              color: Colors.grey.withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: const Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ]*/),
                                      height: 30,
                                      width: 85,
                                      child: TextButton(
                                        onPressed: () async {
                                          setState(() {
                                            EtellerDeposit = true;
                                            EtellerDepositAccount = allaccounts?[index]["AccountNumber"];
                                          });
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => QRScanPage(
                                                    coopList: coopList,
                                                    userId: userId,
                                                    accesstoken: accesstoken,
                                                    balance: balance,
                                                    baseUrl: baseUrl,
                                                    accountno: accountno,
                                                    primaryColor: primaryColor,
                                                    loginButtonTitleColor: loginButtonTitleColor,
                                                    loginbuttonColor: loginbuttonColor,
                                                    loginTextFieldColor: loginTextFieldColor,
                                                    dasboardIconColor: dasboardIconColor,
                                                    dashboardTopTitleColor: dashboardTopTitleColor,
                                                    SecondaryColor: SecondaryColor,
                                                  )));

                                          /*Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => FundTransferV2Page(
                                                    coopList: coopList,
                                                    userId: userId,
                                                    accesstoken: accesstoken,
                                                    balance: balance,
                                                    baseUrl: baseUrl,
                                                    accountno: accountno,
                                                    primaryColor: primaryColor,
                                                    loginButtonTitleColor: loginButtonTitleColor,
                                                    loginbuttonColor: loginbuttonColor,
                                                    loginTextFieldColor: loginTextFieldColor,
                                                    dasboardIconColor: dasboardIconColor,
                                                    dashboardTopTitleColor: dashboardTopTitleColor,
                                                    SecondaryColor: SecondaryColor,
                                                    AccountNo: allaccounts?[index]["AccountNumber"],
                                                  )));*/
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("deposit"),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Icon(
                                              Icons.arrow_forward,
                                              color: Colors.white,
                                              size: 14,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                            if (allaccounts?[index]["AccountType"].toString() == "Loan") ...[
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0, top: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Color(int.parse(
                                            primaryColor.toString())),
                                      ),
                                      height: 30,
                                      width: 100,
                                      child: TextButton(
                                        onPressed: () async {
                                          setState(() {
                                            _isLoading = true;
                                          });
                                          postLoanInformation(allaccounts![index]["AccountNumber"].toString());
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("more_info"),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Icon(
                                              Icons.arrow_forward,
                                              color: Colors.white,
                                              size: 14,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Color(int.parse(
                                            primaryColor.toString())),
                                      ),
                                      height: 30,
                                      width: 100,
                                      child: TextButton(
                                        onPressed: () async {
                                          setState(() {
                                            _isLoading = true;
                                          });
                                          postLoanSchedule(allaccounts![index]["AccountNumber"].toString());
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("loan_schedule"),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Icon(
                                              Icons.arrow_forward,
                                              color: Colors.white,
                                              size: 14,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        color: Color(int.parse(
                                            primaryColor.toString())),
                                      ),
                                      height: 30,
                                      width: 100,
                                      child: TextButton(
                                        onPressed: () async {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => LoanStatementPage(
                                                    coopList: coopList,
                                                    userId: userId,
                                                    accesstoken: accesstoken,
                                                    balance: balance,
                                                    baseUrl: baseUrl,
                                                    accountno: accountno,
                                                    loanaccountno: allaccounts![index]["AccountNumber"].toString(),
                                                    primaryColor: primaryColor,
                                                    loginButtonTitleColor:
                                                    loginButtonTitleColor,
                                                    loginbuttonColor: loginbuttonColor,
                                                    loginTextFieldColor: loginTextFieldColor,
                                                    dasboardIconColor: dasboardIconColor,
                                                    dashboardTopTitleColor:
                                                    dashboardTopTitleColor,
                                                    SecondaryColor: SecondaryColor,
                                                  )));
                                        },
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("statement"),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Icon(
                                              Icons.arrow_forward,
                                              color: Colors.white,
                                              size: 14,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                            SizedBox(
                              height: 10,
>>>>>>> sudeep
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Divider(
                            thickness: 1,
                            color: Colors.green[900],
                          )
                        ],
                      ),
                    ),
                  );
                }),
      ),
    );
  }
}
