// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// // import 'package:barcode_scan2/barcode_scan2.dart';
// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
// import 'package:qr_flutter/qr_flutter.dart';
// import 'package:sahakari_pay/pmc/constants.dart';
//
// import '../../utils/localizations.dart';
// import '../pmc_homepage.dart';
// import 'qrfundtransfer/moneytransfer_page.dart';
//
// class ScanPayPage extends StatefulWidget {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//
//   ScanPayPage({
//     Key? key,
//     this.coopList,
//     this.userId,
//     this.accesstoken,
//     this.balance,
//     this.baseUrl,
//     this.accountno,
//     this.primaryColor,
//     this.loginButtonTitleColor,
//     this.loginbuttonColor,
//     this.loginTextFieldColor,
//     this.dasboardIconColor,
//     this.dashboardTopTitleColor,
//     this.SecondaryColor,
//   }) : super(key: key);
//
//   @override
//   _ScanPayPageState createState() => _ScanPayPageState(
//         coopList,
//         userId,
//         accesstoken,
//         balance,
//         baseUrl,
//         accountno,
//         primaryColor,
//         loginButtonTitleColor,
//         loginbuttonColor,
//         loginTextFieldColor,
//         dasboardIconColor,
//         dashboardTopTitleColor,
//         SecondaryColor,
//       );
// }
//
// class _ScanPayPageState extends State<ScanPayPage> {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//   _ScanPayPageState(
//     this.coopList,
//     this.userId,
//     this.accesstoken,
//     this.balance,
//     this.baseUrl,
//     this.accountno,
//     this.primaryColor,
//     this.loginButtonTitleColor,
//     this.loginbuttonColor,
//     this.loginTextFieldColor,
//     this.dasboardIconColor,
//     this.dashboardTopTitleColor,
//     this.SecondaryColor,
//   );
//
//   // double? balance;
//   String? accountName;
//
//   @override
//   void initState() {
//     super.initState();
//     // this.getJsonData();
//   }
//
//   // Future<String> getJsonData() async {
//   //   String url = "${baseUrl}api/v1/pes/account/${accountno}/info";
//   //   // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
//   //   // Map authorization = {
//   //   //   "Token":
//   //   //       "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI3NWNiMGU4Yy1hZjZlLWVjMTEtODEzNS0wMDUwNTY4OWRlYjkiLCJlbWFpbElkIjoidGVzdEB0ZXN0LmNvbSIsImFjY291bnRObyI6IlNTLTAwMDAwMDYiLCJtUElOIjoiYTZhYzg2MDA0MDZhODViNDIwNTE1MGQ2YWRiZDljYjQiLCJpc1JlZnJlc2hUb2tlbiI6ZmFsc2UsImV4cCI6MTY0NTAzNTI3M30.Tvd0PcwlDyqxDAdCNnwXzrkJtSDEF0fTRx0dAYKjOBY"
//   //   // };
//   //   final response = await http.get(Uri.parse(url),
//   //       headers: {'Authorization': "Bearer ${accesstoken}"});
//   //   print(response.body);
//   //   if (response.statusCode == 200) {
//   //     setState(() {
//   //       var convertDataToJson = json.decode(response.body);
//   //       balance = double.parse(convertDataToJson["Balance"].toString());
//   //       accountName = convertDataToJson["AccountName"];
//   //       print(convertDataToJson);
//   //       // sharedPreferences.setStringList(
//   //       //     'Cooperatives', accountinfo!.map((e) => e.toString()).toList());
//   //     });
//   //     return "success";
//   //   } else {
//   //     showDialog(
//   //         context: context,
//   //         builder: (BuildContext context) {
//   //           return AlertDialog(
//   //             title: Center(
//   //               child: Column(
//   //                 children: [
//   //                   Text(
//   //                     AppLocalizations.of(context)!.localizedString("alert"),
//   //                     style: TextStyle(color: Colors.black87),
//   //                   ),
//   //                   Icon(
//   //                     Icons.add_alert,
//   //                     size: 40,
//   //                     color: Colors.red,
//   //                   )
//   //                 ],
//   //               ),
//   //             ),
//   //             content: Text(
//   //               AppLocalizations.of(context)!.localizedString("server_down"),
//   //               style: TextStyle(color: Colors.black87),
//   //             ),
//   //             actions: [
//   //               TextButton(
//   //                   onPressed: () {
//   //                     Navigator.of(context).pop();
//   //                   },
//   //                   child: Text(
//   //                     AppLocalizations.of(context)!.localizedString("okay"),
//   //                     style: TextStyle(color: Colors.black87),
//   //                   ))
//   //             ],
//   //           );
//   //         });
//   //   }
//   //   return "success";
//   // }
//
//   movetohome() {
//     Navigator.of(context).pushAndRemoveUntil(
//         MaterialPageRoute(
//             builder: (context) => HomePage(
//                   coopList: coopList,
//                   userId: userId,
//                   accesstoken: accesstoken,
//                   baseUrl: baseUrl,
//                   accountno: accountno,
//                   primaryColor: primaryColor,
//                   loginButtonTitleColor: loginButtonTitleColor,
//                   loginbuttonColor: loginbuttonColor,
//                   loginTextFieldColor: loginTextFieldColor,
//                   dasboardIconColor: dasboardIconColor,
//                   dashboardTopTitleColor: dashboardTopTitleColor,
//                   SecondaryColor: SecondaryColor,
//                 )),
//         (Route<dynamic> route) => false);
//   }
//
//   final qrKey = GlobalKey(debugLabel: 'QR');
//   // QRViewController? controller;
//   String? result;
//
//   Future<void> scanBarcode() async {
//     try {
//       final qrcode = await FlutterBarcodeScanner.scanBarcode(
//           "#ff6666", "Cancel", true, ScanMode.QR);
//       if (!mounted) return;
//       setState(() {
//         this.result = qrcode.toString();
//         print("this is the require QR code: ${result}");
//       });
//     } on PlatformException {
//       setState(() {
//         result = "Failed to get Platform version.";
//         // if (ex.code == FlutterBarcodeScanner.) {
//         //   result = "Failed to get platform version.";
//         // } else {
//         //   setState(() {
//         //     result = "Unknown Error $ex";
//         //   });
//         // }
//       });
//     }
//     if (result!.contains("#PES#")) {
//       toaccountName = result?.split("#PES#")[1];
//       toaccountno = result?.split("#PES#")[0];
//     } else {
//       toaccountName = "";
//       toaccountno = "";
//     }
//     print(
//         "this is the required QRcode: ${toaccountno.toString()} and ${toaccountName.toString()}");
//     setState(() {
//       if (toaccountName != "" && toaccountno != "") {
//         print("this is the user id of the tranfeR:${userId}");
//         Navigator.push(
//             context,
//             MaterialPageRoute(
//                 builder: (context) => MoneyTransferPage(
//                       coopList: coopList,
//                       userId: userId,
//                       accesstoken: accesstoken,
//                       baseUrl: baseUrl,
//                       accountno: accountno,
//                       toaccountno: toaccountno,
//                       toaccountName: toaccountName,
//                       balance: balance,
//                       accountName: accountName,
//                       primaryColor: primaryColor,
//                       loginButtonTitleColor: loginButtonTitleColor,
//                       loginbuttonColor: loginbuttonColor,
//                       loginTextFieldColor: loginTextFieldColor,
//                       dasboardIconColor: dasboardIconColor,
//                       dashboardTopTitleColor: dashboardTopTitleColor,
//                       SecondaryColor: SecondaryColor,
//                     )));
//       } else {
//         showDialog(
//             context: context,
//             builder: (BuildContext context) {
//               return AlertDialog(
//                 title: Center(
//                   child: Column(
//                     children: [
//                       Text(
//                         AppLocalizations.of(context)!.localizedString("alert"),
//                         style: TextStyle(color: Colors.black87),
//                       ),
//                       Icon(
//                         Icons.add_alert,
//                         size: 40,
//                         color: Colors.red,
//                       )
//                     ],
//                   ),
//                 ),
//                 content: Text(
//                   AppLocalizations.of(context)!.localizedString("invalid_qr"),
//                   style: TextStyle(color: Colors.black87),
//                 ),
//                 actions: [
//                   TextButton(
//                       onPressed: () {
//                         Navigator.of(context).pop();
//                       },
//                       child: Text(
//                         AppLocalizations.of(context)!.localizedString("okay"),
//                         style: TextStyle(color: Colors.black87),
//                       ))
//                 ],
//               );
//             });
//       }
//     });
//   }
//
//   String? toaccountno;
//   String? toaccountName;
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () {
//         return movetohome();
//       },
//       child: Scaffold(
//         appBar: AppBar(
//           backgroundColor: Color(int.parse(primaryColor.toString())),
//           centerTitle: true,
//           title: Text(
//             AppLocalizations.of(context)!.localizedString("sahakaari_scan_pay"),
//           ),
//           leading: IconButton(
//             onPressed: () {
//               movetohome();
//             },
//             icon: Icon(
//               Icons.arrow_back,
//               size: 30,
//               color: Colors.white,
//             ),
//           ),
//         ),
//         body: SingleChildScrollView(
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Padding(
//                 padding: const EdgeInsets.only(left: 20, top: 20.0),
//                 child: Center(
//                   child: Row(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       const Icon(
//                         Icons.account_balance_wallet_outlined,
//                         color: Colors.green,
//                         size: 20,
//                       ),
//                       const SizedBox(
//                         width: 10,
//                       ),
//                       Text(
//                         AppLocalizations.of(context)!
//                             .localizedString("npr_balance"),
//                         style: TextStyle(
//                           color: Colors.black87,
//                           fontSize: 16,
//                         ),
//                       ),
//                       const SizedBox(
//                         width: 10,
//                       ),
//                       Text(
//                         AppLocalizations.of(context)!.localizedString("rs") +
//                             "${balance.toString()}",
//                         style: const TextStyle(
//                           color: Colors.black87,
//                           fontSize: 16,
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 80.0),
//                 child: Center(
//                   child: QrImage(
//                     data:
//                         accountno.toString() + "#PES#" + AccountName.toString(),
//                     size: 200,
//                     backgroundColor: Colors.white,
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(top: 30.0),
//                 child: Center(
//                   child: Container(
//                     height: 100,
//                     width: 120,
//                     decoration: BoxDecoration(
//                         color: Color(int.parse(loginbuttonColor.toString())),
//                         borderRadius: BorderRadius.circular(10)),
//                     child: TextButton(
//                         onPressed: scanBarcode,
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           children: [
//                             Padding(
//                               padding: EdgeInsets.only(top: 10.0),
//                               child: Text(
//                                 AppLocalizations.of(context)!
//                                     .localizedString("qr_scanner"),
//                                 style: TextStyle(
//                                     color: Colors.white, fontSize: 14),
//                                 textAlign: TextAlign.center,
//                               ),
//                             ),
//                             SizedBox(
//                               height: 5,
//                             ),
//                             Center(
//                               child: Icon(
//                                 Icons.qr_code_scanner,
//                                 size: 40,
//                                 color: Colors.white,
//                               ),
//                             ),
//                           ],
//                         )),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
