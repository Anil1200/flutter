import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';

import '../../utils/localizations.dart';

class AboutUsPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  AboutUsPage(
      {Key? key,
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor})
      : super(key: key);

  @override
  _AboutUsPageState createState() => _AboutUsPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _AboutUsPageState extends State<AboutUsPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _AboutUsPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor);

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          centerTitle: true,
          title:
              Text(AppLocalizations.of(context)!.localizedString("about_us")),
          leading: IconButton(
            onPressed: () {
              movetohome();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              SizedBox(
                height: 30.0,
              ),
              Padding(
                padding: EdgeInsets.only(left: 30.0, right: 25.0),
                child: SizedBox(
                  height: 250,
                  width: 400,
                  child: Text(
                      "SahakaariPay Mobile Banking App is an easy and secure way to do banking and payment related transactions. Through this app you can connect with your bank account for most of the banking services along with the online payments via Internet or SMS channel. You can enquire balance of your account, view mini statement, transfer fund to another account, top up your mobile balance, and pay your landline bills, internet bills, cable operator's bills and electricity bills."),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 30.0, right: 25.0),
                child: SizedBox(
                  height: 100,
                  width: 400,
                  child: Text(
                      "If you have any suggestion please write to us at info@planetearthsolution.com"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
