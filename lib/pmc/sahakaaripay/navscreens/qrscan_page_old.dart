// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_zxing/flutter_zxing.dart';
// import 'package:image/image.dart' as imglib;
// import 'package:image_picker/image_picker.dart';
// import 'package:sahakari_pay/pmc/constants.dart';
// import 'package:sahakari_pay/pmc/sahakaaripay/services/fonepay/phone_scanpay.dart';
// import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/fundtransferV2/fundtransferV2_page.dart';
//
// import '../../utils/localizations.dart';
// import 'qrfundtransfer/moneytransfer_page.dart';
//
// class QRScanPage extends StatefulWidget {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//
//   QRScanPage({
//     Key? key,
//     this.coopList,
//     this.userId,
//     this.accesstoken,
//     this.balance,
//     this.baseUrl,
//     this.accountno,
//     this.primaryColor,
//     this.loginButtonTitleColor,
//     this.loginbuttonColor,
//     this.loginTextFieldColor,
//     this.dasboardIconColor,
//     this.dashboardTopTitleColor,
//     this.SecondaryColor,
//   }) : super(key: key);
//
//   @override
//   _QRScanPageState createState() => _QRScanPageState(
//     coopList,
//     userId,
//     accesstoken,
//     balance,
//     baseUrl,
//     accountno,
//     primaryColor,
//     loginButtonTitleColor,
//     loginbuttonColor,
//     loginTextFieldColor,
//     dasboardIconColor,
//     dashboardTopTitleColor,
//     SecondaryColor,
//   );
// }
//
// class _QRScanPageState extends State<QRScanPage> {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//   _QRScanPageState(
//       this.coopList,
//       this.userId,
//       this.accesstoken,
//       this.balance,
//       this.baseUrl,
//       this.accountno,
//       this.primaryColor,
//       this.loginButtonTitleColor,
//       this.loginbuttonColor,
//       this.loginTextFieldColor,
//       this.dasboardIconColor,
//       this.dashboardTopTitleColor,
//       this.SecondaryColor,
//       );
//
//   String? accountName;
//   Uint8List? qrCodeData;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   moveback() {
//     Navigator.of(context).pop();
//   }
//
//   String? result;
//
//   String? toaccountno;
//   String? toaccountName;
//
//   bool validQRCode = true;
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () {
//         return moveback();
//       },
//       child: Scaffold(
//         body: Stack(
//           children: [
//             ReaderWidget(
//               scanDelay: const Duration(milliseconds: 1),
//               scanDelaySuccess: const Duration(milliseconds: 1),
//               onScan: (value) async {
//                 print("This is scanned qrcode : ${value.text!}");
//
//                 result = value.text;
//
//                 if (!result!.contains("#PES#") && !result!.contains("fonepay")) {
//                   setState(() {
//                     validQRCode = false;
//                   });
//                 }
//
//                 if (validQRCode) {
//                   if (result!.contains("#PES#")) {
//                     toaccountName = result?.split("#PES#")[1];
//                     toaccountno = result?.split("#PES#")[0];
//                   } else {
//                     toaccountName = "";
//                     toaccountno = "";
//                   }
//                   setState(() {
//                     if (toaccountName != "" && toaccountno != "") {
//                       if (EtellerDeposit!) {
//                         EtellerDepositFromAccount = toaccountno;
//                         print("eteller deposit $EtellerDepositAccount");
//                         Navigator.pushReplacement(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) => FundTransferV2Page(
//                                   coopList: coopList,
//                                   userId: userId,
//                                   accesstoken: accesstoken,
//                                   balance: 0,
//                                   baseUrl: baseUrl,
//                                   accountno: accountno,
//                                   primaryColor: primaryColor,
//                                   loginButtonTitleColor: loginButtonTitleColor,
//                                   loginbuttonColor: loginbuttonColor,
//                                   loginTextFieldColor: loginTextFieldColor,
//                                   dasboardIconColor: dasboardIconColor,
//                                   dashboardTopTitleColor: dashboardTopTitleColor,
//                                   SecondaryColor: SecondaryColor,
//                                   AccountNo: EtellerDepositAccount,
//                                 )));
//                       } else {
//                         Navigator.pushReplacement(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) =>
//                                     MoneyTransferPage(
//                                       coopList: coopList,
//                                       userId: userId,
//                                       accesstoken: accesstoken,
//                                       baseUrl: baseUrl,
//                                       accountno: accountno,
//                                       toaccountno: toaccountno,
//                                       toaccountName: toaccountName,
//                                       balance: balance,
//                                       accountName: accountName,
//                                       primaryColor: primaryColor,
//                                       loginButtonTitleColor: loginButtonTitleColor,
//                                       loginbuttonColor: loginbuttonColor,
//                                       loginTextFieldColor: loginTextFieldColor,
//                                       dasboardIconColor: dasboardIconColor,
//                                       dashboardTopTitleColor: dashboardTopTitleColor,
//                                       SecondaryColor: SecondaryColor,
//                                     )));
//                       }
//                     } else {
//                       if (!EtellerWithdraw! && !EtellerDeposit!) {
//                         Navigator.pushReplacement(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) =>
//                                     PhoneScanPayPage(
//                                       coopList: coopList,
//                                       userId: userId,
//                                       accesstoken: accesstoken,
//                                       baseUrl: baseUrl,
//                                       accountno: accountno,
//                                       balance: balance,
//                                       result: result,
//                                       primaryColor: primaryColor,
//                                       loginButtonTitleColor: loginButtonTitleColor,
//                                       loginbuttonColor: loginbuttonColor,
//                                       loginTextFieldColor: loginTextFieldColor,
//                                       dasboardIconColor: dasboardIconColor,
//                                       dashboardTopTitleColor: dashboardTopTitleColor,
//                                       SecondaryColor: SecondaryColor,
//                                     )));
//                       } else {
//                         ScaffoldMessenger.of(context).showSnackBar(
//                             const SnackBar(
//                               duration: Duration(seconds: 2),
//                               content: Text("Invalid QR Code!"),
//                             ));
//
//                         EtellerWithdraw = false;
//
//                         Navigator.of(context).pop();
//                       }
//                     }
//                   });
//                 } else {
//                   ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
//                     duration: Duration(seconds: 2),
//                     content: Text("Invalid QR Code!"),
//                   ));
//
//                   Navigator.of(context).pop();
//                 }
//               },
//             ),
//             Positioned(
//               bottom: 20,
//               right: 20,
//               child: ClipOval(
//                 child: Material(
//                   color: Colors.black26,
//                   child: InkWell(
//                     onTap: () async {
//                       ImagePicker picker = ImagePicker();
//
//                       XFile? xFile = await picker.pickImage(source: ImageSource.gallery);
//
//                       Code? resultFromXFile = await zx.readBarcodeImagePath(xFile!);
//
//                       print("This is file qrcode : ${resultFromXFile!.text!}");
//
//                       result = resultFromXFile.text;
//
//                       if (!result!.contains("#PES#") && !result!.contains("fonepay")) {
//                         setState(() {
//                           validQRCode = false;
//                         });
//                       }
//
//                       if (validQRCode) {
//                         if (result!.contains("#PES#")) {
//                           toaccountName = result?.split("#PES#")[1];
//                           toaccountno = result?.split("#PES#")[0];
//                         } else {
//                           toaccountName = "";
//                           toaccountno = "";
//                         }
//                         setState(() {
//                           if (toaccountName != "" && toaccountno != "") {
//                             if (EtellerDeposit!) {
//                               EtellerDepositFromAccount = toaccountno;
//                               print("eteller deposit $EtellerDepositAccount");
//                               Navigator.pushReplacement(
//                                   context,
//                                   MaterialPageRoute(
//                                       builder: (context) => FundTransferV2Page(
//                                         coopList: coopList,
//                                         userId: userId,
//                                         accesstoken: accesstoken,
//                                         balance: 0,
//                                         baseUrl: baseUrl,
//                                         accountno: accountno,
//                                         primaryColor: primaryColor,
//                                         loginButtonTitleColor: loginButtonTitleColor,
//                                         loginbuttonColor: loginbuttonColor,
//                                         loginTextFieldColor: loginTextFieldColor,
//                                         dasboardIconColor: dasboardIconColor,
//                                         dashboardTopTitleColor: dashboardTopTitleColor,
//                                         SecondaryColor: SecondaryColor,
//                                         AccountNo: EtellerDepositAccount,
//                                       )));
//                             } else {
//                               Navigator.pushReplacement(
//                                   context,
//                                   MaterialPageRoute(
//                                       builder: (context) =>
//                                           MoneyTransferPage(
//                                             coopList: coopList,
//                                             userId: userId,
//                                             accesstoken: accesstoken,
//                                             baseUrl: baseUrl,
//                                             accountno: accountno,
//                                             toaccountno: toaccountno,
//                                             toaccountName: toaccountName,
//                                             balance: balance,
//                                             accountName: accountName,
//                                             primaryColor: primaryColor,
//                                             loginButtonTitleColor: loginButtonTitleColor,
//                                             loginbuttonColor: loginbuttonColor,
//                                             loginTextFieldColor: loginTextFieldColor,
//                                             dasboardIconColor: dasboardIconColor,
//                                             dashboardTopTitleColor: dashboardTopTitleColor,
//                                             SecondaryColor: SecondaryColor,
//                                           )));
//                             }
//                           } else {
//                             if(!EtellerWithdraw! && !EtellerDeposit!) {
//                               Navigator.pushReplacement(
//                                   context,
//                                   MaterialPageRoute(
//                                       builder: (context) =>
//                                           PhoneScanPayPage(
//                                             coopList: coopList,
//                                             userId: userId,
//                                             accesstoken: accesstoken,
//                                             baseUrl: baseUrl,
//                                             accountno: accountno,
//                                             balance: balance,
//                                             result: result,
//                                             primaryColor: primaryColor,
//                                             loginButtonTitleColor: loginButtonTitleColor,
//                                             loginbuttonColor: loginbuttonColor,
//                                             loginTextFieldColor: loginTextFieldColor,
//                                             dasboardIconColor: dasboardIconColor,
//                                             dashboardTopTitleColor: dashboardTopTitleColor,
//                                             SecondaryColor: SecondaryColor,
//                                           )));
//                             } else {
//                               ScaffoldMessenger.of(context).showSnackBar(
//                                   const SnackBar(
//                                     duration: Duration(seconds: 2),
//                                     content: Text("Invalid QR Code!"),
//                                   ));
//
//                               EtellerWithdraw = false;
//
//                               Navigator.of(context).pop();
//                             }
//                           }
//                         });
//                       } else {
//                         ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
//                           duration: Duration(seconds: 2),
//                           content: Text("Invalid QR Code!"),
//                         ));
//
//                         Navigator.of(context).pop();
//                       }
//                     },
//                     child: const SizedBox(
//                       width: 56,
//                       height: 56,
//                       child: Icon(
//                         Icons.photo,
//                         color: Colors.white,
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 25,
//               right: 5,
//               child: ClipOval(
//                 child: Material(
//                   color: Colors.black26,
//                   child: InkWell(
//                     onTap: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: const SizedBox(
//                       width: 56,
//                       height: 56,
//                       child: Icon(
//                         Icons.close,
//                         color: Colors.white,
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 50,
//               child: SizedBox(
//                 width: MediaQuery.of(context).size.width,
//                 child: Padding(
//                   padding: const EdgeInsets.all(8.0),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Image.asset(
//                         'assets/images/spay.png',
//                         height: 125,
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: MediaQuery.of(context).size.height / 2 + 120,
//               left: 10,
//               right: 10,
//               child: Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Row(
//                   children: [
//                     Image.asset(
//                       'assets/images/eteller.png',
//                       width: 75,
//                     ),
//                     const SizedBox(
//                       width: 20,
//                     ),
//                     Image.asset(
//                       'assets/images/fonepay2.png',
//                       width: 75,
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
