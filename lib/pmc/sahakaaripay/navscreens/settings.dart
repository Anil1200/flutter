<<<<<<< HEAD
=======
import 'dart:convert';

import 'package:http/http.dart' as http;
>>>>>>> sudeep
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/chnagempinandpassword/changempin_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/navscreens/chnagempinandpassword/changepassword_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../local_auth_api.dart';
import '../../utils/localizations.dart';
import '../../utils/utils.dart';
import '../models/loading.dart';
import '../pmc_homepage.dart';
import 'changelanguage_page.dart';

class SettingsPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  SettingsPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<SettingsPage> createState() => _SettingsPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _SettingsPageState extends State<SettingsPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  bool loginBiometric = false;
  bool transactionBiometric = false;

  bool _isLoading = false;

  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  _SettingsPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    loginBiometric = showBiometricbutton;
    transactionBiometric = showBiometricbuttonTransaction;
    print("this is the coop short name: ${coopShortName}");
  }

  bool validateMPINForm() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  showBiometricLogin(ConnectivityResult result) async {
    if (result == ConnectivityResult.none) {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("no_internet"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      final isAuthenticated = await LocalAuthApi.authenticate();
      if (isAuthenticated) {
        SharedPreferences logincredentials =
            await SharedPreferences.getInstance();
        logincredentials.setString("username${coopShortName}", login_username!);
        logincredentials.setString("password${coopShortName}", login_password!);
        logincredentials.commit();
        fingerprintstring =
            logincredentials.getString("username${coopShortName}");
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text("FingerPrint is set.")));
        setState(() {
          showBiometricbutton = true;
        });
      } else {
        setState(() {
          loginBiometric = false;
        });
      }
    }
  }

  showBiometricTransaction(ConnectivityResult result, String mpin) async {
    if (result == ConnectivityResult.none) {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("no_internet"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      final isAuthenticated = await LocalAuthApi.authenticate();
      if (isAuthenticated) {
        mpin = Utils.encryptAES(mpin);
        SharedPreferences logincredentials = await SharedPreferences.getInstance();
        logincredentials.setString("transactionusername${coopShortName}", login_username!);
        logincredentials.setString("transactionpassword${coopShortName}", mpin);
        logincredentials.commit();
        fingerprintstring = logincredentials.getString("transactionusername${coopShortName}");
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("FingerPrint is set.")));
        setState(() {
          showBiometricbuttonTransaction = true;
          transactionMPIN = Utils.decryptAES(mpin);
          print(
              "this is the mpin value from the shared prefrences for biometric: ${mpin}");
        });
      } else {
        setState(() {
          transactionBiometric = false;
          showBiometricbuttonTransaction = false;
        });
      }
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: SizedBox(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("enter_valid_pin"),
                      style: const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: const TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          setState(() {
                            transactionBiometric = false;
                          });
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            authMPIN().then((value) async {
                              setState(() {
                                _isLoading = false;
                              });
                              if (value) {
                                String mpin = mpincontroller.text.toString();
                                final result = await Connectivity().checkConnectivity();
                                showBiometricTransaction(result, mpin);
                              } else {
                                setState(() {
                                  transactionBiometric = false;
                                });
                                //ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context)!.localizedString("invalid_mpin"))));
                              }

                              mpincontroller.clear();
                            });
                            Navigator.of(context).pop();
                          } else {
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: const TextStyle(fontSize: 18),
                                          ),
                                          const SizedBox(height: 10.0),
                                          const Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  Future<bool> authMPIN() async {
    String url = "${baseUrl}api/v1/authmpin";
    Map body = {
      "userId": userId,
      "MPIN": mpincontroller.text.toString()
    };
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      return true;
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(AppLocalizations.of(context)!.localizedString("invalid_mpin"))));

      return false;
    }
  }

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.localizedString("settings"),
          ),
          leading: IconButton(
            onPressed: () {
              movetohome();
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
        ),
        body: _isLoading
          ? const Loading()
          : SingleChildScrollView(
          child: Column(
            children: [
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChangePasswordPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor:
                            dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
                },
                child: ListTile(
                  leading: const Icon(
                    Icons.lock,
                  ),
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    size: 16,
                  ),
                  title: Text(
                      AppLocalizations.of(context)!.localizedString("change_password")
                  ),
                ),
              ),
              ListTile(
                leading: const Icon(
                  Icons.fingerprint,
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("use_biometric")
                ),
                subtitle: Text(
                  AppLocalizations.of(context)!.localizedString("use_biometric_login")
                ),
                trailing: Switch(
                  value: loginBiometric,
                  onChanged: (value) async {
                    if (!value) {
                      SharedPreferences logincredentials = await SharedPreferences.getInstance();
                      logincredentials.setString("username${coopShortName}", "");
                      logincredentials.setString("password${coopShortName}", "");
                      setState(() {
                        showBiometricbutton = false;
                      });
                    } else {
                      final result = await Connectivity().checkConnectivity();
                      showBiometricLogin(result);
                    }

                    setState(() {
                      loginBiometric=!loginBiometric;
                    });
                  },
                ),
              ),
              /*const SizedBox(
                height: 30,
              ),*/
              const Divider(),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChangeMPINPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor:
                            dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
                },
                child: ListTile(
                  leading: const Icon(
                    Icons.pin,
                  ),
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    size: 16,
                  ),
                  title: Text(
                      AppLocalizations.of(context)!.localizedString("change_mpin")
                  ),
                ),
              ),
              ListTile(
                leading: const Icon(
                  Icons.fingerprint,
                ),
                title: Text(
                    AppLocalizations.of(context)!.localizedString("use_biometric")
                ),
                subtitle: Text(
                    AppLocalizations.of(context)!.localizedString("use_biometric_transaction")
                ),
                trailing: Switch(
                  value: transactionBiometric,
                  onChanged: (value) async {
                    if (!value) {
                      SharedPreferences logincredentials = await SharedPreferences.getInstance();
                      logincredentials.setString("transactionusername${coopShortName}", "");
                      logincredentials.setString("transactionpassword${coopShortName}", "");
                      setState(() {
                        showBiometricbuttonTransaction = false;
                      });
                    } else {
                      transactionPinMessage();
                    }

                    setState(() {
                      transactionBiometric=!transactionBiometric;
                    });
                  },
                ),
              ),
              /*const SizedBox(
                height: 30,
              ),*/
              const Divider(),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => LanguagePage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor: dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
                },
                child: ListTile(
                  leading: const Icon(
                    Icons.language,
                  ),
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    size: 16,
                  ),
                  title: Text(
                      AppLocalizations.of(context)!.localizedString("change_language")
                  ),
                ),
              ),
<<<<<<< HEAD
              showBiometricbutton
<<<<<<< HEAD
                  ? Padding(
                      padding: const EdgeInsets.only(top: 80.0),
                      child: Center(
                        child: Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width / 2,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Color(
                                  int.parse(loginbuttonColor.toString()))),
                          child: TextButton(
                            onPressed: () async {
                              SharedPreferences logincredentials =
                                  await SharedPreferences.getInstance();
                              logincredentials.setString(
                                  "username${coopShortName}", "");
                              logincredentials.setString(
                                  "password${coopShortName}", "");
                              setState(() {
                                showBiometricbutton = false;
                              });
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("remove_fingerprint"),
                              style: TextStyle(
                                  color: Color(int.parse(
                                      loginButtonTitleColor.toString()))),
                            ),
                          ),
                        ),
                      ),
                    )
                  : Padding(
                      padding: const EdgeInsets.only(top: 80.0),
                      child: Center(
                          child: Container(
                        height: 80,
                        width: MediaQuery.of(context).size.width / 2,
                        decoration: BoxDecoration(
                            color: Colors.green[900],
                            borderRadius: BorderRadius.circular(20)),
                        child: TextButton(
                          onPressed: () async {
                            setState(() {
                              _isLoading = true;
                            });
                            final result =
                                await Connectivity().checkConnectivity();
                            showBiometric(result);
                          },
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.fingerprint,
                                size: 40,
                                color: Color(int.parse(
                                    loginButtonTitleColor.toString())),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
=======
                      ? Padding(
                          padding: const EdgeInsets.only(top: 80.0),
                          child: Center(
                            child: Container(
                              height: 50,
                              width: MediaQuery.of(context).size.width / 2,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color(
                                      int.parse(loginbuttonColor.toString()))),
                              child: TextButton(
                                onPressed: () async {
                                  SharedPreferences logincredentials =
                                      await SharedPreferences.getInstance();
                                  logincredentials.setString(
                                      "username${coopShortName}", "");
                                  logincredentials.setString(
                                      "password${coopShortName}", "");
                                  setState(() {
                                    showBiometricbutton = false;
                                  });
                                },
>>>>>>> sudeep
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("set_fingerprint"),
                                  style: TextStyle(
                                      color: Color(int.parse(
                                          loginButtonTitleColor.toString()))),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ],
                          ),
<<<<<<< HEAD
                        ),
                      )),
                    )
=======
                        )
                      : Padding(
                          padding: const EdgeInsets.only(top: 80.0),
                          child: Center(
                              child: Container(
                            height: 80,
                            width: MediaQuery.of(context).size.width / 2,
                            decoration: BoxDecoration(
                                color: Colors.green[900],
                                borderRadius: BorderRadius.circular(20)),
                            child: TextButton(
                              onPressed: () async {
                                setState(() {
                                  _isLoading = true;
                                });
                                final result =
                                    await Connectivity().checkConnectivity();
                                showBiometric(result);
                              },
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.fingerprint,
                                    size: 40,
                                    color: Color(int.parse(
                                        loginButtonTitleColor.toString())),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("set_fingerprint"),
                                      style: TextStyle(
                                          color: Color(int.parse(
                                              loginButtonTitleColor
                                                  .toString()))),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )),
                        ),
>>>>>>> sudeep
=======
>>>>>>> sudeep
            ],
          ),
        ),
      ),
    );
  }
}
