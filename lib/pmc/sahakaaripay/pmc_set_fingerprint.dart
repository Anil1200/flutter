import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/main.dart';
import 'package:sahakari_pay/pmc/local_auth_api.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';
import '../utils/localizations.dart';

class PMCSetFingerprintPage extends StatefulWidget {
  List? coopList;
  String? logoUrl;
  String? baseUrl;
  String? name;
  String? primaryColor;
  String? loginButtonTitle;
  String? loginButtonBackground;
  String? loginTextField;
  String? dashboardTopIcon;
  String? dashboardTopTitle;
  String? secondaryColor;
  String? smsCode;

  PMCSetFingerprintPage({
    Key? key,
    this.coopList,
    this.logoUrl,
    this.baseUrl,
    this.name,
    this.primaryColor,
    this.loginButtonTitle,
    this.loginButtonBackground,
    this.loginTextField,
    this.dashboardTopIcon,
    this.dashboardTopTitle,
    this.secondaryColor,
    this.smsCode,
  }) : super(key: key);

  @override
  _PMCSetFingerprintPageState createState() => _PMCSetFingerprintPageState(
        coopList,
        logoUrl,
        baseUrl,
        name,
        primaryColor,
        loginButtonTitle,
        loginButtonBackground,
        loginTextField,
        dashboardTopIcon,
        dashboardTopTitle,
        secondaryColor,
        smsCode,
      );
}

class _PMCSetFingerprintPageState extends State<PMCSetFingerprintPage> {
  final TextEditingController usernamecontroller = TextEditingController();
  final TextEditingController passwordcontroller = TextEditingController();
  final scaffoldKey = GlobalKey<State>();
  final GlobalKey<FormState> globalFormKey = GlobalKey();

  List? coopList;
  String? logoUrl;
  String? baseUrl;
  String? name;
  String? primaryColor;
  String? loginButtonTitle;
  String? loginButtonBackground;
  String? loginTextField;
  String? dashboardTopIcon;
  String? dashboardTopTitle;
  String? secondaryColor;
  String? smsCode;

  _PMCSetFingerprintPageState(
    this.coopList,
    this.logoUrl,
    this.baseUrl,
    this.name,
    this.primaryColor,
    this.loginButtonTitle,
    this.loginButtonBackground,
    this.loginTextField,
    this.dashboardTopIcon,
    this.dashboardTopTitle,
    this.secondaryColor,
    this.smsCode,
  );
  bool _obscureText = true;
  bool _isLoading = false;
  String? accesstoken;
  double? balance;
  // String? color;
  // String? loginButtonTitleColor;
  // String? loginbuttonColor;
  // String? loginTextFieldColor;
  // String? dasboardIconColor;
  // String? dashboardTopTitleColor;
  // String? SecondaryColor;
  // String? smsCode;
  //

  String? userId;

  showMessage(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("no_internet"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      signIn(usernamecontroller.text, passwordcontroller.text);
    }
  }

  String? accountno;
  String? authUserName;
  bool? isOTPVerification;

  void signIn(String username, String password) async {
    String url = "${baseUrl}api/v1/auth";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map body = {
      "username": username,
      "password": password,
      "DeviceId": deviceId,
      "FCMToken": FCM_TOKEN.toString(),
    };
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body);
    //checking the status of api
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = true;
        print("Response status: ${res.statusCode}");
        print("Response status: ${jsonResponse}");
        if (jsonResponse != null) {
          setState(() async {
            _isLoading = false;
            userId = jsonResponse["UserId"];
            accesstoken = jsonResponse["AccessToken"];
            accountno = jsonResponse["LinkedAccountNo"];
            authUserName = jsonResponse["UserName"];
            FCM_TOPIC = smsCode!;
            isOTPVerification = jsonResponse["IsOTPVerification"];
            login_username = username;
            print("this is the LOGIN USERNAME: ${login_username}");
            login_password = password;
            coopName = name;
            //Set Shared Preferences for Fingerprint
            final result = await Connectivity().checkConnectivity();
            showBiometric(result);
          });
        }
        print("Response status: ${res.body}");
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    ),
                  ],
                ),
              ),
              content: Text(
                jsonResponse.toString(),
                style: TextStyle(color: Colors.black87),
              ),
              actions: [
                Container(
                    height: 50,
                    width: 80,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: const TextStyle(color: Colors.white),
                        )))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    ),
                  ],
                ),
              ),
              content: Text(jsonResponse["Message"]),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the response of login: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("invalid_username_password"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  showBiometric(ConnectivityResult result) async {
    if (result == ConnectivityResult.none) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("no_internet"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = true;
      });
      final isAuthenticated = await LocalAuthApi.authenticate();
      if (isAuthenticated) {
        SharedPreferences logincredentials =
            await SharedPreferences.getInstance();
        print("aako username of login: ${login_username}");
        logincredentials.setString("username${coopShortName}", login_username!);
        logincredentials.setString("password${coopShortName}", login_password!);
        logincredentials.commit();
        fingerprintstring =
            logincredentials.getString("username${coopShortName}");
        print("pachhiko username of login: ${fingerprintstring}");
        // getAllcoop();
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text("FingerPrint is set.")));
        setState(() {
          showBiometricbutton = true;
        });
        movetohome();
      } else {
        setState(() {
          _isLoading = false;
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
                "Fingerprint is not set on your device. Please set your fingerprint on your device first, then use fingerprint setup again.")));
      }
    }
  }

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SahakariScreenPage()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          return movetohome();
        },
        child: Scaffold(
            key: scaffoldKey,
            appBar: AppBar(
              backgroundColor: Colors.green[900],
              leading: IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.white,
                ),
                onPressed: () {
                  movetohome();
                },
              ),
              toolbarHeight: 90,
              centerTitle: true,
              title: Text(AppLocalizations.of(context)!
                  .localizedString("sahakaari_pay")),
            ),
            body: _isLoading
                ? Center(
                    child: Loading(),
                  )
                : SingleChildScrollView(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 10.0,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 20.0, right: 20, top: 15),
                                    child: Container(
                                      height: 300,
                                      width: 350,
                                      decoration: BoxDecoration(
                                        color: Color(
                                            int.parse(primaryColor.toString())),
                                        borderRadius: BorderRadius.circular(20),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 5,
                                            blurRadius: 7,
                                            offset: const Offset(0, 3),
                                          )
                                        ],
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            20.0, 10.0, 20.0, 0.0),
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.vertical,
                                          child: Column(children: [
                                            Form(
                                                key: globalFormKey,
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 20.0,
                                                              top: 5),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal:
                                                                    12.0,
                                                                vertical: 10),
                                                        child: TextFormField(
                                                          controller:
                                                              usernamecontroller,
                                                          maxLength: 10,
                                                          keyboardType:
                                                              TextInputType
                                                                  .number,
                                                          style: TextStyle(
                                                              color: Color(int.parse(
                                                                  loginTextField
                                                                      .toString()))),
                                                          validator: (value) => value!
                                                                  .isEmpty
                                                              ? AppLocalizations
                                                                      .of(
                                                                          context)!
                                                                  .localizedString(
                                                                      "username_empty_message")
                                                              : null,
                                                          onSaved: (value) =>
                                                              usernamecontroller,
                                                          decoration:
                                                              InputDecoration(
                                                                  border:
                                                                      const UnderlineInputBorder(),
                                                                  labelText: AppLocalizations.of(
                                                                          context)!
                                                                      .localizedString(
                                                                          "username"),
                                                                  labelStyle: const TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                  errorStyle: const TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                  counterText:
                                                                      "",
                                                                  icon:
                                                                      const Icon(
                                                                    Icons
                                                                        .supervised_user_circle,
                                                                    size: 30.0,
                                                                    color: Colors
                                                                        .white,
                                                                  )),
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        left: 20.0,
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .symmetric(
                                                                horizontal:
                                                                    12.0,
                                                                vertical: 10),
                                                        child: TextFormField(
                                                          controller:
                                                              passwordcontroller,
                                                          maxLength: 50,
                                                          keyboardType:
                                                              TextInputType
                                                                  .text,
                                                          style: TextStyle(
                                                              color: Color(int.parse(
                                                                  loginTextField
                                                                      .toString()))),
                                                          obscureText:
                                                              _obscureText,
                                                          validator: (value) => value!
                                                                      .isEmpty ||
                                                                  value.length <
                                                                      3
                                                              ? AppLocalizations
                                                                      .of(
                                                                          context)!
                                                                  .localizedString(
                                                                      "password_empty_message")
                                                              : null,
                                                          onSaved: (value) =>
                                                              passwordcontroller,
                                                          decoration:
                                                              InputDecoration(
                                                            border:
                                                                const UnderlineInputBorder(),
                                                            labelText: AppLocalizations
                                                                    .of(
                                                                        context)!
                                                                .localizedString(
                                                                    "password"),
                                                            labelStyle:
                                                                const TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                            errorStyle:
                                                                const TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                            counterText: "",
                                                            icon: const Icon(
                                                              Icons.lock,
                                                              size: 30.0,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                            suffixIcon:
                                                                IconButton(
                                                              onPressed: () {
                                                                _toggle();
                                                              },
                                                              icon: Icon(
                                                                _obscureText
                                                                    ? Icons
                                                                        .visibility_off
                                                                    : Icons
                                                                        .visibility,
                                                                color: Colors
                                                                    .white,
                                                                size: 30.0,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 20.0,
                                              ),
                                              child: Center(
                                                child: InkWell(
                                                  onTap: () async {
                                                    if (validateAndSave()) {
                                                      setState(() {
                                                        _isLoading = true;
                                                      });
                                                      final result =
                                                          await Connectivity()
                                                              .checkConnectivity();
                                                      showMessage(result);
                                                    } else {
                                                      setState(() {
                                                        _isLoading = false;
                                                      });
                                                    }
                                                  },
                                                  child: Container(
                                                    height: 40,
                                                    width: 140,
                                                    decoration: BoxDecoration(
                                                      color: Color(int.parse(
                                                          loginButtonBackground
                                                              .toString())),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      boxShadow: [
                                                        BoxShadow(
                                                          color: Colors.white
                                                              .withOpacity(0.9),
                                                          spreadRadius: 5,
                                                          blurRadius: 7,
                                                          offset: const Offset(
                                                              0, 3),
                                                        )
                                                      ],
                                                    ),
                                                    child: Center(
                                                      child: Text(
                                                        AppLocalizations.of(
                                                                context)!
                                                            .localizedString(
                                                                "set_fingerprint"),
                                                        style: TextStyle(
                                                          color: Color(int.parse(
                                                              loginButtonTitle
                                                                  .toString())),
                                                          fontSize: 16.0,
                                                        ),
                                                        textAlign:
                                                            TextAlign.center,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ]),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ]),
                  )));
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
