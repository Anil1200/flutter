import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_login.dart';

import '../../../main.dart';
import '../constants.dart';

class ResultPage extends StatefulWidget {
  List? coopList;
  ResultPage({Key? key, this.coopList}) : super(key: key);

  @override
  _ResultPageState createState() => _ResultPageState(coopList);
}

class _ResultPageState extends State<ResultPage> {
  List? coopList;
  _ResultPageState(
    this.coopList,
  ) {
    searchcontroller.addListener(() {
      if (searchcontroller.text.isEmpty) {
        setState(() {
          searchText = "";
          searchedCooperativeList.clear();
        });
      } else {
        setState(() {
          searchText = searchcontroller.text;
        });
      }
      print("search text: ${searchText}");
    });
  }
  final TextEditingController searchcontroller = TextEditingController();
  FocusNode searchFocusNode = new FocusNode();

  String searchText = "";

  List<dynamic> searchedCooperativeList = [];

  Icon _searchIcon = Icon(Icons.search);

  movetoSahakaariScreenPage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SahakariScreenPage()),
        (Route<dynamic> route) => false);
  }

  void initState() {
    super.initState();
    _appBarTitle = Text("Search Cooperative");
  }

  Widget? _appBarTitle;

  CoopSearch() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = Icon(Icons.close);
        this._appBarTitle = TextField(
          style: TextStyle(
            color: Colors.black,
            fontSize: 14,
          ),
          controller: searchcontroller,
          focusNode: searchFocusNode,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            hintText: 'Search Cooperative',
            fillColor: Colors.white,
            filled: true,
          ),
        );
        searchFocusNode.requestFocus();
      } else {
        this._searchIcon = Icon(Icons.search);
        this._appBarTitle = Text("Search Cooperative");
        searchText = "";
        searchedCooperativeList.clear();
        searchcontroller.clear();
      }
    });
  }

  Widget _buildList() {
    if (!(searchText.isEmpty)) {
      List<dynamic> tempList = [];
      for (int i = 0; i < coopList!.length; i++) {
        if (coopList?[i]["coopName"]
            .toLowerCase()
            .contains(searchText.toLowerCase())) {
          tempList.add(coopList?[i]);
        }
      }
      searchedCooperativeList = tempList;
      print("this is the searched list: ${searchedCooperativeList}");
    }
    if (searchedCooperativeList.length == 0) {
      if (!(searchText.isEmpty)) {
        return Padding(
          padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
          child: Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                "NO Cooperative Found",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              )),
        );
      } else {
        if (coopList?.length == 0) {
          return Padding(
            padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: Text(
                  "NO Cooperative Found",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                )),
          );
        } else {
<<<<<<< HEAD:lib/pmc/sahakaaripay/services/result_page.dart
          return ListView.builder(
              itemCount: coopList == null ? 0 : coopList?.length,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    ProjectIndex = coopList?[index];
                    banktransferType = coopList?[index]["gateWayName"];
                    coopShortName = coopList?[index]["coopShortName"];
                    HasPhonePay = coopList?[index]["hasPhonePay"];
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PMCLoginPage()));
                    // (name: coopList?[index]["coopName"],logoUrl: coopList?[index]["logoUrl"],baseUrl: coopList?[index]["baseUrl"]);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20, bottom: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(0, 3),
                                      )
                                    ],
                                    borderRadius: BorderRadius.circular(20),
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          coopList?[index]["logoUrl"]),
                                      fit: BoxFit.cover,
                                    )),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 30.0, top: 20),
                                child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Text(coopList?[index]["coopName"])),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        const Divider(
                          thickness: 1,
                          color: Colors.red,
                        )
                      ],
                    ),
                  ),
                );
              });
        }
      }
    } else {
      return ListView.builder(
          itemCount: searchedCooperativeList == null
              ? 0
              : searchedCooperativeList.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                ProjectIndex = index;
                banktransferType =
                    searchedCooperativeList[index]["gateWayName"];
                coopShortName = searchedCooperativeList[index]["coopShortName"];
                HasPhonePay = searchedCooperativeList[index]["hasPhonePay"];
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PMCLoginPage()));
                // (name: coopList?[index]["coopName"],logoUrl: coopList?[index]["logoUrl"],baseUrl: coopList?[index]["baseUrl"]);
              },
              child: Padding(
                padding:
                    const EdgeInsets.only(left: 20.0, right: 20, bottom: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, top: 20),
                      child: Row(
=======
          return ScrollConfiguration(
            behavior: CustomScrollBehavior(
              androidSdkVersion: androidSdkVersion,
            ),
            child: ListView.builder(
                itemCount: coopList == null ? 0 : coopList?.length,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: () {
                      ProjectIndex = index;
                      banktransferType = coopList?[index]["gateWayName"];
                      coopShortName = coopList?[index]["coopShortName"];
                      HasPhonePay = coopList?[index]["hasPhonePay"];
                      HasATM = coopList?[index]["hasATM"];
                      AllowOnePG = coopList?[index]["allowOnePG"];
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PMCLoginPage()));
                      // (name: coopList?[index]["coopName"],logoUrl: coopList?[index]["logoUrl"],baseUrl: coopList?[index]["baseUrl"]);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20, bottom: 30),
                      child: Column(
>>>>>>> sudeep:lib/pmc/sahakaaripay/result_page.dart
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: const Offset(0, 3),
                                  )
                                ],
                                borderRadius: BorderRadius.circular(20),
                                image: DecorationImage(
                                  image: NetworkImage(
                                      searchedCooperativeList[index]
                                          ["logoUrl"]),
                                  fit: BoxFit.cover,
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 30.0, top: 20),
                            child: Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Text(searchedCooperativeList[index]
                                    ["coopName"])),
                          ),
                        ],
                      ),
                    ),
<<<<<<< HEAD:lib/pmc/sahakaaripay/services/result_page.dart
                    const SizedBox(
                      height: 20,
                    ),
                    const Divider(
                      thickness: 1,
                      color: Colors.red,
                    )
                  ],
=======
                  );
                }),
          );
        }
      }
    } else {
      return ScrollConfiguration(
          behavior: CustomScrollBehavior(
          androidSdkVersion: androidSdkVersion,
        ),
        child: ListView.builder(
            itemCount: searchedCooperativeList == null
                ? 0
                : searchedCooperativeList.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () {
                  ProjectIndex = index;
                  for (int i = 0; i < coopList!.length; i++) {
                    if (coopList?[i]["coopShortName"].toString().toLowerCase() == searchedCooperativeList[index]["coopShortName"].toString().toLowerCase()) {
                      ProjectIndex = i;
                    }
                  }

                  banktransferType =
                      searchedCooperativeList[index]["gateWayName"];
                  coopShortName = searchedCooperativeList[index]["coopShortName"];
                  HasPhonePay = searchedCooperativeList[index]["hasPhonePay"];
                  HasATM = searchedCooperativeList[index]["hasATM"];
                  AllowOnePG = searchedCooperativeList[index]["allowOnePG"];
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PMCLoginPage()));
                  // (name: coopList?[index]["coopName"],logoUrl: coopList?[index]["logoUrl"],baseUrl: coopList?[index]["baseUrl"]);
                },
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 20.0, right: 20, bottom: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: const Offset(0, 3),
                                    )
                                  ],
                                  borderRadius: BorderRadius.circular(20),
                                  image: DecorationImage(
                                    image: NetworkImage(
                                        searchedCooperativeList[index]
                                            ["logoUrl"]),
                                    fit: BoxFit.cover,
                                  )),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 30.0, top: 20),
                              child: Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(searchedCooperativeList[index]
                                      ["coopName"])),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      const Divider(
                        thickness: 1,
                        color: Colors.green,
                      )
                    ],
                  ),
>>>>>>> sudeep:lib/pmc/sahakaaripay/result_page.dart
                ),
              ),
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoSahakaariScreenPage();
      },
      child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 70,
            backgroundColor: Colors.green,
            leading: IconButton(
              onPressed: () {
                movetoSahakaariScreenPage();
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 30,
              ),
            ),
            title: _appBarTitle,
            centerTitle: true,
            actions: [
              IconButton(onPressed: CoopSearch, icon: _searchIcon),
            ],
          ),
          body: _buildList()

          // SingleChildScrollView(
          //   child: Column(
          //     children: [
          //       Padding(
          //         padding: const EdgeInsets.only(
          //             top: 10.0, left: 30, right: 30, bottom: 10),
          //         child: InkWell(
          //           onTap: () {
          //             // Navigator.push(
          //             //     context,
          //             //     MaterialPageRoute(
          //             //         builder: (context) => PMCLoginPage(
          //             //               coopList: coopList,
          //             //             )));
          //           },
          //           child: Container(
          //               height: 200,
          //               decoration: BoxDecoration(
          //                 color: Colors.white,
          //                 borderRadius: BorderRadius.circular(30),
          //                 boxShadow: [
          //                   BoxShadow(
          //                     color: Colors.grey.withOpacity(0.1),
          //                     spreadRadius: 5,
          //                     blurRadius: 7,
          //                     offset:
          //                         const Offset(0, 3), // changes position of shadow
          //                   ),
          //                 ],
          //               ),
          //               child: Column(
          //                 children: [
          //                   Padding(
          //                     padding: const EdgeInsets.only(top: 30.0, bottom: 10),
          //                     child: Center(
          //                       child: SizedBox(
          //                         height: 100,
          //                         width: 100,
          //                         child: Image.network(coopList?[0]["logoUrl"]),
          //                       ),
          //                     ),
          //                   ),
          //                   Center(
          //                       child: Text(coopList?[0]["coopName"],
          //                           style: const TextStyle(
          //                               color: Colors.black87, fontSize: 16))),
          //                 ],
          //               )),
          //         ),
          //       ),
          //       Padding(
          //         padding: const EdgeInsets.only(
          //             top: 10.0, left: 30, right: 30, bottom: 10),
          //         child: InkWell(
          //           onTap: () {},
          //           child: Container(
          //               height: 200,
          //               decoration: BoxDecoration(
          //                 color: Colors.white,
          //                 borderRadius: BorderRadius.circular(30),
          //                 boxShadow: [
          //                   BoxShadow(
          //                     color: Colors.grey.withOpacity(0.1),
          //                     spreadRadius: 5,
          //                     blurRadius: 7,
          //                     offset:
          //                         const Offset(0, 3), // changes position of shadow
          //                   ),
          //                 ],
          //               ),
          //               child: Column(
          //                 children: [
          //                   Padding(
          //                     padding: const EdgeInsets.only(top: 30.0, bottom: 10),
          //                     child: Center(
          //                       child: SizedBox(
          //                         height: 100,
          //                         width: 100,
          //                         child: Image(
          //                             image: NetworkImage(coopList?[1]["logoUrl"])),
          //                       ),
          //                     ),
          //                   ),
          //                   Center(
          //                       child: Text(coopList?[1]["coopName"],
          //                           style: const TextStyle(
          //                               color: Colors.black87, fontSize: 16))),
          //                 ],
          //               )),
          //         ),
          //       ),
          //       Padding(
          //         padding: const EdgeInsets.only(
          //             top: 10.0, left: 30, right: 30, bottom: 10),
          //         child: InkWell(
          //           onTap: () {},
          //           child: Container(
          //               height: 200,
          //               decoration: BoxDecoration(
          //                 color: Colors.white,
          //                 borderRadius: BorderRadius.circular(30),
          //                 boxShadow: [
          //                   BoxShadow(
          //                     color: Colors.grey.withOpacity(0.1),
          //                     spreadRadius: 5,
          //                     blurRadius: 7,
          //                     offset:
          //                         const Offset(0, 3), // changes position of shadow
          //                   ),
          //                 ],
          //               ),
          //               child: Column(
          //                 children: [
          //                   Padding(
          //                     padding: const EdgeInsets.only(top: 30.0, bottom: 10),
          //                     child: Center(
          //                       child: SizedBox(
          //                         height: 100,
          //                         width: 100,
          //                         child: Image(
          //                             image: NetworkImage(coopList?[2]["logoUrl"])),
          //                       ),
          //                     ),
          //                   ),
          //                   Center(
          //                       child: Text(
          //                     coopList?[2]["coopName"],
          //                     style: const TextStyle(
          //                         color: Colors.black87, fontSize: 16),
          //                   )),
          //                 ],
          //               )),
          //         ),
          //       ),
          //       Padding(
          //         padding: const EdgeInsets.only(
          //             top: 10.0, left: 30, right: 30, bottom: 10),
          //         child: InkWell(
          //           onTap: () {},
          //           child: Container(
          //               height: 200,
          //               decoration: BoxDecoration(
          //                 color: Colors.white,
          //                 borderRadius: BorderRadius.circular(30),
          //                 boxShadow: [
          //                   BoxShadow(
          //                     color: Colors.grey.withOpacity(0.1),
          //                     spreadRadius: 5,
          //                     blurRadius: 7,
          //                     offset:
          //                         const Offset(0, 3), // changes position of shadow
          //                   ),
          //                 ],
          //               ),
          //               child: Column(
          //                 children: [
          //                   Padding(
          //                     padding: const EdgeInsets.only(top: 30.0, bottom: 10),
          //                     child: Center(
          //                       child: SizedBox(
          //                         height: 100,
          //                         width: 100,
          //                         child: Image(
          //                             image: NetworkImage(coopList?[3]["logoUrl"])),
          //                       ),
          //                     ),
          //                   ),
          //                   Center(
          //                       child: Text(
          //                     coopList?[3]["coopName"],
          //                     style: TextStyle(color: Colors.black87, fontSize: 16),
          //                   )),
          //                 ],
          //               )),
          //         ),
          //       ),
          //       Padding(
          //         padding: const EdgeInsets.only(
          //             top: 10.0, left: 30, right: 30, bottom: 10),
          //         child: InkWell(
          //           onTap: () {},
          //           child: Container(
          //               height: 200,
          //               decoration: BoxDecoration(
          //                 color: Colors.white,
          //                 borderRadius: BorderRadius.circular(30),
          //                 boxShadow: [
          //                   BoxShadow(
          //                     color: Colors.grey.withOpacity(0.1),
          //                     spreadRadius: 5,
          //                     blurRadius: 7,
          //                     offset:
          //                         const Offset(0, 3), // changes position of shadow
          //                   ),
          //                 ],
          //               ),
          //               child: Column(
          //                 children: [
          //                   Padding(
          //                     padding: const EdgeInsets.only(top: 30.0, bottom: 10),
          //                     child: Center(
          //                       child: SizedBox(
          //                         height: 100,
          //                         width: 100,
          //                         child: Image(
          //                             image: NetworkImage(coopList?[4]["logoUrl"])),
          //                       ),
          //                     ),
          //                   ),
          //                   Center(
          //                       child: Text(
          //                     coopList?[4]["coopName"],
          //                     style: TextStyle(color: Colors.black87, fontSize: 16),
          //                   )),
          //                 ],
          //               )),
          //         ),
          //       ),
          //       Padding(
          //         padding: const EdgeInsets.only(
          //             top: 10.0, left: 30, right: 30, bottom: 10),
          //         child: InkWell(
          //           onTap: () {},
          //           child: Container(
          //               height: 200,
          //               decoration: BoxDecoration(
          //                 color: Colors.white,
          //                 borderRadius: BorderRadius.circular(30),
          //                 boxShadow: [
          //                   BoxShadow(
          //                     color: Colors.grey.withOpacity(0.1),
          //                     spreadRadius: 5,
          //                     blurRadius: 7,
          //                     offset:
          //                         const Offset(0, 3), // changes position of shadow
          //                   ),
          //                 ],
          //               ),
          //               child: Column(
          //                 children: [
          //                   Padding(
          //                     padding: const EdgeInsets.only(top: 30.0, bottom: 10),
          //                     child: Center(
          //                       child: SizedBox(
          //                         height: 100,
          //                         width: 100,
          //                         child: Image(
          //                             image: NetworkImage(coopList?[5]["logoUrl"])),
          //                       ),
          //                     ),
          //                   ),
          //                   Center(
          //                       child: Text(
          //                     coopList?[5]["coopName"],
          //                     style: TextStyle(color: Colors.black87, fontSize: 16),
          //                   )),
          //                 ],
          //               )),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          ),
    );
  }
}

// class Payload {
//   final String baseUrl;
//   final String logoUrl;
//   final String name;
//   Payload({required this.logoUrl, required this.baseUrl, required this.name});
//
// }
