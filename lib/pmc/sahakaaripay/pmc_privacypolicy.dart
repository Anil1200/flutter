import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/main.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_mobilebanking.dart';
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';

import '../utils/localizations.dart';
import 'models/loading.dart';

class PMCPrivacyPolicyPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? name;
  String? logoUrl;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  int? privacyPolicyType;

  PMCPrivacyPolicyPage({
    Key? key,
    this.coopList,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.name,
    this.logoUrl,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.privacyPolicyType,
  }) : super(key: key);

  @override
  _PMCPrivacyPolicyPageState createState() => _PMCPrivacyPolicyPageState(
        coopList,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        name,
        logoUrl,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
        privacyPolicyType,
      );
}

class _PMCPrivacyPolicyPageState extends State<PMCPrivacyPolicyPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? name;
  String? logoUrl;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  int? privacyPolicyType;

  _PMCPrivacyPolicyPageState(
    this.coopList,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.name,
    this.logoUrl,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.privacyPolicyType,
  );

  bool _isLoading = false;

  String privacyPolicyTitle = "";
  String privacyPolicyDescription = "";

  String privacyPolicyTitle1 = "";
  String privacyPolicyDescription1 = "";

  String privacyPolicyTitle2 = "";
  String privacyPolicyDescription2 = "";

  String privacyPolicyTitle3 = "";
  String privacyPolicyDescription3 = "";

  String privacyPolicyTitle4 = "";
  String privacyPolicyDescription4 = "";

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    print("Privacy Policy Type $privacyPolicyType");
    getJsonData();
  }

  Future<String> getJsonData() async {
    String url = "https://planetearthsolution.com/Policy/Index?id=10";
    final response = await http.get(Uri.parse(url));
    var convertDataToJson;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        convertDataToJson = json.decode(response.body);

        if (convertDataToJson != null) {
          setState(() {
            privacyPolicyTitle = convertDataToJson["ParagraphBlocks"][0]["ParagraphTitle"];
            privacyPolicyDescription = convertDataToJson["ParagraphBlocks"][0]["ParagraphContent"];

            privacyPolicyTitle1 = convertDataToJson["ParagraphBlocks"][1]["ParagraphTitle"];
            privacyPolicyDescription1 = convertDataToJson["ParagraphBlocks"][1]["ParagraphContent"];

            privacyPolicyTitle2 = convertDataToJson["ParagraphBlocks"][2]["ParagraphTitle"];
            privacyPolicyDescription2 = convertDataToJson["ParagraphBlocks"][2]["ParagraphContent"];

            privacyPolicyTitle3 = convertDataToJson["ParagraphBlocks"][3]["ParagraphTitle"];
            privacyPolicyDescription3 = convertDataToJson["ParagraphBlocks"][3]["ParagraphContent"];

            privacyPolicyTitle4 = convertDataToJson["ParagraphBlocks"][4]["ParagraphTitle"];
            privacyPolicyDescription4 = convertDataToJson["ParagraphBlocks"][4]["ParagraphContent"];
          });
        }
      });
      return "success";
    }
    return "failed";
  }

  moveToHome() {
    if (privacyPolicyType == 0) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => const SahakariScreenPage()),
              (Route<dynamic> route) => false);
    } else {
      setState(() {
        privacyPolicyType == 0;
      });
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return moveToHome();
      },
      child: Scaffold(
        body: _isLoading
          ? const Loading()
          : ScrollConfiguration(
              behavior: CustomScrollBehavior(
              androidSdkVersion: androidSdkVersion,
            ),
            child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 30.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(14.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(privacyPolicyTitle.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(privacyPolicyDescription.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 14.0,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Text(privacyPolicyTitle1.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(privacyPolicyDescription1.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 14.0,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Text(privacyPolicyTitle2.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(privacyPolicyDescription2.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 14.0,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Text(privacyPolicyTitle3.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(privacyPolicyDescription3.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 14.0,
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Text(privacyPolicyTitle4.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Text(privacyPolicyDescription4.replaceAll("\r\n", ""),
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.normal,
                              fontSize: 14.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    if (privacyPolicyType == 0) ...[
                      Center(
                        child: SizedBox(
                          width: 200,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              side: const BorderSide(width: 1.0),
                            ),
                            onPressed: () async {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          PMCMobileBankingPage(
                                            coopList: coopList,
                                            accesstoken: accesstoken,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            name: name,
                                            logoUrl: logoUrl,
                                            primaryColor: primaryColor,
                                            loginButtonTitleColor:
                                            loginButtonTitleColor,
                                            loginbuttonColor:
                                            loginbuttonColor,
                                            loginTextFieldColor:
                                            loginTextFieldColor,
                                            dasboardIconColor:
                                            dasboardIconColor,
                                            dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                            SecondaryColor:
                                            SecondaryColor,
                                          )));
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("accept")
                                  .toUpperCase(),
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 14.0),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: SizedBox(
                          width: 200,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              //side: const BorderSide(width: 1.0),
                              backgroundColor: Colors.redAccent,
                            ),
                            onPressed: () async {
                              moveToHome();
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("decline")
                                  .toUpperCase(),
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 14.0),
                            ),
                          ),
                        ),
                      ),
                    ] else ...[
                      Center(
                        child: SizedBox(
                          width: 200,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              side: const BorderSide(width: 1.0),
                            ),
                            onPressed: () async {
                              moveToHome();
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("back")
                                  .toUpperCase(),
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 14.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
      ),
    );
  }
}
