import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/cooptransfer/cooptransferpdfbill/mobile.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class ManakamanaCableCarPdfBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? Amount;
  String? paymentmessage;
  String? TransactionId;
  String? tripType;
  String? noPassenger;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  ManakamanaCableCarPdfBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.Amount,
    this.paymentmessage,
    this.TransactionId,
    this.tripType,
    this.noPassenger,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _ManakamanaCableCarPdfBillPageState createState() => _ManakamanaCableCarPdfBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        Amount,
        paymentmessage,
        TransactionId,
        tripType,
        noPassenger,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _ManakamanaCableCarPdfBillPageState extends State<ManakamanaCableCarPdfBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? Amount;
  String? paymentmessage;
  String? TransactionId;
  String? tripType;
  String? noPassenger;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _ManakamanaCableCarPdfBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.Amount,
    this.paymentmessage,
    this.TransactionId,
    this.tripType,
    this.noPassenger,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    this.nullcheck();
  }

  String? Rid;
  String? Tid;
  String? Rno;
  String? PayBillDate;

  nullcheck() {
    if (TransactionId == null) {
      Tid = "TransactionId not available.";
    } else {
      Tid = TransactionId;
    }
  }

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    /*page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));*/

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'Manakamana Cable Car Bill Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );

    final grid = PdfGrid();
    grid.columns.add(count: 2);
    PdfGridRow row;

    for (var item in manakamanaCableCarTicketList) {
      row = grid.rows.add();
      row.cells[0].value = "TransactionId:";
      row.cells[1].value = Tid.toString();

      row = grid.rows.add();
      row.cells[0].value = "Purchased By: ";
      row.cells[1].value = "${item["PurchasedBy"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Trip Type: ";
      row.cells[1].value = "${item["TripType"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Passenger Type: ";
      row.cells[1].value = "${item["PassengerTypeDesc"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Ticket No.: ";
      row.cells[1].value = "${item["TicketNo"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Bill No.: ";
      row.cells[1].value = "${item["BillNo"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Price: ";
      row.cells[1].value = "${item["Price"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Issue Date: ";
      row.cells[1].value = "${item["IssueDate"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Expiry Date: ";
      row.cells[1].value = "${item["ExpiryDate"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "";
      row.cells[1].value = "${item["ExpiryDate"].toString()}";

      row = grid.rows.add();
      row.height = 30;
      row.cells[0].value = "";
      row.cells[1].value = "";
    }

    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));

    List<int> bytes = await document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoHomePage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "Manakamana Cable Car Bill Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: manakamanaCableCarTicketList.isEmpty ? 0 : manakamanaCableCarTicketList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "TransactionId:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 50,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text("${Tid}")),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Purchased By:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 50,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["PurchasedBy"])),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Trip Type:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 80,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["TripType"])),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Passenger Type:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 40,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["PassengerTypeDesc"])),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Ticket No.:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 78,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["TicketNo"])),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Bill No.:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 98,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["BillNo"])),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Price:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 110,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["Price"])),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Issue Date:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 76,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["IssueDate"].toString())),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0, top: 30),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Text(
                                "Expiry Date:",
                                style: TextStyle(color: Colors.black87, fontSize: 14),
                              ),
                              const SizedBox(
                                width: 74,
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: Text(manakamanaCableCarTicketList[index]["ExpiryDate"].toString())),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Divider(
                          thickness: 1,
                          color: Colors.green[700],
                        ),
                      ],
                    );
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 100,
                    ),
                    TextButton(
                        onPressed: () {
                          movetoHomePage();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("home"),
                          style:
                          TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                    const SizedBox(
                      width: 60,
                    ),
                    TextButton(
                        onPressed: createPDF,
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("pdf"),
                          style:
                          TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
