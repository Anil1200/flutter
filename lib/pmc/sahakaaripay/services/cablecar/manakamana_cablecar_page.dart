import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/manakamana_cable_car_passengers_model.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/cablecar/manakamana_cablecar_pdfbill_page.dart';
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:sahakari_pay/pmc/utils/utils.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:xml2json/xml2json.dart';

import '../../../local_auth_api.dart';

class ManakamanaCableCarPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  ManakamanaCableCarPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _ManakamanaCableCarPageState createState() => _ManakamanaCableCarPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        accountName,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _ManakamanaCableCarPageState extends State<ManakamanaCableCarPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _ManakamanaCableCarPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  List<TextEditingController> passengerController1 = [];

  final TextEditingController nameController = TextEditingController();
  final TextEditingController mobilenoController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  String? dropdownValue1;
  String? dropdownValue2;
  String? dropdownValue3;
  String? dropdownValue4;
  String? dropdownValue5;
  String? dropdownValue6;
  String? dropdownValue7;
  String? dropdownValue8;
  String? dropdownValue9;
  List? CableCarService;
  String? TripTypeTwoWay;
  List? TwoWayRate;
  String? TripTypeOneWay;
  List? OneWayRate;
  String? PassengerType;
  String PassengerDescription = " ";
  List? DisplayTerms;

  int traveller = 0;

  double TotalAmount = 0.0;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });
    print("the account Name of me : ${accountName} ");
    // nullCheck();
    print("cooperative name of logged in account: ${coopName}");
    getCableCartype();
    setState(() {
      serviceType = "One Way";
    });
  }

  Future<bool> getCableCartype() async {
    String url = "${baseUrl}api/v1/manakamanacablecar/rate";
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status : ${jsonResponse}");
      setState(() {
        CableCarService = jsonResponse["Trips"];
        DisplayTerms = jsonResponse["DisplayTerms"];
        for (int i = 0; i < DisplayTerms!.length; i++) {
          PassengerDescription = PassengerDescription.toString() +
              "\n" +
              DisplayTerms![i].toString();
          print("this ihe the description bvalue:${PassengerDescription}");
        }
        print("thi ishte terms to display:${DisplayTerms?[0]}");
        TripTypeTwoWay = CableCarService?[0]["TripType"];
        TwoWayRate = CableCarService?[0]["Rates"];
        TripTypeOneWay = CableCarService?[1]["TripType"];
        OneWayRate = CableCarService?[1]["Rates"];
        print("this is the trip type:${TripTypeTwoWay} and ${TripTypeOneWay}");
        print("the required list of CableCarService: ${CableCarService}");
        print("the required list of TwoWayRate: ${TwoWayRate}");
        print("the required list of OneWayRate: ${OneWayRate}");
        for (int i = 0; i < OneWayRate!.length; i++) {
          passengerController1.add(TextEditingController());
          passengerController1[i].text = 0.toString();
        }
        _isLoading = false;
      });
      return true;
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error response:${jsonResponse}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(AppLocalizations.of(context)!
                  .localizedString("something_went_wrong")),
              actions: [
                // const Spacer(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
      return false;
    }
    return true;
  }

  int passengernumber = 0;

  String? serviceType;

  bool showBalance = false;

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : ScrollConfiguration(
                behavior: CustomScrollBehavior(
                androidSdkVersion: androidSdkVersion,
              ),
              child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 150,
                        decoration: BoxDecoration(
                          color: Color(int.parse(primaryColor.toString())),
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              left: 10.0,
                              top: 40,
                              child: IconButton(
                                  onPressed: () {
                                    movetohomepage();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back,
                                    size: 30.0,
                                    color: Colors.white,
                                  )),
                            ),
                            Positioned(
                              left: 100,
                              top: 50,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("sahakaari_pay"),
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 20,
                              top: 95,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: Image(
                                        image: AssetImage(
                                            "assets/images/wallet_icon.png")),
                                  ),
                                  const SizedBox(
                                    width: 10.0,
                                  ),
                                  Text(
                                    showBalance
                                        ? AppLocalizations.of(context)!
                                                .localizedString("rs") +
                                            "${balance.toString()}"
                                        : "xxx.xx".toUpperCase(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 180.0),
                                    child: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            if (showBalance == false) {
                                              showBalance = true;
                                            } else {
                                              showBalance = false;
                                            }
                                          });
                                        },
                                        icon: Icon(
                                          showBalance
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          size: 25.0,
                                          color: Colors.white,
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Center(
                        child: ToggleSwitch(
                          minWidth: 120.0,
                          minHeight: 50.0,
                          fontSize: 16.0,
                          cornerRadius: 20.0,
                          initialLabelIndex: serviceType == "Two Way" ? 1 : 0,
                          activeBgColor: [Colors.green],
                          activeFgColor: Colors.white,
                          inactiveBgColor: Colors.grey[350],
                          inactiveFgColor: Colors.grey[900],
                          totalSwitches: 2,
                          labels: ['One Way', 'Two Way'],
                          onToggle: (index) {
                            print('switched to: $index');
                            setState(() {
                              traveller = 0;
                              TotalAmount = 0.0;
                              for (int i = 0; i < passengerController1.length; i++) {
                                passengerController1[i].text = 0.toString();
                              }
                              if (index == 0) {
                                serviceType = "One Way";
                              } else {
                                serviceType = "Two Way";
                              }
                            });
                          },
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 25.0, right: 25, top: 20),
                        child: Center(
                          child: Container(
                            height: 167,
                            width: MediaQuery.of(context).size.width - 40,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.black12),
                                color: Colors.white),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 15.0, right: 10),
                              child: Text(
                                PassengerDescription.toString(),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.black87),
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 20),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.only(
                                      left: 50.0, right: 30),
                                  child: Container(
                                    height: 60,
                                    width:
                                        MediaQuery.of(context).size.width - 120,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.3),
                                            spreadRadius: 1,
                                            blurRadius: 1,
                                            offset: const Offset(0,
                                                2), // changes position of shadow
                                          ),
                                        ]),
                                    child: TextButton(
                                      onPressed: () {
                                        showMaterialModalBottomSheet(
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(30),
                                                    topRight:
                                                        Radius.circular(30))),
                                            isDismissible: false,
                                            context: context,
                                            builder: (context) {
                                              return SingleChildScrollView(
                                                child: Column(
                                                  children: [
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 25,
                                                              right: 25,
                                                              top: 20.0),
                                                      child: Text(
                                                        "Add Passenger",
                                                        style: TextStyle(
                                                            fontSize: 16,
                                                            color:
                                                                Colors.black87),
                                                      ),
                                                    ),
                                                    Padding(
                                                        padding:
                                                            const EdgeInsets.only(
                                                                left: 25,
                                                                right: 25,
                                                                top: 10.0),
                                                        child: ListView.builder(
                                                            physics:
                                                                NeverScrollableScrollPhysics(),
                                                            shrinkWrap: true,
                                                            itemCount: serviceType ==
                                                                    'One Way'
                                                                ? OneWayRate ==
                                                                        null
                                                                    ? 0
                                                                    : OneWayRate
                                                                        ?.length
                                                                : TwoWayRate ==
                                                                        null
                                                                    ? 0
                                                                    : TwoWayRate
                                                                        ?.length,
                                                            itemBuilder:
                                                                (BuildContext
                                                                        context,
                                                                    int index) {
                                                              return Row(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Container(
                                                                      width: MediaQuery.of(context).size.width /
                                                                              2 -
                                                                          25,
                                                                      child:
                                                                          Column(
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment
                                                                                .start,
                                                                        children: [
                                                                          Text(serviceType ==
                                                                                  'One Way'
                                                                              ? "${OneWayRate?[index]["PassengerType"].toString()}"
                                                                              : "${TwoWayRate?[index]["PassengerType"].toString()}"),
                                                                          SizedBox(
                                                                            height:
                                                                                10,
                                                                          ),
                                                                          Text(
                                                                            serviceType == 'One Way'
                                                                                ? "${OneWayRate?[index]["Price"].toString()}"
                                                                                : "${TwoWayRate?[index]["Price"].toString()}",
                                                                            style:
                                                                                TextStyle(color: Colors.black87),
                                                                          ),
                                                                          SizedBox(
                                                                              height:
                                                                                  20),
                                                                        ],
                                                                      )),
                                                                  Container(
                                                                    width: MediaQuery.of(context)
                                                                                .size
                                                                                .width /
                                                                            2 -
                                                                        25,
                                                                    child: Row(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        TextButton(
                                                                          onPressed:
                                                                              () {
                                                                            passengernumber = int.parse(passengerController1[index]
                                                                                .text
                                                                                .toString());
                                                                            if (passengernumber >
                                                                                0) {
                                                                              passengernumber =
                                                                                  passengernumber - 1;
                                                                            } else {
                                                                              passengernumber =
                                                                                  0;
                                                                            }

                                                                            print(
                                                                                "this ishe new number:${passengernumber}");
                                                                            passengerController1[index].text =
                                                                                passengernumber.toString();
                                                                          },
                                                                          child:
                                                                              Icon(
                                                                            Icons
                                                                                .remove,
                                                                            size:
                                                                                20,
                                                                            color:
                                                                                Colors.black87,
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          height:
                                                                              50,
                                                                          width:
                                                                              50,
                                                                          child:
                                                                              TextField(
                                                                            readOnly:
                                                                                true,
                                                                            textAlign:
                                                                                TextAlign.center,
                                                                            controller:
                                                                                passengerController1[index],
                                                                            decoration: InputDecoration(
                                                                                border: OutlineInputBorder(
                                                                              borderRadius:
                                                                                  BorderRadius.circular(10.0),
                                                                            )),
                                                                          ),
                                                                        ),
                                                                        TextButton(
                                                                          onPressed:
                                                                              () {
                                                                            passengernumber = int.parse(passengerController1[index]
                                                                                .text
                                                                                .toString());
                                                                            if (passengernumber <
                                                                                10) {
                                                                              passengernumber =
                                                                                  passengernumber + 1;
                                                                            }
                                                                            print(
                                                                                "this ishe new number:${passengernumber}");
                                                                            passengerController1[index].text =
                                                                                passengernumber.toString();
                                                                          },
                                                                          child:
                                                                              Icon(
                                                                            Icons
                                                                                .add,
                                                                            size:
                                                                                20,
                                                                            color:
                                                                                Colors.black87,
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              );
                                                            })),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              120,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.circular(
                                                                10),
                                                        color: Color(int.parse(
                                                            primaryColor
                                                                .toString())),
                                                      ),
                                                      child: TextButton(
                                                        onPressed: () {
                                                          traveller = 0;
                                                          TotalAmount = 0.0;
                                                          for (int i = 0;
                                                              i <
                                                                  passengerController1
                                                                      .length;
                                                              i++) {
                                                            setState(() {
                                                              double price = serviceType == "One Way"
                                                                  ? (double.parse(OneWayRate![i]["Price"].toString()))
                                                                  : (double.parse(TwoWayRate![i]["Price"].toString()));
                                                              traveller = traveller +
                                                                  int.parse(
                                                                      passengerController1[
                                                                              i]
                                                                          .text
                                                                          .toString());
                                                              TotalAmount += double.parse(
                                                                      passengerController1[
                                                                              i]
                                                                          .text
                                                                          .toString()) * price;
                                                              print(
                                                                  "this us the TotalAmount:${TotalAmount}");
                                                            });
                                                          }
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: Text(
                                                          "Apply",
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              color:
                                                                  Colors.white),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                  ],
                                                ),
                                              );
                                            });
                                      },
                                      child: Text(traveller == 0
                                          ? "Select Travellers"
                                          : "${traveller.toString()} Travellers"),
                                    ),
                                  )),
                              if (traveller != 0) ...[
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 60.0, top: 20),
                                  child: Text(
                                    "Total Amount: Rs. ${TotalAmount.toString()}",
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                              Form(
                                  key: globalFormKey,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, top: 20),
                                            child: Container(
                                                width: 320,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10.0),
                                                ),
                                                child: TextFormField(
                                                  controller: nameController,
                                                  maxLength: 30,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  validator: (value) => value!
                                                          .isEmpty
                                                      ? AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "enter_receivers_name")
                                                      : null,
                                                  onSaved: (value) =>
                                                      nameController,
                                                  decoration: InputDecoration(
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10.0),
                                                    ),
                                                    labelText: AppLocalizations
                                                            .of(context)!
                                                        .localizedString(
                                                            "enter_receivers_name"),
                                                    labelStyle: const TextStyle(
                                                        color: Colors.green),
                                                    counterText: "",
                                                    icon: const Icon(
                                                      Icons.person,
                                                      color: Colors.green,
                                                      size: 20.0,
                                                    ),
                                                  ),
                                                ))),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0, top: 10),
                                          child: Container(
                                            width: 320,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            child: TextFormField(
                                              controller: mobilenoController,
                                              maxLength: 10,
                                              keyboardType: TextInputType.number,
                                              validator: (value) => value!
                                                          .isEmpty ||
                                                      validateMobile(
                                                              mobilenoController
                                                                  .text
                                                                  .toString()) ==
                                                          false
                                                  ? AppLocalizations.of(context)!
                                                      .localizedString(
                                                          "mobile_number")
                                                  : null,
                                              onSaved: (value) =>
                                                  mobilenoController,
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(10.0),
                                                ),
                                                labelText:
                                                    AppLocalizations.of(context)!
                                                        .localizedString(
                                                            "mobile_number"),
                                                labelStyle: const TextStyle(
                                                    color: Colors.green),
                                                counterText: "",
                                                icon: const Icon(
                                                  Icons.phone_android_sharp,
                                                  color: Colors.green,
                                                  size: 20.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, top: 10),
                                            child: Container(
                                                width: 320,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10.0),
                                                ),
                                                child: TextFormField(
                                                  controller: emailController,
                                                  maxLength: 100,
                                                  keyboardType:
                                                      TextInputType.emailAddress,
                                                  validator: (value) => value!
                                                              .isEmpty ||
                                                          validateEmail(
                                                                  emailController
                                                                      .text
                                                                      .toString()) ==
                                                              false
                                                      ? AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "enter_valid_email")
                                                      : null,
                                                  onSaved: (value) =>
                                                      emailController,
                                                  decoration: InputDecoration(
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10.0),
                                                    ),
                                                    labelText:
                                                        AppLocalizations.of(
                                                                context)!
                                                            .localizedString(
                                                                "enter_email"),
                                                    labelStyle: const TextStyle(
                                                        color: Colors.green),
                                                    counterText: "",
                                                    icon: const Icon(
                                                      Icons.money,
                                                      color: Colors.green,
                                                      size: 20.0,
                                                    ),
                                                  ),
                                                ))),
                                      ])),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 25.0, top: 30),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: Color(int.parse(
                                              loginbuttonColor.toString())),
                                          boxShadow: [
                                            BoxShadow(
                                              color: Colors.grey.withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: const Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ]),
                                      height: 50,
                                      width:
                                          MediaQuery.of(context).size.width - 120,
                                      child: TextButton(
                                        onPressed: () {
                                          if (validateAndSave()) {
                                            if (showBiometricbuttonTransaction) {
                                              transactionPinMessageBiometric();
                                            } else {
                                              transactionPinMessage();
                                            }
                                          } else {
                                            //////////
                                          }
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("pay"),
                                          style: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
            ),
      ),
    );
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${TotalAmount} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            payManakamanaCableCarBill();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(AppLocalizations.of(context)!
                                        .localizedString("enter_valid_pin")),
                                    actions: [
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${TotalAmount} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      payManakamanaCableCarBill();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  String? code;
  String? TransactionId;
  String? data;
  String? Message;

  List<ManakamanaCableCarPassengersModel> passengers = [];

  List? ticketList;

  payManakamanaCableCarBill() async {
    passengers.clear();
    manakamanaCableCarTicketList.clear();

    if (serviceType == "One Way") {
      for (int i = 0; i < OneWayRate!.length; i++) {
        if (passengerController1[i].text != "0") {
          passengers.add(ManakamanaCableCarPassengersModel(
            TripType: "One Way",
            PassengerType: OneWayRate![i]["PassengerType"].toString(),
            NoOfPassenger: int.parse(passengerController1[i].text.toString()),
            Price: double.parse(OneWayRate![i]["Price"].toString())
          ));
        }
      }
    } else if (serviceType == "Two Way") {
      for (int i = 0; i < TwoWayRate!.length; i++) {
        if (passengerController1[i].text != "0") {
          passengers.add(ManakamanaCableCarPassengersModel(
              TripType: "Two Way",
              PassengerType: TwoWayRate![i]["PassengerType"].toString(),
              NoOfPassenger: int.parse(passengerController1[i].text.toString()),
              Price: double.parse(TwoWayRate![i]["Price"].toString())
          ));
        }
      }
    }

    String url = "${baseUrl}api/v1/manakamanacablecar/issueticket";
    Map body = {
      "OperatorCode": "72",
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
      "ContactPerson": nameController.text.toString().trim(),
      "ContactEmail": emailController.text.toString().trim(),
      "ContactNo": mobilenoController.text.toString().trim(),
      "Amount": TotalAmount.toString(),
      "Passengers": passengers,
    };
    mpincontroller.clear();
    var confirmbody = json.encode(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: confirmbody, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${accesstoken}"
    });
    print(body);

    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          code = jsonResponse["Code"];
          TransactionId = jsonResponse["TransactionId"];
          Message = jsonResponse["Message"];
          data = jsonResponse["Data"];
          if (code.toString().toLowerCase() == "000".toString().toLowerCase()) {
            final Xml2Json xml2Json = Xml2Json();
            /*int n = 1;
            if (n > 1) {
              xml2Json.parse('<Tickets><Ticket><PurchasedBy>aaaaa/ 9818000000</PurchasedBy><TripType>One Way</TripType><PassengerTypeDesc>Normal</PassengerTypeDesc><TicketNo>4TEST10000948</TicketNo><BillNo>T4TEST10000494</BillNo><Price>395.00</Price><IssueDate>2022-07-26 19:11:37</IssueDate><ExpiryDate>2022-07-28 00:00:00</ExpiryDate><QRCode>8a7800954TEST10000948fd8cd7a3</QRCode><BarCode>8a7800954TEST10000948fd8cd7a3</BarCode><TicketURL>https://testmerchant.prabhupay.com/Transaction/Receipt/71822?p=9fa59841aeae4b13abad8532a412af5f697cce28743c406591a8d4ec1cf5a32c</TicketURL></Ticket><Ticket><PurchasedBy>aaaaa/ 9818000000</PurchasedBy><TripType>One Way</TripType><PassengerTypeDesc>Normal</PassengerTypeDesc><TicketNo>4TEST10000948</TicketNo><BillNo>T4TEST10000494</BillNo><Price>395.00</Price><IssueDate>2022-07-26 19:11:37</IssueDate><ExpiryDate>2022-07-28 00:00:00</ExpiryDate><QRCode>8a7800954TEST10000948fd8cd7a3</QRCode><BarCode>8a7800954TEST10000948fd8cd7a3</BarCode><TicketURL>https://testmerchant.prabhupay.com/Transaction/Receipt/71822?p=9fa59841aeae4b13abad8532a412af5f697cce28743c406591a8d4ec1cf5a32c</TicketURL></Ticket></Tickets>');
            } else {
              xml2Json.parse('<Tickets><Ticket><PurchasedBy>aaaaa/ 9818000000</PurchasedBy><TripType>One Way</TripType><PassengerTypeDesc>Normal</PassengerTypeDesc><TicketNo>4TEST10000948</TicketNo><BillNo>T4TEST10000494</BillNo><Price>395.00</Price><IssueDate>2022-07-26 19:11:37</IssueDate><ExpiryDate>2022-07-28 00:00:00</ExpiryDate><QRCode>8a7800954TEST10000948fd8cd7a3</QRCode><BarCode>8a7800954TEST10000948fd8cd7a3</BarCode><TicketURL>https://testmerchant.prabhupay.com/Transaction/Receipt/71822?p=9fa59841aeae4b13abad8532a412af5f697cce28743c406591a8d4ec1cf5a32c</TicketURL></Ticket></Tickets>');
            }*/
            xml2Json.parse(data!);
            var jsonString = xml2Json.toParker();
            var TicketData = jsonDecode(jsonString);

            if (serviceType == "Two Way") {
              ticketList = TicketData["Tickets"]["Ticket"];
              manakamanaCableCarTicketList = ticketList!;
            } else {
              if (traveller > 1) {
                ticketList = TicketData["Tickets"]["Ticket"];
                manakamanaCableCarTicketList = ticketList!;
              } else {
                var ticketMap = {
                  'PurchasedBy': TicketData["Tickets"]["Ticket"]["PurchasedBy"],
                  'TripType': TicketData["Tickets"]["Ticket"]["TripType"],
                  'PassengerTypeDesc': TicketData["Tickets"]["Ticket"]["PassengerTypeDesc"],
                  'TicketNo': TicketData["Tickets"]["Ticket"]["TicketNo"],
                  'BillNo': TicketData["Tickets"]["Ticket"]["BillNo"],
                  'Price': TicketData["Tickets"]["Ticket"]["Price"],
                  'IssueDate': TicketData["Tickets"]["Ticket"]["IssueDate"],
                  'ExpiryDate': TicketData["Tickets"]["Ticket"]["ExpiryDate"],
                  'QRCode': TicketData["Tickets"]["Ticket"]["QRCode"],
                  'BarCode': TicketData["Tickets"]["Ticket"]["BarCode"],
                  'TicketURL': TicketData["Tickets"]["Ticket"]["TicketURL"],
                };
                manakamanaCableCarTicketList.add(ticketMap);
              }
            }

            Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
              setState(() {
                _isLoading = false;
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Row(
                      children: [
                        const Icon(
                          Icons.verified,
                          color: Colors.green,
                          size: 40,
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Text(
                          jsonResponse["Message"],
                          style: const TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ],
                    )));
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ManakamanaCableCarPdfBillPage(
                          coopList: coopList,
                          userId: userId,
                          accesstoken: accesstoken,
                          balance: balance,
                          baseUrl: baseUrl,
                          accountno: accountno,
                          tripType: serviceType,
                          noPassenger: traveller.toString(),
                          paymentmessage: Message,
                          TransactionId: TransactionId,
                          primaryColor: primaryColor,
                          loginButtonTitleColor: loginButtonTitleColor,
                          loginbuttonColor: loginbuttonColor,
                          loginTextFieldColor: loginTextFieldColor,
                          dasboardIconColor: dasboardIconColor,
                          dashboardTopTitleColor: dashboardTopTitleColor,
                          SecondaryColor: SecondaryColor,
                        )));
              });
            });
          } else {
            setState(() {
              _isLoading = false;
            });
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("error_alert"),
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 50.0,
                            )
                          ],
                        )),
                    content: Text(
                      AppLocalizations.of(context)!
                          .localizedString("process_error"),
                      style: TextStyle(fontSize: 14),
                    ),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("okay"),
                          ))
                    ],
                  );
                });
          }
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateEmail(String value) {
    String pattern =
        r'(^[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)*$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateMobile(String value) {
    String pattern =
        r'(^(961|962|972|974|975|976|980|981|982|984|985|986|988)?[0-9]{7}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }
}
