import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/lifeinsurance/nepallife/nepallife_insurancebillpage.dart';

import '../../../../../utils/localizations.dart';
import '../../../../models/loading.dart';

class NepalLifeInsurancePage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  NepalLifeInsurancePage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _NepalLifeInsurancePageState createState() => _NepalLifeInsurancePageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _NepalLifeInsurancePageState extends State<NepalLifeInsurancePage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _NepalLifeInsurancePageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  String? plan;
  String? message;
  String? policyno;
  String? PolicyName;
  String? insuranceduedate;
  String? billamount;

  bool showBalance = false;

  Future<String> getjsonData() async {
    String url = "${baseUrl}api/v1/insurancepremium/life/checkpolicy";
    Map body = {
      "OperatorCode": "20",
      "PolicyNumber": policynoController.text.toString().trim(),
      "Dob": dobController.text.toString().trim()
    };
    final response = await http.post(
      Uri.parse(url),
      body: body,
      headers: {'Authorization': "Bearer ${accesstoken}"},
    );
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            plan = jsonResponse["Plan"];
            message = jsonResponse["Message"];
            policyno = jsonResponse["PolicyNo"];
            PolicyName = jsonResponse["Name"];
            billamount = jsonResponse["BillAmount"];
            insuranceduedate = jsonResponse["DueDate"];

            print("this is the  plan: ${plan}");
            if (message.toString().toLowerCase() ==
                "Success".toString().toLowerCase()) {
              setState(() {
                _isLoading = false;
              });
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NepalLifeInsuranceBillPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            balance: balance,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            plan: plan,
                            policyno: policyno,
                            PolicyName: PolicyName,
                            billamount: billamount,
                            insuranceduedate: insuranceduedate,
                            dateofbirth: dobController.text,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor: dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
            }
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => InsurancePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  bool _isLoading = false;

  final GlobalKey<FormState> globalFormKey = GlobalKey();

  final TextEditingController policynoController = TextEditingController();
  final TextEditingController dobController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            top: 40,
                            child: IconButton(
                                onPressed: () {
                                  movetohomepage();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                          Positioned(
                            left: 100,
                            top: 50,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sahakaari_pay"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 95,
                            child: Row(
                              children: [
                                const SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: Image(
                                      image: AssetImage(
                                          "assets/images/wallet_icon.png")),
                                ),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  showBalance
                                      ? AppLocalizations.of(context)!
                                              .localizedString("rs") +
                                          "${balance.toString()}"
                                      : "xxx.xx".toUpperCase(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 180.0),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          if (showBalance == false) {
                                            showBalance = true;
                                          } else {
                                            showBalance = false;
                                          }
                                        });
                                      },
                                      icon: Icon(
                                        showBalance
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        size: 25.0,
                                        color: Colors.white,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("nepal_life"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Form(
                              key: globalFormKey,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 25.0, right: 25),
                                    child: TextFormField(
                                      controller: policynoController,
                                      maxLength: 40,
                                      keyboardType: TextInputType.number,
                                      validator: (value) => value!.isEmpty
                                          ? AppLocalizations.of(context)!
                                              .localizedString("policy_no")
                                          : null,
                                      onSaved: (value) => policyno,
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        labelText: AppLocalizations.of(context)!
                                            .localizedString("policy_no"),
                                        labelStyle:
                                            TextStyle(color: Colors.green),
                                        counterText: "",
                                        icon: Icon(
                                          Icons.confirmation_number,
                                          color: Colors.green,
                                          size: 20.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 25.0, right: 25, top: 30),
                                    child: TextFormField(
                                      controller: dobController,
                                      maxLength: 10,
                                      keyboardType: TextInputType.text,
                                      validator: (value) => value!.isEmpty ||
                                              validateDate(dobController.text
                                                      .toString()) ==
                                                  false
                                          ? AppLocalizations.of(context)!
                                              .localizedString("dob_format")
                                          : null,
                                      onSaved: (value) => insuranceduedate,
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        labelText: AppLocalizations.of(context)!
                                            .localizedString("dob"),
                                        labelStyle:
                                            TextStyle(color: Colors.green),
                                        counterText: "",
                                        icon: Icon(
                                          Icons.calendar_today,
                                          color: Colors.green,
                                          size: 20.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 30,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: Color(int.parse(
                                            loginbuttonColor.toString())),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 5,
                                            blurRadius: 7,
                                            offset: const Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ]),
                                    height: 50,
                                    width: MediaQuery.of(context).size.width - 40,
                                    child: TextButton(
                                      onPressed: () {
                                        if (validateAndSave()) {
                                          setState(() {
                                            _isLoading = true;
                                          });
                                          getjsonData();
                                        } else {
                                          setState(() {
                                            _isLoading = false;
                                          });
                                        }
                                      },
                                      child: Text(
                                        AppLocalizations.of(context)!
                                            .localizedString("get_details"),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateDate(String value) {
    String pattern = r'([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }
}
