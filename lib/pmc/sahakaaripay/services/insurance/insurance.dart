import 'dart:core';

import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/lifeinsurance/jyotilife/jyotilife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/lifeinsurance/prabhulife/prabhulife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/lifeinsurance/reliancelife/reliancelife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/lifeinsurance/suryalife/suryalife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/lifeinsurance/unionlife/unionlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/ajod/ajodnonlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/general/generalnonlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/himalayan/himalayannonlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/nlg/nlgnonlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/prabhu/prabhunonlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/premier/premiernonlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/sagarmatha/sagarmathanonlife_insurance.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/insurance/nonlife/siddhartha/siddharthanonlife_insurance.dart';

import '../../../utils/localizations.dart';
import '../../pmc_homepage.dart';
import 'lifeinsurance/nepallife/nepallife_insurance.dart';

class InsurancePage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  InsurancePage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<InsurancePage> createState() => _InsurancePagePageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _InsurancePagePageState extends State<InsurancePage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _InsurancePagePageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 70,
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetohomepage();
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
          ),
          title:
              Text(AppLocalizations.of(context)!.localizedString("insurance")),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NepalLifeInsurancePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/nepallife_logo.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("nepal_life"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      RelianceLifeInsurancePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/reliance1.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("reliance_life"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PrabhuLifeInsurancePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/prabhu.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("prabhu_life"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SuryaLifeInsurancePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/surya.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("surya_life"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => JyotiLifeInsurancePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/jyoti.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("jyoti_life"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => UnionLifeInsurancePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/union.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("union_life"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PrabhuNonLifeInsurancePage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                    loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor:
                                    loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                    dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/prabhu_insurance.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("prabhu"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PremierNonLifeInsurancePage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                    loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor:
                                    loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                    dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/premier.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("premier"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SagarmathaNonLifeInsurancePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/sagarmatha.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("sagarmatha"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NLGNonLifeInsurancePage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                    loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor:
                                    loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                    dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/nlg.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("nlg"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AjodNonLifeInsurancePage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                    loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor:
                                    loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                    dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/ajod.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("ajod"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => GeneralNonLifeInsurancePage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                    loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor:
                                    loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                    dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/general.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("general"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SiddharthaNonLifeInsurancePage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                    loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor:
                                    loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                    dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/siddartha.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("siddhartha"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HimalayanNonLifeInsurancePage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                    loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor:
                                    loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                    dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    "assets/images/insurance/himalayan.png"),
                                width: 60,
                                height: 60,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("himalayan"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
