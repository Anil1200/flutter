import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/onepg/onepg_payment.dart';

import '../../../utils/localizations.dart';
import '../../models/loading.dart';
import '../../pmc_homepage.dart';

class OnePGListPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  OnePGListPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<OnePGListPage> createState() => _OnePGListPageState(
    coopList,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
  );
}

class _OnePGListPageState extends State<OnePGListPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _OnePGListPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      );

  bool _isLoading = false;

  List? onepgList;

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    getOnePGList();
  }

  Future<String> getOnePGList() async {
    var jsonResponse;
    String url = "${baseUrl}api/v1/onepg/getpaymentinstrumentdetails";
    var response = await http.post(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        onepgList = jsonResponse['Data'];
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(
                  jsonResponse["Message"]),
                actions: [
                  // Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
              coopList: coopList,
              userId: userId,
              accesstoken: accesstoken,
              baseUrl: baseUrl,
              accountno: accountno,
              primaryColor: primaryColor,
              loginButtonTitleColor: loginButtonTitleColor,
              loginbuttonColor: loginbuttonColor,
              loginTextFieldColor: loginTextFieldColor,
              dasboardIconColor: dasboardIconColor,
              dashboardTopTitleColor: dashboardTopTitleColor,
              SecondaryColor: SecondaryColor,
            )),
            (Route<dynamic> route) => false);
  }

  bool showBalance = false;

  Widget _buildListViewMBanking(List<dynamic> dataList) {
    if (dataList.isEmpty) {
      return const Center(
        child: Text('No data'),
      );
    }
    List<dynamic> dataListFilter = dataList.where((x) => x['BankType'] == "MBanking").toList();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
        itemCount: dataListFilter.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OnePGPaymentPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                    InstrumentCode: dataListFilter[index]['InstrumentCode'],
                    InstitutionName: dataListFilter[index]['InstitutionName'],
                  ),
                ),
              );
            },
            child: ListTile(
              leading: Image.network(
                dataListFilter[index]['LogoUrl'],
                height: 50,
              ),
              title: Text(
                dataListFilter[index]['InstitutionName'],
              ),
            ),
          );
        }
      ),
      /*child: GridView.builder(
        itemCount: dataListFilter.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OnePGPaymentPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                    InstrumentCode: dataListFilter[index]['InstrumentCode'],
                  ),
                ),
              );
            },
            child: Column(
              children: [
                dataListFilter[index]['LogoUrl'] != null
                    ? Image.network(
                  dataListFilter[index]['LogoUrl'],
                  height: 50,
                )
                    : Container(),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  dataListFilter[index]['InstitutionName'] ?? '',
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          );
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 1.3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
      ),*/
    );
  }

  Widget _buildListViewEBanking(List<dynamic> dataList) {
    if (dataList.isEmpty) {
      return const Center(
        child: Text('No data'),
      );
    }
    List<dynamic> dataListFilter = dataList.where((x) => x['BankType'] == "EBanking").toList();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
          itemCount: dataListFilter.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => OnePGPaymentPage(
                      coopList: coopList,
                      userId: userId,
                      accesstoken: accesstoken,
                      balance: balance,
                      baseUrl: baseUrl,
                      accountno: accountno,
                      primaryColor: primaryColor,
                      loginButtonTitleColor: loginButtonTitleColor,
                      loginbuttonColor: loginbuttonColor,
                      loginTextFieldColor: loginTextFieldColor,
                      dasboardIconColor: dasboardIconColor,
                      dashboardTopTitleColor: dashboardTopTitleColor,
                      SecondaryColor: SecondaryColor,
                      InstrumentCode: dataListFilter[index]['InstrumentCode'],
                      InstitutionName: dataListFilter[index]['InstitutionName'],
                    ),
                  ),
                );
              },
              child: ListTile(
                leading: Image.network(
                  dataListFilter[index]['LogoUrl'],
                  height: 50,
                ),
                title: Text(
                  dataListFilter[index]['InstitutionName'],
                ),
              ),
            );
          }
      ),
      /*child: GridView.builder(
        itemCount: dataListFilter.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OnePGPaymentPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                    InstrumentCode: dataListFilter[index]['InstrumentCode'],
                  ),
                ),
              );
            },
            child: Column(
              children: [
                dataListFilter[index]['LogoUrl'] != null
                    ? Image.network(
                  dataListFilter[index]['LogoUrl'],
                  height: 50,
                )
                    : Container(),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  dataListFilter[index]['InstitutionName'] ?? '',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          );
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 1.3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
      ),*/
    );
  }

  Widget _buildListViewGateway(List<dynamic> dataList) {
    if (dataList.isEmpty) {
      return const Center(
        child: Text('No data'),
      );
    }
    List<dynamic> dataListFilter = dataList.where((x) => x['BankType'] == "CheckoutGateway").toList();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
          itemCount: dataListFilter.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => OnePGPaymentPage(
                      coopList: coopList,
                      userId: userId,
                      accesstoken: accesstoken,
                      balance: balance,
                      baseUrl: baseUrl,
                      accountno: accountno,
                      primaryColor: primaryColor,
                      loginButtonTitleColor: loginButtonTitleColor,
                      loginbuttonColor: loginbuttonColor,
                      loginTextFieldColor: loginTextFieldColor,
                      dasboardIconColor: dasboardIconColor,
                      dashboardTopTitleColor: dashboardTopTitleColor,
                      SecondaryColor: SecondaryColor,
                      InstrumentCode: dataListFilter[index]['InstrumentCode'],
                      InstitutionName: dataListFilter[index]['InstitutionName'],
                    ),
                  ),
                );
              },
              child: ListTile(
                leading: Image.network(
                  dataListFilter[index]['LogoUrl'],
                  height: 50,
                ),
                title: Text(
                  dataListFilter[index]['InstitutionName'],
                ),
              ),
            );
          }
      ),
      /*child: GridView.builder(
        itemCount: dataListFilter.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OnePGPaymentPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                    InstrumentCode: dataListFilter[index]['InstrumentCode'],
                  ),
                ),
              );
            },
            child: Column(
              children: [
                dataListFilter[index]['LogoUrl'] != null
                    ? Image.network(
                  dataListFilter[index]['LogoUrl'],
                  height: 50,
                )
                    : Container(),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  dataListFilter[index]['InstitutionName'] ?? '',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          );
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 1.3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
      ),*/
    );
  }

  Widget _buildListViewCard(List<dynamic> dataList) {
    if (dataList.isEmpty) {
      return const Center(
        child: Text('No data'),
      );
    }
    List<dynamic> dataListFilter = dataList.where((x) => x['BankType'] == "checkoutcard").toList();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
          itemCount: dataListFilter.length,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => OnePGPaymentPage(
                      coopList: coopList,
                      userId: userId,
                      accesstoken: accesstoken,
                      balance: balance,
                      baseUrl: baseUrl,
                      accountno: accountno,
                      primaryColor: primaryColor,
                      loginButtonTitleColor: loginButtonTitleColor,
                      loginbuttonColor: loginbuttonColor,
                      loginTextFieldColor: loginTextFieldColor,
                      dasboardIconColor: dasboardIconColor,
                      dashboardTopTitleColor: dashboardTopTitleColor,
                      SecondaryColor: SecondaryColor,
                      InstrumentCode: dataListFilter[index]['InstrumentCode'],
                      InstitutionName: dataListFilter[index]['InstitutionName'],
                    ),
                  ),
                );
              },
              child: ListTile(
                leading: Image.network(
                  dataListFilter[index]['LogoUrl'],
                  height: 50,
                ),
                title: Text(
                  dataListFilter[index]['InstitutionName'],
                ),
              ),
            );
          }
      ),
      /*child: GridView.builder(
        itemCount: dataListFilter.length,
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => OnePGPaymentPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                    InstrumentCode: dataListFilter[index]['InstrumentCode'],
                  ),
                ),
              );
            },
            child: Column(
              children: [
                dataListFilter[index]['LogoUrl'] != null
                    ? Image.network(
                  dataListFilter[index]['LogoUrl'],
                  height: 50,
                )
                    : Container(),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  dataListFilter[index]['InstitutionName'] ?? '',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          );
        },
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          childAspectRatio: 1.3,
          crossAxisSpacing: 5,
          mainAxisSpacing: 5,
        ),
      ),*/
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetohomepage();
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          title: const Text("SahakaariPay",
              style: TextStyle(
                color: Colors.white,
              )),
          centerTitle: true,
        ),
        body: _isLoading
          ? const Loading()
          : DefaultTabController(
          initialIndex: 0,
          length: 3,
          child: Column(
            children: [
              Container(
                color: Color(int.parse(primaryColor.toString())),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: TabBar(
                    isScrollable: true,
                    //indicatorColor: Color(int.parse(SecondaryColor.toString())),
                    indicator: BoxDecoration(
                      color: Color(int.parse(SecondaryColor.toString())),
                    ),
                    labelColor: Colors.black54,
                    unselectedLabelColor: Colors.white,
                    tabs: const [
                      Tab(
                        child: Text(
                          'Mobile Banking',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Tab(
                        child: Text(
                          'Internet Banking',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      Tab(
                        child: Text(
                          'Wallet',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      /*Tab(
                        child: Text(
                          'Card',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),*/
                    ],
                  ),
                ),
              ),
              Expanded(
                child: TabBarView(
                  children: <Widget>[
                    _buildListViewMBanking(onepgList!),
                    _buildListViewEBanking(onepgList!),
                    _buildListViewGateway(onepgList!),
                    //_buildListViewCard(onepgList!),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
