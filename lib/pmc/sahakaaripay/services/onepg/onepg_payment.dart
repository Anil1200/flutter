import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/onepg/onepg_payment_redirect.dart';

import '../../../constants.dart';
import '../../../local_auth_api.dart';
import '../../../utils/localizations.dart';
import '../../models/loading.dart';
import '../../pmc_homepage.dart';

class OnePGPaymentPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? InstrumentCode;
  String? InstitutionName;

  OnePGPaymentPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.InstrumentCode,
    this.InstitutionName,
  }) : super(key: key);

  @override
  State<OnePGPaymentPage> createState() => _OnePGPaymentPageState(
    coopList,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
    InstrumentCode,
    InstitutionName,
  );
}

class _OnePGPaymentPageState extends State<OnePGPaymentPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? InstrumentCode;
  String? InstitutionName;
  _OnePGPaymentPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.InstrumentCode,
      this.InstitutionName,
      );

  bool _isLoading = false;

  String? PaymentUrl = "";
  String? PaymentAmount = "";
  String? PaymentMerchantId = "";
  String? PaymentMerchantName = "";
  String? PaymentMerchantTxnId = "";
  String? PaymentProcessId = "";

  String? serviceCharges;

  @override
  void initState() {
    super.initState();
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 20),
                    child: Text(
                      "Rs.${amountcontroller.text.toString()} will be loaded with extra Service Charge of Rs.${serviceCharges.toString()} ",
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          setState(() {
                            _isLoading = false;
                          });
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postOnePGUrl();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 20),
                    child: Text(
                      "Rs.${amountcontroller.text.toString()} will be loaded with extra Service Charge of Rs.${serviceCharges.toString()} ",
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      postOnePGUrl();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  Future<String> postOnePGUrl() async {
    var jsonResponse;
    String url = "${baseUrl}api/v1/onepg/getprocessid";
    Map body = {
      "Amount": amountcontroller.text.toString().trim(),
      "ServiceCharge": serviceCharges.toString().trim(),
      "Remarks": remarkscontroller.text.toString().trim(),
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
    };
    mpincontroller.clear();
    print(body);
    var response = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });
          PaymentUrl = jsonResponse["PaymentUrl"];
          PaymentAmount = jsonResponse["PaymentAmount"];
          PaymentMerchantId = jsonResponse["PaymentMerchantId"];
          PaymentMerchantName = jsonResponse["PaymentMerchantName"];
          PaymentMerchantTxnId = jsonResponse["PaymentMerchantTxnId"];
          PaymentProcessId = jsonResponse["PaymentProcessId"];
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => OnePGPaymentRedirectPage(
                coopList: coopList,
                userId: userId,
                accesstoken: accesstoken,
                balance: balance,
                baseUrl: baseUrl,
                accountno: accountno,
                primaryColor: primaryColor,
                loginButtonTitleColor: loginButtonTitleColor,
                loginbuttonColor: loginbuttonColor,
                loginTextFieldColor: loginTextFieldColor,
                dasboardIconColor: dasboardIconColor,
                dashboardTopTitleColor: dashboardTopTitleColor,
                SecondaryColor: SecondaryColor,
                PaymentUrl: PaymentUrl,
                PaymentAmount: PaymentAmount,
                PaymentMerchantId: PaymentMerchantId,
                PaymentMerchantName: PaymentMerchantName,
                PaymentMerchantTxnId: PaymentMerchantTxnId,
                PaymentTransactionRemarks: remarkscontroller.text.toString().trim(),
                PaymentProcessId: PaymentProcessId,
                InstrumentCode: InstrumentCode,
              ),
            ),
          );
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(
                  jsonResponse["Message"]),
                actions: [
                  // Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  Future<String> postOnePGServiceCharge() async {
    var jsonResponse;
    String url = "${baseUrl}api/v1/onepg/getservicecharge";
    Map body = {
      "Amount": amountcontroller.text.toString().trim(),
      "InstrumentCode": InstrumentCode,
    };
    print(body);
    var response = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });
          if (jsonResponse["Message"].toString().toUpperCase() == "Success".toUpperCase()) {
            serviceCharges = jsonResponse['Data']["TotalChargeAmount"].toString();
          }
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(
                    jsonResponse["Message"]),
                actions: [
                  // Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
              coopList: coopList,
              userId: userId,
              accesstoken: accesstoken,
              baseUrl: baseUrl,
              accountno: accountno,
              primaryColor: primaryColor,
              loginButtonTitleColor: loginButtonTitleColor,
              loginbuttonColor: loginbuttonColor,
              loginTextFieldColor: loginTextFieldColor,
              dasboardIconColor: dasboardIconColor,
              dashboardTopTitleColor: dashboardTopTitleColor,
              SecondaryColor: SecondaryColor,
            )),
            (Route<dynamic> route) => false);
  }

  moveback() {
    Navigator.of(context).pop();
  }

  bool showBalance = false;

  final TextEditingController amountcontroller = TextEditingController();
  final TextEditingController remarkscontroller = TextEditingController();
  final TextEditingController mpincontroller = TextEditingController();
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return moveback();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(
                  color: Color(int.parse(primaryColor.toString())),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: 10.0,
                      top: 40,
                      child: IconButton(
                          onPressed: () {
                            moveback();
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            size: 30.0,
                            color: Colors.white,
                          )),
                    ),
                    Positioned(
                      left: 100,
                      top: 50,
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("sahakaari_pay"),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20,
                      top: 95,
                      child: Row(
                        children: [
                          const SizedBox(
                            height: 30,
                            width: 30,
                            child: Image(
                                image: AssetImage(
                                    "assets/images/wallet_icon.png")),
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            showBalance
                                ? AppLocalizations.of(context)!
                                .localizedString("rs") +
                                "${balance.toString()}"
                                : "xxx.xx".toUpperCase(),
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 180.0),
                            child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (showBalance == false) {
                                      showBalance = true;
                                    } else {
                                      showBalance = false;
                                    }
                                  });
                                },
                                icon: Icon(
                                  showBalance
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  size: 25.0,
                                  color: Colors.white,
                                )),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Center(
                          child: Text(
                            InstitutionName!,
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Form(
                            key: globalFormKey,
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 50.0,
                                  ),
                                  child: Container(
                                    width: MediaQuery.of(context)
                                        .size
                                        .width -
                                        120,
                                    child: TextFormField(
                                      controller: amountcontroller,
                                      maxLength: 50,
                                      keyboardType: TextInputType.number,
                                      validator: (value) => value!.isEmpty
                                          ? AppLocalizations.of(context)!
                                          .localizedString("transaction_amount")
                                          : null,
                                      onSaved: (value) =>
                                      amountcontroller,
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        labelText:
                                        AppLocalizations.of(context)!
                                            .localizedString("transaction_amount"),
                                        labelStyle: TextStyle(
                                            color: Colors.green),
                                        counterText: "",
                                        icon: Icon(
                                          Icons.confirmation_number,
                                          color: Colors.green,
                                          size: 20.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),

                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 50.0,
                                  ),
                                  child: Container(
                                    width: MediaQuery.of(context)
                                        .size
                                        .width -
                                        120,
                                    child: TextFormField(
                                      controller: remarkscontroller,
                                      maxLength: 50,
                                      keyboardType: TextInputType.text,
                                      validator: (value) => value!.isEmpty
                                          ? AppLocalizations.of(context)!
                                          .localizedString("transaction_remark")
                                          : null,
                                      onSaved: (value) =>
                                      amountcontroller,
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        labelText:
                                        AppLocalizations.of(context)!
                                            .localizedString("transaction_remark"),
                                        labelStyle: TextStyle(
                                            color: Colors.green),
                                        counterText: "",
                                        icon: Icon(
                                          Icons.content_copy,
                                          color: Colors.green,
                                          size: 20.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            )),
                      ),
                      Padding(
                        padding:
                        const EdgeInsets.only(left: 25.0, top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.circular(10.0),
                                  color: Color(int.parse(
                                      loginbuttonColor.toString())),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: const Offset(0,
                                          3), // changes position of shadow
                                    ),
                                  ]),
                              height: 50,
                              width:
                              MediaQuery.of(context).size.width - 120,
                              child: TextButton(
                                onPressed: () async {
                                  if (validateAndSave() && validateAmount(double.parse(amountcontroller.text.toString()))) {
                                    setState(() {
                                      _isLoading = true;
                                    });
                                    postOnePGServiceCharge().then((value) {
                                      if (value.toUpperCase() == "Success".toUpperCase()) {
                                        if (showBiometricbuttonTransaction) {
                                          transactionPinMessageBiometric();
                                        } else {
                                          transactionPinMessage();
                                        }
                                      }
                                    });
                                  } else {
                                    setState(() {
                                      _isLoading = false;
                                      if (!validateAmount(double.parse(amountcontroller.text))) {
                                        showDialog(
                                            context: context,
                                            builder:
                                                (BuildContext context) {
                                              return AlertDialog(
                                                title: Center(
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        AppLocalizations.of(
                                                            context)!
                                                            .localizedString(
                                                            "alert"),
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      const Icon(
                                                        Icons.add_alert,
                                                        color: Colors.red,
                                                        size: 50,
                                                      )
                                                    ],
                                                  ),
                                                ),
                                                content: Text(
                                                  AppLocalizations.of(
                                                      context)!
                                                      .localizedString(
                                                      "minimum_amount"),
                                                ),
                                                actions: [
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.of(
                                                          context)
                                                          .pop();
                                                    },
                                                    child: Text(
                                                      AppLocalizations.of(
                                                          context)!
                                                          .localizedString(
                                                          "okay"),
                                                      style: TextStyle(
                                                          color: Colors
                                                              .green[
                                                          900]),
                                                    ),
                                                  )
                                                ],
                                              );
                                            });
                                      }
                                    });
                                  }
                                },
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("submit"),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.all(40.0),
                        child: Text(
                            "Payment will be made to NEPAL PAYMENT SOLUTION, amount will be credited in your account on cooperative after successful transaction."),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  bool validateAmount(double value) {
    if (value < 10) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }
}
