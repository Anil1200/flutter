import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/onepg/onepg_list.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';

import '../../models/loading.dart';

class OnePGPaymentRedirectPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? PaymentUrl;
  String? PaymentAmount;
  String? PaymentMerchantId;
  String? PaymentMerchantName;
  String? PaymentMerchantTxnId;
  String? PaymentTransactionRemarks;
  String? PaymentProcessId;
  String? InstrumentCode;

  OnePGPaymentRedirectPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.PaymentUrl,
    this.PaymentAmount,
    this.PaymentMerchantId,
    this.PaymentMerchantName,
    this.PaymentMerchantTxnId,
    this.PaymentTransactionRemarks,
    this.PaymentProcessId,
    this.InstrumentCode,
  }) : super(key: key);

  @override
  State<OnePGPaymentRedirectPage> createState() => _OnePGPaymentRedirectPageState(
    coopList,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
    PaymentUrl,
    PaymentAmount,
    PaymentMerchantId,
    PaymentMerchantName,
    PaymentMerchantTxnId,
    PaymentTransactionRemarks,
    PaymentProcessId,
    InstrumentCode,
  );
}

class _OnePGPaymentRedirectPageState extends State<OnePGPaymentRedirectPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? PaymentUrl;
  String? PaymentAmount;
  String? PaymentMerchantId;
  String? PaymentMerchantName;
  String? PaymentMerchantTxnId;
  String? PaymentTransactionRemarks;
  String? PaymentProcessId;
  String? InstrumentCode;
  _OnePGPaymentRedirectPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.PaymentUrl,
      this.PaymentAmount,
      this.PaymentMerchantId,
      this.PaymentMerchantName,
      this.PaymentMerchantTxnId,
      this.PaymentTransactionRemarks,
      this.PaymentProcessId,
      this.InstrumentCode,
      );

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    //_isLoading = true;
  }

  movetoonepgpaymentpage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => OnePGListPage(
              coopList: coopList,
              userId: userId,
              accesstoken: accesstoken,
              balance: balance,
              baseUrl: baseUrl,
              accountno: accountno,
              primaryColor: primaryColor,
              loginButtonTitleColor: loginButtonTitleColor,
              loginbuttonColor: loginbuttonColor,
              loginTextFieldColor: loginTextFieldColor,
              dasboardIconColor: dasboardIconColor,
              dashboardTopTitleColor: dashboardTopTitleColor,
              SecondaryColor: SecondaryColor,
            )),
            (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoonepgpaymentpage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetoonepgpaymentpage();
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Text(
                "SahakaariPay",
              ),
              if (PaymentUrl!.contains("nepalpayment.com")) ...[
                const SizedBox(
                  height: 5,
                ),
                const Text(
                  "Please wait...", //PaymentUrl!.contains("nepalpayment.com") ? "Please wait..." : PaymentUrl!,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ],
            ],
          ),
          centerTitle: false,
          //toolbarHeight: 100,
        ),
        body: _isLoading
            ? const Loading()
            : PaymentUrl == null
            ? Center(child: Text(AppLocalizations.of(context)!.localizedString("service_not_available")))
            : InAppWebView(
          initialUrlRequest: URLRequest(
              url: Uri.parse(PaymentUrl!),
              method: 'POST',
              body: Uint8List.fromList(
                utf8.encode(
                    "Amount=$PaymentAmount&InstrumentCode=$InstrumentCode&MerchantId=$PaymentMerchantId&MerchantName=$PaymentMerchantName&MerchantTxnId=$PaymentMerchantTxnId&TransactionRemarks=$PaymentTransactionRemarks&ProcessId=$PaymentProcessId"
                ),
              ),
              headers: {
                'Content-Type': 'multipart/form-data',
              }
          ),
          onWebViewCreated: (controller) {

          },
          onLoadStart: (controller, url) {
            setState(() {
              _isLoading = false;
            });
          },
          onLoadStop: (controller, url) {
            setState(() {
              if (!url.toString().contains("nepalpayment.com")) {
                PaymentUrl = "$url";
              }
              _isLoading = false;
            });
          },
          onLoadError: (controller, url, code, message) {
            setState(() {
              _isLoading = false;
            });
          },
          onLoadHttpError: (controller, url, code, message) {
            setState(() {
              _isLoading = false;
            });
          },
        ),
      ),
    );
  }
}
