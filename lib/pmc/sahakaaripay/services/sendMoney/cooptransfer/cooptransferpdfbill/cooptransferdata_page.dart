import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/cooptransfer/cooptransferpdfbill/cooptransferpdf_billpage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/sendmoney_page.dart';

import '../../../../../utils/localizations.dart';
import '../../../../../utils/utils.dart';
import '../../../../models/loading.dart';

class CoopTranferDataDetails extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  String? amount;
  String? destinationaccount;
  String? receiverName;
  String? receiverMobile;
  List? wepaycoopList;
  String? serviceCharges;
  String? cooperativeName;
  String? cooperativeCode;
  String? destinationBranchCode;
  String? statement;
  String? totalAmount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  CoopTranferDataDetails({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.amount,
    this.destinationaccount,
    this.receiverName,
    this.receiverMobile,
    this.wepaycoopList,
    this.serviceCharges,
    this.cooperativeName,
    this.cooperativeCode,
    this.destinationBranchCode,
    this.statement,
    this.totalAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _CoopTranferDataDetailsState createState() => _CoopTranferDataDetailsState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        accountName,
        amount,
        destinationaccount,
        receiverName,
        receiverMobile,
        wepaycoopList,
        serviceCharges,
        cooperativeName,
        cooperativeCode,
        destinationBranchCode,
        statement,
        totalAmount,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _CoopTranferDataDetailsState extends State<CoopTranferDataDetails> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  String? amount;
  String? destinationaccount;
  String? receiverName;
  String? receiverMobile;
  List? wepaycoopList;
  String? serviceCharges;
  String? cooperaativeName;
  String? cooperativeCode;
  String? destinationBranchCode;
  String? statement;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _CoopTranferDataDetailsState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.amount,
    this.destinationaccount,
    this.receiverName,
    this.receiverMobile,
    this.wepaycoopList,
    this.serviceCharges,
    this.cooperaativeName,
    this.cooperativeCode,
    this.destinationBranchCode,
    this.statement,
    this.totalAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;

  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      addamount();
    });
  }

  double? sum;
  String? totalAmount;

  addamount() {
    sum = double.parse(amount.toString()) +
        double.parse(serviceCharges.toString());
    // totalAmount = sum.toString();
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${amount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${destinationaccount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("serviceCharge") +
                          "${double.parse(serviceCharges.toString()).toStringAsFixed(1)}" +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          setState(() {
                            _isLoading = false;
                          });
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const SizedBox(
                      width: 100,
                    ),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postTransferData();
                          } else {
                            _isLoading = false;
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  String? serviceinfoid;
  String? gw_originatedid;
  String? Status;
  // String? paymentmessage;
  postTransferData() async {
    String url = "${baseUrl}api/v1/pes/account/wepaycooptransfer";
    print("this is the amount to post for transfer: ${amount}");
    Map body = {
      "APIUSERID": "003",
      "SourceBank": coopName.toString(),
      "SourceAccountNumber": accountno.toString().trim(),
      "SourceAccountName": accountName.toString().trim(),
      "SourceBranch": "001",
      "DestinationBank": cooperativeCode.toString().trim(),
      "DestinationAccountNumber": destinationaccount.toString().trim(),
      "MReceivable": "",
      "DestinationAccountName": receiverName.toString().trim(),
      "DestinationBranchCode": destinationBranchCode.toString().trim(),
      "Amount": amount.toString().trim(),
      "ServiceCharge": serviceCharges.toString().trim(),
      "TransactionID": "",
      "PartnerTransactionID": "",
      "TransactionType": "COOP",
      "TransferType": "OUT",
      "GatewayName": "WePay",
      "Remarks": statement.toString().trim(),
      "Status": "false",
      "MPIN": mpincontroller.text.toString().trim(),
      "SourceCustomerMobileNumber": login_username,
      "DestinationCustomerMobileNumber": receiverMobile.toString().trim()
    };
    mpincontroller.clear();
    print("this is the body to post for transferring: ${body}");
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      "Token": "q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP",
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print(
          "Response status of transferring money from coop to coop: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          serviceinfoid = jsonResponse["service_info_id"];
          gw_originatedid = jsonResponse["gw_originated_id"];
          TransactionId = jsonResponse["TransactionId"];
          paymentmessage = jsonResponse["Message"];
          Status = jsonResponse["Status"];
          print("this is payment Message: ${paymentmessage}");
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              if (Status.toString().toLowerCase() == "True".toLowerCase()) {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Row(
                  children: [
                    const Icon(
                      Icons.verified,
                      color: Colors.green,
                      size: 40,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      jsonResponse["Message"],
                      style: const TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )));
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CoopTransferPdfBillPage(
                              coopList: coopList,
                              userId: userId,
                              accesstoken: accesstoken,
                              balance: balance,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              // accountName: accountName,
                              Amount: amount,
                              destinationaccount: destinationaccount,
                              receiverName: receiverName,
                              wepaycoopList: wepaycoopList,
                              serviceCharges: serviceCharges,
                              cooperativeName: cooperaativeName,
                              cooperativeCode: cooperativeCode,
                              remarks: statement,
                              paymentmessage: paymentmessage,
                              // planName: planName,
                              ReferenceId: ReferenceId,
                              TransactionId: TransactionId,
                              ReceiptNo: ReceiptNo,
                              PaymentBillDate: PaymentBillDate,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                            )));
              } else {
                setState(() {
                  _isLoading = false;
                });
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Center(
                            child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("error_alert"),
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 50.0,
                            )
                          ],
                        )),
                        content: Text(
                          AppLocalizations.of(context)!
                              .localizedString("process_error"),
                          style: TextStyle(fontSize: 14),
                        ),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("okay"),
                              ))
                        ],
                      );
                    });
              }
              // confirmTransaction();
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error response: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? paymentmessage;

  movetoSendMoneyPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => SendMoneyPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoSendMoneyPage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 80.0, left: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SendMoneyPage(
                                            coopList: coopList,
                                            userId: userId,
                                            accesstoken: accesstoken,
                                            balance: balance,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            primaryColor: primaryColor,
                                            loginButtonTitleColor:
                                                loginButtonTitleColor,
                                            loginbuttonColor: loginbuttonColor,
                                            loginTextFieldColor:
                                                loginTextFieldColor,
                                            dasboardIconColor:
                                                dasboardIconColor,
                                            dashboardTopTitleColor:
                                                dashboardTopTitleColor,
                                            SecondaryColor: SecondaryColor,
                                          )));
                            },
                            icon: const Icon(
                              Icons.arrow_back,
                              size: 30,
                              color: Colors.green,
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 14.0),
                            child: Text(
                              AppLocalizations.of(context)!.localizedString(
                                  "cooperative_transfer_details"),
                              style: TextStyle(
                                  color: Colors.green[900],
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 2.9,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("match_percent"),
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 14),
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${MatchPercent} %")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("validation_message"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 90,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${ValidationMessage}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("receiver_name"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 55,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${receiverName}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("receiver_account"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 40,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${destinationaccount}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("transfer_amount"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 42,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${amount}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("service_charge"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 60,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text(
                                  "${double.parse(serviceCharges.toString()).toStringAsFixed(1)}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("total_amount"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 65,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${sum}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("statement"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 94,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${statement}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 280.0, top: 40, right: 20),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color:
                                Color(int.parse(loginbuttonColor.toString())),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ]),
                        height: 60,
                        width: 120,
                        child: TextButton(
                          onPressed: () {
                            setState(() {
                              transactionPinMessage();
                            });
                            // showMessage();
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("transfer_amount"),
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }
}
