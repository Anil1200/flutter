import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
// import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/cooptransfer/cooptransferpdfbill/cooptransferdata_page.dart';

import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';

class CooperativeTransferPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  List? wepaycoopList;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  CooperativeTransferPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.wepaycoopList,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _CooperativeTransferPageState createState() => _CooperativeTransferPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        accountName,
        wepaycoopList,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _CooperativeTransferPageState extends State<CooperativeTransferPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  List? wepaycoopList;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _CooperativeTransferPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.wepaycoopList,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  final GlobalKey<FormState> globalFormKey = GlobalKey();

  final TextEditingController accountNoController = TextEditingController();
  final TextEditingController amountController = TextEditingController();
  final TextEditingController statementController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController mobilenoController = TextEditingController();

  String? dropdownValue;
  String? dropdownValuebranch;
  List? branchList;
  String? cooperativeName;
  String? cooperativeCode;

  bool _isLoading = false;

  List<String> coopNames = [];
  // List<SearchModel> cooptransferlist = [];

  @override
  void initState() {
    super.initState();
    print("the account Name of me : ${accountName} ");
    nullCheck();
    print("cooperative name of logged in account: ${coopName}");
    // wepaycoopList?.remove(coopName);
  }

  nullCheck() {
    if (wepaycoopList != null) {
      for (var item in wepaycoopList!) {
        print(
            "this is the coop Name from list: ${item["CooperativeName"]} and this is the coop Name from constant: ${coopName}");
        if (item["CooperativeName"].toString().toUpperCase() !=
            coopName?.toUpperCase()) {
          coopNames.add(item["CooperativeName"]);
        }
      }
    }
  }

  bool balanceCheck() {
    print("this is new balance: ${balance}");
    if (balance! <= 0.0 || double.parse(amountController.text) > balance!) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text(AppLocalizations.of(context)!
                  .localizedString("insufficient_balance")),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
      return false;
    } else {
      return true;
    }
  }

  Future<String> getbranchListData() async {
    String url =
        "https://merchant.sahakaari.com/merchant/api/v1/wepaycooperativebranchlist";
    Map body = {"CooperativeCode": cooperativeCode.toString()};
    print("this is the code sent: ${body}");
    var jsonResponse;
    final response = await http.post(Uri.parse(url), body: body, headers: {
      "Token": "q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP"
      // 'Authorization': "Bearer ${accesstoken}",
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            branchList = jsonResponse["result"]["data"];
            print("the required wepay coopList: ${wepaycoopList}");
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            print(jsonResponse);
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ),
                    content: Text(jsonResponse["Message"]),
                    actions: [
                      Container(
                          height: 80,
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("okay"),
                                style: TextStyle(color: Colors.white),
                              )))
                    ],
                  );
                });
          });
        }
      });
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            print(jsonResponse);
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ),
                    content: Text(jsonResponse["Message"]),
                    actions: [
                      Container(
                          height: 80,
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("okay"),
                                style: TextStyle(color: Colors.white),
                              )))
                    ],
                  );
                });
          });
        }
      });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  showMessage() {
    if (dropdownValue == null || dropdownValuebranch == null) {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("select_cooperative_transfer"),
              ),
              actions: [
                // const Spacer(),
                Container(
                  height: 40,
                  width: 80,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(30)),
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                )
              ],
            );
          });
    } else {
      // serviceCharge();
      validateAccount();
    }
  }

  validateAccount() async {
    String url =
        "https://merchant.sahakaari.com/merchant/api/v1/wepayaccountvalidation";
    Map body = {
      "ApiUserId": "003",
      "CooperativeCode": cooperativeCode.toString(),
      "AccountNumber": accountNoController.text.toString().trim(),
      "AccountName": nameController.text.toString().trim(),
      "BranchCode": dropdownValuebranch.toString(),
      "MobileNumber": login_username,
    };
    print("this is the data for validation: ${body}");
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      "Token": "q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP",
    });
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
        if (jsonResponse != null) {
          if (jsonResponse["result"]["message"].toString().toLowerCase() !=
              "error".toLowerCase()) {
            transferMessage = jsonResponse["result"]["message"];
            MatchPercent = jsonResponse["result"]["data"]["MatchPercent"];
            ValidationMessage =
                jsonResponse["result"]["data"]["ValidationMessage"];
            // setState(() {
            //   _isLoading = false;
            // });
            serviceCharge();
          } else {
            setState(() {
              _isLoading = false;
            });
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 40,
                          )
                        ],
                      ),
                    ),
                    content: Text(
                        "${jsonResponse["result"]["errors"][0]["error_message"]}"),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("okay"),
                          ))
                    ],
                  );
                });
          }
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the 400 error response:${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text(
                  "${jsonResponse["Message"]},Please enter correct AccountNo and Name."),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the 401 error response:${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text(
                  "${jsonResponse["Message"]},Please enter correct AccountNo and Name."),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error message${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  String? serviceCharges;

  serviceCharge() async {
    String url =
        "https://merchant.sahakaari.com/merchant/api/v1/wepayservicecharge";
    Map body = {
      "Amount": amountController.text.toString().trim(),
    };
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      "Token": "q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP",
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("this is the response from service charge:${jsonResponse}");
      serviceCharges = jsonResponse["result"]["data"]["ServiceCharge"];
      print("this is service Charge: ${serviceCharges}");
      print("Response status : ${jsonResponse}");
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(
                  "${transferMessage},Account matched ${MatchPercent}, ${ValidationMessage}")));
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CoopTranferDataDetails(
                        coopList: coopList,
                        userId: userId,
                        accesstoken: accesstoken,
                        balance: balance,
                        baseUrl: baseUrl,
                        accountno: accountno,
                        accountName: accountName,
                        wepaycoopList: wepaycoopList,
                        serviceCharges: serviceCharges,
                        amount: amountController.text,
                        destinationaccount: accountNoController.text,
                        receiverName: nameController.text,
                        receiverMobile: mobilenoController.text,
                        cooperativeName: cooperativeName,
                        cooperativeCode: cooperativeCode,
                        destinationBranchCode: dropdownValuebranch,
                        statement: statementController.text,
                        primaryColor: primaryColor,
                        loginButtonTitleColor: loginButtonTitleColor,
                        loginbuttonColor: loginbuttonColor,
                        loginTextFieldColor: loginTextFieldColor,
                        dasboardIconColor: dasboardIconColor,
                        dashboardTopTitleColor: dashboardTopTitleColor,
                        SecondaryColor: SecondaryColor,
                      )));
        });
      }
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _isLoading
          ? Loading()
          : SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20.0, right: 20.0, top: 10),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20.0, top: 20, right: 10),
                            child: DropdownSearch<String>(
                              popupProps: PopupProps.dialog(
                                searchFieldProps: TextFieldProps(
                                  // autofocus: true,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 24, vertical: 10),
                                  decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      hintText: AppLocalizations.of(context)!
                                          .localizedString("search"),
                                      hintStyle: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: Colors.grey[400]),
                                      suffixIcon: Icon(
                                        Icons.search,
                                        size: 30,
                                        color: Colors.grey[500],
                                      )),
                                ),
                                showSearchBox: true,
                              ),
                              dropdownDecoratorProps: DropDownDecoratorProps(
                                dropdownSearchDecoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)!
                                        .localizedString("select_cooperative"),
                                    hintStyle: TextStyle(
                                        fontSize: 16, color: Colors.black87)),
                              ),
                              items: coopNames,
                              selectedItem: dropdownValue,
                              // hint: AppLocalizations.of(context)!
                              //     .localizedString("search"),
                              onChanged: (String? newValue) {
                                final index = wepaycoopList!.indexWhere(
                                    (element) =>
                                        element["CooperativeName"] == newValue);
                                setState(() {
                                  dropdownValue = newValue!;
                                  cooperativeName = newValue!;

                                  newValue =
                                      wepaycoopList![index]["CooperativeCode"];
                                  cooperativeCode = newValue!;
                                  print(
                                      "This is the cooperative code: ${cooperativeCode} and coopertiveName: ${cooperativeName}");
                                  getbranchListData();
                                });
                              },
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 20.0, right: 30),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: dropdownValuebranch,
                                alignment: AlignmentDirectional.center,
                                hint: Padding(
                                  padding: EdgeInsets.only(left: 20.0),
                                  child: Text(AppLocalizations.of(context)!
                                      .localizedString("select_branch")),
                                ),
                                // itemHeight: 90,
                                borderRadius: BorderRadius.circular(20.0),
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black87,
                                ),
                                iconSize: 34,
                                elevation: 16,
                                style: const TextStyle(
                                    color: Colors.black87,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    dropdownValuebranch = newValue!;
                                    print(
                                        "This is the branch code: ${dropdownValuebranch}");
                                  });
                                },
                                items: branchList?.map((item) {
                                  return DropdownMenuItem<String>(
                                      value: item["BranchCode"],
                                      child: Text(item["BranchName"]));
                                }).toList(),
                              ),
                            ),
                          ),
                          _isLoading
                              ? Center(
                                  child: Column(
                                    children: const [
                                      SizedBox(
                                        height: 20.0,
                                      ),
                                      Loading(),
                                    ],
                                  ),
                                )
                              : Form(
                                  key: globalFormKey,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, top: 10),
                                            child: Container(
                                                width: 320,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ),
                                                child: TextFormField(
                                                  controller: amountController,
                                                  maxLength: 30,
                                                  keyboardType:
                                                      TextInputType.number,
                                                  validator: (value) => value!
                                                          .isEmpty
                                                      ? "Please Enter Amount"
                                                      : null,
                                                  onSaved: (value) =>
                                                      amountController,
                                                  decoration: InputDecoration(
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20.0),
                                                    ),
                                                    labelText:
                                                        AppLocalizations.of(
                                                                context)!
                                                            .localizedString(
                                                                "amount"),
                                                    labelStyle: const TextStyle(
                                                        color: Colors.green),
                                                    counterText: "",
                                                    icon: const Icon(
                                                      Icons.money,
                                                      color: Colors.green,
                                                      size: 20.0,
                                                    ),
                                                  ),
                                                ))),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0, top: 10),
                                          child: Container(
                                            width: 320,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                            ),
                                            child: TextFormField(
                                              controller: accountNoController,
                                              maxLength: 30,
                                              keyboardType: TextInputType.text,
                                              validator: (value) => value!
                                                      .isEmpty
                                                  ? "Please Enter Destination Account Number"
                                                  : null,
                                              onSaved: (value) =>
                                                  accountNoController,
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ),
                                                labelText: AppLocalizations.of(
                                                        context)!
                                                    .localizedString(
                                                        "to_account_number"),
                                                labelStyle: const TextStyle(
                                                    color: Colors.green),
                                                counterText: "",
                                                icon: const Icon(
                                                  Icons.supervised_user_circle,
                                                  color: Colors.green,
                                                  size: 20.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, top: 10),
                                            child: Container(
                                                width: 320,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ),
                                                child: TextFormField(
                                                  controller: nameController,
                                                  maxLength: 30,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  validator: (value) => value!
                                                          .isEmpty
                                                      ? "Please Enter Receiver's Name"
                                                      : null,
                                                  onSaved: (value) =>
                                                      nameController,
                                                  decoration: InputDecoration(
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              20.0),
                                                    ),
                                                    labelText: AppLocalizations
                                                            .of(context)!
                                                        .localizedString(
                                                            "enter_receivers_name"),
                                                    labelStyle: const TextStyle(
                                                        color: Colors.green),
                                                    counterText: "",
                                                    icon: const Icon(
                                                      Icons.person,
                                                      color: Colors.green,
                                                      size: 20.0,
                                                    ),
                                                  ),
                                                ))),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0, top: 10),
                                          child: Container(
                                            width: 320,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                            ),
                                            child: TextFormField(
                                              controller: mobilenoController,
                                              maxLength: 10,
                                              keyboardType:
                                                  TextInputType.number,
                                              validator: (value) => value!
                                                      .isEmpty
                                                  ? "Please Enter Receiver's MobileNumber"
                                                  : null,
                                              onSaved: (value) =>
                                                  accountNoController,
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                ),
                                                labelText: AppLocalizations.of(
                                                        context)!
                                                    .localizedString(
                                                        "mobile_number"),
                                                labelStyle: const TextStyle(
                                                    color: Colors.green),
                                                counterText: "",
                                                icon: const Icon(
                                                  Icons.phone_android_sharp,
                                                  color: Colors.green,
                                                  size: 20.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 20.0, top: 10),
                                          child: Container(
                                              width: 320,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20.0),
                                              ),
                                              child: TextFormField(
                                                controller: statementController,
                                                maxLength: 300,
                                                keyboardType:
                                                    TextInputType.text,
                                                validator: (value) => value!
                                                        .isEmpty
                                                    ? "Please Enter Statement"
                                                    : null,
                                                onSaved: (value) =>
                                                    statementController,
                                                decoration: InputDecoration(
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20.0),
                                                  ),
                                                  labelText:
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "statement"),
                                                  labelStyle: const TextStyle(
                                                      color: Colors.green),
                                                  counterText: "",
                                                  icon: const Icon(
                                                    Icons.content_copy,
                                                    color: Colors.green,
                                                    size: 20.0,
                                                  ),
                                                ),
                                              )),
                                        )
                                      ])),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 235.0, top: 20),
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: Color(
                                      int.parse(loginbuttonColor.toString())),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: const Offset(
                                          0, 3), // changes position of shadow
                                    ),
                                  ]),
                              height: 50,
                              width: 100,
                              child: TextButton(
                                onPressed: () {
                                  if (validateAndSave() &&
                                      balanceCheck() &&
                                      validateAmount(double.parse(
                                          amountController.text))) {
                                    setState(() {
                                      _isLoading = true;
                                      // transactionPinMessage()
                                    });
                                    showMessage();
                                  } else {
                                    setState(() {
                                      _isLoading = false;
                                    });
                                    if (!validateAmount(
                                        double.parse(amountController.text))) {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              title: Center(
                                                child: Column(
                                                  children: [
                                                    Text(
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "alert"),
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    const Icon(
                                                      Icons.add_alert,
                                                      color: Colors.red,
                                                      size: 50,
                                                    )
                                                  ],
                                                ),
                                              ),
                                              content: Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "amount_validation"),
                                              ),
                                              actions: [
                                                TextButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Text(
                                                    AppLocalizations.of(
                                                            context)!
                                                        .localizedString(
                                                            "okay"),
                                                    style: TextStyle(
                                                        color:
                                                            Colors.green[900]),
                                                  ),
                                                )
                                              ],
                                            );
                                          });
                                    }
                                  }
                                },
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("proceed_name"),
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAmount(double value) {
    if (value < minimumAmount!) {
      print("Does not match: ${value}");
      return false;
    }
    if (value > maximumAmount!) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }
}
