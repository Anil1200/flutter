import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/cooptransfer/cooperativetransfer_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/sendMoney/fundtransfer/fundtransfer_page.dart';

import '../../../utils/localizations.dart';
import '../../models/loading.dart';
import '../../pmc_homepage.dart';
import 'fundtransfer/fundtransfer_page.dart';

class SendMoneyPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  SendMoneyPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _SendMoneyPageState createState() => _SendMoneyPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        accountName,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _SendMoneyPageState extends State<SendMoneyPage>
    with TickerProviderStateMixin {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? accountName;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _SendMoneyPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.accountName,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 2, vsync: this);

    _controller!.addListener(() {
      setState(() {
        _selectedIndex = _controller!.index;
        if (_selectedIndex == 1) {
          if (wepaycoopList == null) {
            print("this is the wepaycooplist: ${wepaycoopList}");
            postJsonData();
            _isLoading = true;
          }
        }
      });
    });
  }

  bool _isLoading = false;

  bool showBalance = false;

  List? wepaycoopList;

  Future<String> postJsonData() async {
    String url = "https://merchant.sahakaari.com/merchant/api/v1/wepaycooplist";
    var jsonResponse;
    final response = await http.post(Uri.parse(url), headers: {
      "Token": "q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP"
      // 'Authorization': "Bearer ${accesstoken}",
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            wepaycoopList = jsonResponse["result"]["data"];
            print("the required wepay coopList: ${wepaycoopList}");
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            print("the error 400 statusCode response: ${jsonResponse}");
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ),
                    content: Text(jsonResponse["message"]),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("okay"),
                            style: TextStyle(color: Colors.green[900]),
                          ))
                    ],
                  );
                });
          });
        }
      });
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            print(jsonResponse);
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 30,
                            ),
                          ),
                        ],
                      ),
                    ),
                    content: Text(jsonResponse["Message"]),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("okay"),
                            style: TextStyle(color: Colors.green[900]),
                          ))
                    ],
                  );
                });
          });
        }
      });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  TabController? _controller;
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              movetohomepage();
            },
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          backgroundColor: Color(int.parse(primaryColor.toString())),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30))),
          toolbarHeight: 110,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Text(
                  AppLocalizations.of(context)!
                      .localizedString("sahakaari_pay"),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
              ),
              Row(
                children: [
                  const SizedBox(
                    height: 20,
                    width: 20,
                    child: Image(
                        image: AssetImage("assets/images/wallet_icon.png")),
                  ),
                  // const SizedBox(
                  //   width: 10.0,
                  // ),
                  Text(
                    showBalance
                        ? AppLocalizations.of(context)!.localizedString("rs") +
                            "${balance.toString()}"
                        : "xxx.xx".toUpperCase(),
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 150.0),
                    child: IconButton(
                        onPressed: () {
                          setState(() {
                            if (showBalance == false) {
                              showBalance = true;
                            } else {
                              showBalance = false;
                            }
                          });
                        },
                        icon: Icon(
                          showBalance ? Icons.visibility : Icons.visibility_off,
                          size: 25.0,
                          color: Colors.white,
                        )),
                  ),
                ],
              )
            ],
          ),
          bottom: PreferredSize(
            preferredSize: const Size(60, 60),
            child: Padding(
              padding: const EdgeInsets.only(top: 10.0, left: 10.0, right: 10),
              child: Container(
                height: 60,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.1),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ]),
                child: TabBar(
                    controller: _controller,
                    indicatorColor: Colors.red,
                    indicator: UnderlineTabIndicator(
                      borderSide: BorderSide(width: 3.0, color: Colors.red),
                      insets: EdgeInsets.symmetric(
                        horizontal: 30.0,
                      ),
                    ),
                    labelColor: Colors.black87,
                    tabs: [
                      Tab(
                        text: AppLocalizations.of(context)!
                            .localizedString("fund_transfer"),
                        icon: Icon(
                          Icons.speed,
                          color: Colors.red,
                        ),
                      ),
                      Tab(
                        text: AppLocalizations.of(context)!
                            .localizedString("external_coop_transfer"),
                        icon: Icon(
                          Icons.all_inclusive_rounded,
                          color: Colors.red,
                        ),
                      )
                    ]),
              ),
            ),
          ),
        ),
        body: _isLoading
            ? Center(
                child: Column(
                children: [
                  SizedBox(
                    height: 150,
                  ),
                  Loading(),
                ],
              ))
            : TabBarView(
                controller: _controller,
                children: [
                  FundTransferPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                  ),
                  CooperativeTransferPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    accountName: accountName,
                    wepaycoopList: wepaycoopList,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                  ),
                ],
              ),
      ),
    );
  }
}
