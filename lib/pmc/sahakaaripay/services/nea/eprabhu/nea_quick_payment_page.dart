import 'dart:convert';
import 'dart:core';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:dropdown_search/dropdown_search.dart';

class EprabhuNEAQuickPaymentPage extends StatefulWidget {
  List? coopList;
  List? neaOffices;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  EprabhuNEAQuickPaymentPage({
    Key? key,
    this.coopList,
    this.neaOffices,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<EprabhuNEAQuickPaymentPage> createState() => _EprabhuNEAQuickPaymentPageState(
    coopList,
    neaOffices,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
  );
}

class _EprabhuNEAQuickPaymentPageState extends State<EprabhuNEAQuickPaymentPage> {
  List? coopList;
  List? neaOffices;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _EprabhuNEAQuickPaymentPageState(
    this.coopList,
    this.neaOffices,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;
  bool showBalance = false;

  List<String> dropNEAOffice = [];

  String? BranchName;

  final GlobalKey<FormState> globalFormKey = GlobalKey();

  final TextEditingController scnumbercontroller = TextEditingController();
  final TextEditingController customeridcontroller = TextEditingController();

  String? dropdownValue;

  void initState() {
    super.initState();
    _isLoading = false;
    NEAOffices = neaOffices;
    forSearch();
  }

  List? NEAOffices;

  forSearch() {
    if (NEAOffices != null) {
      for (var item in NEAOffices!) {
        setState(() {
          _isLoading = false;
        });
        dropNEAOffice.add(item["Office"]);
        print(dropNEAOffice);
      }
    }
  }

  postQuickPaymentNEA() async {
    String url = "${baseUrl}api/v1/quickpaymentnea";
    Map body = {
      "BranchCode": dropdownValue.toString(),
      "BranchName": BranchName.toString(),
      "SCNumber": scnumbercontroller.text.toString(),
      "CustomerId": customeridcontroller.text.toString(),
      "GatewayName": "Khalti",
    };
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            if (jsonResponse["Code"] == "000") {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                    children: [
                      const Icon(
                        Icons.verified,
                        color: Colors.green,
                        size: 40,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width / 2,
                        child: Text(
                          jsonResponse["Message"],
                          style:
                          const TextStyle(color: Colors.white, fontSize: 16),
                        ),
                      ),
                    ],
                  )));
              Navigator.of(context).pop();
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Center(
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 40,
                            )
                          ],
                        ),
                      ),
                      content: Text(jsonResponse["Message"]),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(AppLocalizations.of(context)!
                                .localizedString("okay")))
                      ],
                    );
                  });
            }
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error response:${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              ),
              content: Text(
                  AppLocalizations.of(context)!
                      .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        backgroundColor: Color(int.parse(primaryColor.toString())),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 30,
          ),
        ),
        title: Text(
            AppLocalizations.of(context)!.localizedString("quick_payment")),
        centerTitle: true,
      ),
      body: _isLoading
          ? Loading()
          : SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 30.0),
                  child: Center(
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("nea"),
                      style: TextStyle(
                        color: Colors.black87,
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 30.0, right: 10, top: 20),
                  child: DropdownSearch<String>(
                    popupProps: PopupProps.dialog(
                      searchFieldProps: TextFieldProps(
                        // autofocus: true,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 10),
                        decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            hintText: AppLocalizations.of(context)!
                                .localizedString("search"),
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: Colors.grey[400]),
                            suffixIcon: Icon(
                              Icons.search,
                              size: 30,
                              color: Colors.grey[500],
                            )),
                      ),
                      showSearchBox: true,
                    ),
                    dropdownDecoratorProps: DropDownDecoratorProps(
                      dropdownSearchDecoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: AppLocalizations.of(context)!
                              .localizedString("select_branch"),
                          hintStyle: TextStyle(
                              fontSize: 16, color: Colors.black87)),
                    ),
                    items: dropNEAOffice,
                    selectedItem: BranchName,
                    // hint: AppLocalizations.of(context)!
                    //     .localizedString("search"),
                    onChanged: (String? newValue) {
                      final index = neaOffices!.indexWhere(
                              (element) =>
                          element["Office"] == newValue);
                      //print(index);
                      //print(neaOffices![index]["OfficeCodes"]);
                      newValue = neaOffices![index]["OfficeCodes"];
                      setState(() {
                        dropdownValue = newValue!;
                        BranchName = neaOffices![index]["Office"];
                        print("this is offices : ${dropdownValue}");
                      });
                    },
                  ),
                ),
                Form(
                  key: globalFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, top: 20),
                        child: Container(
                          width: 320,
                          decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.circular(10.0),
                          ),
                          child: TextFormField(
                            controller: scnumbercontroller,
                            maxLength: 200,
                            keyboardType: TextInputType.text,
                            validator: (value) => value!.isEmpty
                                ? AppLocalizations.of(context)!
                                .localizedString("sc_no")
                                : null,
                            onSaved: (value) =>
                            scnumbercontroller,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.circular(10.0),
                              ),
                              labelText:
                              AppLocalizations.of(context)!
                                  .localizedString(
                                  "sc_no"),
                              labelStyle: const TextStyle(
                                  color: Colors.green),
                              counterText: "",
                              icon: const Icon(
                                Icons.supervised_user_circle,
                                color: Colors.green,
                                size: 20.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, top: 20),
                        child: Container(
                          width: 320,
                          decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.circular(10.0),
                          ),
                          child: TextFormField(
                            controller: customeridcontroller,
                            maxLength: 200,
                            keyboardType: TextInputType.text,
                            validator: (value) => value!.isEmpty
                                ? AppLocalizations.of(context)!
                                .localizedString(
                                "customer_id")
                                : null,
                            onSaved: (value) => customeridcontroller,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.circular(10.0),
                              ),
                              labelText:
                              AppLocalizations.of(context)!
                                  .localizedString(
                                  "customer_id"),
                              labelStyle: const TextStyle(
                                  color: Colors.green),
                              counterText: "",
                              icon: const Icon(
                                Icons.person,
                                color: Colors.green,
                                size: 20.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            borderRadius:
                            BorderRadius.circular(10.0),
                            color: Color(int.parse(
                                loginbuttonColor.toString())),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: const Offset(0,
                                    3), // changes position of shadow
                              ),
                            ]),
                        height: 50,
                        width:
                        MediaQuery.of(context).size.width - 120,
                        child: TextButton(
                          onPressed: () async {
                            if (validateAndSave()) {
                              setState(() {
                                _isLoading = true;
                                postQuickPaymentNEA();
                              });
                            } else {
                              setState(() {
                                _isLoading = false;
                              });
                            }
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("Add Quick Payment"),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        )
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
