import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:syncfusion_flutter_pdf/pdf.dart';

<<<<<<< HEAD:lib/pmc/sahakaaripay/services/nea/neapdfbill_page.dart
import '../../pmc_homepage.dart';
import '../water/ubs_water/mobile.dart'
=======
import '../../../../utils/localizations.dart';
import '../../../pmc_homepage.dart';
import '../../water/ubs_water/mobile.dart'
>>>>>>> sudeep:lib/pmc/sahakaaripay/services/nea/eprabhu/neapdfbill_page.dart
    if (dart.library.html) '../water/ubs_water/web.dart';

class EprabhuNeaPdfBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? scNo;
  String? CustomerName;
  String? ConsumerId;
  String? OfficeCode;
  String? Office;
  double? TotalDueAmount;
  List? BillDetail;
  List? NeaOffices;
  String? ReferenceId;
  String? TransactionId;
  String? paymentMessage;
  String? PaymentBillDate;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  EprabhuNeaPdfBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.scNo,
    this.CustomerName,
    this.ConsumerId,
    this.OfficeCode,
    this.Office,
    this.TotalDueAmount,
    this.BillDetail,
    this.NeaOffices,
    this.ReferenceId,
    this.TransactionId,
    this.paymentMessage,
    this.PaymentBillDate,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _EprabhuNeaPdfBillPageState createState() => _EprabhuNeaPdfBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        scNo,
        CustomerName,
        ConsumerId,
        OfficeCode,
        Office,
        TotalDueAmount,
        BillDetail,
        NeaOffices,
        ReferenceId,
        TransactionId,
        paymentMessage,
        PaymentBillDate,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _EprabhuNeaPdfBillPageState extends State<EprabhuNeaPdfBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? scNo;
  String? CustomerName;
  String? ConsumerId;
  String? OfficeCode;
  String? Office;
  double? TotalDueAmount;
  List? BillDetail;
  List? NeaOffices;
  String? ReferenceId;
  String? TransactionId;
  String? paymentMessage;
  String? PaymentBillDate;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _EprabhuNeaPdfBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.scNo,
    this.CustomerName,
    this.ConsumerId,
    this.OfficeCode,
    this.Office,
    this.TotalDueAmount,
    this.BillDetail,
    this.NeaOffices,
    this.ReferenceId,
    this.TransactionId,
    this.paymentMessage,
    this.PaymentBillDate,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  String? timeforBill = DateTime.now().year.toString() +
      "-" +
      DateTime.now().month.toString() +
      "-" +
      DateTime.now().day.toString() +
      " " +
      DateTime.now().hour.toString() +
      ":" +
      DateTime.now().minute.toString();

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'Nea Bill Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );

    final grid = PdfGrid();
    grid.columns.add(count: 2);
    var row = grid.rows.add();
    row.cells[0].value = "TransactionId:";
    row.cells[1].value = TransactionId.toString();

    row = grid.rows.add();
    row.cells[0].value = "Payment Message: ";
    row.cells[1].value = "${paymentMessage.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Customer Name: ";
    row.cells[1].value = "${CustomerName.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Office: ";
    row.cells[1].value = "${Office.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Remaining Due: ";
    row.cells[1].value = "${TotalDueAmount.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Total Amount: ";
    row.cells[1].value = "${TotalDueAmount.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Date: ";
    row.cells[1].value = "${PaymentBillDate.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Bill Amount:: ";
    row.cells[1].value = "${BillDetail?[0]["BillAmount"].toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Payable Amt: ";
    row.cells[1].value = "${BillDetail?[0]["PayableAmount"].toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Bill Due of: ";
    row.cells[1].value = "${BillDetail?[0]["DueBillOf"].toString()}";

    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));

    List<int> bytes = document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoHomePage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
              child: Text("hsis flkfjlkas akjfslda ksgslakgms ")
              // Column(
              //   crossAxisAlignment: CrossAxisAlignment.start,
              //   children: [
              //     Padding(
              //       padding: const EdgeInsets.only(top: 50.0),
              //       child: Center(
              //         child: Text(
              //           "NEA BILL DETAILS",
              //           style: TextStyle(
              //               color: Colors.green[900],
              //               fontSize: 16,
              //               fontWeight: FontWeight.bold),
              //         ),
              //       ),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0, top: 30),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           const Text(
              //             "Sc No:",
              //             style: TextStyle(color: Colors.black87, fontSize: 14),
              //           ),
              //           const SizedBox(
              //             width: 200,
              //           ),
              //           Container(
              //               width: MediaQuery.of(context).size.width / 2.5,
              //               child: Text("${scNo}")),
              //         ],
              //       ),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0, top: 30),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           const Text(
              //             "TransactionId:",
              //             style: TextStyle(color: Colors.black87, fontSize: 14),
              //           ),
              //           const SizedBox(
              //             width: 200,
              //           ),
              //           Container(
              //               width: MediaQuery.of(context).size.width / 2,
              //               child: Text("${TransactionId}")),
              //         ],
              //       ),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0, top: 30),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           const Text(
              //             "Payment Message:",
              //             style: TextStyle(color: Colors.black87, fontSize: 14),
              //           ),
              //           const SizedBox(
              //             width: 200,
              //           ),
              //           Container(
              //               width: MediaQuery.of(context).size.width / 2.5,
              //               child: Text("${paymentMessage.toString()}")),
              //         ],
              //       ),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0, top: 30),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           const Text(
              //             "Customer Name:",
              //             style: TextStyle(color: Colors.black87, fontSize: 14),
              //           ),
              //           const SizedBox(
              //             width: 130,
              //           ),
              //           Container(
              //               width: MediaQuery.of(context).size.width / 2.5,
              //               child: Text("${CustomerName}")),
              //         ],
              //       ),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0, top: 30),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           const Text(
              //             "Office:",
              //             style: TextStyle(color: Colors.black87, fontSize: 14),
              //           ),
              //           const SizedBox(
              //             width: 202,
              //           ),
              //           Container(
              //               width: MediaQuery.of(context).size.width / 2.5,
              //               child: Text("${Office}")),
              //         ],
              //       ),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0, top: 30),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           const Text(
              //             "Remaining Due:",
              //             style: TextStyle(color: Colors.black87, fontSize: 14),
              //           ),
              //           const SizedBox(
              //             width: 140,
              //           ),
              //           Container(
              //               width: MediaQuery.of(context).size.width / 2.5,
              //               child: Text("${TotalDueAmount.toString()}")),
              //         ],
              //       ),
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0, top: 30),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: [
              //           const Text(
              //             "Total Amount:",
              //             style: TextStyle(color: Colors.black87, fontSize: 14),
              //           ),
              //           const SizedBox(
              //             width: 150,
              //           ),
              //           Container(
              //               width: MediaQuery.of(context).size.width / 2.5,
              //               child: Text("${TotalDueAmount.toString()}")),
              //         ],
              //       ),
              //     ),
              //     const SizedBox(
              //       height: 20,
              //     ),
              //     Divider(
              //       thickness: 1,
              //       color: Colors.green[700],
              //     ),
              //     const SizedBox(
              //       height: 20,
              //     ),
              //     Padding(
              //       padding: const EdgeInsets.only(left: 20.0),
              //       child: Row(
              //         crossAxisAlignment: CrossAxisAlignment.start,
              //         children: const [
              //           Text(
              //             "Date",
              //             style: TextStyle(
              //                 color: Colors.black87,
              //                 fontSize: 14,
              //                 fontWeight: FontWeight.bold),
              //           ),
              //           SizedBox(
              //             width: 60,
              //           ),
              //           Text("Bill Amount",
              //               style: TextStyle(
              //                   color: Colors.black87,
              //                   fontSize: 14,
              //                   fontWeight: FontWeight.bold)),
              //           SizedBox(
              //             width: 20,
              //           ),
              //           Text("Payable Amt",
              //               style: TextStyle(
              //                   color: Colors.black87,
              //                   fontSize: 14,
              //                   fontWeight: FontWeight.bold)),
              //           SizedBox(
              //             width: 30,
              //           ),
              //           Text("Bill Of",
              //               style: TextStyle(
              //                   color: Colors.black87,
              //                   fontSize: 14,
              //                   fontWeight: FontWeight.bold)),
              //         ],
              //       ),
              //     ),
              //     Divider(
              //       thickness: 1,
              //       color: Colors.green[700],
              //     ),
              //     Container(
              //       height: 200,
              //       decoration: const BoxDecoration(color: Colors.white),
              //       child: ListView.builder(
              //           itemCount: BillDetail == null ? 0 : BillDetail?.length,
              //           itemBuilder: (BuildContext context, int index) {
              //             return SingleChildScrollView(
              //               child: Column(
              //                 children: [
              //                   Padding(
              //                     padding: const EdgeInsets.only(left: 20.0),
              //                     child: Row(
              //                       crossAxisAlignment: CrossAxisAlignment.start,
              //                       children: [
              //                         Text(
              //                           "${BillDetail?[index]["BillDate"]}",
              //                           style: const TextStyle(
              //                             color: Colors.black87,
              //                             fontSize: 14,
              //                           ),
              //                         ),
              //                         const SizedBox(
              //                           width: 40,
              //                         ),
              //                         Text("${BillDetail?[index]["BillAmount"]}",
              //                             style: const TextStyle(
              //                               color: Colors.black87,
              //                               fontSize: 14,
              //                             )),
              //                         const SizedBox(
              //                           width: 70,
              //                         ),
              //                         Text(
              //                             "${BillDetail?[index]["PayableAmount"]}",
              //                             style: const TextStyle(
              //                               color: Colors.black87,
              //                               fontSize: 14,
              //                             )),
              //                         const SizedBox(
              //                           width: 40,
              //                         ),
              //                         Text("${BillDetail?[index]["DueBillOf"]}",
              //                             style: const TextStyle(
              //                               color: Colors.black87,
              //                               fontSize: 14,
              //                             )),
              //                       ],
              //                     ),
              //                   ),
              //                   const SizedBox(
              //                     height: 20,
              //                   ),
              //                   Divider(
              //                     thickness: 1,
              //                     color: Colors.green[700],
              //                   ),
              //                 ],
              //               ),
              //             );
              //           }),
              //     ),
              //     const SizedBox(
              //       height: 20,
              //     ),
              //     Row(
              //       crossAxisAlignment: CrossAxisAlignment.start,
              //       children: [
              //         const SizedBox(
              //           width: 100,
              //         ),
              //         TextButton(
              //             onPressed: () {
              //               movetoHomePage();
              //             },
              //             child: Text(
              //               AppLocalizations.of(context)!.localizedString("home"),
              //               style:
              //                   TextStyle(fontSize: 16, color: Colors.green[900]),
              //             )),
              //         const SizedBox(
              //           width: 60,
              //         ),
              //         TextButton(
              //             onPressed: createPDF,
              //             child: Text(
              //               AppLocalizations.of(context)!.localizedString("pdf"),
              //               style:
              //                   TextStyle(fontSize: 16, color: Colors.green[900]),
              //             )),
              //       ],
              //     )
              //   ],
              // ),
              ),
        ));
  }
}
