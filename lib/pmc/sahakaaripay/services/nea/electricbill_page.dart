import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/nea/neabill_page.dart';

import '../../../utils/localizations.dart';
import '../../models/loading.dart';
import '../../pmc_homepage.dart';

class ElectricBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  ElectricBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<ElectricBillPage> createState() => _ElectricBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _ElectricBillPageState extends State<ElectricBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _ElectricBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  List? neaOffices;
  List? locationList;
  bool _isLoading = false;
  String? BranchName;

  List<String> neaOfficeName = [];

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    this.getJsonData();
  }

  Future<String> getJsonData() async {
    String url = "${baseUrl}api/v1/billpayment/nea-office-codes";
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final response = await http.get(Uri.parse(url), headers: {
      // "Token": "${accesstoken}",
      // "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP",
      'Authorization': "Bearer ${accesstoken}",
    });
    setState(() {
      _isLoading = true;
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        var convertDataToJson = json.decode(response.body);
        neaOffices = convertDataToJson["OfficeCodes"];

        for (var item in neaOffices!) {
          neaOfficeName.add(item["Office"]);
        }

        //print("the list of Nea Offices: ${neaOffices}");

        // CooperativeSharedPreferences.setsmsCode(
        //     convertDataToJson["payload"][0]["smsCode"].toString());
      });
      return "success";
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  showMessage() {
    if (dropdownValue != null) {
      setState(() {
        _isLoading = true;
      });
      getBillList();
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("select_branch"),
              ),
              actions: [
                const Spacer(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
    }
  }

  String? scNo;
  String? CustomerName;
  String? ConsumerId;
  String? OfficeCode;
  String? Office;
  double? TotalDueAmount;
  List? BillDetail;

  Future<String> getBillList() async {
    var jsonResponse;
    String url =
        "${baseUrl}api/v1/billpayment/neabills?scNo=${scnumbercontroller.text.toString()}&officeCode=${dropdownValue.toString()}&consumerId=${customeridcontroller.text.toString()}";
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final response = await http.get(Uri.parse(url), headers: {
      'Authorization': "Bearer ${accesstoken}",
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        // neaOffices = convertDataToJson["OfficeCodes"];
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });
          scNo = jsonResponse["SCNo"];
          CustomerName = jsonResponse["CustomerName"];
          ConsumerId = jsonResponse["ConsumerId"];
          TotalDueAmount =
              double.parse(jsonResponse["TotalDueAmount"].toString());
          Office = jsonResponse["Office"];
          OfficeCode = jsonResponse["OfficeCode"];
          BillDetail = jsonResponse["BillDetail"];
          print("the list of Nea Offices: ${jsonResponse}");
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NeaBillListPage(
                        coopList: coopList,
                        userId: userId,
                        accesstoken: accesstoken,
                        balance: balance,
                        baseUrl: baseUrl,
                        accountno: accountno,
                        scNo: scNo,
                        CustomerName: CustomerName,
                        ConsumerId: ConsumerId,
                        TotalDueAmount: TotalDueAmount,
                        Office: Office,
                        OfficeCode: OfficeCode,
                        BillDetail: BillDetail,
                        primaryColor: primaryColor,
                        loginButtonTitleColor: loginButtonTitleColor,
                        loginbuttonColor: loginbuttonColor,
                        loginTextFieldColor: loginTextFieldColor,
                        dasboardIconColor: dasboardIconColor,
                        dashboardTopTitleColor: dashboardTopTitleColor,
                        SecondaryColor: SecondaryColor,
                      )));
        }

        // CooperativeSharedPreferences.setsmsCode(
        //     convertDataToJson["payload"][0]["smsCode"].toString());
      });
      return "success";
    } else if (response.statusCode == 400) {
      _isLoading = false;
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(
                    // jsonResponse["Message"]
                    "No due Amount."),
                actions: [
                  // Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool showBalance = false;

  String? dropdownValue;
  final TextEditingController scnumbercontroller = TextEditingController();
  final TextEditingController customeridcontroller = TextEditingController();
  final GlobalKey<FormState> globalFormKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            top: 40,
                            child: IconButton(
                                onPressed: () {
                                  movetohomepage();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                          Positioned(
                            left: 100,
                            top: 50,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sahakaari_pay"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 95,
                            child: Row(
                              children: [
                                const SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: Image(
                                      image: AssetImage(
                                          "assets/images/wallet_icon.png")),
                                ),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  showBalance
                                      ? AppLocalizations.of(context)!
                                              .localizedString("rs") +
                                          "${balance.toString()}"
                                      : "xxx.xx".toUpperCase(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 180.0),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          if (showBalance == false) {
                                            showBalance = true;
                                          } else {
                                            showBalance = false;
                                          }
                                        });
                                      },
                                      icon: Icon(
                                        showBalance
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        size: 25.0,
                                        color: Colors.white,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("nea"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ),
                            // Padding(
                            //   padding: EdgeInsets.only(left: 40.0),
                            //   child: Text(
                            //       AppLocalizations.of(context)!
                            //           .localizedString("select_branch"),
                            //       style: TextStyle(
                            //         color: Colors.black87,
                            //         fontSize: 14,
                            //       )),
                            // ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0, right: 10, top: 20),
                              child: DropdownSearch<String>(
                                mode: Mode.DIALOG,
                                maxHeight:
                                    MediaQuery.of(context).size.height - 100,
                                dropdownSearchDecoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)!
                                        .localizedString("select_branch"),
                                    hintStyle: TextStyle(
                                        fontSize: 16, color: Colors.black87)),
                                selectionListViewProps: SelectionListViewProps(
                                  padding: EdgeInsets.only(left: 15),
                                ),
                                searchFieldProps: TextFieldProps(
                                  // autofocus: true,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 24, vertical: 10),
                                  decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      hintText: AppLocalizations.of(context)!
                                          .localizedString("search"),
                                      hintStyle: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: Colors.grey[400]),
                                      suffixIcon: Icon(
                                        Icons.search,
                                        size: 30,
                                        color: Colors.grey[500],
                                      )),
                                ),
                                // showClearButton: true,
                                showSearchBox: true,
                                items: neaOfficeName,
                                selectedItem: BranchName,
                                // hint: AppLocalizations.of(context)!
                                //     .localizedString("search"),
                                onChanged: (String? newValue) {
                                  final index = neaOffices!.indexWhere(
                                      (element) =>
                                          element["Office"] == newValue);
                                  //print(index);
                                  //print(neaOffices![index]["OfficeCodes"]);
                                  newValue = neaOffices![index]["OfficeCodes"];
                                  setState(() {
                                    dropdownValue = newValue!;
                                    BranchName = neaOffices![index]["Office"];
                                    print("this is offices : ${dropdownValue}");
                                  });
                                },
                              ),
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.only(
                            //       left: 30.0, right: 10.0, top: 10.0),
                            //   child: DropdownSearch<String>(
                            //     mode: Mode.BOTTOM_SHEET,
                            //     showSearchBox: true,
                            //     items: neaOfficeName,
                            //     onChanged: (String? newValue) {
                            //       final index = neaOffices!.indexWhere(
                            //           (element) => element["Office"] == newValue);
                            //       //print(index);
                            //       //print(neaOffices![index]["OfficeCodes"]);
                            //       newValue = neaOffices![index]["OfficeCodes"];
                            //       setState(() {
                            //         dropdownValue = newValue!;
                            //         print("this is offices : ${dropdownValue}");
                            //       });
                            //     },
                            //   ),
                            // ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Form(
                                  key: globalFormKey,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 50.0,
                                        ),
                                        child: Container(
                                          width: 260,
                                          child: TextFormField(
                                            controller: scnumbercontroller,
                                            maxLength: 50,
                                            keyboardType: TextInputType.text,
                                            validator: (value) => value!.isEmpty
                                                ? AppLocalizations.of(context)!
                                                    .localizedString("sc_no")
                                                : null,
                                            onSaved: (value) =>
                                                scnumbercontroller,
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(),
                                              labelText:
                                                  AppLocalizations.of(context)!
                                                      .localizedString("sc_no"),
                                              labelStyle: TextStyle(
                                                  color: Colors.green),
                                              counterText: "",
                                              icon: Icon(
                                                Icons.confirmation_number,
                                                color: Colors.green,
                                                size: 20.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 50.0,
                                        ),
                                        child: Container(
                                          width: 260,
                                          child: TextFormField(
                                            controller: customeridcontroller,
                                            maxLength: 50,
                                            keyboardType: TextInputType.text,
                                            validator: (value) => value!.isEmpty
                                                ? AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "customer_id")
                                                : null,
                                            onSaved: (value) =>
                                                customeridcontroller,
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(),
                                              labelText:
                                                  AppLocalizations.of(context)!
                                                      .localizedString(
                                                          "customer_id"),
                                              labelStyle: TextStyle(
                                                  color: Colors.green),
                                              counterText: "",
                                              icon: Icon(
                                                Icons.verified_user,
                                                color: Colors.green,
                                                size: 20.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 220.0, top: 20),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    color: Color(
                                        int.parse(loginbuttonColor.toString())),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ]),
                                height: 50,
                                width: 100,
                                child: TextButton(
                                  onPressed: () async {
                                    if (validateAndSave()) {
                                      setState(() {
                                        _isLoading = true;
                                      });
                                      showMessage();
                                    } else {
                                      setState(() {
                                        _isLoading = false;
                                        // showDialog(
                                        //     context: context,
                                        //     builder: (BuildContext context) {
                                        //       return AlertDialog(
                                        //         title: Center(
                                        //           child: Column(
                                        //             children: [
                                        //               Text(
                                        //                 AppLocalizations.of(
                                        //                         context)!
                                        //                     .localizedString(
                                        //                         "alert"),
                                        //               ),
                                        //               SizedBox(
                                        //                 height: 10,
                                        //               ),
                                        //               Icon(
                                        //                 Icons.add_alert,
                                        //                 color: Colors.red,
                                        //                 size: 50,
                                        //               )
                                        //             ],
                                        //           ),
                                        //         ),
                                        //         content: Text(
                                        //           AppLocalizations.of(context)!
                                        //               .localizedString(""),
                                        //         ),
                                        //         actions: [
                                        //           const Spacer(),
                                        //           Container(
                                        //             height: 40,
                                        //             width: 80,
                                        //             decoration: BoxDecoration(
                                        //                 color:
                                        //                     Colors.green[900],
                                        //                 borderRadius:
                                        //                     BorderRadius
                                        //                         .circular(30)),
                                        //             child: TextButton(
                                        //               onPressed: () {
                                        //                 Navigator.of(context)
                                        //                     .pop();
                                        //               },
                                        //               child: Text(
                                        //                 AppLocalizations.of(
                                        //                         context)!
                                        //                     .localizedString(
                                        //                         "okay"),
                                        //                 style: TextStyle(
                                        //                     color:
                                        //                         Colors.white),
                                        //               ),
                                        //             ),
                                        //           )
                                        //         ],
                                        //       );
                                        //     });
                                      });
                                    }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("submit"),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
