import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/nea/khalti/nea_quick_payment_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/nea/khalti/neabill_page.dart';
import 'package:sahakari_pay/pmc/utils/custom_dotted_border.dart';
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';
import 'package:sahakari_pay/pmc/utils/horizontal_list.dart';
import 'package:flutter_initicon/flutter_initicon.dart';

import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';
import '../../../pmc_homepage.dart';

class KhaltiNeaPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  KhaltiNeaPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<KhaltiNeaPage> createState() => _KhaltiNeaPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _KhaltiNeaPageState extends State<KhaltiNeaPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _KhaltiNeaPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  List? neaOffices;
  List? locationList;
  bool _isLoading = false;
  String? BranchName;

  List<String> neaOfficeName = [];

  List? ListQuickPayment;

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    getJsonData();
    postforListQuickPayment();
  }

  Future<String> getJsonData() async {
    String url = "${baseUrl}api/v1/khalti/details/neaoffices";
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final response = await http.get(Uri.parse(url), headers: {
      // "Token": "${accesstoken}",
      // "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP",
      'Authorization': "Bearer ${accesstoken}",
    });
    setState(() {
      _isLoading = true;
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        var convertDataToJson = json.decode(response.body);
        print(convertDataToJson);
        neaOffices = convertDataToJson["Data"]["Counters"];

        for (var item in neaOffices!) {
          neaOfficeName.add(item["Name"]);
        }

        //print("the list of Nea Offices: ${neaOffices}");

        // CooperativeSharedPreferences.setsmsCode(
        //     convertDataToJson["payload"][0]["smsCode"].toString());
      });
      return "success";
    } else {
      setState(() {
        _isLoading = false;
      });
      var convertDataToJson = json.decode(response.body);
      print(convertDataToJson);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  showMessage() {
    if (dropdownValue != null) {
      setState(() {
        _isLoading = true;
      });
      getBillList();
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("select_branch"),
              ),
              actions: [
                const Spacer(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
    }
  }

  String? SessionId;
  String? scNo;
  String? CustomerName;
  String? ConsumerId;
  String? OfficeCode;
  String? Office;
  double? TotalDueAmount;
  List? BillDetail;

  Future<String> getBillList() async {
    var jsonResponse;
    String url =
        "${baseUrl}api/v1/khalti/details/neabilldetails";
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map body = {
      "ScNo": scnumbercontroller.text.toString(),
      "ConsumerId": customeridcontroller.text.toString(),
      "OfficeCode": dropdownValue.toString(),
    };
    final response = await http.post(Uri.parse(url), body: body, headers: {
      'Authorization': "Bearer ${accesstoken}",
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(response.body);
        // neaOffices = convertDataToJson["OfficeCodes"];
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });
          if (jsonResponse["Data"] == null) {
            if (jsonResponse["Error"] != null) {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                            child: Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 30,
                            ),
                          )
                        ],
                      ),
                    ),
                    content: Text(jsonResponse["Error"]["details"]),
                    actions: [
                      // Spacer(),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        ),
                      )
                    ],
                  );
                });
            }
          } else {
            SessionId = jsonResponse["Data"]["Session_Id"];
            scNo = scnumbercontroller.text.toString();
            CustomerName = jsonResponse["Data"]["Consumer_Name"];
            ConsumerId = customeridcontroller.text.toString();
            TotalDueAmount = double.parse(jsonResponse["Data"]["Total_Due_Amount"].toString());
            Office = BranchName.toString();
            OfficeCode = dropdownValue.toString();
            BillDetail = jsonResponse["Data"]["Due_Bills"];
            print("the list of Nea Offices: ${jsonResponse}");
            if (TotalDueAmount! > 0) {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => KhaltiNeaBillListPage(
                          coopList: coopList,
                          userId: userId,
                          accesstoken: accesstoken,
                          balance: balance,
                          baseUrl: baseUrl,
                          accountno: accountno,
                          SessionId: SessionId,
                          scNo: scNo,
                          CustomerName: CustomerName,
                          ConsumerId: ConsumerId,
                          TotalDueAmount: TotalDueAmount,
                          Office: Office,
                          OfficeCode: OfficeCode,
                          BillDetail: BillDetail,
                          primaryColor: primaryColor,
                          loginButtonTitleColor: loginButtonTitleColor,
                          loginbuttonColor: loginbuttonColor,
                          loginTextFieldColor: loginTextFieldColor,
                          dasboardIconColor: dasboardIconColor,
                          dashboardTopTitleColor: dashboardTopTitleColor,
                          SecondaryColor: SecondaryColor,
                        )));
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Center(
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 8.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("alert"),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 8.0),
                              child: Icon(
                                Icons.add_alert,
                                color: Colors.red,
                                size: 30,
                              ),
                            )
                          ],
                        ),
                      ),
                      content: const Text(
                          "No Due Amount"),
                      actions: [
                        // Spacer(),
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!.localizedString("okay"),
                            style: TextStyle(color: Colors.green[900]),
                          ),
                        )
                      ],
                    );
                  });
            }
          }
        }

        // CooperativeSharedPreferences.setsmsCode(
        //     convertDataToJson["payload"][0]["smsCode"].toString());
      });
      return "success";
    } else if (response.statusCode == 400) {
      _isLoading = false;
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(
                    jsonResponse["Message"].toString().toLowerCase() ==
                            "Invalid Amount".toString().toLowerCase()
                        ? "No Due Amount"
                        : jsonResponse["Message"]),
                actions: [
                  // Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool showBalance = false;

  String? dropdownValue;
  final TextEditingController scnumbercontroller = TextEditingController();
  final TextEditingController customeridcontroller = TextEditingController();
  final GlobalKey<FormState> globalFormKey = GlobalKey();

  Future<bool> postforListQuickPayment() async {
    String url = "${baseUrl}api/v1/${userId}/Khalti/getquickpaymentnea";
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (mounted) {
      if (res.statusCode == 200) {
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
        setState(() {
          _isLoading = false;
          ListQuickPayment = jsonResponse["Payload"];
        });
        return true;
      } else if (res.statusCode == 400) {
        jsonResponse = json.decode(res.body);
        setState(() {
          _isLoading = false;
        });
        print(jsonResponse);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.localizedString("alert"),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 40,
                      )
                    ],
                  ),
                ),
                content: Text("${jsonResponse["Message"]}"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                      ))
                ],
              );
            });
      } else if (res.statusCode == 401) {
        jsonResponse = json.decode(res.body);
        setState(() {
          _isLoading = false;
        });
        print(jsonResponse);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.localizedString("alert"),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 40,
                      )
                    ],
                  ),
                ),
                content: Text("${jsonResponse["Message"]}"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                      ))
                ],
              );
            });
      } else {
        setState(() {
          _isLoading = false;
        });
        jsonResponse = json.decode(res.body);
        print("this is the error response:${jsonResponse}");
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.localizedString("alert"),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50,
                      )
                    ],
                  ),
                ),
                content: Text(AppLocalizations.of(context)!
                    .localizedString("something_went_wrong")),
                actions: [
                  // const Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
        return false;
      }
    }
    return true;
  }

  Future<bool> removeforListQuickPayment(int id) async {
    String url = "${baseUrl}api/v1/removequickpaymentnea/$id";
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      setState(() {
        //_isLoading = false;
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
      });
      postforListQuickPayment();
      return true;
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error resposne: ${jsonResponse}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "Something went Wrong. PLease try again.",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
      return false;
    }
  }

  showMessageRemove(int id) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
            child: Column(
              children: [
                Text(
                  AppLocalizations.of(context)!.localizedString("alert"),
                ),
                SizedBox(
                  height: 10,
                ),
                Icon(
                  Icons.add_alert,
                  color: Colors.red,
                  size: 50,
                )
              ],
            ),
          ),
          content: Text(AppLocalizations.of(context)!
              .localizedString("transaction_message3")),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                AppLocalizations.of(context)!.localizedString("cancel"),
                style: TextStyle(color: Colors.green[900]),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  _isLoading = true;
                });
                removeforListQuickPayment(id);
              },
              child: Text(
                AppLocalizations.of(context)!.localizedString("okay"),
                style: TextStyle(color: Colors.green[900]),
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            top: 40,
                            child: IconButton(
                                onPressed: () {
                                  movetohomepage();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                          Positioned(
                            left: 100,
                            top: 50,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sahakaari_pay"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 95,
                            child: Row(
                              children: [
                                const SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: Image(
                                      image: AssetImage(
                                          "assets/images/wallet_icon.png")),
                                ),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  showBalance
                                      ? AppLocalizations.of(context)!
                                              .localizedString("rs") +
                                          "${balance.toString()}"
                                      : "xxx.xx".toUpperCase(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 180.0),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          if (showBalance == false) {
                                            showBalance = true;
                                          } else {
                                            showBalance = false;
                                          }
                                        });
                                      },
                                      icon: Icon(
                                        showBalance
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        size: 25.0,
                                        color: Colors.white,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                          const EdgeInsets.only(left: 25.0, right: 25, top: 20),
                          child: Center(
                            child: Container(
                              //height: 100,
                              width: MediaQuery.of(context).size.width - 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Colors.black12),
                                color: Colors.white,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 5.0, right: 20.0, top: 5.0),
                                    child: Container(
                                      child: Text(
                                        AppLocalizations.of(context)!
                                            .localizedString("quick_payment"),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          shadows: [Shadow(color: Colors.grey, blurRadius: 2, offset: Offset(1, 1))],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                          margin: EdgeInsets.symmetric(horizontal: 15),
                                          height: 60,
                                          width: 60,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            border: CustomDottedBorder.all(
                                              width: 1,
                                              color: Colors.grey,
                                            ),
                                            borderRadius: BorderRadius.circular(30),
                                          ),
                                          child: IconButton(
                                            icon: Icon(Icons.add, color: Colors.grey),
                                            onPressed: () async {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) => KhaltiNEAQuickPaymentPage(
                                                    coopList: coopList,
                                                    neaOffices: neaOffices,
                                                    userId: userId,
                                                    accesstoken: accesstoken,
                                                    balance: balance,
                                                    baseUrl: baseUrl,
                                                    accountno: accountno,
                                                    primaryColor: primaryColor,
                                                    loginButtonTitleColor: loginButtonTitleColor,
                                                    loginbuttonColor: loginbuttonColor,
                                                    loginTextFieldColor: loginTextFieldColor,
                                                    dasboardIconColor: dasboardIconColor,
                                                    dashboardTopTitleColor: dashboardTopTitleColor,
                                                    SecondaryColor: SecondaryColor,
                                                  ),
                                                ),
                                              ).then((value) {
                                                setState(() {
                                                  _isLoading = true;
                                                });
                                                postforListQuickPayment();
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width - 158,
                                        child: HorizontalList(
                                          spacing: 15,
                                          itemCount: ListQuickPayment == null ? 0 : ListQuickPayment!.length,
                                          itemBuilder: (context, index) {
                                            return Column(
                                              children: [
                                                Stack(
                                                  alignment: Alignment.topRight,
                                                  children: [
                                                    Container(
                                                      margin: const EdgeInsets.all(8.7),
                                                      child: InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            dropdownValue = ListQuickPayment![index]["BranchCode"];
                                                            BranchName = ListQuickPayment![index]["BranchName"];

                                                            scnumbercontroller.text = ListQuickPayment![index]["SCNumber"];
                                                            customeridcontroller.text = ListQuickPayment![index]["CustomerId"];
                                                          });
                                                        },
                                                        child: Initicon(
                                                          text: ListQuickPayment![index]["BranchName"],
                                                          backgroundColor: Colors.green,
                                                          borderRadius: BorderRadius.circular(5),
                                                          size: 60,
                                                        ),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        showMessageRemove(ListQuickPayment![index]["Id"]);
                                                      },
                                                      child: CircleAvatar(
                                                        radius: 10,
                                                        backgroundColor: Color(int.parse(loginbuttonColor.toString())),
                                                        child: const Icon(
                                                          Icons.close,
                                                          color: Colors.white,
                                                          size: 16,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  width: 60,
                                                  child: Text(
                                                    ListQuickPayment![index]["CustomerId"],
                                                    style: TextStyle(
                                                      fontSize: 10,
                                                      color: Color(int.parse(loginbuttonColor.toString())),
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                              ],
                                            );
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("nea"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ),
                            // Padding(
                            //   padding: EdgeInsets.only(left: 40.0),
                            //   child: Text(
                            //       AppLocalizations.of(context)!
                            //           .localizedString("select_branch"),
                            //       style: TextStyle(
                            //         color: Colors.black87,
                            //         fontSize: 14,
                            //       )),
                            // ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0, right: 10, top: 20),
                              child: DropdownSearch<String>(
                                popupProps: PopupProps.dialog(
                                  searchFieldProps: TextFieldProps(
                                    // autofocus: true,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 24, vertical: 10),
                                    decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        hintText: AppLocalizations.of(context)!
                                            .localizedString("search"),
                                        hintStyle: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                            color: Colors.grey[400]),
                                        suffixIcon: Icon(
                                          Icons.search,
                                          size: 30,
                                          color: Colors.grey[500],
                                        )),
                                  ),
                                  showSearchBox: true,
                                ),
                                dropdownDecoratorProps: DropDownDecoratorProps(
                                  dropdownSearchDecoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)!
                                          .localizedString("select_branch"),
                                      hintStyle: TextStyle(
                                          fontSize: 16, color: Colors.black87)),
                                ),
                                items: neaOfficeName,
                                selectedItem: BranchName,
                                // hint: AppLocalizations.of(context)!
                                //     .localizedString("search"),
                                onChanged: (String? newValue) {
                                  final index = neaOffices!.indexWhere(
                                      (element) =>
                                          element["Name"] == newValue);
                                  //print(index);
                                  //print(neaOffices![index]["OfficeCodes"]);
                                  newValue = neaOffices![index]["Value"];
                                  setState(() {
                                    dropdownValue = newValue!;
                                    BranchName = neaOffices![index]["Name"];
                                    print("this is offices : ${dropdownValue}");
                                  });
                                },
                              ),
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.only(
                            //       left: 30.0, right: 10.0, top: 10.0),
                            //   child: DropdownSearch<String>(
                            //     mode: Mode.BOTTOM_SHEET,
                            //     showSearchBox: true,
                            //     items: neaOfficeName,
                            //     onChanged: (String? newValue) {
                            //       final index = neaOffices!.indexWhere(
                            //           (element) => element["Office"] == newValue);
                            //       //print(index);
                            //       //print(neaOffices![index]["OfficeCodes"]);
                            //       newValue = neaOffices![index]["OfficeCodes"];
                            //       setState(() {
                            //         dropdownValue = newValue!;
                            //         print("this is offices : ${dropdownValue}");
                            //       });
                            //     },
                            //   ),
                            // ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Form(
                                  key: globalFormKey,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 50.0,
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              120,
                                          child: TextFormField(
                                            controller: scnumbercontroller,
                                            maxLength: 50,
                                            keyboardType: TextInputType.text,
                                            validator: (value) => value!.isEmpty
                                                ? AppLocalizations.of(context)!
                                                    .localizedString("sc_no")
                                                : null,
                                            onSaved: (value) =>
                                                scnumbercontroller,
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(),
                                              labelText:
                                                  AppLocalizations.of(context)!
                                                      .localizedString("sc_no"),
                                              labelStyle: TextStyle(
                                                  color: Colors.green),
                                              counterText: "",
                                              icon: Icon(
                                                Icons.confirmation_number,
                                                color: Colors.green,
                                                size: 20.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          left: 50.0,
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width -
                                              120,
                                          child: TextFormField(
                                            controller: customeridcontroller,
                                            maxLength: 50,
                                            keyboardType: TextInputType.text,
                                            validator: (value) => value!.isEmpty
                                                ? AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "customer_id")
                                                : null,
                                            onSaved: (value) =>
                                                customeridcontroller,
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(),
                                              labelText:
                                                  AppLocalizations.of(context)!
                                                      .localizedString(
                                                          "customer_id"),
                                              labelStyle: TextStyle(
                                                  color: Colors.green),
                                              counterText: "",
                                              icon: Icon(
                                                Icons.verified_user,
                                                color: Colors.green,
                                                size: 20.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  )),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 25.0, top: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: Color(int.parse(
                                            loginbuttonColor.toString())),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 5,
                                            blurRadius: 7,
                                            offset: const Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ]),
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width - 120,
                                    child: TextButton(
                                      onPressed: () async {
                                        if (validateAndSave()) {
                                          setState(() {
                                            _isLoading = true;
                                          });
                                          showMessage();
                                        } else {
                                          setState(() {
                                            _isLoading = false;
                                            // showDialog(
                                            //     context: context,
                                            //     builder: (BuildContext context) {
                                            //       return AlertDialog(
                                            //         title: Center(
                                            //           child: Column(
                                            //             children: [
                                            //               Text(
                                            //                 AppLocalizations.of(
                                            //                         context)!
                                            //                     .localizedString(
                                            //                         "alert"),
                                            //               ),
                                            //               SizedBox(
                                            //                 height: 10,
                                            //               ),
                                            //               Icon(
                                            //                 Icons.add_alert,
                                            //                 color: Colors.red,
                                            //                 size: 50,
                                            //               )
                                            //             ],
                                            //           ),
                                            //         ),
                                            //         content: Text(
                                            //           AppLocalizations.of(context)!
                                            //               .localizedString(""),
                                            //         ),
                                            //         actions: [
                                            //           const Spacer(),
                                            //           Container(
                                            //             height: 40,
                                            //             width: 80,
                                            //             decoration: BoxDecoration(
                                            //                 color:
                                            //                     Colors.green[900],
                                            //                 borderRadius:
                                            //                     BorderRadius
                                            //                         .circular(30)),
                                            //             child: TextButton(
                                            //               onPressed: () {
                                            //                 Navigator.of(context)
                                            //                     .pop();
                                            //               },
                                            //               child: Text(
                                            //                 AppLocalizations.of(
                                            //                         context)!
                                            //                     .localizedString(
                                            //                         "okay"),
                                            //                 style: TextStyle(
                                            //                     color:
                                            //                         Colors.white),
                                            //               ),
                                            //             ),
                                            //           )
                                            //         ],
                                            //       );
                                            //     });
                                          });
                                        }
                                      },
                                      child: Text(
                                        AppLocalizations.of(context)!
                                            .localizedString("submit"),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
