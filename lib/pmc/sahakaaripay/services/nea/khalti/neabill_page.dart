import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/nea/khalti/nea_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/nea/khalti/neapdfbill_page.dart';

import '../../../../constants.dart';
import '../../../../local_auth_api.dart';
import '../../../../utils/localizations.dart';
import '../../../../utils/utils.dart';
import '../../../models/loading.dart';

class KhaltiNeaBillListPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? SessionId;
  String? scNo;
  String? CustomerName;
  String? ConsumerId;
  String? OfficeCode;
  String? Office;
  double? TotalDueAmount;
  List? BillDetail;
  List? NeaOffices;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  KhaltiNeaBillListPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.SessionId,
    this.scNo,
    this.CustomerName,
    this.ConsumerId,
    this.OfficeCode,
    this.Office,
    this.TotalDueAmount,
    this.BillDetail,
    this.NeaOffices,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _KhaltiNeaBillListPageState createState() => _KhaltiNeaBillListPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        SessionId,
        scNo,
        CustomerName,
        ConsumerId,
        OfficeCode,
        Office,
        TotalDueAmount,
        BillDetail,
        NeaOffices,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _KhaltiNeaBillListPageState extends State<KhaltiNeaBillListPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? SessionId;
  String? scNo;
  String? CustomerName;
  String? ConsumerId;
  String? OfficeCode;
  String? Office;
  double? TotalDueAmount;
  List? BillDetail;
  List? NeaOffices;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _KhaltiNeaBillListPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.SessionId,
    this.scNo,
    this.CustomerName,
    this.ConsumerId,
    this.OfficeCode,
    this.Office,
    this.TotalDueAmount,
    this.BillDetail,
    this.NeaOffices,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetoNeaPage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => KhaltiNeaPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  // final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    getJsonData();
  }

  String? serviceCharge;

  Future<String> getJsonData() async {
    String url = "${baseUrl}api/v1/khalti/details/neaservicecharge";
    Map body = {
      "SessionId": SessionId.toString(),
      "Amount": TotalDueAmount.toString(),
    };
    final response = await http.post(Uri.parse(url), body: body, headers: {
      'Authorization': "Bearer ${accesstoken}",
    });
    setState(() {
      _isLoading = true;
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        var convertDataToJson = json.decode(response.body);
        serviceCharge = convertDataToJson["Data"]["Service_Charge"].toString();
      });
      return "success";
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoNeaPage();
        },
        child: Scaffold(
          body: _isLoading
              ? Loading()
              : SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 50.0),
                        child: Center(
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("nea_bill_details"),
                            style: TextStyle(
                                color: Colors.green[900],
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 30),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sc_no"),
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 14),
                            ),
                            const SizedBox(
                              width: 100,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text("${scNo}")),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 30),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("customer_name"),
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 14),
                            ),
                            const SizedBox(
                              width: 38,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text("${CustomerName}")),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 30),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("office"),
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 14),
                            ),
                            const SizedBox(
                              width: 108,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text("${Office}")),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 30),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("remaining_due"),
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 14),
                            ),
                            const SizedBox(
                              width: 44,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text("${TotalDueAmount.toString()}")),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 30),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("total_amount"),
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 14),
                            ),
                            const SizedBox(
                              width: 54,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text("${TotalDueAmount.toString()}")),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 30),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("service_charge"),
                              style: TextStyle(
                                  color: Colors.black87, fontSize: 14),
                            ),
                            const SizedBox(
                              width: 54,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width / 2.5,
                                child: Text("${serviceCharge.toString()}")),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.green[700],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 4 - 20,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("date"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 4 - 20,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("bill_amount"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 4 - 20,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("payable_amount"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 4 - 20,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("bill_of"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.green[700],
                      ),
                      for (var item in BillDetail!) ...[
                        Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 4 -
                                            20,
                                    child: Text(
                                      "${item["Bill_Date"]}",
                                      style: const TextStyle(
                                        color: Colors.black87,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 35,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 4 -
                                            20,
                                    child: Text("${item["Bill_Amount"]}",
                                        style: const TextStyle(
                                          color: Colors.black87,
                                          fontSize: 14,
                                        )),
                                  ),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 4 -
                                            20,
                                    child: Text("${item["Payable_Amount"]}",
                                        style: const TextStyle(
                                          color: Colors.black87,
                                          fontSize: 14,
                                        )),
                                  ),
                                  const SizedBox(
                                    width: 05,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 4 -
                                            20,
                                    child: Text("${item["Due_Bill_Of"]}",
                                        style: const TextStyle(
                                          color: Colors.black87,
                                          fontSize: 14,
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Divider(
                              thickness: 1,
                              color: Colors.green[700],
                            ),
                          ],
                        )
                      ],
                      // Container(
                      //   height: 100,
                      //   decoration: const BoxDecoration(color: Colors.white),
                      //   child: ListView.builder(
                      //       physics: NeverScrollableScrollPhysics(),
                      //       itemCount:
                      //           BillDetail == null ? 0 : BillDetail?.length,
                      //       itemBuilder: (BuildContext context, int index) {
                      //         return Column(
                      //           children: [
                      //             Padding(
                      //               padding: const EdgeInsets.only(left: 20.0),
                      //               child: Row(
                      //                 crossAxisAlignment:
                      //                     CrossAxisAlignment.start,
                      //                 children: [
                      //                   Container(
                      //                     width: MediaQuery.of(context)
                      //                                 .size
                      //                                 .width /
                      //                             4 -
                      //                         20,
                      //                     child: Text(
                      //                       "${BillDetail?[index]["BillDate"]}",
                      //                       style: const TextStyle(
                      //                         color: Colors.black87,
                      //                         fontSize: 14,
                      //                       ),
                      //                     ),
                      //                   ),
                      //                   const SizedBox(
                      //                     width: 35,
                      //                   ),
                      //                   Container(
                      //                     width: MediaQuery.of(context)
                      //                                 .size
                      //                                 .width /
                      //                             4 -
                      //                         20,
                      //                     child: Text(
                      //                         "${BillDetail?[index]["BillAmount"]}",
                      //                         style: const TextStyle(
                      //                           color: Colors.black87,
                      //                           fontSize: 14,
                      //                         )),
                      //                   ),
                      //                   const SizedBox(
                      //                     width: 20,
                      //                   ),
                      //                   Container(
                      //                     width: MediaQuery.of(context)
                      //                                 .size
                      //                                 .width /
                      //                             4 -
                      //                         20,
                      //                     child: Text(
                      //                         "${BillDetail?[index]["PayableAmount"]}",
                      //                         style: const TextStyle(
                      //                           color: Colors.black87,
                      //                           fontSize: 14,
                      //                         )),
                      //                   ),
                      //                   const SizedBox(
                      //                     width: 05,
                      //                   ),
                      //                   Container(
                      //                     width: MediaQuery.of(context)
                      //                                 .size
                      //                                 .width /
                      //                             4 -
                      //                         20,
                      //                     child: Text(
                      //                         "${BillDetail?[index]["DueBillOf"]}",
                      //                         style: const TextStyle(
                      //                           color: Colors.black87,
                      //                           fontSize: 14,
                      //                         )),
                      //                   ),
                      //                 ],
                      //               ),
                      //             ),
                      //             const SizedBox(
                      //               height: 20,
                      //             ),
                      //             Divider(
                      //               thickness: 1,
                      //               color: Colors.green[700],
                      //             ),
                      //           ],
                      //         );
                      //       }),
                      // ),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // const SizedBox(
                          //   width: 100,
                          // ),
                          Container(
                            height: 50,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Color(
                                    int.parse(loginbuttonColor.toString())),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                                onPressed: () {
                                  movetoNeaPage();
                                },
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("back"),
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                )),
                          ),
                          const SizedBox(
                            width: 60,
                          ),
                          Container(
                            height: 50,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Color(
                                    int.parse(loginbuttonColor.toString())),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                                onPressed: () {
                                  if (showBiometricbuttonTransaction) {
                                    transactionPinMessageBiometric();
                                  } else {
                                    transactionPinMessage();
                                  }
                                },
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("pay"),
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                )),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
        ));
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${TotalDueAmount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("serviceCharge") +
                          "${serviceCharge.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2a") +
                          " ${scNo.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postDataforBillPay();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      //const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${TotalDueAmount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("serviceCharge") +
                          "${serviceCharge.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2a") +
                          " ${scNo.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      postDataforBillPay();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  String? ReferenceId;
  String? TransactionId;
  String? paymentMessage;
  String? PaymentBillDate;

  postDataforBillPay() async {
    String url = "${baseUrl}api/v1/khalti/payment/neabill";
    Map body = {
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
      "SessionId": SessionId.toString(),
      "Amount": TotalDueAmount.toString(),
      "ServiceCharge": serviceCharge.toString(),
      "ConsumerID": ConsumerId.toString(),
    };
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      // "Content-Type": application/json,
      "Authorization": "Bearer ${accesstoken}"
    });
    print(body);

    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          TransactionId = jsonResponse["TransactionId"];
          paymentMessage = jsonResponse["Message"];
          // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  Icon(
                    TransactionId != null ? Icons.verified : Icons.cancel,
                    color: TransactionId != null ? Colors.green : Colors.red,
                    size: 40,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    jsonResponse["Message"],
                    style: const TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ],
              )));
              if (TransactionId != null) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            KhaltiNeaPdfBillPage(
                              coopList: coopList,
                              userId: userId,
                              accesstoken: accesstoken,
                              balance: balance,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              scNo: scNo,
                              CustomerName: CustomerName,
                              ConsumerId: ConsumerId,
                              TotalDueAmount: TotalDueAmount,
                              Office: Office,
                              OfficeCode: OfficeCode,
                              BillDetail: BillDetail,
                              TransactionId: TransactionId,
                              paymentMessage: paymentMessage,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                            )));
              }
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else if (res.statusCode == 500) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    }
  }
}
