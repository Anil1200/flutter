import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/fonepay/phonepaypdfbill_page.dart';

import '../../../constants.dart';
import '../../../utils/localizations.dart';
import '../../../utils/utils.dart';
import '../../models/loading.dart';

class PhoneScanPayConfirmPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? amount;
  String? purpose;
  String? remarks;
  String? merchantName;
  String? merchantCode;
  String? qrRequestId;
  String? OriginatingUniqueId;
  String? serviceinfoId;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  PhoneScanPayConfirmPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.amount,
    this.purpose,
    this.remarks,
    this.merchantName,
    this.merchantCode,
    this.qrRequestId,
    this.OriginatingUniqueId,
    this.serviceinfoId,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _PhoneScanPayConfirmPageState createState() => _PhoneScanPayConfirmPageState(
      coopList,
      userId,
      accesstoken,
      balance,
      baseUrl,
      accountno,
      amount,
      purpose,
      remarks,
      merchantName,
      merchantCode,
      qrRequestId,
      OriginatingUniqueId,
      serviceinfoId,
      primaryColor,
      loginButtonTitleColor,
      loginbuttonColor,
      loginTextFieldColor,
      dasboardIconColor,
      dashboardTopTitleColor,
      SecondaryColor);
}

class _PhoneScanPayConfirmPageState extends State<PhoneScanPayConfirmPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? amount;
  String? purpose;
  String? remarks;
  String? merchantName;
  String? merchantCode;
  String? qrRequestId;
  String? OriginatingUniqueId;
  String? serviceinfoId;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _PhoneScanPayConfirmPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.amount,
    this.purpose,
    this.remarks,
    this.merchantName,
    this.merchantCode,
    this.qrRequestId,
    this.OriginatingUniqueId,
    this.serviceinfoId,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    // _isLoading = true;
    // nullCheck();
  }

  bool _isLoading = false;

  bool showBalance = false;

  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  // nullCheck() {
  //   if (result == null) {
  //     qrMessage = "No QR code Scanned";
  //   } else {
  //     qrMessage = result;
  //   }
  // }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${amount} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${merchantName.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          setState(() {
                            _isLoading = false;
                          });
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            qrScanPayment();
                            // postTransferData();
                            // qrValidation();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          const SizedBox(height: 10.0),
                                          const Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("confirm_name"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  qrBookingCancel() async {
    String url = "${baseUrl}api/v1/esewa/scanpay/paymentcancels";
    Map body = {
      "qr_requested_id": qrRequestId,
      "merchant_code": merchantCode,
      "transaction_message": remarks.toString(),
      "amount": amount.toString(),
    };
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status of Payment Cncel of fonepay: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("success"),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Icon(
                            Icons.verified,
                            color: Colors.green,
                            size: 50,
                          )
                        ],
                      ),
                    ),
                    content: Text("Booking Canceled Successfully"),
                    actions: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          TextButton(
                              onPressed: () {
                                movetoHomePage();
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("okay"),
                                style: TextStyle(color: Colors.green[900]),
                              )),
                        ],
                      )
                    ],
                  );
                });

            // qrScanPayment();
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Center(
                  child: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              )),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  String? status;
  String? tcTransactionId;
  String? genericMessage;

  qrScanPayment() async {
    String url = "${baseUrl}api/v1/esewa/scanpay/payments";
    Map mapbody = {
      "MPIN": mpincontroller.text.toString().trim(),
      "serviceinfoId": serviceinfoId.toString(),
      "OriginatingUniqueId": OriginatingUniqueId.toString(),
      "Amount": amount.toString(),
      "Statement": "QRCode. " + remarks.toString(),
      "RequestFields": [
        {"Key": "Remarks", "Value": remarks}
      ]
    };
    mpincontroller.clear();
    print(mapbody);
    var body = json.encode(mapbody);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      'Content-Type': "application/json",
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        isQRTransaction = false;
        _isLoading = true;
        if (jsonResponse != null) {
          status = jsonResponse["status"];
          tcTransactionId =
              jsonResponse["transactionDetail"]["tcTransactionId"];
          genericMessage = jsonResponse["genericMessage"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              if (status.toString().toLowerCase() == "Success".toLowerCase()) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PhonePayPdfBillPage(
                              coopList: coopList,
                              userId: userId,
                              accesstoken: accesstoken,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              balance: balance,
                              amount: amount,
                              purpose: purpose,
                              remarks: remarks,
                              merchantName: merchantName,
                              merchantCode: merchantCode,
                              transactionId: tcTransactionId,
                              paymentmessage: genericMessage,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                            )));
              } else {
                setState(() {
                  _isLoading = false;
                });
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Center(
                            child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("error_alert"),
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 50.0,
                            )
                          ],
                        )),
                        content: Text(
                          AppLocalizations.of(context)!
                              .localizedString("process_error"),
                          style: TextStyle(fontSize: 14),
                        ),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                movetoHomePage();
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("okay"),
                              ))
                        ],
                      );
                    });
              }
            });
          });
        } else {
          movetoHomePage();
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          movetoHomePage();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      } else {
        movetoHomePage();
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      movetoHomePage();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error response: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Center(
                  child: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              )),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      movetoHomePage();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  movetoHomePage() {
    setState(() {
      isQRTransaction = false;
    });
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoHomePage();

        // qrBookingCancel();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetoHomePage();
              // qrBookingCancel();
            },
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
          ),
          title: Text(
            "SahakaariPay",
            // style: TextStyle(
            //     color: Color(int.parse(dashboardTopTitleColor.toString()))
            // ),
          ),
          centerTitle: true,
          toolbarHeight: 100,
        ),
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Padding(
                padding: const EdgeInsets.only(top: 30.0, left: 30.0),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 10.0),
                            child: Icon(
                              Icons.account_balance_wallet_outlined,
                              size: 25.0,
                              color: Color(
                                  int.parse(dasboardIconColor.toString())),
                            ),
                          ),
                          const SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("npr_balance"),
                            style: TextStyle(
                              color: Color(
                                  int.parse(dashboardTopTitleColor.toString())),
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          Text(
                              showBalance
                                  ? AppLocalizations.of(context)!
                                          .localizedString("rs") +
                                      "${balance.toString()}"
                                  : "xxx.xx".toUpperCase(),
                              style: TextStyle(
                                color: Color(int.parse(
                                    dashboardTopTitleColor.toString())),
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                              )),
                          Padding(
                            padding: const EdgeInsets.only(left: 80.0),
                            child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (showBalance == false) {
                                      showBalance = true;
                                    } else {
                                      showBalance = false;
                                    }
                                  });
                                },
                                icon: Icon(
                                  showBalance
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  size: 25.0,
                                  color: Colors.green[900],
                                )),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, left: 20, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2 - 60,
                              child: Text(AppLocalizations.of(context)!
                                  .localizedString("merchant_name")),
                            ),
                            Container(
                                width:
                                    MediaQuery.of(context).size.width / 2 - 60,
                                child: Text(merchantName.toString())),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, left: 20, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2 - 60,
                              child: Text(AppLocalizations.of(context)!
                                  .localizedString("merchant_code")),
                            ),
                            Container(
                                width:
                                    MediaQuery.of(context).size.width / 2 - 60,
                                child: Text(merchantCode.toString())),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, left: 20, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2 - 60,
                              child: Text(AppLocalizations.of(context)!
                                  .localizedString("amount")),
                            ),
                            Container(
                                width:
                                    MediaQuery.of(context).size.width / 2 - 60,
                                child: Text(amount.toString())),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, left: 20, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2 - 60,
                              child: Text(AppLocalizations.of(context)!
                                  .localizedString("purpose")),
                            ),
                            Container(
                                width:
                                    MediaQuery.of(context).size.width / 2 - 60,
                                child: Text(purpose.toString())),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, left: 20, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 2 - 60,
                              child: Text(AppLocalizations.of(context)!
                                  .localizedString("remarks")),
                            ),
                            Container(
                                width:
                                    MediaQuery.of(context).size.width / 2 - 60,
                                child: Text(remarks.toString())),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 50.0, left: 20, right: 20),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 60,
                              width: MediaQuery.of(context).size.width / 2 - 90,
                              decoration: BoxDecoration(
                                  color: Color(
                                      int.parse(loginbuttonColor.toString())),
                                  borderRadius: BorderRadius.circular(10)),
                              child: TextButton(
                                onPressed: () {
                                  movetoHomePage();
                                  // qrBookingCancel();
                                },
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("cancel"),
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 60,
                            ),
                            Container(
                                height: 60,
                                width:
                                    MediaQuery.of(context).size.width / 2 - 90,
                                decoration: BoxDecoration(
                                    color: Color(
                                        int.parse(loginbuttonColor.toString())),
                                    borderRadius: BorderRadius.circular(10)),
                                child: TextButton(
                                    onPressed: () {
                                      transactionPinMessage();
                                    },
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("pay"),
                                      style: TextStyle(color: Colors.white),
                                    ))),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }
}
