// import 'dart:convert';
//
// import 'package:dropdown_search/dropdown_search.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
// import 'package:http/http.dart' as http;
// import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
// import 'package:sahakari_pay/pmc/sahakaaripay/services/fonepay/phonepay_confirm.dart';
//
// import '../../../utils/localizations.dart';
// import '../../models/loading.dart';
//
// class PhoneScanPayPage extends StatefulWidget {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? result;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//   PhoneScanPayPage({
//     Key? key,
//     this.coopList,
//     this.userId,
//     this.accesstoken,
//     this.balance,
//     this.baseUrl,
//     this.accountno,
//     this.result,
//     this.primaryColor,
//     this.loginButtonTitleColor,
//     this.loginbuttonColor,
//     this.loginTextFieldColor,
//     this.dasboardIconColor,
//     this.dashboardTopTitleColor,
//     this.SecondaryColor,
//   }) : super(key: key);
//
//   @override
//   _PhoneScanPayPageState createState() => _PhoneScanPayPageState(
//       coopList,
//       userId,
//       accesstoken,
//       balance,
//       baseUrl,
//       accountno,
//       result,
//       primaryColor,
//       loginButtonTitleColor,
//       loginbuttonColor,
//       loginTextFieldColor,
//       dasboardIconColor,
//       dashboardTopTitleColor,
//       SecondaryColor);
// }
//
// class _PhoneScanPayPageState extends State<PhoneScanPayPage> {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? result;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//   _PhoneScanPayPageState(
//     this.coopList,
//     this.userId,
//     this.accesstoken,
//     this.balance,
//     this.baseUrl,
//     this.accountno,
//     this.result,
//     this.primaryColor,
//     this.loginButtonTitleColor,
//     this.loginbuttonColor,
//     this.loginTextFieldColor,
//     this.dasboardIconColor,
//     this.dashboardTopTitleColor,
//     this.SecondaryColor,
//   );
//
//   void initState() {
//     super.initState();
//     _isLoading = true;
//     nullCheck();
//     qrValidation();
//   }
//
//   bool _isLoading = false;
//   final GlobalKey<FormState> globalFormKey = GlobalKey();
//   final GlobalKey<FormState> globalFormKey2 = GlobalKey();
//   final TextEditingController mpincontroller = TextEditingController();
//   final TextEditingController amountController = TextEditingController();
//   final TextEditingController statementController = TextEditingController();
//
//   String? dropdownvalue;
//
//   String? qrMessage;
//
//   String? merchantName;
//   String? merchantCode;
//   String? qrRequestId;
//
//   nullCheck() {
//     if (result == null) {
//       qrMessage = "No QR code Scanned";
//     } else {
//       qrMessage = result;
//     }
//   }
//
//   Future<void> scanBarcode() async {
//     try {
//       final qrcode = await FlutterBarcodeScanner.scanBarcode(
//           "#ff6666", "Cancel", true, ScanMode.QR);
//       if (!mounted) return;
//       setState(() {
//         this.result = qrcode.toString();
//         print("this is the require QR code: ${result}");
//       });
//     } on PlatformException {
//       setState(() {
//         result = "Failed to get Platform version.";
//         // if (ex.code == FlutterBarcodeScanner.) {
//         //   result = "Failed to get platform version.";
//         // } else {
//         //   setState(() {
//         //     result = "Unknown Error $ex";
//         //   });
//         // }
//       });
//     }
//     // toaccountName = result?.split(",")[1];
//     // toaccountno = result?.split(",")[0];
//     // print(
//     //     "this is the required QRcode: ${toaccountno.toString()} and ${toaccountName.toString()}");
//     setState(() {
//       if (result != null) {
//         Navigator.pushReplacement(
//             context,
//             MaterialPageRoute(
//                 builder: (context) => PhoneScanPayPage(
//                       coopList: coopList,
//                       userId: userId,
//                       accesstoken: accesstoken,
//                       baseUrl: baseUrl,
//                       accountno: accountno,
//                       balance: balance,
//                       result: result,
//                       primaryColor: primaryColor,
//                       loginButtonTitleColor: loginButtonTitleColor,
//                       loginbuttonColor: loginbuttonColor,
//                       loginTextFieldColor: loginTextFieldColor,
//                       dasboardIconColor: dasboardIconColor,
//                       dashboardTopTitleColor: dashboardTopTitleColor,
//                       SecondaryColor: SecondaryColor,
//                     )));
//       }
//     });
//   }
//
//   String? success;
//   String? responseMessage;
//
//   qrValidation() async {
//     String url = "${baseUrl}api/v1/esewa/scanpay/qrValidation";
//     Map body = {"qr_message": qrMessage.toString()};
//     print(body);
//     var jsonResponse;
//     var res = await http.post(Uri.parse(url),
//         body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
//     if (res.statusCode == 200) {
//       jsonResponse = json.decode(res.body);
//       print("Response status: ${jsonResponse}");
//       setState(() {
//         _isLoading = true;
//         if (jsonResponse != null) {
//           // success = jsonResponse["qrMessage"];
//           responseMessage = jsonResponse["responseMessage"];
//           print("this is the error response message ${responseMessage}");
//           setState(() {
//             _isLoading = false;
//           });
//           if (responseMessage.toString().toLowerCase() ==
//               "Success".toLowerCase()) {
//             merchantName = jsonResponse["qrMessage"]["merchantName"];
//             merchantCode = jsonResponse["qrMessage"]["merchantCode"];
//             qrRequestId = jsonResponse["qrMessage"]["qrRequestId"];
//           }
//         }
//       });
//     } else if (res.statusCode == 400) {
//       setState(() {
//         _isLoading = true;
//       });
//       jsonResponse = json.decode(res.body);
//       print(jsonResponse);
//       if (jsonResponse != null) {
//         setState(() {
//           _isLoading = false;
//           showDialog(
//               context: context,
//               builder: (BuildContext context) {
//                 return AlertDialog(
//                   title: Center(
//                       child: Column(
//                     children: [
//                       Text(
//                         AppLocalizations.of(context)!
//                             .localizedString("error_alert"),
//                         style: TextStyle(fontSize: 16),
//                       ),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Icon(
//                         Icons.add_alert,
//                         color: Colors.red,
//                         size: 50.0,
//                       )
//                     ],
//                   )),
//                   content: Text(
//                     "${jsonResponse["Message"]}",
//                     style: TextStyle(fontSize: 14),
//                   ),
//                   actions: [
//                     TextButton(
//                         onPressed: () {
//                           Navigator.of(context).pop();
//                         },
//                         child: Text(
//                           AppLocalizations.of(context)!.localizedString("okay"),
//                         ))
//                   ],
//                 );
//               });
//         });
//       }
//     } else if (res.statusCode == 401) {
//       setState(() {
//         _isLoading = false;
//       });
//       jsonResponse = json.decode(res.body);
//       print(jsonResponse);
//       return showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Center(
//                   child: Column(
//                 children: [
//                   Text(
//                     AppLocalizations.of(context)!
//                         .localizedString("error_alert"),
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Icon(
//                     Icons.add_alert,
//                     color: Colors.red,
//                     size: 50.0,
//                   )
//                 ],
//               )),
//               content: Text(
//                 "${jsonResponse["Message"]}",
//                 style: TextStyle(fontSize: 14),
//               ),
//               actions: [
//                 TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Text(
//                       AppLocalizations.of(context)!.localizedString("okay"),
//                     ))
//               ],
//             );
//           });
//     } else {
//       jsonResponse = json.decode(res.body);
//       print("the requires response: ${jsonResponse}");
//       setState(() {
//         _isLoading = false;
//       });
//       return showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Column(
//                 children: [
//                   Text(
//                     AppLocalizations.of(context)!
//                         .localizedString("error_alert"),
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Icon(
//                     Icons.add_alert,
//                     color: Colors.red,
//                     size: 50.0,
//                   )
//                 ],
//               ),
//               content: Text(
//                 AppLocalizations.of(context)!
//                     .localizedString("something_went_wrong"),
//                 style: TextStyle(fontSize: 14),
//               ),
//               actions: [
//                 TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Text(
//                       AppLocalizations.of(context)!.localizedString("okay"),
//                     ))
//               ],
//             );
//           });
//     }
//   }
//
//   String? serviceinfoId;
//   String? genericMessage;
//   String? OriginatingUniqueId;
//
//   showMessage() {
//     if (dropdownvalue == null) {
//       setState(() {
//         _isLoading = false;
//       });
//       return showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Column(
//                 children: [
//                   Text(
//                     AppLocalizations.of(context)!
//                         .localizedString("error_alert"),
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Icon(
//                     Icons.add_alert,
//                     color: Colors.red,
//                     size: 50.0,
//                   )
//                 ],
//               ),
//               content: Text(
//                 AppLocalizations.of(context)!.localizedString("select_purpose"),
//                 style: TextStyle(fontSize: 14),
//               ),
//               actions: [
//                 TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Text(
//                       AppLocalizations.of(context)!.localizedString("okay"),
//                     ))
//               ],
//             );
//           });
//     } else {
//       setState(() {
//         _isLoading = true;
//       });
//       postTransferData();
//     }
//   }
//
//   postTransferData() async {
//     String url = "${baseUrl}api/v1/esewa/scanpay/paymentbookings";
//     Map bodymap = {
//       "TransactionURI": merchantCode.toString(), //get from scanning the qr code
//       // "MPIN": mpincontroller.text.toString(),
//       "Statement": statementController.text.toString(),
//       "AmountInformation": {
//         "Currency": "NRS",
//         "Amount": amountController.text.toString() //from users
//       },
//       "RequestFields": [
//         {"Key": "qrType", "Value": "OFFLINE"},
//         {"Key": "qrRequestId", "Value": qrRequestId},
//         {"Key": "merchantCode", "Value": merchantCode},
//         {"Key": "merchantName", "Value": merchantName}
//       ]
//     };
//     var body = json.encode(bodymap);
//     print(body);
//     var jsonResponse;
//     var res = await http.post(Uri.parse(url), body: body, headers: {
//       'Content-Type': "application/json",
//       'Authorization': "Bearer ${accesstoken}"
//     });
//     if (res.statusCode == 200) {
//       jsonResponse = json.decode(res.body);
//       print("Response status of paymentbooking of fonepay: ${jsonResponse}");
//       setState(() {
//         _isLoading = true;
//         if (jsonResponse != null) {
//           setState(() {
//             _isLoading = false;
//             OriginatingUniqueId = jsonResponse["uniqueOriginatedId"];
//             serviceinfoId = jsonResponse["serviceInfoId"];
//             // data = json.decode(jsonResponse["Data"]);
//             print(
//                 "this is data: ${serviceinfoId} and the OriginatingUniqueId ${OriginatingUniqueId}");
//             genericMessage = jsonResponse["genericMessage"];
//             print("this is message:${genericMessage}");
//             ScaffoldMessenger.of(context).showSnackBar(SnackBar(
//               content: Text(genericMessage.toString()),
//             ));
//             Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                     builder: (context) => PhoneScanPayConfirmPage(
//                           coopList: coopList,
//                           userId: userId,
//                           accesstoken: accesstoken,
//                           baseUrl: baseUrl,
//                           accountno: accountno,
//                           balance: balance,
//                           amount: amountController.text.toString(),
//                           purpose: dropdownvalue.toString(),
//                           remarks: statementController.text.toString(),
//                           merchantName: merchantName,
//                           merchantCode: merchantCode,
//                           qrRequestId: qrRequestId,
//                           OriginatingUniqueId: OriginatingUniqueId,
//                           serviceinfoId: serviceinfoId,
//                           primaryColor: primaryColor,
//                           loginButtonTitleColor: loginButtonTitleColor,
//                           loginbuttonColor: loginbuttonColor,
//                           loginTextFieldColor: loginTextFieldColor,
//                           dasboardIconColor: dasboardIconColor,
//                           dashboardTopTitleColor: dashboardTopTitleColor,
//                           SecondaryColor: SecondaryColor,
//                         )));
//           });
//         }
//       });
//     } else if (res.statusCode == 400) {
//       setState(() {
//         _isLoading = true;
//       });
//       jsonResponse = json.decode(res.body);
//       print(jsonResponse);
//       if (jsonResponse != null) {
//         setState(() {
//           _isLoading = false;
//           showDialog(
//               context: context,
//               builder: (BuildContext context) {
//                 return AlertDialog(
//                   title: Center(
//                       child: Column(
//                     children: [
//                       Text(
//                         AppLocalizations.of(context)!
//                             .localizedString("error_alert"),
//                         style: TextStyle(fontSize: 16),
//                       ),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Icon(
//                         Icons.add_alert,
//                         color: Colors.red,
//                         size: 50.0,
//                       )
//                     ],
//                   )),
//                   content: Text(
//                     "${jsonResponse}",
//                     style: TextStyle(fontSize: 14),
//                   ),
//                   actions: [
//                     TextButton(
//                         onPressed: () {
//                           Navigator.of(context).pop();
//                         },
//                         child: Text(
//                           AppLocalizations.of(context)!.localizedString("okay"),
//                         ))
//                   ],
//                 );
//               });
//         });
//       }
//     } else if (res.statusCode == 401) {
//       setState(() {
//         _isLoading = false;
//       });
//       jsonResponse = json.decode(res.body);
//       print(jsonResponse);
//       return showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Center(
//                   child: Column(
//                 children: [
//                   Text(
//                     AppLocalizations.of(context)!
//                         .localizedString("error_alert"),
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Icon(
//                     Icons.add_alert,
//                     color: Colors.red,
//                     size: 50.0,
//                   )
//                 ],
//               )),
//               content: Text(
//                 "${jsonResponse["Message"]}",
//                 style: TextStyle(fontSize: 14),
//               ),
//               actions: [
//                 TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Text(
//                       AppLocalizations.of(context)!.localizedString("okay"),
//                     ))
//               ],
//             );
//           });
//     } else {
//       setState(() {
//         _isLoading = false;
//       });
//       jsonResponse = json.decode(res.body);
//       print("this is the response from the api: ${jsonResponse}");
//       return showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Column(
//                 children: [
//                   Text(
//                     AppLocalizations.of(context)!
//                         .localizedString("error_alert"),
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Icon(
//                     Icons.add_alert,
//                     color: Colors.red,
//                     size: 50.0,
//                   )
//                 ],
//               ),
//               content: Text(
//                 AppLocalizations.of(context)!
//                     .localizedString("something_went_wrong"),
//                 style: TextStyle(fontSize: 14),
//               ),
//               actions: [
//                 TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Text(
//                       AppLocalizations.of(context)!.localizedString("okay"),
//                     ))
//               ],
//             );
//           });
//     }
//   }
//
//   movetoHomePage() {
//     Navigator.of(context).pushAndRemoveUntil(
//         MaterialPageRoute(
//             builder: (context) => HomePage(
//                   coopList: coopList,
//                   userId: userId,
//                   accesstoken: accesstoken,
//                   baseUrl: baseUrl,
//                   accountno: accountno,
//                   primaryColor: primaryColor,
//                   loginButtonTitleColor: loginButtonTitleColor,
//                   loginbuttonColor: loginbuttonColor,
//                   loginTextFieldColor: loginTextFieldColor,
//                   dasboardIconColor: dasboardIconColor,
//                   dashboardTopTitleColor: dashboardTopTitleColor,
//                   SecondaryColor: SecondaryColor,
//                 )),
//         (Route<dynamic> route) => false);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () {
//         return movetoHomePage();
//       },
//       child: Scaffold(
//         appBar: AppBar(
//           backgroundColor: Color(int.parse(primaryColor.toString())),
//           leading: IconButton(
//             onPressed: () {
//               movetoHomePage();
//             },
//             icon: Icon(
//               Icons.arrow_back,
//               size: 30,
//               color: Colors.white,
//             ),
//           ),
//           title: Text(
//             "SahakaariPay",
//             // style: TextStyle(
//             //     color: Color(int.parse(dashboardTopTitleColor.toString()))
//             // ),
//           ),
//           centerTitle: true,
//           toolbarHeight: 100,
//         ),
//         body: _isLoading
//             ? Loading()
//             : responseMessage == "Invalid username or password" ||
//                     responseMessage == null
//                 ? Center(
//                     child: Padding(
//                     padding:
//                         const EdgeInsets.only(top: 220, left: 20.0, right: 20),
//                     child: Column(
//                       children: [
//                         Text(
//                           AppLocalizations.of(context)!
//                               .localizedString("service_not_available"),
//                           style: TextStyle(
//                             color: Colors.black87,
//                             fontSize: 24,
//                           ),
//                           textAlign: TextAlign.center,
//                         ),
//                       ],
//                     ),
//                   ))
//                 : merchantName == null || merchantCode == null
//                     ? Center(
//                         child: Padding(
//                         padding: const EdgeInsets.only(
//                             top: 200, left: 20.0, right: 20),
//                         child: Column(
//                           children: [
//                             Text(
//                               "Invalid QR Code, Please scan fonepay QR Code.",
//                               style: TextStyle(
//                                 color: Colors.black87,
//                                 fontSize: 24,
//                               ),
//                               textAlign: TextAlign.center,
//                             ),
//                             Padding(
//                               padding: const EdgeInsets.only(top: 30.0),
//                               child: Container(
//                                 width: 90,
//                                 height: 90,
//                                 decoration: BoxDecoration(
//                                     color: Colors.green[900],
//                                     borderRadius: BorderRadius.circular(10)),
//                                 child: Column(
//                                   children: [
//                                     Padding(
//                                       padding: const EdgeInsets.only(top: 4.0),
//                                       child: Icon(
//                                         Icons.qr_code_scanner,
//                                         color: Colors.white,
//                                         size: 35,
//                                       ),
//                                     ),
//                                     TextButton(
//                                       onPressed: () {
//                                         scanBarcode();
//                                       },
//                                       child: Text(
//                                         "QR Scan",
//                                         style: TextStyle(
//                                             color: Colors.white, fontSize: 14),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                       ))
//                     : SingleChildScrollView(
//                         child: Padding(
//                         padding: const EdgeInsets.only(top: 30.0, left: 30.0),
//                         child: Center(
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Row(
//                                 children: [
//                                   Padding(
//                                     padding: const EdgeInsets.only(left: 10.0),
//                                     child: Icon(
//                                       Icons.account_balance_wallet_outlined,
//                                       size: 25.0,
//                                       color: Color(int.parse(
//                                           dasboardIconColor.toString())),
//                                     ),
//                                   ),
//                                   const SizedBox(
//                                     width: 5.0,
//                                   ),
//                                   Text(
//                                     AppLocalizations.of(context)!
//                                         .localizedString("npr_balance"),
//                                     style: TextStyle(
//                                       color: Color(int.parse(
//                                           dashboardTopTitleColor.toString())),
//                                       fontSize: 14.0,
//                                       fontWeight: FontWeight.bold,
//                                     ),
//                                   ),
//                                   const SizedBox(
//                                     width: 10.0,
//                                   ),
//                                   Text(
//                                       AppLocalizations.of(context)!
//                                               .localizedString("rs") +
//                                           "${balance.toString()}",
//                                       style: TextStyle(
//                                         color: Color(int.parse(
//                                             dashboardTopTitleColor.toString())),
//                                         fontSize: 14.0,
//                                         fontWeight: FontWeight.bold,
//                                       )),
//                                 ],
//                               ),
//                               Padding(
//                                 padding: const EdgeInsets.only(top: 30.0),
//                                 child: Text(AppLocalizations.of(context)!
//                                     .localizedString("merchant_name")),
//                               ),
//                               Padding(
//                                 padding: const EdgeInsets.only(top: 30.0),
//                                 child: Text(merchantName.toString()),
//                               ),
//                               Padding(
//                                 padding: const EdgeInsets.only(top: 30.0),
//                                 child: Text(AppLocalizations.of(context)!
//                                     .localizedString("merchant_code")),
//                               ),
//                               Padding(
//                                 padding: const EdgeInsets.only(top: 30.0),
//                                 child: Text(merchantCode.toString()),
//                               ),
//                               Form(
//                                   key: globalFormKey,
//                                   child: Column(
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.start,
//                                     children: [
//                                       Padding(
//                                         padding: const EdgeInsets.only(
//                                             top: 30.0, right: 30),
//                                         child: Container(
//                                           width: 380,
//                                           decoration: BoxDecoration(
//                                             color: Colors.white,
//                                             borderRadius:
//                                                 BorderRadius.circular(5.0),
//                                           ),
//                                           child: TextFormField(
//                                             controller: amountController,
//                                             maxLength: 30,
//                                             keyboardType: TextInputType.number,
//                                             validator: (value) => value!
//                                                         .isEmpty ||
//                                                     validateAmount(double.parse(
//                                                             amountController
//                                                                 .text)) ==
//                                                         false
//                                                 ? AppLocalizations.of(context)!
//                                                     .localizedString(
//                                                         "minimum_amount1")
//                                                 : null,
//                                             onSaved: (value) =>
//                                                 amountController,
//                                             decoration: InputDecoration(
//                                               border: OutlineInputBorder(
//                                                 borderRadius:
//                                                     BorderRadius.circular(20.0),
//                                               ),
//                                               labelText: AppLocalizations.of(
//                                                       context)!
//                                                   .localizedString("amount"),
//                                               labelStyle: const TextStyle(
//                                                   color: Colors.green),
//                                               counterText: "",
//                                               // icon: const Icon(
//                                               //   Icons.money,
//                                               //   color: Colors.green,
//                                               //   size: 20.0,
//                                               // ),
//                                             ),
//                                           ),
//                                         ),
//                                       ),
//                                       Padding(
//                                         padding: const EdgeInsets.only(
//                                             top: 30.0, right: 30),
//                                         child: Container(
//                                           height: 65,
//                                           width: 440,
//                                           padding: const EdgeInsets.symmetric(
//                                               horizontal: 2, vertical: 2),
//                                           child: DropdownSearch<String>(
//                                             mode: Mode.DIALOG,
//                                             maxHeight: MediaQuery.of(context)
//                                                     .size
//                                                     .height -
//                                                 100,
//                                             dropdownSearchDecoration:
//                                                 InputDecoration(
//                                                     border: InputBorder.none,
//                                                     hintText:
//                                                         AppLocalizations.of(
//                                                                 context)!
//                                                             .localizedString(
//                                                                 "purpose"),
//                                                     hintStyle: TextStyle(
//                                                         fontSize: 16,
//                                                         color: Colors.black87)),
//                                             selectionListViewProps:
//                                                 SelectionListViewProps(
//                                               padding:
//                                                   EdgeInsets.only(left: 15),
//                                             ),
//                                             searchFieldProps: TextFieldProps(
//                                               // autofocus: true,
//                                               padding:
//                                                   const EdgeInsets.symmetric(
//                                                       horizontal: 24,
//                                                       vertical: 10),
//                                               decoration: InputDecoration(
//                                                   border:
//                                                       UnderlineInputBorder(),
//                                                   hintText: AppLocalizations.of(
//                                                           context)!
//                                                       .localizedString(
//                                                           "search"),
//                                                   hintStyle: TextStyle(
//                                                       fontWeight:
//                                                           FontWeight.bold,
//                                                       fontSize: 18,
//                                                       color: Colors.grey[400]),
//                                                   suffixIcon: Icon(
//                                                     Icons.search,
//                                                     size: 30,
//                                                     color: Colors.grey[500],
//                                                   )),
//                                             ),
//                                             showSearchBox: true,
//                                             items: [
//                                               "Pursonal Use",
//                                               "Borrow/Lend",
//                                               "Family Expenses",
//                                               "Bill Sharing",
//                                               "Salary",
//                                               "Pathao Ride Payment",
//                                               "Sahara Ride Payment",
//                                               "Lozoom Ride Payment",
//                                               "Others"
//                                             ],
//                                             onChanged: (String? newValue) {
//                                               setState(() {
//                                                 dropdownvalue = newValue!;
//                                               });
//                                             },
//                                           ),
//                                         ),
//                                       ),
//                                       Padding(
//                                         padding: const EdgeInsets.only(
//                                             top: 30.0, right: 30),
//                                         child: Container(
//                                           width: 380,
//                                           decoration: BoxDecoration(
//                                             borderRadius:
//                                                 BorderRadius.circular(5.0),
//                                           ),
//                                           child: TextFormField(
//                                             controller: statementController,
//                                             maxLength: 200,
//                                             keyboardType: TextInputType.text,
//                                             validator: (value) => value!.isEmpty
//                                                 ? "Please Enter Remarks."
//                                                 : null,
//                                             onSaved: (value) =>
//                                                 statementController,
//                                             decoration: InputDecoration(
//                                               border: OutlineInputBorder(
//                                                 borderRadius:
//                                                     BorderRadius.circular(20.0),
//                                               ),
//                                               labelText: AppLocalizations.of(
//                                                       context)!
//                                                   .localizedString("remarks"),
//                                               labelStyle: const TextStyle(
//                                                   color: Colors.green),
//                                               counterText: "",
//                                               // icon: const Icon(
//                                               //   Icons.message,
//                                               //   color: Colors.green,
//                                               //   size: 20.0,
//                                               // ),
//                                             ),
//                                           ),
//                                         ),
//                                       ),
//                                     ],
//                                   )),
//                               Padding(
//                                 padding: const EdgeInsets.only(top: 30.0),
//                                 child: InkWell(
//                                   onTap: () {
//                                     if (validateAndSave()) {
//                                       showMessage();
//                                     }
//                                     // transactionPinMessage();
//                                   },
//                                   child: Center(
//                                     child: Container(
//                                       height: 60,
//                                       width:
//                                           MediaQuery.of(context).size.width / 2,
//                                       decoration: BoxDecoration(
//                                           color: Color(int.parse(
//                                               loginbuttonColor.toString())),
//                                           borderRadius:
//                                               BorderRadius.circular(10)),
//                                       child: Center(
//                                         child: Text(
//                                           AppLocalizations.of(context)!
//                                               .localizedString("proceed_name"),
//                                           style: TextStyle(
//                                               fontSize: 16,
//                                               color: Color(int.parse(
//                                                   loginButtonTitleColor
//                                                       .toString()))),
//                                           textAlign: TextAlign.center,
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               )
//                             ],
//                           ),
//                         ),
//                       )),
//       ),
//     );
//   }
//
//   bool validateAndSave() {
//     final form = globalFormKey.currentState;
//     if (form!.validate()) {
//       form.save();
//       return true;
//     } else {
//       return false;
//     }
//   }
//
//   bool validateAmount(double value) {
//     if (value < 100) {
//       print("Does not match: ${value}");
//       return false;
//     }
//     print("the value match: ${value}");
//     return true;
//   }
// }
