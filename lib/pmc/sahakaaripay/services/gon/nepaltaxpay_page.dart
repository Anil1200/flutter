import 'package:flutter/material.dart';

import '../../../../themes.dart';
import '../../../utils/localizations.dart';
import '../../pmc_homepage.dart';

class NepalTaxPayPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;

  NepalTaxPayPage(
      {Key? key,
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno})
      : super(key: key);

  @override
  State<NepalTaxPayPage> createState() =>
      _NepalTaxPayPageState(coopList, accesstoken, balance, baseUrl, accountno);
}

class _NepalTaxPayPageState extends State<NepalTaxPayPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  _NepalTaxPayPageState(this.coopList, this.accesstoken, this.balance,
      this.baseUrl, this.accountno);

  movetohomepage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                )));
  }

  String dropdownValue = 'Select Amount';
  final TextEditingController paymentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(
                  color: selectedColor,
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: 10.0,
                      top: 40,
                      child: IconButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomePage(
                                          coopList: coopList,
                                          accesstoken: accesstoken,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                        )));
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            size: 30.0,
                            color: Colors.white,
                          )),
                    ),
                    const Positioned(
                      left: 100,
                      top: 50,
                      child: Text(
                        "SahakaariPay",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20,
                      top: 95,
                      child: Row(
                        children: [
                          const SizedBox(
                            height: 30,
                            width: 30,
                            child: Image(
                                image: AssetImage(
                                    "assets/images/wallet_icon.png")),
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          const Text(
                            "Rs.4000",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 220.0),
                            child: IconButton(
                              icon: const Icon(
                                Icons.update,
                                color: Colors.white,
                                size: 30.0,
                              ),
                              onPressed: () {},
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: Container(
                  height: 400,
                  width: 400,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(top: 30.0),
                        child: Center(
                          child: Text(
                            "GoN Revenue Payment",
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 50.0,
                        ),
                        child: Container(
                          width: 260,
                          child: TextFormField(
                            controller: paymentController,
                            maxLength: 30,
                            keyboardType: TextInputType.number,
                            validator: (value) =>
                                value!.isEmpty ? AppLocalizations.of(context)!.localizedString("enter_amount") : null,
                            onSaved: (value) => paymentController,
                            decoration: InputDecoration(
                              border: UnderlineInputBorder(),
                              labelText: AppLocalizations.of(context)!.localizedString("amount"),
                              labelStyle: TextStyle(color: Colors.green),
                              counterText: "",
                              icon: Icon(
                                Icons.confirmation_number,
                                color: Colors.green,
                                size: 20.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 40.0, top: 30),
                        child: Text("Voucher Amount",
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 14,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 30.0, top: 10.0),
                        child: Container(
                          width: 300,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 12, vertical: 4),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                              border:
                                  Border.all(color: Colors.black87, width: 2),
                              borderRadius: BorderRadius.circular(20.0)),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              isExpanded: true,
                              value: dropdownValue,
                              alignment: AlignmentDirectional.center,
                              // itemHeight: 90,
                              borderRadius: BorderRadius.circular(20.0),
                              icon: const Icon(
                                Icons.arrow_drop_down,
                                color: Colors.black87,
                              ),
                              iconSize: 34,
                              elevation: 16,
                              style: const TextStyle(
                                  color: Colors.black87,
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownValue = newValue!;
                                });
                              },
                              items: <String>[
                                'Select Amount',
                                '10',
                                '20',
                                '50',
                                '100',
                                '250',
                                '500',
                                '1000'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    style:
                                        const TextStyle(color: Colors.black87),
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 220.0, top: 20),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.green,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          height: 50,
                          width: 100,
                          child: TextButton(
                            onPressed: () {},
                            child: const Text(
                              "Submit",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
