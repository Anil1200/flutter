import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import '../../../pmc_homepage.dart';
import '../../water/ubs_water/mobile.dart'
    if (dart.library.html) '../water/ubs_water/web.dart';

class TrafficFinePDFPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? TransactionId;
  String? paymentmessage;
  String? CustomerName;
  String? billercode;
  String? chitnumber;
  String? amount;
  String? description;
  String? servicecharge;
  String? totalamount;
  String? dropdownValue;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TrafficFinePDFPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TransactionId,
    this.paymentmessage,
    this.CustomerName,
    this.billercode,
    this.chitnumber,
    this.amount,
    this.description,
    this.servicecharge,
    this.totalamount,
    this.dropdownValue,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _TrafficFinePDFPageState createState() => _TrafficFinePDFPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        TransactionId,
        paymentmessage,
        CustomerName,
        billercode,
        chitnumber,
        amount,
        description,
        servicecharge,
        totalamount,
        dropdownValue,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TrafficFinePDFPageState extends State<TrafficFinePDFPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? TransactionId;
  String? paymentmessage;
  String? CustomerName;
  String? billercode;
  String? chitnumber;
  String? amount;
  String? description;
  String? servicecharge;
  String? totalamount;
  String? dropdownValue;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _TrafficFinePDFPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TransactionId,
    this.paymentmessage,
    this.CustomerName,
    this.billercode,
    this.chitnumber,
    this.amount,
    this.description,
    this.servicecharge,
    this.totalamount,
    this.dropdownValue,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    this.nullcheck();
  }

  String? Tid;

  nullcheck() {
    if (TransactionId == null) {
      Tid = "TransactionId not available.";
    } else {
      Tid = TransactionId;
    }
  }

  String? timeforBill = DateTime.now().year.toString() +
      "-" +
      DateTime.now().month.toString() +
      "-" +
      DateTime.now().day.toString() +
      " " +
      DateTime.now().hour.toString() +
      ":" +
      DateTime.now().minute.toString();

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'Traffic Fine Bill Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );

    final grid = PdfGrid();
    grid.columns.add(count: 2);
    var row = grid.rows.add();
    row = grid.rows.add();
    row.cells[0].value = "TransactionId:";
    row.cells[1].value = Tid.toString();

    row = grid.rows.add();
    row.cells[0].value = "Message: ";
    row.cells[1].value = "${paymentmessage.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "CustomerName: ";
    row.cells[1].value = "${description.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Bill Code: ";
    row.cells[1].value = "${billercode.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Chit No.: ";
    row.cells[1].value = "${chitnumber.toString()}";

    /*row = grid.rows.add();
    row.cells[0].value = "Amount: ";
    row.cells[1].value = "${amount.toString()}";*/

    /*row = grid.rows.add();
    row.cells[0].value = "Description: ";
    row.cells[1].value = "${description.toString()}";*/

    row = grid.rows.add();
    row.cells[0].value = "ServiceCharge: ";
    row.cells[1].value = "${servicecharge.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "TotalAmount: ";
    row.cells[1].value = "${totalamount.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Fiscal Year: ";
    row.cells[1].value = "${dropdownValue.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Paid Date: ";
    row.cells[1].value = "${timeforBill.toString()}";

    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));

    List<int> bytes = await document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoHomePage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "Traffic Fine Payment Bill",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "TransactionId:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Tid}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Message:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 80,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${paymentmessage.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Customer Name:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 38,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${description}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Bill Code:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 87,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${billercode}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Chit No.:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 93,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${chitnumber}")),
                    ],
                  ),
                ),
                /*Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Amount:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 95,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${amount.toString()}")),
                    ],
                  ),
                ),*/
                /*Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Description:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 75,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${description.toString()}")),
                    ],
                  ),
                ),*/
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "ServiceCharge:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 55,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${servicecharge.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "TotalAmount:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 65,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${totalamount.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Fiscal Year:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 80,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${dropdownValue.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Paid Date:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 88,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${timeforBill.toString()}")),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Divider(
                  thickness: 1,
                  color: Colors.green[700],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 100,
                    ),
                    TextButton(
                        onPressed: () {
                          movetoHomePage();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("home"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                    const SizedBox(
                      width: 60,
                    ),
                    TextButton(
                        onPressed: createPDF,
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("pdf"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
