import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/gon/governmentservices.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/gon/trafficfine/trafficfine_billdetailspage.dart';

import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';

class TrafficFineGetDetailPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TrafficFineGetDetailPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _TrafficFineGetDetailPageState createState() =>
      _TrafficFineGetDetailPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TrafficFineGetDetailPageState extends State<TrafficFineGetDetailPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _TrafficFineGetDetailPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    _isLoading = true;
    getTrafficFiscalyear();
    getTrafficProvince();
  }

  String? billercode;
  // String? genericMessage;
  String? CustomerName;
  String? amount;
  String? description;
  String? chitnumber;
  String? servicecharge;
  String? totalamount;
  String? Message;
  List<dynamic> fiscalyear = [];
  List<dynamic> province = [];
  List<dynamic> district = [];
  //List<String?> smojsf = ["2076/77", "2077/78", "2078/79", "2079/80"];
  String? dropdownValue;
  String? dropdownValueProvince;
  String? dropdownValueDistrict;

  Future<String> getTrafficFiscalyear() async {
    String url = "${baseUrl}api/v1/governmentservice/fiscalyears";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        jsonResponse = json.decode(response.body);
        print("this is list:${jsonResponse}");
        if (jsonResponse != null) {
          setState(() {
            if (jsonResponse["Message"].toString().toLowerCase() ==
                "Success".toString().toLowerCase()) {
              fiscalyear = jsonResponse["Data"];
              print("this is the fiscal year${fiscalyear}");
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Center(
                          child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                      content: Text(
                        AppLocalizations.of(context)!
                            .localizedString("something_went_wrong"),
                        style: const TextStyle(fontSize: 14),
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("okay"),
                            ))
                      ],
                    );
                  });
            }
          });
        } else {
          setState(() {
            _isLoading = false;
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  Future<String> getTrafficProvince() async {
    String url = "${baseUrl}api/v1/governmentservice/traffic/provinces";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        jsonResponse = json.decode(response.body);
        print("this is list:${jsonResponse}");
        if (jsonResponse != null) {
          setState(() {
            if (jsonResponse["Message"].toString().toLowerCase() ==
                "Success".toString().toLowerCase()) {
              province = jsonResponse["Provinces"];
              print("this is the province ${province}");
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Center(
                          child: Column(
                            children: [
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("error_alert"),
                                style: const TextStyle(fontSize: 16),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Icon(
                                Icons.add_alert,
                                color: Colors.red,
                                size: 50.0,
                              )
                            ],
                          )),
                      content: Text(
                        AppLocalizations.of(context)!
                            .localizedString("something_went_wrong"),
                        style: const TextStyle(fontSize: 14),
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("okay"),
                            ))
                      ],
                    );
                  });
            }
          });
        } else {
          setState(() {
            _isLoading = false;
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  Future<String> getTrafficDistrict() async {
    String url = "${baseUrl}api/v1/governmentservice/traffic/districts?provinceCode=$dropdownValueProvince";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        jsonResponse = json.decode(response.body);
        print("this is list:${jsonResponse}");
        if (jsonResponse != null) {
          setState(() {
            if (jsonResponse["Message"].toString().toLowerCase() ==
                "Success".toString().toLowerCase()) {
              district = jsonResponse["Districts"];
              print("this is the district ${district}");
            } else {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Center(
                          child: Column(
                            children: [
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("error_alert"),
                                style: const TextStyle(fontSize: 16),
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const Icon(
                                Icons.add_alert,
                                color: Colors.red,
                                size: 50.0,
                              )
                            ],
                          )),
                      content: Text(
                        AppLocalizations.of(context)!
                            .localizedString("something_went_wrong"),
                        style: const TextStyle(fontSize: 14),
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("okay"),
                            ))
                      ],
                    );
                  });
            }
          });
        } else {
          setState(() {
            _isLoading = false;
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  getTrafficDetails() async {
    String url =
        "${baseUrl}api/v1/governmentservice/traffic/traffic-fine-detail";
    Map bodyData = {
      "ChitNumber": dropdownValueProvince! + dropdownValueDistrict! + chitnoIdController.text.toString(),
      "FiscalYear": dropdownValue.toString(),
    };
    var body = json.encode(bodyData);
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      'Content-Type': "application/json",
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      if (jsonResponse != null) {
        Message = jsonResponse["Message"];
        billercode = jsonResponse["BillerCode"];
        CustomerName = jsonResponse["CustomerName"];
        amount = jsonResponse["Amount"].toString();
        description = jsonResponse["Description"];
        chitnumber = jsonResponse["ChitNumber"];
        servicecharge = jsonResponse["ServiceCharge"].toString();
        totalamount = jsonResponse["TotalAmount"].toString();
        if (Message.toString().toLowerCase() ==
            "Success".toString().toLowerCase()) {
          setState(() {
            _isLoading = false;
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => TrafficFineBillDetailsPage(
                          coopList: coopList,
                          userId: userId,
                          accesstoken: accesstoken,
                          baseUrl: baseUrl,
                          accountno: accountno,
                          CustomerName: CustomerName,
                          billercode: billercode,
                          chitnumber: chitnumber,
                          amount: amount,
                          description: description,
                          servicecharge: servicecharge,
                          totalamount: totalamount,
                          dropdownValue: dropdownValue,
                          primaryColor: primaryColor,
                          loginButtonTitleColor: loginButtonTitleColor,
                          loginbuttonColor: loginbuttonColor,
                          loginTextFieldColor: loginTextFieldColor,
                          dasboardIconColor: dasboardIconColor,
                          dashboardTopTitleColor: dashboardTopTitleColor,
                          SecondaryColor: SecondaryColor,
                        )));
          });
        } else {
          setState(() {
            _isLoading = false;
          });
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    AppLocalizations.of(context)!
                        .localizedString("process_error"),
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        }
      }
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the response from the api: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => GovernmentServicesPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;
  bool _obscureText = true;
  bool showBalance = false;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  final TextEditingController chitnoIdController = TextEditingController();
  final TextEditingController amountController = TextEditingController();
  final TextEditingController statementController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            top: 40,
                            child: IconButton(
                                onPressed: () {
                                  movetohomepage();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                          Positioned(
                            left: 100,
                            top: 50,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sahakaari_pay"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 95,
                            child: Row(
                              children: [
                                const SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: Image(
                                      image: AssetImage(
                                          "assets/images/wallet_icon.png")),
                                ),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  showBalance
                                      ? AppLocalizations.of(context)!
                                              .localizedString("rs") +
                                          "${balance.toString()}"
                                      : "xxx.xx".toUpperCase(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 180.0),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          if (showBalance == false) {
                                            showBalance = true;
                                          } else {
                                            showBalance = false;
                                          }
                                        });
                                      },
                                      icon: Icon(
                                        showBalance
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        size: 25.0,
                                        color: Colors.white,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(top: 10.0),
                            child: Center(
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("traffic_fine"),
                                style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 24.0,
                                  // fontStyle: FontStyle.italic,
                                ),
                              ),
                            ),
                          ),
                          Center(
                            child: Container(
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  image: const DecorationImage(
                                      image: AssetImage(
                                          "assets/images/gov/traffic.jpg"),
                                      fit: BoxFit.contain)),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Form(
                            key: globalFormKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 30.0, right: 30, top: 20),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      isExpanded: true,
                                      value: dropdownValueProvince,
                                      alignment: AlignmentDirectional.centerStart,
                                      hint: Text(
                                        "Choose Province",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14),
                                      ),
                                      // itemHeight: 90,
                                      borderRadius: BorderRadius.circular(20.0),
                                      icon: const Icon(
                                        Icons.arrow_drop_down,
                                        color: Colors.black87,
                                      ),
                                      iconSize: 34,
                                      elevation: 16,
                                      style: const TextStyle(
                                          color: Colors.black87,
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          dropdownValueProvince = newValue!;
                                          print(
                                              "this is province: ${dropdownValueProvince}");

                                          dropdownValueDistrict = null;

                                          getTrafficDistrict();
                                        });
                                      },
                                      items: province?.map((item) {
                                        return DropdownMenuItem<String>(
                                            value: item["Code"],
                                            child: Text(item["Name"]));
                                      }).toList(),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 30.0, right: 30, top: 20),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      isExpanded: true,
                                      value: dropdownValueDistrict,
                                      alignment: AlignmentDirectional.centerStart,
                                      hint: Text(
                                        "Choose District",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14),
                                      ),
                                      // itemHeight: 90,
                                      borderRadius: BorderRadius.circular(20.0),
                                      icon: const Icon(
                                        Icons.arrow_drop_down,
                                        color: Colors.black87,
                                      ),
                                      iconSize: 34,
                                      elevation: 16,
                                      style: const TextStyle(
                                          color: Colors.black87,
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          dropdownValueDistrict = newValue!;
                                          print(
                                              "this is district: ${dropdownValueDistrict}");
                                        });
                                      },
                                      items: district?.map((item) {
                                        return DropdownMenuItem<String>(
                                            value: item["Code"],
                                            child: Text(item["Name"]));
                                      }).toList(),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 25.0, right: 25),
                                  child: TextFormField(
                                    controller: chitnoIdController,
                                    //maxLength: 10,
                                    keyboardType: TextInputType.number,
                                    validator: (value) => value!.isEmpty
                                        ? AppLocalizations.of(context)!
                                            .localizedString("enter_chit")
                                        : null,
                                    onSaved: (value) => chitnoIdController,
                                    decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText: AppLocalizations.of(context)!
                                          .localizedString("chit_no"),
                                      labelStyle:
                                          TextStyle(color: Colors.green),
                                      counterText: "",
                                      icon: Icon(
                                        Icons.confirmation_number,
                                        color: Colors.green,
                                        size: 20.0,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 30.0, right: 30, top: 20),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton<String>(
                                      isExpanded: true,
                                      value: dropdownValue,
                                      alignment: AlignmentDirectional.centerStart,
                                      hint: Text(
                                        "Choose Fiscal Year",
                                        style: TextStyle(
                                            color: Colors.black87,
                                            fontSize: 14),
                                      ),
                                      // itemHeight: 90,
                                      borderRadius: BorderRadius.circular(20.0),
                                      icon: const Icon(
                                        Icons.arrow_drop_down,
                                        color: Colors.black87,
                                      ),
                                      iconSize: 34,
                                      elevation: 16,
                                      style: const TextStyle(
                                          color: Colors.black87,
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          dropdownValue = newValue!;
                                          print(
                                              "this is fiscal year: ${dropdownValue}");
                                        });
                                      },
                                      items: fiscalyear.map((item) {
                                        return DropdownMenuItem<String>(
                                            value: item, child: Text(item));
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      color: Color(int.parse(
                                          loginbuttonColor.toString())),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: const Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  height: 50,
                                  width: MediaQuery.of(context).size.width - 40,
                                  child: TextButton(
                                    onPressed: () async {
                                      if (validateAndSave()) {
                                        if (dropdownValue != null && dropdownValueProvince != null && dropdownValueDistrict != null) {
                                          setState(() {
                                            _isLoading = true;
                                          });
                                          getTrafficDetails();
                                        } else {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: Center(
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            AppLocalizations.of(context)!
                                                                .localizedString("error_alert"),
                                                            style: const TextStyle(fontSize: 16),
                                                          ),
                                                          const SizedBox(
                                                            height: 10,
                                                          ),
                                                          const Icon(
                                                            Icons.add_alert,
                                                            color: Colors.red,
                                                            size: 50.0,
                                                          )
                                                        ],
                                                      )),
                                                  content: Text(
                                                    AppLocalizations.of(context)!
                                                        .localizedString("enter_all_data"),
                                                    style: const TextStyle(fontSize: 14),
                                                  ),
                                                  actions: [
                                                    TextButton(
                                                        onPressed: () {
                                                          Navigator.of(context).pop();
                                                        },
                                                        child: Text(
                                                          AppLocalizations.of(context)!
                                                              .localizedString("okay"),
                                                        ))
                                                  ],
                                                );
                                              });
                                        }
                                      } else {
                                        setState(() {
                                          _isLoading = false;
                                        });
                                      }
                                    },
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("get_details"),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
