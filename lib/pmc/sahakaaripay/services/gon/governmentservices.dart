import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/gon/trafficfine/trafficfine_getdetails.dart';

import '../../../utils/localizations.dart';
import '../../pmc_homepage.dart';

class GovernmentServicesPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  GovernmentServicesPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<GovernmentServicesPage> createState() => _GovernmentServicesPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _GovernmentServicesPageState extends State<GovernmentServicesPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _GovernmentServicesPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );
  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 100,
                decoration: BoxDecoration(
                  color: Color(int.parse(primaryColor.toString())),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: 10.0,
                      top: 40,
                      child: IconButton(
                          onPressed: () {
                            movetohomepage();
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            size: 30.0,
                            color: Colors.white,
                          )),
                    ),
                    Positioned(
                      left: 100,
                      top: 50,
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("gov"),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 10.0, right: 10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: GridView.count(
                      physics: NeverScrollableScrollPhysics(),
                      //childAspectRatio: 0.2 / 2,
                      crossAxisCount: 4,
                      shrinkWrap: true,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        TrafficFineGetDetailPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor:
                                              dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          child: Column(
                            children: [
                              SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/gov/traffic.jpg")),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("traffic_fine"),
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
