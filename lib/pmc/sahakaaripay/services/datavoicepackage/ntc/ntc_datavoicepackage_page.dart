import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart';
import 'package:flutter_native_contact_picker/model/contact.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/datavoicepackage/datavoicepackage_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/topup/ntc/pre_paid/prepaidpdf_billpage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/topup/topup_page.dart';

import '../../../../utils/localizations.dart';
import '../../../../utils/utils.dart';
import '../../../models/loading.dart';

class NTCDataVoicePackagePage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? packageCode;
  NTCDataVoicePackagePage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.packageCode,
  }) : super(key: key);

  @override
  _NTCDataVoicePackagePageState createState() => _NTCDataVoicePackagePageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
        packageCode,
      );
}

class _NTCDataVoicePackagePageState extends State<NTCDataVoicePackagePage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? packageCode;
  _NTCDataVoicePackagePageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.packageCode,
  );

  bool _isLoading = false;
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  void initState() {
    super.initState();
  }

  Future<PermissionStatus> getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    print("this is contacts permission: $permission");
    if (permission != PermissionStatus.granted) {
      final Map<Permission, PermissionStatus> permissionStatus = await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ?? PermissionStatus.denied;
    } else {
      return permission;
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  showMessage() {
    //if (dropdownValue == 'Select Amount') {
    if (amountController.value.text == "") {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("select_top_up_amount"),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 20.0, bottom: 20),
                  child: Container(
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(30)),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                )
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      transactionPinMessage();
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${amountController.text} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${phoneNoController.text.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postDataVoicePackageData();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 20.0, right: 20),
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("okay"),
                                            style: TextStyle(
                                                color: Colors.green[900]),
                                          ),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? paymentmessage;
  List<dynamic> data = [];
  bool showBalance = false;

  postDataVoicePackageData() async {
    String url = "${baseUrl}api/v1/topup";
    Map body = {
      "OperatorCode": "124",
      "Amount": amountController.text.toString().trim(),
      "MPIN": mpincontroller.text.toString().trim(),
      "MobileNo": phoneNoController.text.toString().trim(),
      "ExtraField1": packageCode
    };
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          paymentmessage = jsonResponse["Message"];
          ReferenceId = jsonResponse["Reference Id"];
          TransactionId = jsonResponse["TransactionId"];
          // data = jsonResponse["Data"];
          // print("this is the required response data for top up: ${data}");
          // ReceiptNo = jsonResponse["Data"]["BillDetail"]["ReceiptNo"];
          // PaymentBillDate = jsonResponse["Data"]["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  const Icon(
                    Icons.verified,
                    color: Colors.green,
                    size: 40,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.5,
                    child: Text(
                      jsonResponse["Message"],
                      style: const TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ],
              )));
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PrePaidPdfBillPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            balance: balance,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            phoneNo: phoneNoController.text.toString(),
                            Amount: amountController.text.toString(),
                            paymentmessage: paymentmessage,
                            // renewalplans: renewalplans,
                            // planName: planName,
                            ReferenceId: ReferenceId,
                            TransactionId: TransactionId,
                            ReceiptNo: ReceiptNo,
                            PaymentBillDate: PaymentBillDate,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor: dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0, bottom: 20),
                      child: Container(
                        height: 60,
                        width: 100,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("okay"),
                            )),
                      ),
                    )
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, bottom: 20),
                  child: Container(
                    height: 60,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        )),
                  ),
                )
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, bottom: 20),
                  child: Container(
                    height: 60,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        )),
                  ),
                )
              ],
            );
          });
    }
  }

  movetoDataVoicePackagePage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => DataVoicePackagePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  String dropdownValue = 'Select Amount';
  final TextEditingController phoneNoController = TextEditingController();
  final TextEditingController amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoDataVoicePackagePage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Container(
                        height: 150,
                        decoration: BoxDecoration(
                          color: Color(int.parse(primaryColor.toString())),
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              left: 10.0,
                              top: 40,
                              child: IconButton(
                                  onPressed: () {
                                    movetoDataVoicePackagePage();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back,
                                    size: 30.0,
                                    color: Colors.white,
                                  )),
                            ),
                            Positioned(
                              left: 100,
                              top: 50,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("sahakaari_pay"),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 20,
                              top: 95,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: Image(
                                        image: AssetImage(
                                            "assets/images/wallet_icon.png")),
                                  ),
                                  const SizedBox(
                                    width: 10.0,
                                  ),
                                  Text(
                                    showBalance
                                        ? AppLocalizations.of(context)!
                                                .localizedString("rs") +
                                            "${balance.toString()}"
                                        : "xxx.xx".toUpperCase(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 180.0),
                                    child: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            if (showBalance == false) {
                                              showBalance = true;
                                            } else {
                                              showBalance = false;
                                            }
                                          });
                                        },
                                        icon: Icon(
                                          showBalance
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          size: 25.0,
                                          color: Colors.white,
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 30.0),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("nt_top_up"),
                                    style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 20.0,
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width - 120,
                                child: Form(
                                    key: globalFormKey,
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width - 170,
                                              child: TextFormField(
                                                controller: phoneNoController,
                                                maxLength: 10,
                                                keyboardType: TextInputType.number,
                                                inputFormatters: <TextInputFormatter>[
                                                  FilteringTextInputFormatter.allow(RegExp("[0-9]")),
                                                ],
                                                validator: (value) => value!.isEmpty
                                                    ? AppLocalizations.of(context)!
                                                        .localizedString(
                                                            "enter_phone_number")
                                                    : null,
                                                onSaved: (value) => phoneNoController,
                                                decoration: InputDecoration(
                                                  border: UnderlineInputBorder(),
                                                  labelText:
                                                      AppLocalizations.of(context)!
                                                          .localizedString(
                                                              "phone_number"),
                                                  labelStyle:
                                                      TextStyle(color: Colors.green),
                                                  counterText: "",
                                                  icon: Icon(
                                                    Icons.confirmation_number,
                                                    color: Colors.green,
                                                    size: 20.0,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            IconButton(
                                              onPressed: () async {
                                                if (Platform.isAndroid) {
                                                  final PermissionStatus permissionStatus = await getPermission();
                                                  if (permissionStatus == PermissionStatus.granted) {
                                                    Contact? contact = await FlutterNativeContactPicker().selectContact();
                                                    var selectedPhoneNumber = contact!.phoneNumbers!.first.toString();
                                                    selectedPhoneNumber = selectedPhoneNumber.replaceAll(RegExp('[^0-9]'), '');
                                                    phoneNoController.text = selectedPhoneNumber;
                                                  } else {
                                                    ScaffoldMessenger.of(context)
                                                        .showSnackBar(const SnackBar(content: Text("No Permission Granted. Grant Permission in Settings.")));
                                                  }
                                                } else {
                                                  Contact? contact = await FlutterNativeContactPicker().selectContact();
                                                  var selectedPhoneNumber = contact!.phoneNumbers!.first.toString();
                                                  selectedPhoneNumber = selectedPhoneNumber.replaceAll(RegExp('[^0-9]'), '');
                                                  phoneNoController.text = selectedPhoneNumber;
                                                }
                                              },
                                              icon: Image.asset('assets/images/phonebook.png'),
                                              iconSize: 32,
                                            ),
                                          ],
                                        ),
                                        /*Row(
                                          children: [
                                            SizedBox(
                                              width: MediaQuery.of(context).size.width - 170,
                                              child: Autocomplete<ContactModel>(
                                                optionsBuilder: (TextEditingValue textEditingValue) {
                                                  return cModelList!
                                                      .where((ContactModel contact) => contact.displayName!.toLowerCase()
                                                      .contains(textEditingValue.text.toLowerCase())
                                                      || contact.phoneNumber!
                                                      .contains(textEditingValue.text.toLowerCase())
                                                      //.where((ContactModel contact) => contact.displayName!.toLowerCase()
                                                      //.startsWith(textEditingValue.text.toLowerCase())
                                                  ).toList();
                                                },
                                                displayStringForOption: (ContactModel option) => option.phoneNumber!,
                                                fieldViewBuilder: (
                                                    BuildContext context,
                                                    TextEditingController fieldTextEditingController,
                                                    FocusNode fieldFocusNode,
                                                    VoidCallback onFieldSubmitted
                                                ) {
                                                  fieldTextEditingController.text = phoneNoController.text;
                                                  return TextFormField(
                                                    controller: fieldTextEditingController,
                                                    focusNode: fieldFocusNode,
                                                    maxLength: 10,
                                                    keyboardType: TextInputType.number,
                                                    validator: (value) => value!.isEmpty
                                                        ? AppLocalizations.of(context)!
                                                        .localizedString(
                                                        "enter_phone_number")
                                                        : null,
                                                    decoration: InputDecoration(
                                                      border: UnderlineInputBorder(),
                                                      labelText:
                                                      AppLocalizations.of(context)!
                                                          .localizedString(
                                                          "phone_number"),
                                                      labelStyle:
                                                      TextStyle(color: Colors.green),
                                                      counterText: "",
                                                      icon: Icon(
                                                        Icons.confirmation_number,
                                                        color: Colors.green,
                                                        size: 20.0,
                                                      ),
                                                    ),
                                                    //style: const TextStyle(fontWeight: FontWeight.bold),
                                                  );
                                                },
                                                onSelected: (ContactModel selection) {
                                                  phoneNoController.text = selection.phoneNumber!;
                                                },
                                                optionsViewBuilder: (
                                                    BuildContext context,
                                                    AutocompleteOnSelected<ContactModel> onSelected,
                                                    Iterable<ContactModel> options
                                                ) {
                                                  return Align(
                                                    alignment: Alignment.topLeft,
                                                    child: Material(
                                                      child: Container(
                                                        padding: EdgeInsets.all(10),
                                                        //color: Colors.grey,
                                                        width: 330,
                                                        //height: 150,
                                                        decoration: BoxDecoration(
                                                          color: Colors.grey[200],
                                                          borderRadius: BorderRadius.all(Radius.circular(10)),
                                                        ),
                                                        child: ScrollConfiguration(
                                                            behavior: CustomScrollBehavior(
                                                            androidSdkVersion: androidSdkVersion,
                                                          ),
                                                          child: ListView.builder(
                                                            padding: EdgeInsets.zero,
                                                            shrinkWrap: true,
                                                            itemCount: options.length,
                                                            itemBuilder: (BuildContext context, int index) {
                                                              final ContactModel option = options.elementAt(index);
                                                              if (option.phoneNumber != "") {
                                                                return GestureDetector(
                                                                  onTap: () {
                                                                    onSelected(option);
                                                                  },
                                                                  child: Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: [
                                                                      Text(
                                                                        option.displayName!,
                                                                        style: const TextStyle(
                                                                          fontSize: 16,
                                                                          color: Colors.green,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                        option.phoneNumber!,
                                                                        style: const TextStyle(
                                                                          color: Colors.black,
                                                                        ),
                                                                      ),
                                                                      SizedBox(
                                                                        height: 10,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                );
                                                              } else {
                                                                return Container();
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                            IconButton(
                                              onPressed: () async {
                                                //final contact = await FlutterContacts.openExternalPick();
                                                //print(contact);
                                                ncp.Contact? contact = await _contactPicker.selectContact();
                                                print("This is number : " + contact!.phoneNumbers!.first.toString());
                                                phoneNoController.text = contact!.phoneNumbers!.first.toString();
                                              },
                                              icon: Image.asset('assets/images/phonebook.png'),
                                              iconSize: 32,
                                            ),
                                          ],
                                        ),*/
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        TextFormField(
                                          controller: amountController,
                                          maxLength: 10,
                                          keyboardType: TextInputType.number,
                                          validator: (value) => value!.isEmpty
                                              ? AppLocalizations.of(context)!
                                                  .localizedString("amount")
                                              : null,
                                          onSaved: (value) => amountController,
                                          decoration: InputDecoration(
                                            border: UnderlineInputBorder(),
                                            labelText:
                                                AppLocalizations.of(context)!
                                                    .localizedString("amount"),
                                            labelStyle:
                                                TextStyle(color: Colors.green),
                                            counterText: "",
                                            icon: Icon(
                                              Icons.money,
                                              color: Colors.green,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 20, top: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: Color(int.parse(
                                              loginbuttonColor.toString())),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: const Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ]),
                                      height: 50,
                                      width: MediaQuery.of(context).size.width -
                                          120,
                                      child: TextButton(
                                        onPressed: () async {
                                          if (validateAndSave() &&
                                              validateMobile(
                                                  phoneNoController.text) &&
                                              validateAmount(double.parse(
                                                  amountController.text))) {
                                            setState(() {
                                              _isLoading = true;
                                            });
                                            showMessage();
                                            // transactionPinMessage();
                                          } else {
                                            setState(() {
                                              _isLoading = false;
                                              if (validateAndSave() && !validateMobile(
                                                  phoneNoController.text)) {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return AlertDialog(
                                                        elevation: 24,
                                                        title: Center(
                                                          child: Column(
                                                            children: [
                                                              Text(
                                                                AppLocalizations.of(
                                                                        context)!
                                                                    .localizedString(
                                                                        "alert"),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              const Icon(
                                                                Icons.add_alert,
                                                                color:
                                                                    Colors.red,
                                                                size: 50,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        content: Text(
                                                          AppLocalizations.of(
                                                                  context)!
                                                              .localizedString(
                                                                  "valid_mobile"),
                                                        ),
                                                        actions: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    right: 20.0,
                                                                    bottom: 20),
                                                            child: TextButton(
                                                              onPressed: () {
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              },
                                                              child: Text(
                                                                AppLocalizations.of(
                                                                        context)!
                                                                    .localizedString(
                                                                        "okay"),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        900]),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      );
                                                    });
                                              }
                                              if (validateAndSave() && !validateAmount(double.parse(
                                                  amountController.text == "" ? "0" : amountController.text))) {
                                                showDialog(
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return AlertDialog(
                                                        title: Center(
                                                          child: Column(
                                                            children: [
                                                              Text(
                                                                AppLocalizations.of(
                                                                        context)!
                                                                    .localizedString(
                                                                        "alert"),
                                                              ),
                                                              const SizedBox(
                                                                height: 10,
                                                              ),
                                                              const Icon(
                                                                Icons.add_alert,
                                                                color:
                                                                    Colors.red,
                                                                size: 50,
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                        content: Text(
                                                          AppLocalizations.of(
                                                                  context)!
                                                              .localizedString(
                                                                  "minimum_amount"),
                                                        ),
                                                        actions: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    right: 20.0,
                                                                    bottom: 20),
                                                            child: Container(
                                                              height: 40,
                                                              width: 80,
                                                              decoration: BoxDecoration(
                                                                  color: Colors
                                                                      .green,
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              30)),
                                                              child: TextButton(
                                                                onPressed: () {
                                                                  Navigator.of(
                                                                          context)
                                                                      .pop();
                                                                },
                                                                child: Text(
                                                                  AppLocalizations.of(
                                                                          context)!
                                                                      .localizedString(
                                                                          "okay"),
                                                                  style: const TextStyle(
                                                                      color: Colors
                                                                          .white),
                                                                ),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      );
                                                    });
                                              }
                                            });
                                          }
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("submit"),
                                          style: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMobile(String value) {
    String pattern = r'(^(984|986)?[0-9]{7}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateAmount(double value) {
    if (value < 10) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }
}
