import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/datavoicepackage/datavoicepackage_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/datavoicepackage/ntc/ntc_datavoicepackagepayment_page.dart';
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';

import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';

class NTCDataVoicePackageListPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? packageCode;
  NTCDataVoicePackageListPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.packageCode,
  }) : super(key: key);

  @override
  _NTCDataVoicePackageListPageState createState() => _NTCDataVoicePackageListPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
        packageCode,
      );
}

class _NTCDataVoicePackageListPageState extends State<NTCDataVoicePackageListPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? packageCode;
  _NTCDataVoicePackageListPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.packageCode,
  );

  bool _isLoading = false;

  void initState() {
    super.initState();
    _isLoading = true;
    getJsonData();
  }

  List<dynamic> data = [];
  bool showBalance = false;

  movetoDataVoicePackagePage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => DataVoicePackagePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  Future<String> getJsonData() async {
    String url = "${baseUrl}api/v1/productpackage?operatorCode=124";
    final response = await http.get(Uri.parse(url), headers: {
      'Authorization': "Bearer ${accesstoken}",
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        var convertDataToJson = json.decode(response.body);
        data = convertDataToJson["Packages"];
      });
      return "success";
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    }
    return "success";
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoDataVoicePackagePage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    height: 150,
                    decoration: BoxDecoration(
                      color: Color(int.parse(primaryColor.toString())),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          left: 10.0,
                          top: 40,
                          child: IconButton(
                              onPressed: () {
                                movetoDataVoicePackagePage();
                              },
                              icon: const Icon(
                                Icons.arrow_back,
                                size: 30.0,
                                color: Colors.white,
                              )),
                        ),
                        Positioned(
                          left: 100,
                          top: 50,
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("sahakaari_pay"),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                        Positioned(
                          left: 20,
                          top: 95,
                          child: Row(
                            children: [
                              const SizedBox(
                                height: 30,
                                width: 30,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/wallet_icon.png")),
                              ),
                              const SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                showBalance
                                    ? AppLocalizations.of(context)!
                                            .localizedString("rs") +
                                        "${balance.toString()}"
                                    : "xxx.xx".toUpperCase(),
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 180.0),
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (showBalance == false) {
                                          showBalance = true;
                                        } else {
                                          showBalance = false;
                                        }
                                      });
                                    },
                                    icon: Icon(
                                      showBalance
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      size: 25.0,
                                      color: Colors.white,
                                    )),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Expanded(
                  child: ScrollConfiguration(
                    behavior: CustomScrollBehavior(
                      androidSdkVersion: androidSdkVersion,
                    ),
                    child: ListView.builder(
                        itemCount:
                        data == null ? 0 : data?.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.only(
                                left: 10.0, right: 10, bottom: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                    padding:
                                    const EdgeInsets.only(left: 10.0),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 5,
                                            blurRadius: 7,
                                            offset: const Offset(0, 3),
                                          )
                                        ],
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10.0, top: 15),
                                                child: Container(
                                                  height: 40,
                                                  width: 30,
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                      BorderRadius.circular(20),
                                                      image: const DecorationImage(
                                                          image: AssetImage(
                                                              "assets/images/topup/ntc_logo.png"))),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10.0, top: 20),
                                                child: Container(
                                                  width: MediaQuery.of(context).size.width - 80,
                                                  child: Text(
                                                    data?[index]
                                                    ["Name"],
                                                    style: const TextStyle(
                                                      fontSize: 18,
                                                        fontWeight: FontWeight.bold),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Visibility(
                                            visible: data?[index]["Description"] != "",
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 50.0, top: 5),
                                              child: Container(
                                                width: MediaQuery.of(context).size.width - 80,
                                                child: Text(
                                                  data?[index]
                                                  ["Description"],
                                                  style: const TextStyle(
                                                      fontSize: 14,),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 50, top: 10.0),
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                  color: Colors.white),
                                              child: Text(
                                                "NRP: ${data?[index]["Amount"].toStringAsFixed(2)}",
                                                style: const TextStyle(
                                                    color: Colors.green,
                                                fontWeight: FontWeight.bold,),
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 50, top: 10.0, right: 20.0, bottom: 20.0),
                                            child: Container(
                                              width: MediaQuery.of(context).size.width - 50,
                                              decoration: const BoxDecoration(
                                                  color: Colors.white),
                                              child: Row(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                children: [
                                                  InkWell(
                                                    onTap: () async {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                              builder: (context) => NTCDataVoicePackagePaymentPage(
                                                                coopList: coopList,
                                                                userId: userId,
                                                                accesstoken: accesstoken,
                                                                balance: balance,
                                                                baseUrl: baseUrl,
                                                                accountno: accountno,
                                                                primaryColor: primaryColor,
                                                                loginButtonTitleColor:
                                                                loginButtonTitleColor,
                                                                loginbuttonColor:
                                                                loginbuttonColor,
                                                                loginTextFieldColor:
                                                                loginTextFieldColor,
                                                                dasboardIconColor:
                                                                dasboardIconColor,
                                                                dashboardTopTitleColor:
                                                                dashboardTopTitleColor,
                                                                SecondaryColor:
                                                                SecondaryColor,
                                                                packageCode: data[index]["Code"].toString().trim(),
                                                                packageName: data[index]["Name"].toString().trim(),
                                                                packageAmount: data[index]["Amount"].toStringAsFixed(2).trim(),
                                                              )));
                                                    },
                                                    child: Container(
                                                      height: 40,
                                                      width: 140,
                                                      decoration: BoxDecoration(
                                                        color: Color(int.parse(
                                                            primaryColor.toString())),
                                                        borderRadius: BorderRadius.circular(10),
                                                        boxShadow: [
                                                          BoxShadow(
                                                            color: Colors.white.withOpacity(0.9),
                                                            spreadRadius: 5,
                                                            blurRadius: 7,
                                                            offset: const Offset(0, 3),
                                                          )
                                                        ],
                                                      ),
                                                      child: Center(
                                                        child: Text(
                                                          AppLocalizations.of(context)!
                                                              .localizedString("buy"),
                                                          style: TextStyle(
                                                            color: Color(int.parse(
                                                                loginButtonTitleColor
                                                                    .toString())),
                                                            fontSize: 16.0,
                                                          ),
                                                          textAlign: TextAlign.center,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                                const SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          );
                        }),
                  ),
                ),
              ],
            ),
      ),
    );
  }
}
