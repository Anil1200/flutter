import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/datavoicepackage/ncell/ncell_datavoicepackage_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/datavoicepackage/ncell/ncell_datavoicepackagelist_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/datavoicepackage/ntc/ntc_datavoicepackage_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/datavoicepackage/ntc/ntc_datavoicepackagelist_page.dart';

import '../../../utils/localizations.dart';
import '../../pmc_homepage.dart';

class DataVoicePackagePage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  DataVoicePackagePage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<DataVoicePackagePage> createState() => _DataVoicePackagePageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _DataVoicePackagePageState extends State<DataVoicePackagePage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _DataVoicePackagePageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  // double? balance;

  @override
  void initState() {
    super.initState();

    // print(coopList);
    print("this is the required passed balance value: ${balance.toString()}");
    // this.getJsonData();

    print(coopList);
    this.getJsonData();
  }

  Future<String> getJsonData() async {
    String url = "${baseUrl}api/v1/pes/account/${accountno}/info";
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // Map authorization = {
    //   "Token":
    //       "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI3NWNiMGU4Yy1hZjZlLWVjMTEtODEzNS0wMDUwNTY4OWRlYjkiLCJlbWFpbElkIjoidGVzdEB0ZXN0LmNvbSIsImFjY291bnRObyI6IlNTLTAwMDAwMDYiLCJtUElOIjoiYTZhYzg2MDA0MDZhODViNDIwNTE1MGQ2YWRiZDljYjQiLCJpc1JlZnJlc2hUb2tlbiI6ZmFsc2UsImV4cCI6MTY0NTAzNTI3M30.Tvd0PcwlDyqxDAdCNnwXzrkJtSDEF0fTRx0dAYKjOBY"
    // };
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        var convertDataToJson = json.decode(response.body);
        balance = double.parse(convertDataToJson["Balance"].toString());
        print(convertDataToJson);
        // sharedPreferences.setStringList(
        //     'Cooperatives', accountinfo!.map((e) => e.toString()).toList());
      });
      return "success";
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 100,
                decoration: BoxDecoration(
                  color: Color(int.parse(primaryColor.toString())),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: 10.0,
                      top: 40.0,
                      child: IconButton(
                          onPressed: () {
                            movetohomepage();
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            size: 30.0,
                            color: Colors.white,
                          )),
                    ),
                    Positioned(
                      left: 100,
                      top: 50,
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("data_voice_package"),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: GridView.count(
                    physics: NeverScrollableScrollPhysics(),
                    // childAspectRatio: 0.2 / 2,
                    crossAxisCount: 4,
                    shrinkWrap: true,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NTCDataVoicePackageListPage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor:
                                            loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor:
                                            dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor:
                                            SecondaryColor,
                                      )));
                        },
                        child: Column(
                          children: [
                            SizedBox(
                              height: 50,
                              width: 50,
                              child: Image(
                                  image: AssetImage(
                                      "assets/images/topup/ntc_logo.png")),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Flexible(
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("ntc"),
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => NcellDataVoicePackageListPage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor:
                                            loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor:
                                            dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor:
                                            SecondaryColor,
                                      )));
                        },
                        child: Column(
                          children: [
                            SizedBox(
                              height: 50,
                              width: 50,
                              child: Image(
                                  image: AssetImage(
                                      "assets/images/topup/ncell_logo.png")),
                            ),
                            SizedBox(
                              height: 5.0,
                            ),
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("ncell"),
                              style: TextStyle(
                                color: Colors.green,
                                fontSize: 12,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
