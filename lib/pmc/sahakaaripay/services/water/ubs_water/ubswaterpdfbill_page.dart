import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import '../../../pmc_homepage.dart';
import '../../water/ubs_water/mobile.dart'
    if (dart.library.html) '../water/ubs_water/web.dart';

class UBSWaterPdfBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? CustomerName;
  String? CustomerId;
  String? CustomerAddress;
  String? ReferenceId;
  String? TransactionId;
  String? paymentMessage;
  String? PaymentBillDate;
  double? totaldueAmount;
  String? Office;
  String? OfficeCode;
  String? CurrentMonthDues;
  String? CurrentMonthFine;
  String? CurrentMonthDiscount;
  String? RemainingDues;
  String? NetAmount;
  String? TotalAmount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  UBSWaterPdfBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.CustomerName,
    this.CustomerId,
    this.CustomerAddress,
    this.ReferenceId,
    this.TransactionId,
    this.paymentMessage,
    this.PaymentBillDate,
    this.totaldueAmount,
    this.Office,
    this.OfficeCode,
    this.CurrentMonthDues,
    this.CurrentMonthFine,
    this.CurrentMonthDiscount,
    this.RemainingDues,
    this.NetAmount,
    this.TotalAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _UBSWaterPdfBillPageState createState() => _UBSWaterPdfBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        CustomerName,
        CustomerId,
        CustomerAddress,
        ReferenceId,
        TransactionId,
        paymentMessage,
        PaymentBillDate,
        totaldueAmount,
        Office,
        OfficeCode,
        CurrentMonthDues,
        CurrentMonthFine,
        CurrentMonthDiscount,
        RemainingDues,
        NetAmount,
        TotalAmount,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _UBSWaterPdfBillPageState extends State<UBSWaterPdfBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? CustomerName;
  String? CustomerId;
  String? CustomerAddress;
  String? ReferenceId;
  String? TransactionId;
  String? paymentMessage;
  String? PaymentBillDate;
  double? totaldueAmount;
  String? Office;
  String? OfficeCode;
  String? CurrentMonthDues;
  String? CurrentMonthFine;
  String? CurrentMonthDiscount;
  String? RemainingDues;
  String? NetAmount;
  String? TotalAmount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _UBSWaterPdfBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.CustomerName,
    this.CustomerId,
    this.CustomerAddress,
    this.ReferenceId,
    this.TransactionId,
    this.paymentMessage,
    this.PaymentBillDate,
    this.totaldueAmount,
    this.Office,
    this.OfficeCode,
    this.CurrentMonthDues,
    this.CurrentMonthFine,
    this.CurrentMonthDiscount,
    this.RemainingDues,
    this.NetAmount,
    this.TotalAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    this.nullcheck();
  }

  String? Rid;
  String? Tid;

  nullcheck() {
    if (ReferenceId == null) {
      Rid = "ReferenceID not available.";
    } else {
      Rid = ReferenceId;
    }
    if (TransactionId == null) {
      Tid = "TransactionId not available";
    } else {
      Tid = TransactionId;
    }
  }

  String? timeforBill = DateTime.now().year.toString() +
      "-" +
      DateTime.now().month.toString() +
      "-" +
      DateTime.now().day.toString() +
      " " +
      DateTime.now().hour.toString() +
      ":" +
      DateTime.now().minute.toString();

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'UBS Water Bill Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );
    final grid = PdfGrid();
    grid.columns.add(count: 2);
    var row = grid.rows.add();
    row.cells[0].value = "ReferenceId:";
    row.cells[1].value = Rid.toString();

    row = grid.rows.add();
    row.cells[0].value = "TransactionId:";
    row.cells[1].value = Tid.toString();

    // row = grid.rows.add();
    // row.cells[0].value = "ReceiptNo: ";
    // row.cells[1].value = "${Rno.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Date: ";
    row.cells[1].value = "${timeforBill.toString()}";

    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));

    page.graphics.drawString(
        'Customer Details',
        PdfStandardFont(
          PdfFontFamily.helvetica,
          30,
        ),
        bounds: Rect.fromLTWH(0, 150, 0, 0));

    final grid2 = PdfGrid();
    grid2.columns.add(count: 2);

    row = grid2.rows.add();
    row.cells[0].value = "Customer Name: ";
    row.cells[1].value = "${CustomerName.toString()}";

    row = grid2.rows.add();
    row.cells[0].value = "Customer Id: ";
    row.cells[1].value = "${CustomerId.toString()}";

    row = grid2.rows.add();
    row.cells[0].value = "Customer Address: ";
    row.cells[1].value = "${CustomerAddress.toString()}";

    grid2.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid2.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid2.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 170, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));

    page.graphics.drawString(
      'Payment Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
      bounds: Rect.fromLTWH(0, 260, 0, 0),
    );
    drawGrid3(page);

    List<int> bytes = document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  void drawGrid3(page) {
    final grid = PdfGrid();
    grid.columns.add(count: 2);

    var row = grid.rows.add();
    row.cells[0].value = "Office: ";
    row.cells[1].value = Office;

    row = grid.rows.add();
    row.cells[0].value = "Current Month Due: ";
    row.cells[1].value = CurrentMonthDues.toString();

    row = grid.rows.add();
    row.cells[0].value = "Current Month Fine: ";
    row.cells[1].value = CurrentMonthFine.toString();

    row = grid.rows.add();
    row.cells[0].value = "Current Month Discount: ";
    row.cells[1].value = CurrentMonthDiscount.toString();

    row = grid.rows.add();
    row.cells[0].value = "Remaining Dues: ";
    row.cells[1].value = RemainingDues.toString();

    row = grid.rows.add();
    row.cells[0].value = "Total Amount: ";
    row.cells[1].value = totaldueAmount.toString();

    row = grid.rows.add();
    row.cells[0].value = "Payment Message: ";
    row.cells[1].value = paymentMessage.toString();

    grid.style = PdfGridStyle(
      font: PdfStandardFont(PdfFontFamily.timesRoman, 20),
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );
    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(page: page, bounds: Rect.fromLTWH(0, 280, 0, 0));
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoHomePage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "UBS Water Bill Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "ReferenceId:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 84,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Rid}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "TransactionId:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 72,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Tid}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Date:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 134,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${timeforBill.toString()}")),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Divider(
                  thickness: 1,
                  color: Colors.green[900],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Center(
                    child: Text(
                      "Customer Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Customer Name:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${CustomerName.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Customer Id:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 80,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${CustomerId.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Customer Address:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 38,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${CustomerAddress.toString()}")),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Divider(
                  thickness: 1,
                  color: Colors.green[900],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Center(
                    child: Text(
                      "Payment Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Office:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 126,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Office.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "CurrentMonthDues:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 37,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${CurrentMonthDues.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "CurrentMonthFine:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 44,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${CurrentMonthFine.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "CurrentMonthDiscount:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${CurrentMonthDiscount.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "TotalAmount:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 80,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${totaldueAmount.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Payment Message:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 42,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${paymentMessage.toString()}")),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Divider(
                  thickness: 1,
                  color: Colors.green[900],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 100,
                    ),
                    TextButton(
                        onPressed: () {
                          movetoHomePage();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("home"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                    const SizedBox(
                      width: 60,
                    ),
                    TextButton(
                        onPressed: createPDF,
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("pdf"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
