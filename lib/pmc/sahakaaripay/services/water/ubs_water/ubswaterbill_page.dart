import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/water/ubs_water/ubswater_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/water/ubs_water/ubswaterpdfbill_page.dart';

import '../../../../constants.dart';
import '../../../../local_auth_api.dart';
import '../../../../utils/localizations.dart';
import '../../../../utils/utils.dart';
import '../../../models/loading.dart';

class UBSWaterBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? CustomerName;
  String? CustomerId;
  String? CustomerAddress;
  String? Office;
  String? OfficeCode;
  double? totaldueAmount;
  double? CurrentMonthDues;
  double? CurrentMonthFine;
  double? CurrentMonthDiscount;
  double? RemainingDues;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  UBSWaterBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.CustomerName,
    this.CustomerId,
    this.CustomerAddress,
    this.Office,
    this.OfficeCode,
    this.totaldueAmount,
    this.CurrentMonthDues,
    this.CurrentMonthFine,
    this.CurrentMonthDiscount,
    this.RemainingDues,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _UBSWaterBillPageState createState() => _UBSWaterBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        CustomerName,
        CustomerId,
        CustomerAddress,
        Office,
        OfficeCode,
        totaldueAmount,
        CurrentMonthDues,
        CurrentMonthFine,
        CurrentMonthDiscount,
        RemainingDues,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _UBSWaterBillPageState extends State<UBSWaterBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? CustomerName;
  String? CustomerId;
  String? CustomerAddress;
  String? Office;
  String? OfficeCode;
  double? totaldueAmount;
  double? CurrentMonthDues;
  double? CurrentMonthFine;
  double? CurrentMonthDiscount;
  double? RemainingDues;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _UBSWaterBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.CustomerName,
    this.CustomerId,
    this.CustomerAddress,
    this.Office,
    this.OfficeCode,
    this.totaldueAmount,
    this.CurrentMonthDues,
    this.CurrentMonthFine,
    this.CurrentMonthDiscount,
    this.RemainingDues,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  bool _isLoading = false;
  bool _obscureText = true;
  String? dropdownValue;

  String? RemDue;
  String? CMDue;
  String? CMFine;
  String? CMDiscount;

  String? ReferenceId;
  String? TransactionId;
  String? paymentMessage;
  String? PaymentBillDate;

  @override
  void initState() {
    super.initState();
    print("this is the Current Month Dues : ${CurrentMonthDues}");
    this.nullcheck();
    // this.nullcheck();
  }

  nullcheck() {
    if (CurrentMonthDues == null) {
      CMDue = "0.0";
    } else {
      CMDue = CurrentMonthDues.toString();
    }
    if (CurrentMonthFine == null) {
      CMFine = "0.0";
    } else {
      CMFine = CurrentMonthFine.toString();
    }
    if (CurrentMonthDiscount == null) {
      CMDiscount = "0.0";
    } else {
      CMDiscount = CurrentMonthDiscount.toString();
    }
    if (RemainingDues == null) {
      RemDue = "0.0";
    } else {
      RemDue = RemainingDues.toString();
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${totaldueAmount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2a") +
                          " ${Office.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            payUBSWaterBill();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${totaldueAmount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2a") +
                          " ${Office.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  double? serviceCharge;

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      payUBSWaterBill();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  postServiceCharge() async {
    String url =
        "${baseUrl}api/v1/ubswater/charge?customerId=${CustomerId.toString()}&officeCode=${OfficeCode.toString()}&sessionId=1&amount=${totaldueAmount.toString()}";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    setState(() {
      _isLoading = true;
    });
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        var jsonResponse = json.decode(response.body);
        print("this is the reponse for service Charge:${jsonResponse}");
        if (jsonResponse != null) {
          serviceCharge = jsonResponse["SCharge"];
          setState(() {
            _isLoading = false;
            if (showBiometricbuttonTransaction) {
              transactionPinMessageBiometric();
            } else {
              transactionPinMessage();
            }
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print("this is the 400 error:${jsonResponse}");
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print("this is the 401 error:${jsonResponse}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print("this is the error response:${jsonResponse}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("ALERT"),
              content: Text(
                  AppLocalizations.of(context)!.localizedString("server_down")),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
    return "success";
  }

  payUBSWaterBill() async {
    String url = "${baseUrl}api/v1/billpayments";
    Map body = {
      "OperatorCode": "41",
<<<<<<< HEAD
      "MPIN": mpincontroller.text.toString().toString().trim(),
=======
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
>>>>>>> sudeep
      "Amount": totaldueAmount.toString(),
      "ExtraField1": OfficeCode.toString(),
      "ExtraField2": serviceCharge.toString(),
      "Subscriber": CustomerId.toString(),
    };
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      // "Content-Type": application/json,
      "Authorization": "Bearer ${accesstoken}"
    });
    print(body);

    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          ReferenceId = jsonResponse["RefId"];
          TransactionId = jsonResponse["TransactionId"];
          paymentMessage = jsonResponse["Message"];
          // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              if (TransactionId == null)
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Row(
                  children: [
                    const Icon(
                      Icons.verified,
                      color: Colors.green,
                      size: 40,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      jsonResponse["Message"],
                      style: const TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )));
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UBSWaterPdfBillPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            balance: balance,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            CustomerName: CustomerName,
                            CustomerId: CustomerId,
                            CustomerAddress: CustomerAddress,
                            ReferenceId: ReferenceId,
                            TransactionId: TransactionId,
                            paymentMessage: paymentMessage,
                            PaymentBillDate: PaymentBillDate,
                            totaldueAmount: totaldueAmount,
                            Office: Office,
                            OfficeCode: OfficeCode,
                            CurrentMonthDues: CMDue,
                            CurrentMonthFine: CMFine,
                            CurrentMonthDiscount: CMDiscount,
                            RemainingDues: RemDue,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor: dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, bottom: 20),
                  child: Container(
                    height: 60,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        )),
                  ),
                )
              ],
            );
          });
    }
  }

  movetoUBSPage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => UBSWaterPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoUBSPage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 60.0, left: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              movetoUBSPage();
                            },
                            icon: const Icon(
                              Icons.arrow_back,
                              size: 30,
                              color: Colors.green,
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, top: 12),
                            child: Center(
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("ubs_water_bill_details"),
                                style: TextStyle(
                                    color: Colors.green[900],
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 40),
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("customer_details"),
                        style: const TextStyle(
                          color: Colors.black87,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("customer_name"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${CustomerName}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("address"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 65,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${CustomerAddress}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("customer_id"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 40,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2,
                              child: Text("${CustomerId}")),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      color: Colors.green[900],
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 40),
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("payment_details"),
                        style: const TextStyle(
                          color: Colors.black87,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("office"),
                            style: const TextStyle(
                                color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 162,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2 - 40,
                              child: Text("${Office}")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("current_month_due"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 70,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2 - 40,
                              child: Text(CMDue.toString())),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("current_month_fine"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 68,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2 - 40,
                              child: Text(CMFine.toString())),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("current_month_discount"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 36,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2 - 40,
                              child: Text(CMDiscount.toString())),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("remaining_due"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 96,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2 - 40,
                              child: Text(RemDue.toString())),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("total_amount"),
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 107,
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 2 - 40,
                              child: Text(totaldueAmount.toString())),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 60, top: 40),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 50,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Color(
                                    int.parse(loginbuttonColor.toString())),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => UBSWaterPage(
                                              coopList: coopList,
                                              userId: userId,
                                              accesstoken: accesstoken,
                                              balance: balance,
                                              baseUrl: baseUrl,
                                              primaryColor: primaryColor,
                                              loginButtonTitleColor:
                                                  loginButtonTitleColor,
                                              loginbuttonColor:
                                                  loginbuttonColor,
                                              loginTextFieldColor:
                                                  loginTextFieldColor,
                                              dasboardIconColor:
                                                  dasboardIconColor,
                                              dashboardTopTitleColor:
                                                  dashboardTopTitleColor,
                                              SecondaryColor: SecondaryColor,
                                            )));
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("back"),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 80,
                          ),
                          Container(
                            height: 50,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Color(
                                    int.parse(loginbuttonColor.toString())),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () {
                                if (totaldueAmount == 0.0) {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Center(
                                              child: Column(
                                            children: [
                                              Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "error_alert"),
                                                style: TextStyle(fontSize: 16),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Icon(
                                                Icons.add_alert,
                                                color: Colors.red,
                                                size: 50.0,
                                              )
                                            ],
                                          )),
                                          content: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("no_amount"),
                                            style: TextStyle(fontSize: 14),
                                          ),
                                          actions: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 8.0, bottom: 20),
                                              child: Container(
                                                height: 60,
                                                width: 100,
                                                decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: TextButton(
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: Text(
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "okay"),
                                                    )),
                                              ),
                                            )
                                          ],
                                        );
                                      });
                                } else {
                                  postServiceCharge();
                                }
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("pay"),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}
