import 'dart:io';

import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

Future<void> saveAndLaunchFile(List<int> bytes, String UBSWaterBill) async {
  final path = (await getExternalStorageDirectory())?.path;
  final file = File('$path/$UBSWaterBill');
  await file.writeAsBytes(bytes, flush: true);
  OpenFile.open('$path/$UBSWaterBill');
}
