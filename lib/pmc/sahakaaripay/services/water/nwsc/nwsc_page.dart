import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/water/nwsc/nwscbilldetails_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/water/waterbill_page.dart';

import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';

class NWSCWaterPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  NWSCWaterPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _NWSCWaterPageState createState() => _NWSCWaterPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _NWSCWaterPageState extends State<NWSCWaterPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _NWSCWaterPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  List? renewalplans;
  String? paymentmessage;
  String? ubswaterUserName;
  String? CustomerName;
  String? CustomerId;
  String? CustomerAddress;
  String? message;
  double? totaldueAmount;

  List? counterList;
  List? NWSCBillList;

  bool showBalance = false;

  List<String> ubsOfficeName = [];

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    this.getjsonData();
  }

  Future<String> getjsonData() async {
    String url = "${baseUrl}api/v1/billpayment/nwsc-office-codes";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    setState(() {
      _isLoading = true;
    });
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            counterList = jsonResponse["OfficeCodes"];
            print("this is the counter list: ${counterList}");

            for (var item in counterList!) {
              ubsOfficeName.add(item["Office"]);
            }
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  Future<String> BillInquiryData() async {
    String url =
        "${baseUrl}api/v1/billpayment/nwscbills?customerId=${nswcwaterIDController.text.toString()}&officeCode=${OfficeCode.toString()}";
    // Map body = {
    //   "OfficeCodes": dropdownValue.toString(),
    //   "CustomerId": nswcwaterIDController.text.toString(),
    // };
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    setState(() {
      _isLoading = true;
    });
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          NWSCBillList = jsonResponse["BillDetail"];
          CustomerName = jsonResponse["CustomerName"];
          CustomerId = jsonResponse["CustomerId"];
          CustomerAddress = jsonResponse["CustomerAddress"];
          Office = jsonResponse["Office"];
          message = jsonResponse["Message"];
          totaldueAmount = jsonResponse["TotalDueAmount"];
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("${message}"),
          ));
          setState(() {
            _isLoading = false;
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => NWSCBillDetailPage(
                          coopList: coopList,
                          userId: userId,
                          accesstoken: accesstoken,
                          balance: balance,
                          baseUrl: baseUrl,
                          accountno: accountno,
                          NWSCBillList: NWSCBillList,
                          CustomerName: CustomerName,
                          CustomerId: CustomerId,
                          CustomerAddress: CustomerAddress,
                          Office: Office,
                          OfficeCode: OfficeCode,
                          totaldueAmount: totaldueAmount,
                          primaryColor: primaryColor,
                          loginButtonTitleColor: loginButtonTitleColor,
                          loginbuttonColor: loginbuttonColor,
                          loginTextFieldColor: loginTextFieldColor,
                          dasboardIconColor: dasboardIconColor,
                          dashboardTopTitleColor: dashboardTopTitleColor,
                          SecondaryColor: SecondaryColor,
                        )));
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetoWaterBillPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => WaterBillPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  bool _isLoading = false;
  String? dropdownValue;
  String? oName;
  String? Office;
  String? OfficeCode;

  final GlobalKey<FormState> globalFormKey = GlobalKey();

  final TextEditingController nswcwaterIDController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoWaterBillPage();
      },
      child: Scaffold(
        body: _isLoading
            ? Center(child: Loading())
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Container(
                        height: 150,
                        decoration: BoxDecoration(
                          color: Color(int.parse(primaryColor.toString())),
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              left: 10.0,
                              top: 40,
                              child: IconButton(
                                  onPressed: () {
                                    Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => WaterBillPage(
                                                  coopList: coopList,
                                                  userId: userId,
                                                  accesstoken: accesstoken,
                                                  balance: balance,
                                                  baseUrl: baseUrl,
                                                  accountno: accountno,
                                                  primaryColor: primaryColor,
                                                  loginButtonTitleColor:
                                                      loginButtonTitleColor,
                                                  loginbuttonColor:
                                                      loginbuttonColor,
                                                  loginTextFieldColor:
                                                      loginTextFieldColor,
                                                  dasboardIconColor:
                                                      dasboardIconColor,
                                                  dashboardTopTitleColor:
                                                      dashboardTopTitleColor,
                                                  SecondaryColor:
                                                      SecondaryColor,
                                                )));
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back,
                                    size: 30.0,
                                    color: Colors.white,
                                  )),
                            ),
                            Positioned(
                              left: 100,
                              top: 50,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("sahakaari_pay"),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 20,
                              top: 95,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: Image(
                                        image: AssetImage(
                                            "assets/images/wallet_icon.png")),
                                  ),
                                  const SizedBox(
                                    width: 10.0,
                                  ),
                                  Text(
                                    showBalance
                                        ? AppLocalizations.of(context)!
                                                .localizedString("rs") +
                                            "${balance.toString()}"
                                        : "xxx.xx".toUpperCase(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 180.0),
                                    child: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            if (showBalance == false) {
                                              showBalance = true;
                                            } else {
                                              showBalance = false;
                                            }
                                          });
                                        },
                                        icon: Icon(
                                          showBalance
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          size: 25.0,
                                          color: Colors.white,
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 30.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("nwsc_water"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0, right: 10, top: 20),
                              child: DropdownSearch<String>(
                                popupProps: PopupProps.dialog(
                                  searchFieldProps: TextFieldProps(
                                    // autofocus: true,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 24, vertical: 10),
                                    decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        hintText: AppLocalizations.of(context)!
                                            .localizedString("search"),
                                        hintStyle: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                            color: Colors.grey[400]),
                                        suffixIcon: Icon(
                                          Icons.search,
                                          size: 30,
                                          color: Colors.grey[500],
                                        )),
                                  ),
                                  showSearchBox: true,
                                ),
                                dropdownDecoratorProps: DropDownDecoratorProps(
                                  dropdownSearchDecoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)!
                                          .localizedString("select_branch"),
                                      hintStyle: TextStyle(
                                          fontSize: 16, color: Colors.black87)),
                                ),
                                items: ubsOfficeName,
                                selectedItem: oName,
                                onChanged: (String? newValue) {
                                  final index = counterList!.indexWhere(
                                      (element) =>
                                          element["Office"] == newValue);
                                  newValue = counterList![index]["OfficeCodes"];
                                  setState(() {
                                    dropdownValue = newValue!;
                                    oName = counterList![index]["Office"];
                                    print("this is offices : ${dropdownValue}");
                                  });
                                },
                              ),
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.only(
                            //       left: 30.0, right: 10.0, top: 10.0),
                            //   child: DropdownSearch<String>(
                            //     mode: Mode.BOTTOM_SHEET,
                            //     showSearchBox: true,
                            //     items: ubsOfficeName,
                            //     onChanged: (String? newValue) {
                            //       final index = counterList!.indexWhere(
                            //           (element) => element["Office"] == newValue);
                            //       newValue = counterList![index]["OfficeCodes"];
                            //       setState(() {
                            //         dropdownValue = newValue!;
                            //         print("this is offices : ${dropdownValue}");
                            //       });
                            //     },
                            //   ),
                            // ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 40.0, top: 40),
                              child: Container(
                                width: 300,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: Form(
                                  key: globalFormKey,
                                  child: TextFormField(
                                    controller: nswcwaterIDController,
                                    maxLength: 30,
                                    keyboardType: TextInputType.text,
                                    validator: (value) => value!.isEmpty
                                        ? "Please Enter ID"
                                        : null,
                                    onSaved: (value) => nswcwaterIDController,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                      ),
                                      labelText: AppLocalizations.of(context)!
                                          .localizedString("enter_id"),
                                      labelStyle:
                                          const TextStyle(color: Colors.green),
                                      counterText: "",
                                      icon: const Icon(
                                        Icons.supervised_user_circle,
                                        color: Colors.green,
                                        size: 20.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 220.0, top: 40),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    color: Color(
                                        int.parse(loginbuttonColor.toString())),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ]),
                                height: 50,
                                width: 140,
                                child: TextButton(
                                  onPressed: () {
                                    if (validateAndSave()) {
                                      setState(() {
                                        _isLoading = true;
                                        BillInquiryData();
                                      });
                                    } else {
                                      setState(() {
                                        _isLoading = false;
                                      });
                                    }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("inquire_balance"),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
