import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/water/nwsc/nwsc_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/water/nwsc/nwscpdfbill_page.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import '../../../../constants.dart';
import '../../../../local_auth_api.dart';
import '../../../../utils/utils.dart';
import '../../../models/loading.dart';
import '../ubs_water/mobile.dart'
    if (dart.library.html) '../ubs_water/web.dart';

class NWSCBillDetailPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? NWSCBillList;
  String? CustomerName;
  String? CustomerId;
  String? CustomerAddress;
  String? Office;
  String? OfficeCode;
  double? totaldueAmount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  NWSCBillDetailPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.NWSCBillList,
    this.CustomerName,
    this.CustomerId,
    this.CustomerAddress,
    this.Office,
    this.OfficeCode,
    this.totaldueAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _NWSCBillDetailPageState createState() => _NWSCBillDetailPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        NWSCBillList,
        CustomerName,
        CustomerId,
        CustomerAddress,
        Office,
        OfficeCode,
        totaldueAmount,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _NWSCBillDetailPageState extends State<NWSCBillDetailPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? NWSCBillList;
  String? CustomerName;
  String? CustomerId;
  String? CustomerAddress;
  String? Office;
  String? OfficeCode;
  double? totaldueAmount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _NWSCBillDetailPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.NWSCBillList,
    this.CustomerName,
    this.CustomerId,
    this.CustomerAddress,
    this.Office,
    this.OfficeCode,
    this.totaldueAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  bool _isLoading = false;
  bool _obscureText = true;
  String? dropdownValue;

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;

  @override
  void initState() {
    super.initState();
    this.nullcheck();
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${totaldueAmount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2a") +
                          " ${Office.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            payNWSCWaterBill();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: const [
                                          Text(
                                            "ALERT",
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: const Text(
                                        "Please enter 4 digit valid MPIN number"),
                                    actions: [
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${totaldueAmount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message2a") +
                          " ${Office.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      payNWSCWaterBill();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  double? serviceCharge;

  payNWSCWaterBill() async {
    String url = "${baseUrl}api/v1/billpayments";
    Map body = {
      "OperatorCode": "40",
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
      "ExtraField1": OfficeCode.toString(),
      "ExtraField2": "0",
      "Subscriber": CustomerId.toString(),
      "Amount": TotalAmount.toString(),
    };
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      // "Content-Type": application/json,
      "Authorization": "Bearer ${accesstoken}"
    });
    print(body);

    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          ReferenceId = jsonResponse["ReferenceId"];
          TransactionId = jsonResponse["TransactionId"];
          ReceiptNo = jsonResponse["ReceiptNo"];
          // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  const Icon(
                    Icons.verified,
                    color: Colors.green,
                    size: 40,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    jsonResponse["Message"],
                    style: const TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ],
              )));
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NWSCWaterPdfBillPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            balance: balance,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            CustomerName: CustomerName,
                            CustomerId: CustomerId,
                            CustomerAddress: CustomerAddress,
                            ReferenceId: ReferenceId,
                            TransactionId: TransactionId,
                            ReceiptNo: ReceiptNo,
                            PaymentBillDate: PaymentBillDate,
                            NWSCBillList: NWSCBillList,
                            totaldueAmount: totaldueAmount,
                            Office: Office,
                            OfficeCode: OfficeCode,
                            CurrentMonthDues: CurrentMonthDues,
                            CurrentMonthFine: CurrentMonthFine,
                            CurrentMonthDiscount: CurrentMonthDiscount,
                            RemainingDues: RemainingDues,
                            TotalAmount: TotalAmount,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor: dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 8.0, bottom: 20),
                  child: Container(
                    height: 60,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        )),
                  ),
                )
              ],
            );
          });
    }
  }

  movetoNWSCWaterpage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => NWSCWaterPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  String? CurrentMonthDues;
  String? CurrentMonthFine;
  String? CurrentMonthDiscount;
  String? RemainingDues;
  String? NetAmount;
  String? TotalAmount;

  nullcheck() {
    if (NWSCBillList?[0]["CurrentMonthDues"] == null) {
      CurrentMonthDues = "0.0";
    } else {
      CurrentMonthDues = NWSCBillList?[0]["CurrentMonthDues"].toString();
    }
    if (NWSCBillList?[0]["CurrentMonthFine"] == null) {
      CurrentMonthFine = "0.0";
    } else {
      CurrentMonthFine = NWSCBillList?[0]["CurrentMonthFine"].toString();
    }
    if (NWSCBillList?[0]["CurrentMonthDiscount"] == null) {
      CurrentMonthDiscount = "0.0";
    } else {
      CurrentMonthDiscount =
          NWSCBillList?[0]["CurrentMonthDiscount"].toString();
    }
    if (NWSCBillList?[0]["PreviousDues"] == null) {
      RemainingDues = "0.0";
    } else {
      RemainingDues = NWSCBillList?[0]["PreviousDues"].toString();
    }
    if (NWSCBillList?[0]["TotalDues"] == null) {
      NetAmount = "0.0";
    } else {
      NetAmount = NWSCBillList?[0]["TotalDues"].toString();
    }
    if (NWSCBillList?[0]["TotalDues"] == null) {
      TotalAmount = "0.0";
    } else {
      TotalAmount = NWSCBillList?[0]["TotalDues"].toString();
    }
  }

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'Customer Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );

    final grid = PdfGrid();
    grid.columns.add(count: 2);
    var row = grid.rows.add();
    row.cells[0].value = "Customer Name:";
    row.cells[1].value = CustomerName.toString();

    row = grid.rows.add();
    row.cells[0].value = "Customer Address:";
    row.cells[1].value = CustomerAddress.toString();

    row = grid.rows.add();
    row.cells[0].value = "CustomerId: ";
    row.cells[1].value = "${CustomerId.toString()}";

    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));
    // drawGrid(page);
    page.graphics.drawString(
      'Payment Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
      bounds: Rect.fromLTWH(0, 200, 0, 0),
    );
    drawGrid2(page);

    List<int> bytes = document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  void drawGrid2(page) {
    final grid = PdfGrid();
    grid.columns.add(count: 2);

    var row = grid.rows.add();
    row.cells[0].value = "Office: ";
    row.cells[1].value = Office;

    row = grid.rows.add();
    row.cells[0].value = "Current Month Due: ";
    row.cells[1].value = CurrentMonthDues.toString();

    row = grid.rows.add();
    row.cells[0].value = "Current Month Fine: ";
    row.cells[1].value = CurrentMonthFine.toString();

    row = grid.rows.add();
    row.cells[0].value = "Current Month Discount: ";
    row.cells[1].value = CurrentMonthDiscount.toString();

    row = grid.rows.add();
    row.cells[0].value = "Remaining Dues: ";
    row.cells[1].value = RemainingDues.toString();

    row = grid.rows.add();
    row.cells[0].value = "Net Amount: ";
    row.cells[1].value = NetAmount.toString();

    row = grid.rows.add();
    row.cells[0].value = "Total Amount: ";
    row.cells[1].value = TotalAmount.toString();

    grid.style = PdfGridStyle(
      font: PdfStandardFont(PdfFontFamily.timesRoman, 20),
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );
    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(page: page, bounds: Rect.fromLTWH(0, 260, 0, 0));
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoNWSCWaterpage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 60.0, left: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => NWSCWaterPage(
                                            coopList: coopList,
                                            userId: userId,
                                            accesstoken: accesstoken,
                                            balance: balance,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            primaryColor: primaryColor,
                                            loginButtonTitleColor:
                                                loginButtonTitleColor,
                                            loginbuttonColor: loginbuttonColor,
                                            loginTextFieldColor:
                                                loginTextFieldColor,
                                            dasboardIconColor:
                                                dasboardIconColor,
                                            dashboardTopTitleColor:
                                                dashboardTopTitleColor,
                                            SecondaryColor: SecondaryColor,
                                          )));
                            },
                            icon: const Icon(
                              Icons.arrow_back,
                              size: 30,
                              color: Colors.green,
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, top: 12),
                            child: Center(
                              child: Text(
                                "NWSC Bill Details",
                                style: TextStyle(
                                    color: Colors.green[900],
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 40),
                      child: Text(
                        "Customer Details",
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Name:",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 70,
                          ),
                          Text("${CustomerName}"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Address:",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 55,
                          ),
                          Text("${CustomerAddress}"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "CustomerId:",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 70,
                          ),
                          Text("${CustomerId.toString()}"),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      color: Colors.green[900],
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 40),
                      child: Text(
                        "Payment Details",
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Office: ",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 150,
                          ),
                          Text("${Office}"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Current Month Due: ",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 70,
                          ),
                          Text(CurrentMonthDues.toString()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Current Month Fine: ",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 68,
                          ),
                          Text(CurrentMonthFine.toString()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Current Month Discount: ",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 36,
                          ),
                          Text(CurrentMonthDiscount.toString()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Remaining Due: ",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 96,
                          ),
                          Text(RemainingDues.toString()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Net Amount: ",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 117,
                          ),
                          Text(NetAmount.toString()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0, top: 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Total Amount: ",
                            style:
                                TextStyle(color: Colors.black87, fontSize: 14),
                          ),
                          const SizedBox(
                            width: 107,
                          ),
                          Text(TotalAmount.toString()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 60, top: 40),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 50,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Color(
                                    int.parse(loginbuttonColor.toString())),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => NWSCWaterPage(
                                              coopList: coopList,
                                              userId: userId,
                                              accesstoken: accesstoken,
                                              balance: balance,
                                              baseUrl: baseUrl,
                                              primaryColor: primaryColor,
                                              loginButtonTitleColor:
                                                  loginButtonTitleColor,
                                              loginbuttonColor:
                                                  loginbuttonColor,
                                              loginTextFieldColor:
                                                  loginTextFieldColor,
                                              dasboardIconColor:
                                                  dasboardIconColor,
                                              dashboardTopTitleColor:
                                                  dashboardTopTitleColor,
                                              SecondaryColor: SecondaryColor,
                                            )));
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("back"),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 80,
                          ),
                          Container(
                            height: 50,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Color(
                                    int.parse(loginbuttonColor.toString())),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () {
                                if (showBiometricbuttonTransaction) {
                                  transactionPinMessageBiometric();
                                } else {
                                  transactionPinMessage();
                                }
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("pay"),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}
