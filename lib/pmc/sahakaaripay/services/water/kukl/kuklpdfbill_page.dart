import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import '../../../pmc_homepage.dart';
import '../../water/ubs_water/mobile.dart'
    if (dart.library.html) '../water/ubs_water/web.dart';

class KUKLWaterPdfBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? BillMonth;
  String? Address;
  String? AreaNumber;
  String? Penalty;
  String? CustomerName;
  String? Amount;
  String? CustomerCode;
  String? CustomerNo;
  String? BoardAmount;
  String? TransactionId;
  String? Office;
  String? OfficeCode;
  String? TotalAmount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  KUKLWaterPdfBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.BillMonth,
    this.Address,
    this.AreaNumber,
    this.Penalty,
    this.CustomerName,
    this.Amount,
    this.CustomerCode,
    this.CustomerNo,
    this.BoardAmount,
    this.TransactionId,
    this.Office,
    this.OfficeCode,
    this.TotalAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _KUKLWaterPdfBillPageState createState() => _KUKLWaterPdfBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        BillMonth,
        Address,
        AreaNumber,
        Penalty,
        CustomerName,
        Amount,
        CustomerCode,
        CustomerNo,
        BoardAmount,
        TransactionId,
        Office,
        OfficeCode,
        TotalAmount,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _KUKLWaterPdfBillPageState extends State<KUKLWaterPdfBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? BillMonth;
  String? Address;
  String? AreaNumber;
  String? Penalty;
  String? CustomerName;
  String? Amount;
  String? CustomerCode;
  String? CustomerNo;
  String? BoardAmount;
  String? TransactionId;
  String? Office;
  String? OfficeCode;
  String? TotalAmount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _KUKLWaterPdfBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.BillMonth,
    this.Address,
    this.AreaNumber,
    this.Penalty,
    this.CustomerName,
    this.Amount,
    this.CustomerCode,
    this.CustomerNo,
    this.BoardAmount,
    this.TransactionId,
    this.Office,
    this.OfficeCode,
    this.TotalAmount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
  }

  String? PayBillDate;

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'Customer Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );

    final grid = PdfGrid();
    grid.columns.add(count: 2);
    var row = grid.rows.add();
    row.cells[0].value = "Name:";
    row.cells[1].value = CustomerName.toString();

    row = grid.rows.add();
    row.cells[0].value = "Address:";
    row.cells[1].value = Address.toString();

    row = grid.rows.add();
    row.cells[0].value = "Customer Code: ";
    row.cells[1].value = "${CustomerCode.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Customer Number: ";
    row.cells[1].value = "${CustomerNo.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Area Number: ";
    row.cells[1].value = "${AreaNumber.toString()}";

    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));
    // drawGrid(page);
    page.graphics.drawString(
      'Payment Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
      bounds: Rect.fromLTWH(0, 250, 0, 0),
    );
    drawGrid2(page);

    List<int> bytes = await document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  void drawGrid2(page) {
    final grid = PdfGrid();
    grid.columns.add(count: 2);

    var row = grid.rows.add();
    row.cells[0].value = "Transaction Id:";
    row.cells[1].value = TransactionId.toString();

    row = grid.rows.add();
    row.cells[0].value = "Date: ";
    row.cells[1].value = "${timeforBill.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Counter: ";
    row.cells[1].value = Office;

    row = grid.rows.add();
    row.cells[0].value = "Amount: ";
    row.cells[1].value = Amount.toString();

    row = grid.rows.add();
    row.cells[0].value = "Penalty: ";
    row.cells[1].value = Penalty.toString();

    row = grid.rows.add();
    row.cells[0].value = "Total Amount: ";
    row.cells[1].value = TotalAmount.toString();

    grid.style = PdfGridStyle(
      font: PdfStandardFont(PdfFontFamily.timesRoman, 20),
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );
    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(page: page, bounds: Rect.fromLTWH(0, 310, 0, 0));
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  String? timeforBill = DateTime.now().year.toString() +
      "-" +
      DateTime.now().month.toString() +
      "-" +
      DateTime.now().day.toString() +
      " " +
      DateTime.now().hour.toString() +
      ":" +
      DateTime.now().minute.toString();

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoHomePage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "KUKL Bill Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "TransactionId:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${TransactionId}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Date:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 90,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${timeforBill.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "Customer Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Name:",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 95,
                      ),
                      Text("${CustomerName}"),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Address:",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 82,
                      ),
                      Text("${Address}"),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Customer Code:",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 35,
                      ),
                      Text("${CustomerCode.toString()}"),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Customer Number:",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Text("${CustomerNo.toString()}"),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Area Number:",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 55,
                      ),
                      Text("${AreaNumber.toString()}"),
                    ],
                  ),
                ),
                Divider(
                  thickness: 1,
                  color: Colors.green[900],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "Payment Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Counter: ",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Text("${Office}"),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Amount: ",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 50,
                      ),
                      Text(Amount.toString()),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Penalty: ",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 56,
                      ),
                      Text(Penalty.toString()),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Total Amount: ",
                        style:
                        TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 18,
                      ),
                      Text(TotalAmount.toString()),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Divider(
                  thickness: 1,
                  color: Colors.green[900],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 100,
                    ),
                    TextButton(
                        onPressed: () {
                          movetoHomePage();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("home"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                    const SizedBox(
                      width: 60,
                    ),
                    TextButton(
                        onPressed: createPDF,
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("pdf"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
