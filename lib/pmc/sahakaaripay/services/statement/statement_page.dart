import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/statement/statements.dart';

import '../../../utils/localizations.dart';
import '../../models/loading.dart';
import '../../pmc_homepage.dart';

class StatementPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  StatementPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _StatementPageState createState() => _StatementPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _StatementPageState extends State<StatementPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _StatementPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });
    this.getStatementData();
  }

  bool _isLoading = false;

  bool showBalance = false;

  List? ministatement;

  Future<String> getStatementData() async {
    String url = "${baseUrl}api/v1/pes/account/${accountno}/mini-statement";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    setState(() {
      _isLoading = true;
    });
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            ministatement = jsonResponse["MiniStatements"];
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          toolbarHeight: 110,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
            onPressed: () {
              return movetohomepage();
            },
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 40, top: 20),
                child: Text(
                  AppLocalizations.of(context)!
                      .localizedString("mini_statements"),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Row(
                  children: [
                    const SizedBox(
                      height: 30,
                      width: 20,
                      child: Image(
                          image: AssetImage("assets/images/wallet_icon.png")),
                    ),
                    const SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      showBalance
                          ? AppLocalizations.of(context)!
                                  .localizedString("rs") +
                              "${balance.toString()}"
                          : "xxx.xx".toUpperCase(),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 150.0),
                      child: IconButton(
                          onPressed: () {
                            setState(() {
                              if (showBalance == false) {
                                showBalance = true;
                              } else {
                                showBalance = false;
                              }
                            });
                          },
                          icon: Icon(
                            showBalance
                                ? Icons.visibility
                                : Icons.visibility_off,
                            size: 25.0,
                            color: Colors.white,
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        body: _isLoading
            ? Loading()
            : ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: ministatement == null ? 0 : ministatement?.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding:
                        const EdgeInsets.only(left: 30.0, right: 20, top: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Statements(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    accountNumber: ministatement?[index]
                                        ["AccountNumber"],
                                    transdate: ministatement?[index]
                                        ["TranDate"],
                                    transtype: ministatement?[index]["Trans"],
                                    amount: ministatement?[index]["Amount"],
                                    statement: ministatement?[index]
                                        ["Statement"],
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                        loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor: loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                        dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  ),
                                ));
                          },
                          child: Center(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Icon(Icons.calendar_today,
                                    size: 16, color: Colors.green[900]),
                                SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  ministatement?[index]["TranDate"],
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  width: 50,
                                ),
                                Text(
                                  ministatement?[index]["Trans"],
                                  style: TextStyle(
                                      color: Colors.black87,
                                      // ministatement?[index]["Trans"]
                                      //             .toString() ==
                                      //         "Dr"
                                      //     ? Colors.red
                                      //     : Colors.green,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(
                                  width: 40,
                                ),
                                Text(
                                  "Rs. ${ministatement?[index]["Amount"].toString()}",
                                  style: TextStyle(
                                      color: ministatement?[index]["Trans"]
                                                  .toString() ==
                                              "Dr"
                                          ? Colors.red
                                          : Colors.green,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Icon(
                                  ministatement?[index]["Trans"].toString() ==
                                          "Dr"
                                      ? Icons.arrow_upward
                                      : Icons.arrow_downward,
                                  size: 22,
                                  color: ministatement?[index]["Trans"]
                                              .toString() ==
                                          "Dr"
                                      ? Colors.red
                                      : Colors.green,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        const Divider(
                          thickness: 1,
                          color: Colors.green,
                        ),
                      ],
                    ),
                  );
                },
              ),
      ),
    );
  }
}
