import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/twoway/twowayflightdetail_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/twoway/twowayflightlist_page.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';

class TwoWayFlightArrivalListPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? TwowayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  List? TwowayFlightList2;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TwoWayFlightArrivalListPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TwowayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.TwowayFlightList2,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _TwoWayFlightArrivalListPageState createState() =>
      _TwoWayFlightArrivalListPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        TwowayFlightList,
        airlineName,
        flightdatetime,
        flightdeparturetime,
        flightdeparturefrom,
        flightarrivaltime,
        flightarrivalto,
        adultno,
        childno,
        baggage,
        adultfare,
        childfare,
        tax,
        totalamount,
        TwowayFlightList2,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TwoWayFlightArrivalListPageState
    extends State<TwoWayFlightArrivalListPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? TwowayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  List? TwowayFlightList2;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _TwoWayFlightArrivalListPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TwowayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.TwowayFlightList2,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetoFlightPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => TwoWayFlightListPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  TwowayFlightList: TwowayFlightList,
                  TwowayFlightList2: TwowayFlightList2,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return movetoFlightPage();
      },
      child: Scaffold(
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    color: Color(int.parse(primaryColor.toString())),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TwoWayFlightListPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          TwowayFlightList: TwowayFlightList,
                                          TwowayFlightList2: TwowayFlightList2,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30,
                          )),
                    ),
                    const Padding(
                        padding: EdgeInsets.only(left: 40.0),
                        child: Text("TwoWay Flight List",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)))
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                  height: 40,
                  width: 600,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(10)),
                  child: const Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 8),
                    child: Text(
                      "Arrival Flights",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  )),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount:
                      TwowayFlightList2 == null ? 0 : TwowayFlightList2?.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Center(
                                      child: Column(
                                    children: const [
                                      Text("Arrival Flight"),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Icon(
                                        Icons.add_alert,
                                        size: 30,
                                        color: Colors.red,
                                      ),
                                    ],
                                  )),
                                  content: const Text(
                                      "Are you sure about this flight? Click back if you want to select Departure Flight again. If not then click Yes."),
                                  actions: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        TextButton(
                                            onPressed: () {
                                              Navigator.pushReplacement(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          TwoWayFlightListPage(
                                                            coopList: coopList,
                                                            userId: userId,
                                                            accesstoken:
                                                                accesstoken,
                                                            balance: balance,
                                                            baseUrl: baseUrl,
                                                            accountno:
                                                                accountno,
                                                            TwowayFlightList:
                                                                TwowayFlightList,
                                                            TwowayFlightList2:
                                                                TwowayFlightList2,
                                                            primaryColor:
                                                                primaryColor,
                                                            loginButtonTitleColor:
                                                                loginButtonTitleColor,
                                                            loginbuttonColor:
                                                                loginbuttonColor,
                                                            loginTextFieldColor:
                                                                loginTextFieldColor,
                                                            dasboardIconColor:
                                                                dasboardIconColor,
                                                            dashboardTopTitleColor:
                                                                dashboardTopTitleColor,
                                                            SecondaryColor:
                                                                SecondaryColor,
                                                          )));
                                            },
                                            child: Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("back"),
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.green[900]),
                                            )),
                                        const SizedBox(
                                          width: 80,
                                        ),
                                        TextButton(
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          TwoWayFlightDetailPage(
                                                            coopList: coopList,
                                                            userId: userId,
                                                            accesstoken:
                                                                accesstoken,
                                                            balance: balance,
                                                            baseUrl: baseUrl,
                                                            accountno:
                                                                accountno,
                                                            TwowayFlightList:
                                                                TwowayFlightList,
                                                            airlineName:
                                                                airlineName,
                                                            flightdatetime:
                                                                flightdatetime,
                                                            flightdeparturetime:
                                                                flightdeparturetime,
                                                            flightdeparturefrom:
                                                                flightdeparturefrom,
                                                            flightarrivaltime:
                                                                flightarrivaltime,
                                                            flightarrivalto:
                                                                flightarrivalto,
                                                            adultno: adultno,
                                                            childno: childno,
                                                            baggage: baggage
                                                                .toString(),
                                                            adultfare: adultfare
                                                                .toString(),
                                                            childfare: childfare
                                                                .toString(),
                                                            tax: tax.toString(),
                                                            totalamount:
                                                                totalamount
                                                                    .toString(),
                                                            TwowayFlightList2:
                                                                TwowayFlightList2,
                                                            airlineName2:
                                                                TwowayFlightList2?[
                                                                        index][
                                                                    "AirlineName"],
                                                            flightdatetime2:
                                                                TwowayFlightList2?[
                                                                        index][
                                                                    "FlightDate"],
                                                            flightdeparturetime2:
                                                                TwowayFlightList2?[
                                                                        index][
                                                                    "DepartureTime"],
                                                            flightdeparturefrom2:
                                                                TwowayFlightList2?[
                                                                        index][
                                                                    "Departure"],
                                                            flightarrivaltime2:
                                                                TwowayFlightList2?[
                                                                        index][
                                                                    "ArrivalTime"],
                                                            flightarrivalto2:
                                                                TwowayFlightList2?[
                                                                        index]
                                                                    ["Arrival"],
                                                            primaryColor:
                                                                primaryColor,
                                                            loginButtonTitleColor:
                                                                loginButtonTitleColor,
                                                            loginbuttonColor:
                                                                loginbuttonColor,
                                                            loginTextFieldColor:
                                                                loginTextFieldColor,
                                                            dasboardIconColor:
                                                                dasboardIconColor,
                                                            dashboardTopTitleColor:
                                                                dashboardTopTitleColor,
                                                            SecondaryColor:
                                                                SecondaryColor,
                                                          )));
                                            },
                                            child: Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("yes"),
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.green[900]),
                                            ))
                                      ],
                                    )
                                  ],
                                );
                              });
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10, bottom: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                                padding:
                                    const EdgeInsets.only(left: 10.0, top: 10),
                                child: Container(
                                  height: 160,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(0, 3),
                                      )
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  image: const DecorationImage(
                                                      image: AssetImage(
                                                          "assets/images/flight/ic_flight.webp"))),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 20),
                                            child: Text(
                                              TwowayFlightList2?[index]
                                                  ["AirlineName"],
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 40, top: 10.0),
                                            child: Container(
                                              height: 60,
                                              width: 180,
                                              decoration: const BoxDecoration(
                                                  color: Colors.white),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "NRP: ${TwowayFlightList2?[index]["TotalAmount"].toStringAsFixed(1)}",
                                                    style: const TextStyle(
                                                        color: Colors.green),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 10.0),
                                                    child: Text(
                                                      "CashBack: Rs. ${TwowayFlightList2?[index]["CashBack"].toStringAsFixed(1)}",
                                                      style: const TextStyle(
                                                          color: Colors.green),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const Divider(
                                        thickness: 1,
                                        color: Colors.black,
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Text(
                                                "${TwowayFlightList2?[index]["FlightDate"]}"),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${TwowayFlightList2?[index]["DepartureTime"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${TwowayFlightList2?[index]["Departure"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${TwowayFlightList2?[index]["ArrivalTime"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${TwowayFlightList2?[index]["Arrival"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${TwowayFlightList2?[index]["Refundable"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${TwowayFlightList2?[index]["FlightClassCode"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                )),
                            const SizedBox(
                              height: 20,
                            ),
                            const Divider(
                              thickness: 1,
                              color: Colors.red,
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
