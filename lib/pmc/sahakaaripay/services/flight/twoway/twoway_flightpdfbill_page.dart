import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:sahakari_pay/pmc/utils/localizations.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

import '../../../../constants.dart';
import '../../../pmc_homepage.dart';
import '../../water/ubs_water/mobile.dart'
    if (dart.library.html) '../../water/ubs_water/web.dart';

class TwoWayFlightPdfBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? Amount;
  String? ContactPerson;
  String? ContactNo;
  String? ContactEmail;
  String? CustomerAccountNo;
  String? Adult;
  List? AdultPassengers;
  String? Child;
  List? ChildPassengers;
  String? paymentmessage;
  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TwoWayFlightPdfBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.Amount,
    this.ContactPerson,
    this.ContactNo,
    this.ContactEmail,
    this.CustomerAccountNo,
    this.Adult,
    this.AdultPassengers,
    this.Child,
    this.ChildPassengers,
    this.paymentmessage,
    // this.renewalplans,
    // this.planName,
    this.ReferenceId,
    this.TransactionId,
    this.ReceiptNo,
    this.PaymentBillDate,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _TwoWayFlightPdfBillPageState createState() => _TwoWayFlightPdfBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        Amount,
        ContactPerson,
        ContactNo,
        ContactEmail,
        CustomerAccountNo,
        Adult,
        AdultPassengers,
        Child,
        ChildPassengers,
        paymentmessage,
        // renewalplans,
        // planName,
        ReferenceId,
        TransactionId,
        ReceiptNo,
        PaymentBillDate,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TwoWayFlightPdfBillPageState extends State<TwoWayFlightPdfBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? Amount;
  String? ContactPerson;
  String? ContactNo;
  String? ContactEmail;
  String? CustomerAccountNo;
  String? Adult;
  List? AdultPassengers;
  String? Child;
  List? ChildPassengers;
  String? paymentmessage;
  // List? renewalplans;
  // String? planName;
  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _TwoWayFlightPdfBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.Amount,
    this.ContactPerson,
    this.ContactNo,
    this.ContactEmail,
    this.CustomerAccountNo,
    this.Adult,
    this.AdultPassengers,
    this.Child,
    this.ChildPassengers,
    this.paymentmessage,
    // this.renewalplans,
    // this.planName,
    this.ReferenceId,
    this.TransactionId,
    this.ReceiptNo,
    this.PaymentBillDate,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    this.nullcheck();
  }

  String? Rid;
  String? Tid;
  String? Rno;
  String? PayBillDate;

  nullcheck() {
    if (ReferenceId == null) {
      Rid = "ReferenceID not available.";
    } else {
      Rid = ReferenceId;
    }
    if (TransactionId == null) {
      Tid = "TransactionId not available.";
    } else {
      Tid = TransactionId;
    }
    if (ReceiptNo == null) {
      Rno = "ReceiptNo not available.";
    } else {
      Rno = ReceiptNo.toString();
    }
    if (PaymentBillDate == null) {
      PayBillDate = "Date not available.";
    } else {
      PayBillDate = PaymentBillDate.toString();
    }
  }

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'Two Way Flight Bill Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );

    final grid = PdfGrid();
    grid.columns.add(count: 2);
    var row = grid.rows.add();

    for (var item in twoWayTicketList!) {
      var row = grid.rows.add();
      row.cells[0].value = "TransactionId:";
      row.cells[1].value = Tid.toString();

      row = grid.rows.add();
      row.cells[0].value = "AirlineCode: ";
      row.cells[1].value = "${item["ArlineCode"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "AirlineName: ";
      row.cells[1].value = "${item["AirlineName"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "PnrNo: ";
      row.cells[1].value = "${item["PnrNo"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Gender: ";
      row.cells[1].value = "${item["Gender"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Name: ";
      row.cells[1].value = "${item["Name"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "PaxType: ";
      row.cells[1].value = "${item["PaxType"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Nationality: ";
      row.cells[1].value = "${item["Nationality"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "IssueDate: ";
      row.cells[1].value = "${item["IssueDate"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "FlightNo: ";
      row.cells[1].value = "${item["FlightNo"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "FlightDate: ";
      row.cells[1].value = "${item["FlightDate"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Departure: ";
      row.cells[1].value = "${item["Departure"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "FlightTime: ";
      row.cells[1].value = "${item["FlightTime"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "TicketNo: ";
      row.cells[1].value = "${item["TicketNo"].toString()}";

      row = grid.rows.add();
      row.cells[0].value = "Arrival: ";
      row.cells[1].value = "${item["Arrival"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "ArrivalTime: ";
      row.cells[1].value = "${item["ArrivalTime"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "Sector: ";
      row.cells[1].value = "${item["Sector"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "ClassCode: ";
      row.cells[1].value = "${item["ClassCode"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "Currency: ";
      row.cells[1].value = "${item["Currency"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "Fare: ";
      row.cells[1].value = "${item["Fare"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "Surcharge: ";
      row.cells[1].value = "${item["Surcharge"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "Tax: ";
      row.cells[1].value = "${item["Tax"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "Commission: ";
      row.cells[1].value = "${item["Commission"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "Refundable: ";
      row.cells[1].value = "${item["Refundable"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "ReportingTime: ";
      row.cells[1].value = "${item["ReportingTime"].toString()}";
      row = grid.rows.add();
      row.cells[0].value = "FreeBaggage: ";
      row.cells[1].value = "${item["FreeBaggage"].toString()}";
    }



    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));

    List<int> bytes = document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoHomePage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "Two Way Flight Bill Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(height: 20,),
                Container(
                  height: 600,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                    itemCount: twoWayTicketList == null ? 0 : twoWayTicketList?.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "TransactionId:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 50,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${Tid}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "AirlineCode:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 65,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"]}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "AirlineName:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 65,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"]}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "PnrNo:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 102,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"]}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Gender:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 98,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"]}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Name:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 107,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"]}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "PaxType:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 90,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"]}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Nationality:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 75,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "IssueDate:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 77,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "FlightNo:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 90,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "FlightDate:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 82,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Departure:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 87,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "FlightTime:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 84,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "TicketNo:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 95,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Arrival:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 117,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "ArrivalTime:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 82,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Sector:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 116,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "ClassCode:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 87,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Currency:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 100,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Fare:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 132,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Surcharge:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 93,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Tax:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 140,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Commission:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 79,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Refundable:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 88,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "ReportingTime:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 67,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 30),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "FreeBaggage:",
                                  style: TextStyle(color: Colors.black87, fontSize: 14),
                                ),
                                const SizedBox(
                                  width: 78,
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width / 2,
                                    child: Text("${twoWayTicketList?[index]["AirlineCode"].toString()}")),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Divider(
                            thickness: 1,
                            color: Colors.green[700],
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "AirlineCode:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 65,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${AirlineCode2}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "AirlineName:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 65,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${AirlineName2}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "PnrNo:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 102,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${PnrNo2}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Gender:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 98,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Gender2}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Name:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 107,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Name2}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "PaxType:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 90,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${PaxType2}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Nationality:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 75,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Nationality2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "IssueDate:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 77,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${IssueDate2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "FlightNo:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 90,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${FlightNo2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "FlightDate:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 82,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${FlightDate2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Departure:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 87,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Departure2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "FlightTime:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 84,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${FlightTime2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "TicketNo:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 95,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${TicketNo2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Arrival:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 117,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Arrival2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "ArrivalTime:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 82,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${ArrivalTime2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Sector:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 116,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Sector2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "ClassCode:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 87,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${ClassCode2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Currency:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 100,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Currency2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Fare:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 132,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Fare2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Surcharge:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 93,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Surcharge2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Tax:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 140,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Tax2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Commission:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 79,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Commission2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "Refundable:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 88,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${Refundable2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "ReportingTime:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 67,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${ReportingTime2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Padding(
                          //   padding: const EdgeInsets.only(left: 20.0, top: 30),
                          //   child: Row(
                          //     crossAxisAlignment: CrossAxisAlignment.start,
                          //     children: [
                          //       const Text(
                          //         "FreeBaggage:",
                          //         style: TextStyle(color: Colors.black87, fontSize: 14),
                          //       ),
                          //       const SizedBox(
                          //         width: 78,
                          //       ),
                          //       Container(
                          //           width: MediaQuery.of(context).size.width / 2,
                          //           child: Text("${FreeBaggage2.toString()}")),
                          //     ],
                          //   ),
                          // ),
                          // Divider(
                          //   thickness: 1,
                          //   color: Colors.green[700],
                          // ),
                        ],
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 100,
                    ),
                    TextButton(
                        onPressed: () {
                          movetoHomePage();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("home"),
                          style:
                          TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                    const SizedBox(
                      width: 60,
                    ),
                    TextButton(
                        onPressed: createPDF,
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("pdf"),
                          style:
                          TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
