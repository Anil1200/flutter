import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/twoway/twowayflightlist_page.dart';

import '../../../../constants.dart';
import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';

class TwoWayPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TwoWayPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<TwoWayPage> createState() => _TwoWayPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TwoWayPageState extends State<TwoWayPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _TwoWayPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  String? dropdownValuefrom;

  String? dropdownValueto;

  String dropdownValueadult = '1';

  String dropdownValuechild = '0';

  String? dropdownValue2 = "Nepalese";

  DateTime? _dateTime;
  DateTime? _dateTime1;
  bool _isLoading = false;

  String? nationalityCode = "NP";

  List<String?> nationality = [
    'Nepalese',
    'Indian',
  ];

  // List? flightlocationList;

  // List? flightlocationList;
  // List? locationList;

  @override
  void initState() {
    super.initState();
    // this.getJsonData();
    _dateTime1 = DateTime.now();
    _dateTime = DateTime.now();
  }

  // Future<String> getJsonData() async {
  //   String url = "https://merchant.sahakaari.com/merchant/api/v1/getFlightList";
  //   // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  //   final response = await http.get(Uri.parse(url), headers: {
  //     "Token": "${accesstoken}",
  //     "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP",
  //     'Authorization': "Bearer ${accesstoken}",
  //   });
  //   if (response.statusCode == 200) {
  //     setState(() {
  //       _isLoading = false;
  //       var convertDataToJson = json.decode(response.body);
  //       flightlocationList = convertDataToJson["payload"];
  //       locationList = convertDataToJson["payload"];
  //
  //       print(
  //           "the foirst location of flight: ${flightlocationList?[0]["sectorName"]}");
  //       print("This is the flight location list: ${flightlocationList}");
  //
  //       // CooperativeSharedPreferences.setsmsCode(
  //       //     convertDataToJson["payload"][0]["smsCode"].toString());
  //     });
  //     return "success";
  //   } else {
  //     _isLoading = false;
  //     showDialog(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return AlertDialog(
  //             title: Text(
  //               AppLocalizations.of(context)!.localizedString("alert"),
  //             ),
  //             content:
  //                 const Text("Server down please try again after sometime"),
  //             actions: [
  //               TextButton(
  //                   onPressed: () {
  //                     Navigator.of(context).pop();
  //                   },
  //                   child: const Text("OKAY"))
  //             ],
  //           );
  //         });
  //   }
  //   return "success";
  // }

  List? TwowayFlightList;
  List? TwowayFlightList2;

  showMessage(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: const Text(
                  "No Internet, Please check your internet connection"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else if (dropdownValuefrom == null || dropdownValueto == null) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content:
                  const Text("Please select origin and destination place."),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      Twowayflight();
    }
  }

  Twowayflight() async {
    String url =
        "${baseUrl}api/v1/pes/users/4cbb63d8-cf43-e911-80be-00505689deb9/domestic-search-flight";
    Map body = {
      "child": dropdownValuechild,
      "SectorTo": dropdownValueto.toString(),
      "adult": dropdownValueadult,
      "SectorFrom": dropdownValuefrom.toString(),
      "departureDate": _dateTime.toString(),
      "flightMode": "2",
      "returnDate": _dateTime1.toString(),
      "Nationality": "NP",
    };
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      // print("Response status: ${jsonResponse}");

      if (jsonResponse != null) {
        setState(() {
          _isLoading = true;
          TwowayFlightList = jsonResponse["Outbound"];
          TwowayFlightList2 = jsonResponse["Inbound"];
          print("This is the two way flight list : ${TwowayFlightList}");

          print(
              "This is the two way flight list of in bound : ${TwowayFlightList}");
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => TwoWayFlightListPage(
                        coopList: coopList,
                        userId: userId,
                        accesstoken: accesstoken,
                        balance: balance,
                        baseUrl: baseUrl,
                        accountno: accountno,
                        TwowayFlightList: TwowayFlightList,
                        TwowayFlightList2: TwowayFlightList2,
                        primaryColor: primaryColor,
                        loginButtonTitleColor: loginButtonTitleColor,
                        loginbuttonColor: loginbuttonColor,
                        loginTextFieldColor: loginTextFieldColor,
                        dasboardIconColor: dasboardIconColor,
                        dashboardTopTitleColor: dashboardTopTitleColor,
                        SecondaryColor: SecondaryColor,
                      )));
        });
      }
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        return showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(
                    "${jsonResponse["Message"]} Please Login again and try again."),
                actions: [
                  // Spacer(),
                  Container(
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        "OKAY",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )
                ],
              );
            });
      }
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        return showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(jsonResponse["Message"]),
                actions: [
                  // Spacer(),
                  Container(
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text(
                        "OKAY",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )
                ],
              );
            });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? Center(
            child: Column(
            children: const [
              SizedBox(
                height: 150,
              ),
              Loading(),
            ],
          ))
        : Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20.0),
            child: SingleChildScrollView(
                child: Center(
              child: Padding(
                padding:
                    const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("from"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValuefrom,
                                  alignment: AlignmentDirectional.center,
                                  hint: Text(
                                    "Choose Here",
                                    style: TextStyle(
                                        color: Colors.black87, fontSize: 14),
                                  ),
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValuefrom = newValue!;
                                      print(
                                          "this is arrival location: ${dropdownValuefrom}");
                                    });
                                  },
                                  items: FlightList1?.map((item) {
                                    return DropdownMenuItem<String>(
                                        value: item["sectorCode"],
                                        child: Text(item["sectorName"]));
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("to"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValueto,
                                  alignment: AlignmentDirectional.center,
                                  hint: Text(
                                    "Choose Here",
                                    style: TextStyle(
                                        color: Colors.black87, fontSize: 14),
                                  ),
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValueto = newValue!;
                                      print(
                                          "this is the arrival code: ${dropdownValueto}");
                                    });
                                  },
                                  items: FlightList2?.map((items) {
                                    return DropdownMenuItem<String>(
                                        value: items["sectorCode"],
                                        child: Text(items["sectorName"]));
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextButton(
                          child: Column(
                            children: [
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("departure_date"),
                                style: TextStyle(color: Colors.black87),
                              ),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                _dateTime == null
                                    ? "${DateTime.now().year}- ${DateTime.now().month} - ${DateTime.now().day}"
                                    : " ${_dateTime!.year} - ${_dateTime!.month} - ${_dateTime!.day}",
                                style: const TextStyle(
                                  color: Colors.black87,
                                ),
                              ),
                            ],
                          ),
                          onPressed: () {
                            showDatePicker(
                                    locale: Locale(DEFAULT_LOCALE),
                                    context: context,
                                    initialDate: _dateTime == null
                                        ? DateTime.now()
                                        : _dateTime!,
                                    firstDate: DateTime(2018),
                                    lastDate: DateTime(2050))
                                .then((date) {
                              setState(() {
                                _dateTime = date;
                              });
                            });
                          },
                        ),
                        Spacer(),
                        TextButton(
                          child: Column(
                            children: [
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("return_date"),
                                style: TextStyle(color: Colors.black87),
                              ),
                              const SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                _dateTime1 == null
                                    ? "${DateTime.now().year}- ${DateTime.now().month} - ${DateTime.now().day}"
                                    : " ${_dateTime1!.year} - ${_dateTime1!.month} - ${_dateTime1!.day}",
                                style: const TextStyle(
                                  color: Colors.black87,
                                ),
                              ),
                            ],
                          ),
                          onPressed: () {
                            showDatePicker(
                              locale: Locale(DEFAULT_LOCALE),
                              context: context,
                              initialDate: _dateTime1 == null
                                  ? DateTime.now()
                                  : _dateTime1!,
                              firstDate: DateTime(2018),
                              lastDate: DateTime(2050),
                              // builder: (context, child) => Theme(
                              //   data: Theme.of(context).copyWith(
                              //       colorScheme: const ColorScheme.dark(
                              //         primary: Colors.red,
                              //         surface: Colors.green,
                              //         onBackground: Colors.blue,
                              //       ),
                              //       dialogBackgroundColor: Colors.black87),
                              //   child: child ?? Text(""),
                              // ),
                            ).then((date) {
                              setState(() {
                                _dateTime1 = date;
                              });
                            });
                          },
                        ),
                      ],
                    ),
                    const Divider(
                      thickness: 1,
                      color: Colors.red,
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("passenger_onboarding"),
                      style: TextStyle(color: Colors.black87, fontSize: 16),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("adult"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValueadult,
                                  alignment: AlignmentDirectional.center,
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValueadult = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    '1',
                                    '2',
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8',
                                    '9',
                                    '10'
                                  ].map<DropdownMenuItem<String>>(
                                      (String? value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value!,
                                        style: TextStyle(color: Colors.black87),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("children"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValuechild,
                                  alignment: AlignmentDirectional.center,
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValuechild = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    '0',
                                    '1',
                                    '2',
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8',
                                    '9',
                                    '10'
                                  ].map<DropdownMenuItem<String>>(
                                      (String? value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value!,
                                        style: const TextStyle(
                                            color: Colors.black87),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("nationality"),
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      height: 50,
                      width: 150,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 4),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          isExpanded: true,
                          value: dropdownValue2,
                          alignment: AlignmentDirectional.center,
                          // itemHeight: 90,
                          borderRadius: BorderRadius.circular(20.0),
                          icon: const Icon(
                            Icons.arrow_drop_down,
                            color: Colors.black87,
                          ),
                          iconSize: 34,
                          elevation: 16,
                          style: const TextStyle(
                              color: Colors.black87,
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                          onChanged: (nationality) {
                            if (nationality == 'Nepalese') {
                              nationalityCode = 'NP';
                            } else if (nationality == 'Indian') {
                              nationalityCode = 'INR';
                            }
                            setState(() {
                              // dropdownValue2 = "Choose Nationality";
                              dropdownValue2 = nationality.toString();
                            });
                          },
                          items: nationality.map((String? newValue) {
                            return DropdownMenuItem<String>(
                                value: newValue, child: Text(newValue!));
                          }).toList(),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 230.0),
                      child: InkWell(
                        onTap: () async {
                          setState(() {
                            _isLoading = true;
                          });
                          final result =
                              await Connectivity().checkConnectivity();
                          showMessage(result);
                        },
                        child: Container(
                          height: 40,
                          width: 100,
                          decoration: BoxDecoration(
                              color:
                                  Color(int.parse(loginbuttonColor.toString())),
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Center(
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("search"),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )),
          );
  }
}
