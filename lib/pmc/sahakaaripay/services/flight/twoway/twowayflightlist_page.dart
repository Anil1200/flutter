import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/flight_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/twoway/twowayflightlistarrival_page.dart';

import '../../../../utils/localizations.dart';

class TwoWayFlightListPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? TwowayFlightList;
  List? TwowayFlightList2;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TwoWayFlightListPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TwowayFlightList,
    this.TwowayFlightList2,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _TwoWayFlightListPageState createState() => _TwoWayFlightListPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        TwowayFlightList,
        TwowayFlightList2,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TwoWayFlightListPageState extends State<TwoWayFlightListPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? TwowayFlightList;
  List? TwowayFlightList2;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _TwoWayFlightListPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TwowayFlightList,
    this.TwowayFlightList2,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetoFlightPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => FlightPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return movetoFlightPage();
      },
      child: Scaffold(
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    color: Color(int.parse(primaryColor.toString())),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FlightPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30,
                          )),
                    ),
                    const Padding(
                        padding: EdgeInsets.only(left: 40.0),
                        child: Text("TwoWay Flight List",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)))
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                  height: 40,
                  width: 600,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(10)),
                  child: const Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 8),
                    child: Text(
                      "Departure Flights",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  )),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount:
                      TwowayFlightList == null ? 0 : TwowayFlightList?.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        setState(() {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Center(
                                      child: Column(
                                    children: const [
                                      Text("Departure Flight"),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Icon(
                                        Icons.add_alert,
                                        size: 30,
                                        color: Colors.red,
                                      ),
                                    ],
                                  )),
                                  content: const Text(
                                      "Are you sure about this flight? Click Yes to select Arrival Flight."),
                                  actions: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("cancel"),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.green[900]),
                                            )),
                                        const SizedBox(
                                          width: 80,
                                        ),
                                        TextButton(
                                            onPressed: () {
                                              OutboundId =
                                                  TwowayFlightList?[index]
                                                      ["FlightId"];
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          TwoWayFlightArrivalListPage(
                                                            coopList: coopList,
                                                            userId: userId,
                                                            accesstoken:
                                                                accesstoken,
                                                            balance: balance,
                                                            baseUrl: baseUrl,
                                                            accountno:
                                                                accountno,
                                                            TwowayFlightList:
                                                                TwowayFlightList,
                                                            airlineName:
                                                                TwowayFlightList?[
                                                                        index][
                                                                    "AirlineName"],
                                                            flightdatetime:
                                                                TwowayFlightList?[
                                                                        index][
                                                                    "FlightDate"],
                                                            //     .toString(),
                                                            flightdeparturetime:
                                                                TwowayFlightList?[
                                                                        index][
                                                                    "DepartureTime"],
                                                            flightdeparturefrom:
                                                                TwowayFlightList?[
                                                                        index][
                                                                    "Departure"],
                                                            flightarrivaltime:
                                                                TwowayFlightList?[
                                                                        index][
                                                                    "ArrivalTime"],
                                                            flightarrivalto:
                                                                TwowayFlightList?[
                                                                        index]
                                                                    ["Arrival"],
                                                            adultno:
                                                                TwowayFlightList?[
                                                                            index]
                                                                        [
                                                                        "Adult"]
                                                                    .toString(),
                                                            childno:
                                                                TwowayFlightList?[
                                                                            index]
                                                                        [
                                                                        "Child"]
                                                                    .toString(),
                                                            baggage:
                                                                TwowayFlightList?[
                                                                        index][
                                                                    "FreeBaggage"],
                                                            adultfare: TwowayFlightList?[
                                                                        index][
                                                                    "AdultFare"]
                                                                .toStringAsFixed(
                                                                    1),
                                                            childfare: TwowayFlightList?[
                                                                        index][
                                                                    "ChildFare"]
                                                                .toStringAsFixed(
                                                                    1),
                                                            tax: TwowayFlightList?[
                                                                        index]
                                                                    ["Tax"]
                                                                .toStringAsFixed(
                                                                    1),
                                                            totalamount: TwowayFlightList?[
                                                                        index][
                                                                    "TotalAmount"]
                                                                .toStringAsFixed(
                                                                    1),
                                                            TwowayFlightList2:
                                                                TwowayFlightList2,
                                                            primaryColor:
                                                                primaryColor,
                                                            loginButtonTitleColor:
                                                                loginButtonTitleColor,
                                                            loginbuttonColor:
                                                                loginbuttonColor,
                                                            loginTextFieldColor:
                                                                loginTextFieldColor,
                                                            dasboardIconColor:
                                                                dasboardIconColor,
                                                            dashboardTopTitleColor:
                                                                dashboardTopTitleColor,
                                                            SecondaryColor:
                                                                SecondaryColor,
                                                          )));
                                            },
                                            child: Text(
                                              AppLocalizations.of(context)!
                                                  .localizedString("yes"),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.green[900]),
                                            ))
                                      ],
                                    )
                                  ],
                                );
                              });
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10, bottom: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                                padding:
                                    const EdgeInsets.only(left: 10.0, top: 10),
                                child: Container(
                                  height: 160,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(0, 3),
                                      )
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Container(
                                              height: 40,
                                              width: 40,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  image: const DecorationImage(
                                                      image: AssetImage(
                                                          "assets/images/flight/ic_flight.webp"))),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 20),
                                            child: Text(
                                              TwowayFlightList?[index]
                                                  ["AirlineName"],
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 40, top: 10.0),
                                            child: Container(
                                              height: 60,
                                              width: 180,
                                              decoration: const BoxDecoration(
                                                  color: Colors.white),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "NRP: ${TwowayFlightList?[index]["TotalAmount"].toStringAsFixed(1)}",
                                                    style: const TextStyle(
                                                        color: Colors.green),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 10.0),
                                                    child: Text(
                                                      "CashBack: Rs. ${TwowayFlightList?[index]["CashBack"].toStringAsFixed(1)}",
                                                      style: const TextStyle(
                                                          color: Colors.green),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const Divider(
                                        thickness: 1,
                                        color: Colors.black,
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Text(
                                                "${TwowayFlightList?[index]["FlightDate"]}"),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${TwowayFlightList?[index]["DepartureTime"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${TwowayFlightList?[index]["Departure"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${TwowayFlightList?[index]["ArrivalTime"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${TwowayFlightList?[index]["Arrival"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${TwowayFlightList?[index]["Refundable"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${TwowayFlightList?[index]["FlightClassCode"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                )),
                            const SizedBox(
                              height: 20,
                            ),
                            const Divider(
                              thickness: 1,
                              color: Colors.red,
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
