import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/twoway/confirmflight_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/twoway/twowayflightlist_page.dart';

class TwoWayFlightDetailPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? TwowayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  List? TwowayFlightList2;
  String? airlineName2;
  String? flightdatetime2;
  String? flightdeparturetime2;
  String? flightdeparturefrom2;
  String? flightarrivaltime2;
  String? flightarrivalto2;
  String? adultno2;
  String? childno2;
  String? baggage2;
  String? adultfare2;
  String? childfare2;
  String? tax2;
  String? totalamount2;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TwoWayFlightDetailPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TwowayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.TwowayFlightList2,
    this.airlineName2,
    this.flightdatetime2,
    this.flightdeparturetime2,
    this.flightdeparturefrom2,
    this.flightarrivaltime2,
    this.flightarrivalto2,
    this.adultno2,
    this.childno2,
    this.baggage2,
    this.adultfare2,
    this.childfare2,
    this.tax2,
    this.totalamount2,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _TwoWayFlightDetailPageState createState() => _TwoWayFlightDetailPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        TwowayFlightList,
        airlineName,
        flightdatetime,
        flightdeparturetime,
        flightdeparturefrom,
        flightarrivaltime,
        flightarrivalto,
        adultno,
        childno,
        baggage,
        adultfare,
        childfare,
        tax,
        totalamount,
        TwowayFlightList2,
        airlineName2,
        flightdatetime2,
        flightdeparturetime2,
        flightdeparturefrom2,
        flightarrivaltime2,
        flightarrivalto2,
        adultno2,
        childno2,
        baggage2,
        adultfare2,
        childfare2,
        tax2,
        totalamount2,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TwoWayFlightDetailPageState extends State<TwoWayFlightDetailPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? TwowayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  List? TwowayFlightList2;
  String? airlineName2;
  String? flightdatetime2;
  String? flightdeparturetime2;
  String? flightdeparturefrom2;
  String? flightarrivaltime2;
  String? flightarrivalto2;
  String? adultno2;
  String? childno2;
  String? baggage2;
  String? adultfare2;
  String? childfare2;
  String? tax2;
  String? totalamount2;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  _TwoWayFlightDetailPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.TwowayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.TwowayFlightList2,
    this.airlineName2,
    this.flightdatetime2,
    this.flightdeparturetime2,
    this.flightdeparturefrom2,
    this.flightarrivaltime2,
    this.flightarrivalto2,
    this.adultno2,
    this.childno2,
    this.baggage2,
    this.adultfare2,
    this.childfare2,
    this.tax2,
    this.totalamount2,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetoOnewayflightlist() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TwoWayFlightListPage(
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  TwowayFlightList: TwowayFlightList,
                  TwowayFlightList2: TwowayFlightList2,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    DateTime _dateTime1 =
        DateTime.parse("${flightdatetime} ${flightdeparturetime}:00");
    DateTime _dateTime2 =
        DateTime.parse("${flightdatetime} ${flightarrivaltime}:00");
    Duration diff = _dateTime2.difference(_dateTime1);

    print("This is the difference in time: ${diff}");

    DateTime _dateTimef1 =
        DateTime.parse("${flightdatetime2} ${flightdeparturetime2}:00");
    DateTime _dateTimef2 =
        DateTime.parse("${flightdatetime2} ${flightarrivaltime2}:00");
    Duration diff2 = _dateTimef2.difference(_dateTimef1);
    print("This is the difference in time: ${diff2}");

    return WillPopScope(
      onWillPop: () async {
        return movetoOnewayflightlist();
      },
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 60),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 150,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(
                                left: 30.0,
                              ),
                              child: Icon(
                                Icons.flight_takeoff,
                                color: Colors.green,
                                size: 30,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "${airlineName}",
                                style: const TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(left: 20.0),
                              child: Icon(
                                Icons.timer,
                                color: Colors.green,
                                size: 20,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                "${diff.inMinutes}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 40.0),
                              child: Text(
                                "${flightdatetime}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${flightdeparturetime}",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: Text(
                                      "${flightdeparturefrom}",
                                      style: const TextStyle(fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 160.0, top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${flightarrivaltime}",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: Text(
                                      "${flightarrivalto}",
                                      style: const TextStyle(fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 150,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(
                                left: 30.0,
                              ),
                              child: Icon(
                                Icons.flight_takeoff,
                                color: Colors.green,
                                size: 30,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: Text(
                                "${airlineName2}",
                                style: const TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(left: 20.0),
                              child: Icon(
                                Icons.timer,
                                color: Colors.green,
                                size: 20,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                "${diff2.inMinutes}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 40.0),
                              child: Text(
                                "${flightdatetime2}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${flightdeparturetime2}",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: Text(
                                      "${flightdeparturefrom2}",
                                      style: const TextStyle(fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 160.0, top: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${flightarrivaltime2}",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 10.0),
                                    child: Text(
                                      "${flightarrivalto2}",
                                      style: const TextStyle(fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Container(
                    height: 400,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Divider(
                          thickness: 1,
                          color: Colors.blueGrey,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 20.0, top: 20),
                              child: Text(
                                "Fare BreakUp",
                                style: TextStyle(
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 80.0, top: 20),
                              child: Text(
                                "${adultno}",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 10.0, top: 20),
                              child: Text(
                                "Adult",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 30.0, top: 20),
                              child: Text(
                                "${childno}",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 10.0, top: 20),
                              child: Text(
                                "Child",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 20.0),
                          child: Row(
                            children: [
                              Text("Check-in Baggage",
                                  style: TextStyle(
                                      color: Colors.grey[700],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14)),
                              Padding(
                                padding: const EdgeInsets.only(left: 50.0),
                                child: Text(
                                  "${baggage} kg",
                                  style: TextStyle(
                                      color: Colors.grey[700], fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 20.0),
                          child: Row(
                            children: [
                              Text("Base Fare (Adult)",
                                  style: TextStyle(
                                      color: Colors.grey[700],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14)),
                              Padding(
                                padding: const EdgeInsets.only(left: 50.0),
                                child: Text(
                                  "${adultno}",
                                  style: TextStyle(
                                      color: Colors.grey[700], fontSize: 14),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 120.0),
                                child: Text(
                                  "Rs. ${adultfare}",
                                  style: TextStyle(
                                      color: Colors.grey[700], fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 20.0),
                          child: Row(
                            children: [
                              Text("Base Fare (Child)",
                                  style: TextStyle(
                                      color: Colors.grey[700],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14)),
                              Padding(
                                padding: const EdgeInsets.only(left: 50.0),
                                child: Text(
                                  "${childno}",
                                  style: TextStyle(
                                      color: Colors.grey[700], fontSize: 14),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 120.0),
                                child: Text(
                                  "Rs. ${childfare}",
                                  style: TextStyle(
                                      color: Colors.grey[700], fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 20.0),
                          child: Row(
                            children: [
                              Text("Tax and Surcharges",
                                  style: TextStyle(
                                      color: Colors.grey[700],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14)),
                              Padding(
                                padding: const EdgeInsets.only(left: 160.0),
                                child: Text(
                                  "Rs. ${tax}",
                                  style: TextStyle(
                                      color: Colors.grey[700], fontSize: 14),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                          color: Colors.blueGrey,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20, top: 20.0),
                          child: Row(
                            children: [
                              const Text("Total Amount",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14)),
                              Padding(
                                padding: const EdgeInsets.only(left: 200.0),
                                child: Text(
                                  "Rs. ${totalamount}",
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 290, top: 30.0),
                          child: Container(
                            height: 40,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Color(
                                    int.parse(loginbuttonColor.toString())),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ConfirmFlightPage(
                                              coopList: coopList,
                                              userId: userId,
                                              accesstoken: accesstoken,
                                              balance: balance,
                                              baseUrl: baseUrl,
                                              accountno: accountno,
                                              TwowayFlightList:
                                                  TwowayFlightList,
                                              airlineName: airlineName,
                                              flightdatetime: flightdatetime,
                                              flightdeparturetime:
                                                  flightdeparturetime,
                                              flightdeparturefrom:
                                                  flightdeparturefrom,
                                              flightarrivaltime:
                                                  flightarrivaltime,
                                              flightarrivalto: flightarrivalto,
                                              adultno: adultno,
                                              childno: childno,
                                              baggage: baggage,
                                              adultfare: adultfare,
                                              childfare: childfare,
                                              tax: tax,
                                              totalamount: totalamount,
                                              TwowayFlightList2:
                                                  TwowayFlightList2,
                                              airlineName2: airlineName2,
                                              flightdatetime2: flightdatetime2,
                                              flightdeparturetime2:
                                                  flightdeparturetime2,
                                              flightdeparturefrom2:
                                                  flightdeparturefrom2,
                                              flightarrivaltime2:
                                                  flightarrivaltime2,
                                              flightarrivalto2:
                                                  flightarrivalto2,
                                              adultno2: adultno2,
                                              childno2: childno2,
                                              baggage2: baggage2,
                                              adultfare2: adultfare2,
                                              childfare2: childfare2,
                                              tax2: tax2,
                                              totalamount2: totalamount2,
                                              primaryColor: primaryColor,
                                              loginButtonTitleColor:
                                                  loginButtonTitleColor,
                                              loginbuttonColor:
                                                  loginbuttonColor,
                                              loginTextFieldColor:
                                                  loginTextFieldColor,
                                              dasboardIconColor:
                                                  dasboardIconColor,
                                              dashboardTopTitleColor:
                                                  dashboardTopTitleColor,
                                              SecondaryColor: SecondaryColor,
                                            )));
                              },
                              child: const Text(
                                "Continue",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
