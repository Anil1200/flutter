import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/oneway/oneway_flight_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/twoway/twoway_flight_page.dart';

import '../../../utils/localizations.dart';
import '../../pmc_homepage.dart';

class FlightPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  FlightPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<FlightPage> createState() => _FlightPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _FlightPageState extends State<FlightPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _FlightPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool showBalance = false;

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 30,
              ),
              onPressed: () {
                movetohomepage();
              },
            ),
            backgroundColor: Color(int.parse(primaryColor.toString())),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30))),
            toolbarHeight: 95,
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Text(
                    AppLocalizations.of(context)!
                        .localizedString("online_flight"),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white),
                  ),
                ),
                Row(
                  children: [
                    const SizedBox(
                      height: 20,
                      width: 20,
                      child: Image(
                          image: AssetImage("assets/images/wallet_icon.png")),
                    ),
                    const SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      showBalance
                          ? AppLocalizations.of(context)!
                                  .localizedString("rs") +
                              "${balance.toString()}"
                          : "xxx.xx".toUpperCase(),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 120.0),
                      child: IconButton(
                          onPressed: () {
                            setState(() {
                              if (showBalance == false) {
                                showBalance = true;
                              } else {
                                showBalance = false;
                              }
                            });
                          },
                          icon: Icon(
                            showBalance
                                ? Icons.visibility
                                : Icons.visibility_off,
                            size: 25.0,
                            color: Colors.white,
                          )),
                    ),
                  ],
                ),
              ],
            ),
            bottom: PreferredSize(
              preferredSize: Size(60, 60),
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 10.0, left: 10.0, right: 10),
                child: Container(
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ]),
                  child: TabBar(
                      indicatorColor: Colors.red,
                      indicator: UnderlineTabIndicator(
                        borderSide: BorderSide(width: 3.0, color: Colors.red),
                        insets: EdgeInsets.symmetric(
                          horizontal: 30.0,
                        ),
                      ),
                      labelColor: Colors.black87,
                      tabs: [
                        Tab(
                          text: AppLocalizations.of(context)!
                              .localizedString("one_way"),
                          icon: Icon(Icons.flight),
                        ),
                        Tab(
                          text: AppLocalizations.of(context)!
                              .localizedString("two_way"),
                          icon: Icon(Icons.flight_takeoff),
                        )
                      ]),
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              OneWayPage(
                coopList: coopList,
                userId: userId,
                accesstoken: accesstoken,
                balance: balance,
                baseUrl: baseUrl,
                accountno: accountno,
                primaryColor: primaryColor,
                loginButtonTitleColor: loginButtonTitleColor,
                loginbuttonColor: loginbuttonColor,
                loginTextFieldColor: loginTextFieldColor,
                dasboardIconColor: dasboardIconColor,
                dashboardTopTitleColor: dashboardTopTitleColor,
                SecondaryColor: SecondaryColor,
              ),
              TwoWayPage(
                coopList: coopList,
                userId: userId,
                accesstoken: accesstoken,
                balance: balance,
                baseUrl: baseUrl,
                accountno: accountno,
                primaryColor: primaryColor,
                loginButtonTitleColor: loginButtonTitleColor,
                loginbuttonColor: loginbuttonColor,
                loginTextFieldColor: loginTextFieldColor,
                dasboardIconColor: dasboardIconColor,
                dashboardTopTitleColor: dashboardTopTitleColor,
                SecondaryColor: SecondaryColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
