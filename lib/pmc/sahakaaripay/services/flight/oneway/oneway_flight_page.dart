import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';

import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';
import 'onewayflightlist_page.dart';

class OneWayPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  OneWayPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<OneWayPage> createState() => _OneWayPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _OneWayPageState extends State<OneWayPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  _OneWayPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  String? dropdownValuefrom;
  String? dropdownValueto;
  String dropdownValueadult = '1';
  String dropdownValuechild = '0';
  String? dropdownValue2 = "Nepalese";
  DateTime? _dateTime;
  bool _isLoading = false;

  List? flightlocationList;
  List? locationList;

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    this.getJsonData();
    _dateTime = DateTime.now();
    // this.getFlightJsonData();
  }

  Future<String> getJsonData() async {
    String url = "https://merchant.sahakaari.com/merchant/api/v1/getFlightList";
    // SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    final response = await http.get(Uri.parse(url), headers: {
      "Token": "${accesstoken}",
      "API_KEY": "ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP",
      'Authorization': "Bearer ${accesstoken}",
    });
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = false;
        var convertDataToJson = json.decode(response.body);
        flightlocationList = convertDataToJson["payload"];
        locationList = convertDataToJson["payload"];
        FlightList1 = flightlocationList;
        FlightList2 = locationList;

        print(
            "the foirst location of flight: ${FlightList1?[0]["sectorName"]}");
        print("This is the flight location list: ${FlightList1}");

        // CooperativeSharedPreferences.setsmsCode(
        //     convertDataToJson["payload"][0]["smsCode"].toString());
      });
      return "success";
    } else {
      _isLoading = false;
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    }
    return "success";
  }

  List? OnewayFlightList;

  String? nationalityCode = "NP";

  List<String?> nationality = [
    'Nepalese',
    'Indian',
  ];

  showMessage(ConnectivityResult result) {
    if (result == ConnectivityResult.none) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: const Text(
                  "No Internet, Please check your internet connection"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else if (dropdownValuefrom == null || dropdownValueto == null) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content:
                  const Text("Please select origin and destination place."),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      Onewayflight();
    }
  }

  Onewayflight() async {
    String url = "${baseUrl}api/v1/pes/users/${USERID}/domestic-search-flight";
    Map body = {
      "child": dropdownValuechild,
      "SectorTo": dropdownValueto.toString(),
      "adult": dropdownValueadult,
      "SectorFrom": dropdownValuefrom.toString(),
      "departureDate": _dateTime.toString(),
      "flightMode": "1",
      "returnDate": "",
      "Nationality": nationalityCode,
    };
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");

      if (jsonResponse != null) {
        setState(() {
          _isLoading = true;
          OnewayFlightList = jsonResponse["Outbound"];
          print("This is the one way flight list : ${OnewayFlightList}");
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => OneWayFlightListPage(
                        coopList: coopList,
                        userId: userId,
                        accesstoken: accesstoken,
                        balance: balance,
                        baseUrl: baseUrl,
                        accountno: accountno,
                        OnewayFlightList: OnewayFlightList,
                        primaryColor: primaryColor,
                        loginButtonTitleColor: loginButtonTitleColor,
                        loginbuttonColor: loginbuttonColor,
                        loginTextFieldColor: loginTextFieldColor,
                        dasboardIconColor: dasboardIconColor,
                        dashboardTopTitleColor: dashboardTopTitleColor,
                        SecondaryColor: SecondaryColor,
                      )));
        });
      }
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        return showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                            style: TextStyle(color: Colors.black87)),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text("${jsonResponse["Message"]}"),
                actions: [
                  // Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "OKAY",
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
      }
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        return showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                          style: TextStyle(color: Colors.black87),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 30,
                        ),
                      )
                    ],
                  ),
                ),
                content: Text(
                    AppLocalizations.of(context)!.localizedString("no_flight")),
                actions: [
                  // Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      "OKAY",
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
      }
    }
  }

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   _dateTime = DateTime.now();
  // }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? Center(
            child: Column(
            children: const [
              SizedBox(
                height: 150,
              ),
              Loading(),
            ],
          ))
        : Padding(
            padding: const EdgeInsets.only(top: 20.0, left: 20, right: 20.0),
            child: SingleChildScrollView(
                child: Center(
              child: Padding(
                padding:
                    const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("from"),
                                style: const TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValuefrom,
                                  alignment: AlignmentDirectional.center,
                                  hint: Text(
                                    "Choose Here",
                                    style: TextStyle(
                                        color: Colors.black87, fontSize: 14),
                                  ),
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValuefrom = newValue!;
                                      print(
                                          "this is arrival location: ${dropdownValuefrom}");
                                    });
                                  },
                                  items: flightlocationList?.map((item) {
                                    return DropdownMenuItem<String>(
                                        value: item["sectorCode"],
                                        child: Text(item["sectorName"]));
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("to"),
                                style: const TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValueto,
                                  alignment: AlignmentDirectional.center,
                                  hint: Text(
                                    "Choose Here",
                                    style: TextStyle(
                                        color: Colors.black87, fontSize: 14),
                                  ),
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValueto = newValue!;
                                      print(
                                          "this is arrival location: ${dropdownValueto}");
                                    });
                                  },
                                  items: locationList?.map((items) {
                                    return DropdownMenuItem<String>(
                                        value: items["sectorCode"],
                                        child: Text(items["sectorName"]));
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Center(
                      child: TextButton(
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("departure_date"),
                              style: TextStyle(color: Colors.black87),
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              _dateTime == null
                                  ? "${DateTime.now().year}- ${DateTime.now().month} - ${DateTime.now().day}"
                                  : " ${_dateTime!.year} - ${_dateTime!.month} - ${_dateTime!.day}",
                              style: const TextStyle(
                                color: Colors.black87,
                              ),
                            ),
                          ],
                        ),
                        onPressed: () {
                          showDatePicker(
                            context: context,
                            locale: Locale(DEFAULT_LOCALE),
                            initialDate:
                                _dateTime == null ? DateTime.now() : _dateTime!,
                            firstDate: DateTime(2018),
                            lastDate: DateTime(2050),
                            // builder: (context, child) => Theme(
                            //   data: Theme.of(context).copyWith(
                            //       colorScheme: const ColorScheme.dark(
                            //         primary: Colors.red,
                            //         surface: Colors.green,
                            //         onBackground: Colors.blue,
                            //       ),
                            //       dialogBackgroundColor: Colors.black87),
                            //   child: child ?? Text(""),
                            // ),
                          ).then((date) {
                            setState(() {
                              _dateTime = date;
                            });
                          });
                        },
                      ),
                    ),
                    const Divider(
                      thickness: 1,
                      color: Colors.red,
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("passenger_onboarding"),
                      style: TextStyle(color: Colors.black87, fontSize: 16),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("adult"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValueadult,
                                  alignment: AlignmentDirectional.center,
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValueadult = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    '1',
                                    '2',
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8',
                                    '9',
                                    '10'
                                  ].map<DropdownMenuItem<String>>(
                                      (String? value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value!,
                                        style: TextStyle(color: Colors.black87),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: 30,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 10.0),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("children"),
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 50,
                              width: 150,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  value: dropdownValuechild,
                                  alignment: AlignmentDirectional.center,
                                  // itemHeight: 90,
                                  borderRadius: BorderRadius.circular(20.0),
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black87,
                                  ),
                                  iconSize: 34,
                                  elevation: 16,
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold),
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      dropdownValuechild = newValue!;
                                    });
                                  },
                                  items: <String>[
                                    '0',
                                    '1',
                                    '2',
                                    '3',
                                    '4',
                                    '5',
                                    '6',
                                    '7',
                                    '8',
                                    '9',
                                    '10'
                                  ].map<DropdownMenuItem<String>>(
                                      (String? value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value!,
                                        style: const TextStyle(
                                            color: Colors.black87),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("nationality"),
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      height: 50,
                      width: 150,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 4),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          isExpanded: true,
                          value: dropdownValue2,
                          alignment: AlignmentDirectional.center,
                          // itemHeight: 90,
                          borderRadius: BorderRadius.circular(20.0),
                          icon: const Icon(
                            Icons.arrow_drop_down,
                            color: Colors.black87,
                          ),
                          iconSize: 34,
                          elevation: 16,
                          style: const TextStyle(
                              color: Colors.black87,
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                          onChanged: (nationality) {
                            if (nationality == 'Nepalese') {
                              nationalityCode = 'NP';
                            } else if (nationality == 'Indian') {
                              nationalityCode = 'INR';
                            }
                            setState(() {
                              // dropdownValue2 = "Choose Nationality";
                              dropdownValue2 = nationality.toString();
                            });
                          },
                          items: nationality.map((String? newValue) {
                            return DropdownMenuItem<String>(
                                value: newValue, child: Text(newValue!));
                          }).toList(),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 230.0),
                      child: InkWell(
                        onTap: () async {
                          setState(() {
                            _isLoading = true;
                          });
                          final result =
                              await Connectivity().checkConnectivity();
                          showMessage(result);
                        },
                        child: Container(
                          height: 40,
                          width: 100,
                          decoration: BoxDecoration(
                              color:
                                  Color(int.parse(loginbuttonColor.toString())),
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Center(
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("search"),
                              style:
                                  TextStyle(color: Colors.white, fontSize: 14),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )),
          );
  }
}
