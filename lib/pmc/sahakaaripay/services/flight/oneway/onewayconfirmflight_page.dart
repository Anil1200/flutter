import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/flight_passengers_model.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/oneway/onewayflight_pdfbill_page.dart';
import 'package:xml2json/xml2json.dart';

import '../../../../local_auth_api.dart';
import '../../../../utils/localizations.dart';
import '../../../../utils/utils.dart';
import '../../../models/loading.dart';
import 'onewayflightdetail_page.dart';

class OneWayConfirmFlightPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? OnewayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  List? TwowayFlightList2;
  String? airlineName2;
  String? flightdatetime2;
  String? flightdeparturetime2;
  String? flightdeparturefrom2;
  String? flightarrivaltime2;
  String? flightarrivalto2;
  String? adultno2;
  String? childno2;
  String? baggage2;
  String? adultfare2;
  String? childfare2;
  String? tax2;
  String? totalamount2;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  OneWayConfirmFlightPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.OnewayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _OneWayConfirmFlightPageState createState() => _OneWayConfirmFlightPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        OnewayFlightList,
        airlineName,
        flightdatetime,
        flightdeparturetime,
        flightdeparturefrom,
        flightarrivaltime,
        flightarrivalto,
        adultno,
        childno,
        baggage,
        adultfare,
        childfare,
        tax,
        totalamount,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _OneWayConfirmFlightPageState extends State<OneWayConfirmFlightPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? OnewayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  _OneWayConfirmFlightPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.OnewayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;

  final TextEditingController mpincontroller = TextEditingController();

  var adultFirstNameTECs = <TextEditingController>[];
  var adultLastNameTECs = <TextEditingController>[];

  var adults = <Column>[];

  var childFirstNameTECs = <TextEditingController>[];
  var childLastNameTECs = <TextEditingController>[];

  var children = <Column>[];

  List<FlightPassengersModel> adultPassengers = [];
  List<FlightPassengersModel> childrenPassengers = [];

  // String? adultPassengersJson;
  // String? childrenPassengersJson;

  @override
  void initState() {
    super.initState();
  }

  Column createAdultInfo(int i) {
    var adultFirstNameController = TextEditingController();
    var adultLastNameController = TextEditingController();

    adultFirstNameTECs.add(adultFirstNameController);
    adultLastNameTECs.add(adultLastNameController);

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20.0, top: 10),
          child: Container(
            alignment: Alignment.topLeft,
            child: Text(
              "Adult " + (i + 1).toString(),
              style: TextStyle(
                color: Colors.green,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 5),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 10),
<<<<<<< HEAD
=======
            child: DropDownTextField(
              controller: adultTitleController,
              clearOption: false,
              enableSearch: false,
              dropdownRadius: 0,
              textFieldDecoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: "Title",
                labelStyle: TextStyle(color: Colors.green),
                counterText: "",
                icon: Icon(
                  Icons.supervised_user_circle,
                  size: 30.0,
                  color: Colors.green,
                ),
              ),
              /*textFieldDecoration: const InputDecoration(
                //border: InputBorder.none,
                *//*enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 3, color: Colors.greenAccent),
                ),*//*
                hintText: "Select Title",
                hintStyle: TextStyle(
                  fontSize: 16,
                  color: Colors.black87,
                ),
              ),*/
              validator: (value) =>
              value!.isEmpty ? "Please Enter your Title." : null,
              dropDownItemCount: 3,
              dropDownList: const [
                DropDownValueModel(name: 'Mr', value: "Mr"),
                DropDownValueModel(name: 'Mrs', value: "Mrs"),
                DropDownValueModel(name: 'Ms', value: "Ms"),
              ],
              onChanged: (val) {},
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 5),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 10),
>>>>>>> sudeep
            child: TextFormField(
              controller: adultFirstNameController,
              maxLength: 30,
              keyboardType: TextInputType.text,
              validator: (value) =>
                  value!.isEmpty ? "Please Enter your First Name." : null,
              onSaved: (value) => namecontroller,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: AppLocalizations.of(context)!
                      .localizedString("firstname"),
                  labelStyle: TextStyle(color: Colors.green),
                  counterText: "",
                  icon: Icon(
                    Icons.supervised_user_circle,
                    size: 30.0,
                    color: Colors.green,
                  )),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 5),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 10),
            child: TextFormField(
              controller: adultLastNameController,
              maxLength: 30,
              keyboardType: TextInputType.text,
              validator: (value) =>
                  value!.isEmpty ? "Please enter your Last Name." : null,
              onSaved: (value) => namecontroller,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText:
                      AppLocalizations.of(context)!.localizedString("lastname"),
                  labelStyle: TextStyle(color: Colors.green),
                  counterText: "",
                  icon: Icon(
                    Icons.supervised_user_circle,
                    size: 30.0,
                    color: Colors.green,
                  )),
            ),
          ),
        ),
      ],
    );
  }

  Column createChildInfo(int i) {
    var childFirstNameController = TextEditingController();
    var childLastNameController = TextEditingController();

    childFirstNameTECs.add(childFirstNameController);
    childLastNameTECs.add(childLastNameController);

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20.0, top: 10),
          child: Container(
            alignment: Alignment.topLeft,
            child: Text(
              "Child " + (i + 1).toString(),
              style: TextStyle(
                color: Colors.green,
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 5),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 10),
<<<<<<< HEAD
=======
            child: DropDownTextField(
              controller: childTitleController,
              clearOption: false,
              enableSearch: false,
              dropdownRadius: 0,
              textFieldDecoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: "Title",
                labelStyle: TextStyle(color: Colors.green),
                counterText: "",
                icon: Icon(
                  Icons.supervised_user_circle,
                  size: 30.0,
                  color: Colors.green,
                ),
              ),
              validator: (value) =>
              value!.isEmpty ? "Please Enter your Title." : null,
              dropDownItemCount: 2,
              dropDownList: const [
                DropDownValueModel(name: 'Mr', value: "Mr"),
                DropDownValueModel(name: 'Ms', value: "Ms"),
              ],
              onChanged: (val) {},
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 5),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 10),
>>>>>>> sudeep
            child: TextFormField(
              controller: childFirstNameController,
              maxLength: 30,
              keyboardType: TextInputType.text,
              validator: (value) =>
                  value!.isEmpty ? "Please Enter your First Name." : null,
              onSaved: (value) => namecontroller,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: AppLocalizations.of(context)!
                      .localizedString("firstname"),
                  labelStyle: TextStyle(color: Colors.green),
                  counterText: "",
                  icon: Icon(
                    Icons.supervised_user_circle,
                    size: 30.0,
                    color: Colors.green,
                  )),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, top: 5),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 10),
            child: TextFormField(
              controller: childLastNameController,
              maxLength: 30,
              keyboardType: TextInputType.text,
              validator: (value) =>
                  value!.isEmpty ? "Please enter your Last Name." : null,
              onSaved: (value) => namecontroller,
              decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText:
                      AppLocalizations.of(context)!.localizedString("lastname"),
                  labelStyle: TextStyle(color: Colors.green),
                  counterText: "",
                  icon: Icon(
                    Icons.supervised_user_circle,
                    size: 30.0,
                    color: Colors.green,
                  )),
            ),
          ),
        ),
      ],
    );
  }

  movetoTwoWayFlightDetailPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => OneWayFlightDetailPage(
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  OnewayFlightList: OnewayFlightList,
                  airlineName: airlineName,
                  flightdatetime: flightdatetime,
                  flightdeparturetime: flightdeparturetime,
                  flightdeparturefrom: flightdeparturefrom,
                  flightarrivaltime: flightarrivaltime,
                  flightarrivalto: flightarrivalto,
                  adultno: adultno,
                  childno: childno,
                  baggage: baggage,
                  adultfare: adultfare,
                  childfare: childfare,
                  tax: tax,
                  totalamount: totalamount,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  final TextEditingController namecontroller = TextEditingController();
  final TextEditingController emailcontroller = TextEditingController();
  final TextEditingController contactNumbercontroller = TextEditingController();
  final TextEditingController childfirstnamecontroller =
      TextEditingController();
  final TextEditingController childlastnamecontroller = TextEditingController();
  final TextEditingController adultfirstnamecontroller =
      TextEditingController();
  final TextEditingController adultlastnamecontroller = TextEditingController();
  final scaffoldKey = GlobalKey<State>();
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final GlobalKey<FormState> globalFormKeyMPIN = GlobalKey();

  @override
  Widget build(BuildContext context) {
    if (adults.length == 0) {
      for (int i = 0; i < int.parse(adultno!); i++) {
        adults.add(createAdultInfo(i));
      }
      for (int i = 0; i < int.parse(childno!); i++) {
        children.add(createChildInfo(i));
      }
    }
    return WillPopScope(
      onWillPop: () async {
        return movetoTwoWayFlightDetailPage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Container(
                        height: 100,
                        decoration: BoxDecoration(
                            color: Color(int.parse(primaryColor.toString())),
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10.0, top: 10.0),
                              child: IconButton(
                                  onPressed: () {
                                    movetoTwoWayFlightDetailPage();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back,
                                    color: Colors.white,
                                    size: 30,
                                  )),
                            ),
                            const Padding(
                                padding: EdgeInsets.only(left: 40.0),
                                child: Text("Confirm Flight",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold)))
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Container(
                          height: 40,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(10)),
                          child: const Padding(
                            padding: EdgeInsets.only(left: 20.0, top: 8),
                            child: Text(
                              " Contact Information",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Form(
                        key: globalFormKey,
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, top: 5),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12.0, vertical: 10),
                                child: TextFormField(
                                  controller: namecontroller,
                                  maxLength: 50,
                                  keyboardType: TextInputType.text,
                                  validator: (value) => value!.isEmpty
                                      ? "Please Enter your Full Name."
                                      : null,
                                  onSaved: (value) => namecontroller,
                                  decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText: AppLocalizations.of(context)!
                                          .localizedString("contact_fullname"),
                                      labelStyle:
                                          TextStyle(color: Colors.green),
                                      counterText: "",
                                      icon: Icon(
                                        Icons.supervised_user_circle,
                                        size: 30.0,
                                        color: Colors.green,
                                      )),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 20.0,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12.0, vertical: 10),
                                child: TextFormField(
                                  controller: emailcontroller,
                                  maxLength: 50,
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) => value!.isEmpty
                                      ? "Please enter your email."
                                      : null,
                                  onSaved: (value) => namecontroller,
                                  decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText: AppLocalizations.of(context)!
                                          .localizedString("contact_email"),
                                      labelStyle:
                                          TextStyle(color: Colors.green),
                                      counterText: "",
                                      icon: Icon(
                                        Icons.email,
                                        size: 30.0,
                                        color: Colors.green,
                                      )),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 20.0,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12.0, vertical: 10),
                                child: TextFormField(
                                  controller: contactNumbercontroller,
                                  maxLength: 10,
                                  keyboardType: TextInputType.number,
                                  // obscureText: _obscureText,
                                  validator: (value) => value!.isEmpty ||
                                          value.length < 3
                                      ? "Contact Number must be of 10 digits."
                                      : null,
                                  onSaved: (value) => contactNumbercontroller,
                                  decoration: InputDecoration(
                                    border: UnderlineInputBorder(),
                                    labelText: AppLocalizations.of(context)!
                                        .localizedString("contact_number"),
                                    labelStyle: TextStyle(color: Colors.green),
                                    counterText: "",
                                    icon: Icon(
                                      Icons.phone_android,
                                      size: 30.0,
                                      color: Colors.green,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Form(
                          key: globalFormKey2,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Container(
                                  height: 40,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: const Padding(
                                    padding:
                                        EdgeInsets.only(left: 20.0, top: 8),
                                    child: Text(
                                      " Adult Information",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16),
                                    ),
                                  ),
                                ),
                              ),
                              ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: adults.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return adults[index];
                                },
                              ),
                              if (childno != "0") ...[
                                const SizedBox(
                                  height: 50,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: Container(
                                    height: 40,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: const Padding(
                                      padding:
                                          EdgeInsets.only(left: 20.0, top: 8),
                                      child: Text(
                                        " Child Information",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 16),
                                      ),
                                    ),
                                  ),
                                ),
                                ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: children.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return children[index];
                                  },
                                ),
                              ],
                            ],
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 290, top: 30.0),
                      child: Container(
                        height: 40,
                        width: 100,
                        decoration: BoxDecoration(
                            color:
                                Color(int.parse(loginbuttonColor.toString())),
                            borderRadius: BorderRadius.circular(10)),
                        child: TextButton(
                          onPressed: () async {
                            for (int i = 0; i < adults.length; i++) {
                              var title = "Mr";
                              var firstname = adultFirstNameTECs[i].text;
                              var lastname = adultLastNameTECs[i].text;
                              adultPassengers.add(FlightPassengersModel(
                                  Title: title,
                                  FirstName: firstname,
                                  LastName: lastname));
                            }
                            /*setState(() {
                         adultPassengersJson = jsonEncode(adultPassengers.map((e) => e.toJson()).toList());
                       });
                       print("AdultPassengers: " + adultPassengersJson!);*/

                            for (int i = 0; i < children.length; i++) {
                              var firstname = childFirstNameTECs[i].text;
                              var lastname = childLastNameTECs[i].text;
                              childrenPassengers.add(FlightPassengersModel(
                                  FirstName: firstname, LastName: lastname));
                            }
                            /*setState(() {
                         childrenPassengersJson = jsonEncode(childrenPassengers.map((e) => e.toJson()).toList());
                       });
                       print("ChildPassengers: " + childrenPassengersJson!);*/

                            if (validateAndSave()) {
                              setState(() {
                                _isLoading = true;
                              });
                              showMessage();
                            } else {
                              setState(() {
                                _isLoading = false;
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Center(
                                          child: Column(
                                            children: [
                                              Text(
                                                AppLocalizations.of(context)!
                                                    .localizedString("alert"),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Icon(
                                                Icons.add_alert,
                                                color: Colors.red,
                                                size: 50,
                                              )
                                            ],
                                          ),
                                        ),
                                        content: Text(AppLocalizations.of(
                                                context)!
                                            .localizedString("enter_all_data")),
                                        actions: [
                                          TextButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              "OKAY",
                                              style: TextStyle(
                                                  color: Colors.green[900]),
                                            ),
                                          )
                                        ],
                                      );
                                    });
                              });
                            }
                          },
                          child: const Text(
                            "Confirm",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  showMessage() {
    //if (dropdownValue != 'Select Amount') {
    if (adults.length == 0) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(AppLocalizations.of(context)!
                  .localizedString("adult_number")),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "OKAY",
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });

      if (showBiometricbuttonTransaction) {
        transactionPinMessageBiometric();
      } else {
        transactionPinMessage();
      }
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("book_flight"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKeyMPIN,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        validator: (value) => value!.isEmpty
                            ? AppLocalizations.of(context)!
                                .localizedString("enter_transaction_pin")
                            : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postTopupData();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString(
                                              "something_went_wrong"),
                                      style: const TextStyle(fontSize: 18),
                                    ),
                                    actions: [
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          "OKAY",
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("book_flight"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          "OKAY",
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      postTopupData();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  // postTopupData() async {
  //   String url = "${baseUrl}api/v1/pes/users/${USERID}/domestic-confirm-flight";
  //   Map bodyData = {
  //     "Amount": totalamount,
  //     "MPIN": mpincontroller.text.toString().trim(),
  //     "ContactPerson": namecontroller.text.toString().trim(),
  //     "ContactNo": contactNumbercontroller.text.toString().trim(),
  //     "ContactEmail": emailcontroller.text.toString().trim(),
  //     "CustomerAccountNo": accountno,
  //     "Adult": adultno,
  //     "AdultPassengers": adultPassengers,
  //     "Child": childno == "0" ? "" : childno,
  //     "ChildPassengers": childrenPassengers,
  //     "Currency": "NPR",
  //     "Nationality": "NP",
  //     "InboundFlightId": "0",
  //     "TotalCommission": "120.0",
  //     "OutboundFlightId": "01897c4e-f702-4bd0-99d0-2bd7db4ebeb4",
  //     "OperatorCode": "101"
  //   };
  //
  //   var body = json.encode(bodyData);
  //   print("this is the body for booking: ${body}");
  //
  //   var jsonResponse;
  //   var res = await http.post(Uri.parse(url), body: body, headers: {
  //     'Authorization': "Bearer ${accesstoken}",
  //     "Content-Type": "application/json"
  //   });
  //   if (res.statusCode == 200) {
  //     jsonResponse = json.decode(res.body);
  //     print("Response status: ${jsonResponse}");
  //     setState(() {
  //       _isLoading = true;
  //       if (jsonResponse != null) {
  //         setState(() {
  //           _isLoading = false;
  //           paymentmessage = jsonResponse["Message"];
  //           ReferenceId = jsonResponse["ReferenceId"];
  //           TransactionId = jsonResponse["TransactionId"];
  //           ReceiptNo = jsonResponse["ReceiptNo"];
  //           // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
  //           Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
  //             setState(() {
  //               _isLoading = false;
  //               ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //                   content: Row(
  //                     children: [
  //                       const Icon(
  //                         Icons.verified,
  //                         color: Colors.green,
  //                         size: 40,
  //                       ),
  //                       const SizedBox(
  //                         width: 20,
  //                       ),
  //                       Text(
  //                         jsonResponse["Message"],
  //                         style: const TextStyle(color: Colors.white, fontSize: 16),
  //                       ),
  //                     ],
  //                   )));
  //               Navigator.push(
  //                   context,
  //                   MaterialPageRoute(
  //                       builder: (context) => OneWayFlightPdfBillPage(
  //                         coopList: coopList,
  //                         userId: userId,
  //                         accesstoken: accesstoken,
  //                         balance: balance,
  //                         baseUrl: baseUrl,
  //                         accountno: accountno,
  //                         Amount: totalamount,
  //                         ContactPerson: namecontroller.text,
  //                         ContactNo: contactNumbercontroller.text,
  //                         ContactEmail: emailcontroller.text,
  //                         CustomerAccountNo: accountno,
  //                         Adult: adultno,
  //                         AdultPassengers: adultPassengers,
  //                         Child: childno == "0" ? "" : childno,
  //                         ChildPassengers: childrenPassengers,
  //                         paymentmessage: paymentmessage,
  //                         ReferenceId: ReferenceId,
  //                         TransactionId: TransactionId,
  //                         ReceiptNo: ReceiptNo,
  //                         PaymentBillDate: PaymentBillDate,
  //                         primaryColor: primaryColor,
  //                         loginButtonTitleColor: loginButtonTitleColor,
  //                         loginbuttonColor: loginbuttonColor,
  //                         loginTextFieldColor: loginTextFieldColor,
  //                         dasboardIconColor: dasboardIconColor,
  //                         dashboardTopTitleColor: dashboardTopTitleColor,
  //                         SecondaryColor: SecondaryColor,
  //                       )));
  //             });
  //           });
  //         });
  //       }
  //     });
  //   } else if (res.statusCode == 400) {
  //     setState(() {
  //       _isLoading = true;
  //     });
  //     jsonResponse = json.decode(res.body);
  //     print(jsonResponse);
  //     if (jsonResponse != null) {
  //       setState(() {
  //         _isLoading = false;
  //         showDialog(
  //             context: context,
  //             builder: (BuildContext context) {
  //               return AlertDialog(
  //                 title: Center(
  //                     child: Column(
  //                       children: const [
  //                         Text(
  //                           "Error Alert",
  //                           style: TextStyle(fontSize: 16),
  //                         ),
  //                         SizedBox(
  //                           height: 10,
  //                         ),
  //                         Icon(
  //                           Icons.add_alert,
  //                           color: Colors.red,
  //                           size: 50.0,
  //                         )
  //                       ],
  //                     )),
  //                 content: Text(
  //                   "${jsonResponse["Message"]}",
  //                   style: TextStyle(fontSize: 14),
  //                 ),
  //                 actions: [
  //                   TextButton(
  //                       onPressed: () {
  //                         Navigator.of(context).pop();
  //                       },
  //                       child: const Text("OKAY"))
  //                 ],
  //               );
  //             });
  //       });
  //     }
  //   } else if (res.statusCode == 401) {
  //     setState(() {
  //       _isLoading = false;
  //     });
  //     jsonResponse = json.decode(res.body);
  //     print(jsonResponse);
  //     return showDialog(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return AlertDialog(
  //             title: Center(
  //                 child: Column(
  //                   children: const [
  //                     Text(
  //                       "Error Alert",
  //                       style: TextStyle(fontSize: 16),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Icon(
  //                       Icons.add_alert,
  //                       color: Colors.red,
  //                       size: 50.0,
  //                     )
  //                   ],
  //                 )),
  //             content: Text(
  //               "${jsonResponse["Message"]}",
  //               style: TextStyle(fontSize: 14),
  //             ),
  //             actions: [
  //               TextButton(
  //                   onPressed: () {
  //                     Navigator.of(context).pop();
  //                   },
  //                   child: const Text("OKAY"))
  //             ],
  //           );
  //         });
  //   } else {
  //     setState(() {
  //       _isLoading = false;
  //     });
  //     jsonResponse = json.decode(res.body);
  //     print("this is the bookin error: ${jsonResponse}");
  //     return showDialog(
  //         context: context,
  //         builder: (BuildContext context) {
  //           return AlertDialog(
  //             title: Center(
  //                 child: Column(
  //                   children: const [
  //                     Text(
  //                       "Error Alert",
  //                       style: TextStyle(fontSize: 16),
  //                     ),
  //                     SizedBox(
  //                       height: 10,
  //                     ),
  //                     Icon(
  //                       Icons.add_alert,
  //                       color: Colors.red,
  //                       size: 50.0,
  //                     )
  //                   ],
  //                 )),
  //             content: Center(
  //                 child: Text(
  //                   AppLocalizations.of(context)!
  //                       .localizedString("something_went_wrong"),
  //                   style: TextStyle(fontSize: 14),
  //                 )),
  //             actions: [
  //               TextButton(
  //                   onPressed: () {
  //                     Navigator.of(context).pop();
  //                   },
  //                   child: const Text("OKAY"))
  //             ],
  //           );
  //         });
  //   }
  // }

  String? ReferenceId;
  String? TransactionId;
  String? data;
  List? onewayTicketList;
  String? paymentmessage;

  int passengerNo = 0;

  postTopupData() async {
    passengerNo = int.parse(adultno.toString()) + int.parse(childno.toString());
    String url = "${baseUrl}api/v1/pes/users/${USERID}/domestic-book-flight";
    Map bodyData = {
      // "InboundFlightId": "0",
      "OutboundFlightId": OutboundId,
      // "OperatorCode": "101"
    };
    var body = json.encode(bodyData);
    print("this is the body for booking: ${body}");

    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      'Authorization': "Bearer ${accesstoken}",
      "Content-Type": "application/json"
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("This is the booking Response status: ${jsonResponse}");
      if (jsonResponse != null) {
        String urlconfirm =
            "${baseUrl}api/v1/pes/users/${USERID}/domestic-confirm-flight";
        Map confirmbodyData = {
          "Amount": totalamount,
<<<<<<< HEAD
          "MPIN": mpincontroller.text.toString().trim(),
          "ContactPerson": namecontroller.text.toString().trim(),
=======
          "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
          "ContactPerson": contactnamecontroller.text.toString().trim(),
>>>>>>> sudeep
          "ContactNo": contactNumbercontroller.text.toString().trim(),
          "ContactEmail": emailcontroller.text.toString().trim(),
          "CustomerAccountNo": accountno,
          "Adult": adultno,
          "AdultPassengers": adultPassengers,
          "Child": childno == "0" ? "" : childno,
          "ChildPassengers": childrenPassengers,
          "Currency": "NPR",
          "Nationality": "NP",
          "InboundFlightId": "0",
          "TotalCommission": "120.0",
          "OutboundFlightId": OutboundId,
          "OperatorCode": "101"
        };
        mpincontroller.clear();
        var confirmbody = json.encode(confirmbodyData);
        print("this is the body for confirm: ${confirmbody}");

        var jsonConfirmResponse;
        var confirmres = await http.post(Uri.parse(urlconfirm),
            body: confirmbody,
            headers: {
              'Authorization': "Bearer ${accesstoken}",
              "Content-Type": "application/json"
            });
        if (confirmres.statusCode == 200) {
          jsonConfirmResponse = json.decode(confirmres.body);
          print("this is the confrim response: ${jsonConfirmResponse}");
          setState(() {
            _isLoading = true;
            paymentmessage = jsonConfirmResponse["Message"];
            TransactionId = jsonConfirmResponse["TransactionId"];
            data = jsonConfirmResponse["Data"];
            final Xml2Json xml2Json = Xml2Json();
            xml2Json.parse(data!);
            var jsonString = xml2Json.toParker();
            var Itenerary = jsonDecode(jsonString);
            print("this is the converted json: ${jsonString}");
            print("this is the converted json: ${Itenerary}");
            if (passengerNo > 1) {
              onewayTicketList = Itenerary["Itinerary"]["Ticket"];
              oneWayTicketList = onewayTicketList!;
            } else {
              var productMap = {
                'AirlineCode': Itenerary["Itinerary"]["Ticket"]["AirlineCode"],
                'AirlineName': Itenerary["Itinerary"]["Ticket"]["AirlineName"],
                'PnrNo': Itenerary["Itinerary"]["Ticket"]["PnrNo"],
                'Gender': Itenerary["Itinerary"]["Ticket"]["Gender"],
                'Name': Itenerary["Itinerary"]["Ticket"]["FirstName"],
                'PaxType': Itenerary["Itinerary"]["Ticket"]["PaxType"],
                'Nationality': Itenerary["Itinerary"]["Ticket"]["Nationality"],
                'IssueDate': Itenerary["Itinerary"]["Ticket"]["IssueDate"],
                'FlightNo': Itenerary["Itinerary"]["Ticket"]["FlightNo"],
                'FlightDate': Itenerary["Itinerary"]["Ticket"]["FlightDate"],
                'Departure': Itenerary["Itinerary"]["Ticket"]["Departure"],
                'FlightTime': Itenerary["Itinerary"]["Ticket"]["FlightTime"],
                'TicketNo': Itenerary["Itinerary"]["Ticket"]["TicketNo"],
                'Arrival': Itenerary["Itinerary"]["Ticket"]["Arrival"],
                'ArrivalTime': Itenerary["Itinerary"]["Ticket"]["ArrivalTime"],
                'Sector': Itenerary["Itinerary"]["Ticket"]["Sector"],
                'ClassCode': Itenerary["Itinerary"]["Ticket"]["ClassCode"],
                'Currency': Itenerary["Itinerary"]["Ticket"]["Currency"],
                'Fare': Itenerary["Itinerary"]["Ticket"]["Fare"],
                'Surcharge': Itenerary["Itinerary"]["Ticket"]["Surcharge"],
                'Tax': Itenerary["Itinerary"]["Ticket"]["Tax"],
                'Commission': Itenerary["Itinerary"]["Ticket"]["Commission"],
                'Refundable': Itenerary["Itinerary"]["Ticket"]["Refundable"],
                'ReportingTime': Itenerary["Itinerary"]["Ticket"]
                    ["ReportingTime"],
                'FreeBaggage': Itenerary["Itinerary"]["Ticket"]["FreeBaggage"],
              };
              oneWayTicketList.add(productMap);
              // oneWayTicketList<String?> = [
              //   "${AirlineCode = Itenerary["Itinerary"]["Ticket"]["AirlineCode"]}",
              //   "${AirlineName = Itenerary["Itinerary"]["Ticket"]["AirlineName"]}",
              //   "${PnrNo = Itenerary["Itinerary"]["Ticket"]["PnrNo"]}",
              //   "${Gender = Itenerary["Itinerary"]["Ticket"]["Gender"]}",
              //   "${Name = Itenerary["Itinerary"]["Ticket"]["FirstName"]}",
              //   // PnrNo = Itenerary["Itinerary"]["Ticket"]["PnrNo"],
              //   // Gender = Itenerary["Itinerary"]["Ticket"]["Gender"],
              //   // Name = Itenerary["Itinerary"]["Ticket"]["FirstName"],
              //   // PaxType = Itenerary["Itinerary"]["Ticket"]["PaxType"],
              //   // Nationality = Itenerary["Itinerary"]["Ticket"]["Nationality"],
              //   // IssueDate = Itenerary["Itinerary"]["Ticket"]["IssueDate"],
              //   // FlightNo = Itenerary["Itinerary"]["Ticket"]["FlightNo"],
              //   // FlightDate = Itenerary["Itinerary"]["Ticket"]["FlightDate"],
              //   // Departure = Itenerary["Itinerary"]["Ticket"]["Departure"],
              //   // FlightTime = Itenerary["Itinerary"]["Ticket"]["FlightTime"],
              //   // TicketNo = Itenerary["Itinerary"]["Ticket"]["TicketNo"],
              //   // Arrival = Itenerary["Itinerary"]["Ticket"]["Arrival"],
              //   // ArrivalTime = Itenerary["Itinerary"]["Ticket"]["ArrivalTime"],
              //   // Sector = Itenerary["Itinerary"]["Ticket"]["Sector"],
              //   // ClassCode = Itenerary["Itinerary"]["Ticket"]["ClassCode"],
              //   // Currency = Itenerary["Itinerary"]["Ticket"]["Currency"],
              //   // Fare = Itenerary["Itinerary"]["Ticket"]["Fare"],
              //   // Surcharge = Itenerary["Itinerary"]["Ticket"]["Surcharge"],
              //   // Tax = Itenerary["Itinerary"]["Ticket"]["Tax"],
              //   // Commission = Itenerary["Itinerary"]["Ticket"]["Commission"],
              //   // Refundable = Itenerary["Itinerary"]["Ticket"]["Refundable"],
              //   // ReportingTime = Itenerary["Itinerary"]["Ticket"]["ReportingTime"],
              //   // FreeBaggage = Itenerary["Itinerary"]["Ticket"]["FreeBaggage"],
              // ];
              print("this is the ticket data: ${oneWayTicketList}");
            }
            Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
              setState(() {
                _isLoading = false;
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Row(
                  children: [
                    const Icon(
                      Icons.verified,
                      color: Colors.green,
                      size: 40,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      jsonResponse["Message"],
                      style: const TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )));
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => OneWayFlightPdfBillPage(
                              coopList: coopList,
                              userId: userId,
                              accesstoken: accesstoken,
                              balance: balance,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              Amount: totalamount,
                              ContactPerson: namecontroller.text,
                              ContactNo: contactNumbercontroller.text,
                              ContactEmail: emailcontroller.text,
                              CustomerAccountNo: accountno,
                              Adult: adultno,
                              AdultPassengers: adultPassengers,
                              Child: childno == "0" ? "" : childno,
                              ChildPassengers: childrenPassengers,
                              paymentmessage: paymentmessage,
                              ReferenceId: ReferenceId,
                              TransactionId: TransactionId,
                              // ReceiptNo: ReceiptNo,
                              // PaymentBillDate: PaymentBillDate,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                            )));
              });
            });
          });
        } else if (confirmres.statusCode == 400) {
          setState(() {
            _isLoading = true;
          });
          jsonConfirmResponse = json.decode(confirmres.body);
          print(jsonConfirmResponse);
          if (jsonConfirmResponse != null) {
            setState(() {
              _isLoading = false;
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Center(
                          child: Column(
                        children: const [
                          Text(
                            "Error Alert",
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                      content: Text(
                        "${jsonConfirmResponse["Message"]}",
                        style: TextStyle(fontSize: 14),
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: const Text("OKAY"))
                      ],
                    );
                  });
            });
          }
        } else if (confirmres.statusCode == 401) {
          setState(() {
            _isLoading = false;
          });
          jsonConfirmResponse = json.decode(confirmres.body);
          print(jsonConfirmResponse);
          return showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonConfirmResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        } else {
          setState(() {
            _isLoading = false;
          });
          jsonConfirmResponse = json.decode(confirmres.body);
          print("this is the bookin error: ${jsonConfirmResponse}");
          return showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Center(
                      child: Text(
                    AppLocalizations.of(context)!
                        .localizedString("something_went_wrong"),
                    style: TextStyle(fontSize: 14),
                  )),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        }
      }
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the bookin error: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Center(
                  child: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              )),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateMPINForm() {
    final form2 = globalFormKeyMPIN.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      final form2 = globalFormKey2.currentState;
      if (form2!.validate()) {
        form2.save();
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
}
