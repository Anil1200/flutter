import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/oneway/onewayflightlist_page.dart';

import 'onewayconfirmflight_page.dart';

class OneWayFlightDetailPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? OnewayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  OneWayFlightDetailPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.OnewayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _OneWayFlightDetailPageState createState() => _OneWayFlightDetailPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        OnewayFlightList,
        airlineName,
        flightdatetime,
        flightdeparturetime,
        flightdeparturefrom,
        flightarrivaltime,
        flightarrivalto,
        adultno,
        childno,
        baggage,
        adultfare,
        childfare,
        tax,
        totalamount,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _OneWayFlightDetailPageState extends State<OneWayFlightDetailPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? OnewayFlightList;
  String? airlineName;
  String? flightdatetime;
  String? flightdeparturetime;
  String? flightdeparturefrom;
  String? flightarrivaltime;
  String? flightarrivalto;
  String? adultno;
  String? childno;
  String? baggage;
  String? adultfare;
  String? childfare;
  String? tax;
  String? totalamount;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _OneWayFlightDetailPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.OnewayFlightList,
    this.airlineName,
    this.flightdatetime,
    this.flightdeparturetime,
    this.flightdeparturefrom,
    this.flightarrivaltime,
    this.flightarrivalto,
    this.adultno,
    this.childno,
    this.baggage,
    this.adultfare,
    this.childfare,
    this.tax,
    this.totalamount,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetoOnewayflightlist() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OneWayFlightListPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  OnewayFlightList: OnewayFlightList,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    DateTime _dateTime1 =
        DateTime.parse("${flightdatetime} ${flightdeparturetime}:00");
    DateTime _dateTime2 =
        DateTime.parse("${flightdatetime} ${flightarrivaltime}:00");
    Duration diff = _dateTime2.difference(_dateTime1);

    print("This is the difference in time: ${diff}");

    return WillPopScope(
      onWillPop: () async {
        return movetoOnewayflightlist();
      },
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 60),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 60,
                decoration: BoxDecoration(color: Colors.white),
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(
                          left: 30.0,
                        ),
                        child: Icon(
                          Icons.flight_takeoff,
                          color: Colors.green,
                          size: 30,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Text(
                          "${airlineName}",
                          style: const TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 20.0),
                        child: Icon(
                          Icons.timer,
                          color: Colors.green,
                          size: 20,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: Text(
                          "${diff.inMinutes}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 40.0),
                        child: Text(
                          "${flightdatetime}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Container(
                  height: 450,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${flightdeparturetime}",
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: Text(
                                    "${flightdeparturefrom}",
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 160.0, top: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "${flightarrivaltime}",
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 10.0),
                                  child: Text(
                                    "${flightarrivalto}",
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const Divider(
                        thickness: 1,
                        color: Colors.blueGrey,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 20.0, top: 20),
                            child: Text(
                              "Fare BreakUp",
                              style: TextStyle(
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 80.0, top: 20),
                            child: Text(
                              "${adultno}",
                              style: TextStyle(
                                  color: Colors.grey[700], fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10.0, top: 20),
                            child: Text(
                              "Adult",
                              style: TextStyle(
                                  color: Colors.grey[700], fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 30.0, top: 20),
                            child: Text(
                              "${childno}",
                              style: TextStyle(
                                  color: Colors.grey[700], fontSize: 14),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10.0, top: 20),
                            child: Text(
                              "Child",
                              style: TextStyle(
                                  color: Colors.grey[700], fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 20.0),
                        child: Row(
                          children: [
                            Text("Check-in Baggage",
                                style: TextStyle(
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                            Padding(
                              padding: const EdgeInsets.only(left: 50.0),
                              child: Text(
                                "${baggage} kg",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 20.0),
                        child: Row(
                          children: [
                            Text("Base Fare (Adult)",
                                style: TextStyle(
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                            Padding(
                              padding: const EdgeInsets.only(left: 50.0),
                              child: Text(
                                "${adultno}",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 120.0),
                              child: Text(
                                "Rs. ${adultfare}",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                      ),
                      childno == 0.toString()
                          ? Container()
                          : Padding(
                              padding:
                                  const EdgeInsets.only(left: 20, top: 20.0),
                              child: Row(
                                children: [
                                  Text("Base Fare (Child)",
                                      style: TextStyle(
                                          color: Colors.grey[700],
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14)),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 50.0),
                                    child: Text(
                                      "${childno}",
                                      style: TextStyle(
                                          color: Colors.grey[700],
                                          fontSize: 14),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 120.0),
                                    child: Text(
                                      "Rs. ${childfare}",
                                      style: TextStyle(
                                          color: Colors.grey[700],
                                          fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 20.0),
                        child: Row(
                          children: [
                            Text("Tax and Surcharges",
                                style: TextStyle(
                                    color: Colors.grey[700],
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                            Padding(
                              padding: const EdgeInsets.only(left: 160.0),
                              child: Text(
                                "Rs. ${tax}",
                                style: TextStyle(
                                    color: Colors.grey[700], fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(
                        thickness: 1,
                        color: Colors.blueGrey,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, top: 20.0),
                        child: Row(
                          children: [
                            const Text("Total Amount",
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                            Padding(
                              padding: const EdgeInsets.only(left: 200.0),
                              child: Text(
                                "Rs. ${totalamount}",
                                style: const TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 290, top: 30.0),
                        child: Container(
                          height: 40,
                          width: 100,
                          decoration: BoxDecoration(
                              color:
                                  Color(int.parse(loginbuttonColor.toString())),
                              borderRadius: BorderRadius.circular(10)),
                          child: TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          OneWayConfirmFlightPage(
                                            coopList: coopList,
                                            accesstoken: accesstoken,
                                            balance: balance,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            OnewayFlightList: OnewayFlightList,
                                            airlineName: airlineName,
                                            flightdatetime: flightdatetime,
                                            flightdeparturetime:
                                                flightdeparturetime,
                                            flightdeparturefrom:
                                                flightdeparturefrom,
                                            flightarrivaltime:
                                                flightarrivaltime,
                                            flightarrivalto: flightarrivalto,
                                            adultno: adultno,
                                            childno: childno,
                                            baggage: baggage,
                                            adultfare: adultfare,
                                            childfare: childfare,
                                            tax: tax,
                                            totalamount: totalamount,
                                            primaryColor: primaryColor,
                                            loginButtonTitleColor:
                                                loginButtonTitleColor,
                                            loginbuttonColor: loginbuttonColor,
                                            loginTextFieldColor:
                                                loginTextFieldColor,
                                            dasboardIconColor:
                                                dasboardIconColor,
                                            dashboardTopTitleColor:
                                                dashboardTopTitleColor,
                                            SecondaryColor: SecondaryColor,
                                          )));
                            },
                            child: const Text(
                              "Continue",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
