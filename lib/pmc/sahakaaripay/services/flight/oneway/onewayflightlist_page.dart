import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/flight_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/flight/oneway/onewayflightdetail_page.dart';

import '../../../../constants.dart';

class OneWayFlightListPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? OnewayFlightList;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  OneWayFlightListPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.OnewayFlightList,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _OneWayFlightListPageState createState() => _OneWayFlightListPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        OnewayFlightList,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _OneWayFlightListPageState extends State<OneWayFlightListPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List? OnewayFlightList;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _OneWayFlightListPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.OnewayFlightList,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  @override
  void initState() {
    super.initState();
    print("this is the cooperative list: ${coopList}");
    print("this is the balance: ${balance}");
    print("this is the accesstoken: ${accesstoken}");
    print("this is the base URL: ${baseUrl}");
  }

  movetoFlightPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => FlightPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return movetoFlightPage();
      },
      child: Scaffold(
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    color: Color(int.parse(primaryColor.toString())),
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, top: 10.0),
                      child: IconButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FlightPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                            size: 30,
                          )),
                    ),
                    const Padding(
                        padding: EdgeInsets.only(left: 40.0),
                        child: Text("Oneway Flight List",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)))
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Container(
                  height: 40,
                  width: 600,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(10)),
                  child: const Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 8),
                    child: Text(
                      "Departure Flights",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  )),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount:
                      OnewayFlightList == null ? 0 : OnewayFlightList?.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                      onTap: () {
                        OutboundId = OnewayFlightList?[index]["FlightId"];
                        // InboundId = OnewayFlightList?[index][""];
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => OneWayFlightDetailPage(
                                      coopList: coopList,
                                      userId: userId,
                                      accesstoken: accesstoken,
                                      balance: balance,
                                      baseUrl: baseUrl,
                                      accountno: accountno,
                                      OnewayFlightList: OnewayFlightList,
                                      airlineName: OnewayFlightList?[index]
                                          ["AirlineName"],
                                      flightdatetime: OnewayFlightList?[index]
                                          ["FlightDate"],
                                      //     .toString(),
                                      flightdeparturetime:
                                          OnewayFlightList?[index]
                                              ["DepartureTime"],
                                      flightdeparturefrom:
                                          OnewayFlightList?[index]["Departure"],
                                      flightarrivaltime:
                                          OnewayFlightList?[index]
                                              ["ArrivalTime"],
                                      flightarrivalto: OnewayFlightList?[index]
                                          ["Arrival"],
                                      adultno: OnewayFlightList?[index]["Adult"]
                                          .toString(),
                                      childno: OnewayFlightList?[index]["Child"]
                                          .toString(),
                                      baggage: OnewayFlightList?[index]
                                          ["FreeBaggage"],
                                      adultfare: OnewayFlightList?[index]
                                              ["AdultFare"]
                                          .toStringAsFixed(1),
                                      childfare: OnewayFlightList?[index]
                                              ["ChildFare"]
                                          .toStringAsFixed(1),
                                      tax: OnewayFlightList?[index]["Tax"]
                                          .toStringAsFixed(1),
                                      totalamount: OnewayFlightList?[index]
                                              ["TotalAmount"]
                                          .toStringAsFixed(1),
                                      primaryColor: primaryColor,
                                      loginButtonTitleColor:
                                          loginButtonTitleColor,
                                      loginbuttonColor: loginbuttonColor,
                                      loginTextFieldColor: loginTextFieldColor,
                                      dasboardIconColor: dasboardIconColor,
                                      dashboardTopTitleColor:
                                          dashboardTopTitleColor,
                                      SecondaryColor: SecondaryColor,
                                    )));
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 10.0, right: 10, bottom: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                                padding:
                                    const EdgeInsets.only(left: 10.0, top: 10),
                                child: Container(
                                  height: 160,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(30),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(0, 3),
                                      )
                                    ],
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Container(
                                              height: 40,
                                              width: 30,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  image: const DecorationImage(
                                                      image: AssetImage(
                                                          "assets/images/flight/ic_flight.webp"))),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 20),
                                            child: Text(
                                              OnewayFlightList?[index]
                                                  ["AirlineName"],
                                              style: const TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 40, top: 10.0),
                                            child: Container(
                                              height: 60,
                                              width: 180,
                                              decoration: const BoxDecoration(
                                                  color: Colors.white),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "NRP: ${OnewayFlightList?[index]["TotalAmount"].toStringAsFixed(1)}",
                                                    style: const TextStyle(
                                                        color: Colors.green),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 10.0),
                                                    child: Text(
                                                      "CashBack: Rs. ${OnewayFlightList?[index]["CashBack"].toStringAsFixed(1)}",
                                                      style: const TextStyle(
                                                          color: Colors.green),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const Divider(
                                        thickness: 1,
                                        color: Colors.black,
                                      ),
                                      Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Text(
                                                "${OnewayFlightList?[index]["FlightDate"]}"),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${OnewayFlightList?[index]["DepartureTime"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${OnewayFlightList?[index]["Departure"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${OnewayFlightList?[index]["ArrivalTime"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${OnewayFlightList?[index]["Arrival"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10.0, top: 10),
                                            child: Column(
                                              children: [
                                                Text(
                                                  "${OnewayFlightList?[index]["Refundable"]}",
                                                  style: const TextStyle(
                                                      fontSize: 12),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: Text(
                                                    "${OnewayFlightList?[index]["FlightClassCode"]}",
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                )),
                            const SizedBox(
                              height: 20,
                            ),
                            const Divider(
                              thickness: 1,
                              color: Colors.red,
                            )
                          ],
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
