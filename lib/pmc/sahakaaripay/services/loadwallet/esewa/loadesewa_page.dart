import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/loadwallet/esewa/loadesewapdf_billpage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/loadwallet/loadwallet_page.dart';

import '../../../../constants.dart';
import '../../../../utils/localizations.dart';
import '../../../../utils/utils.dart';

class LoadEsewaPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  LoadEsewaPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _LoadEsewaPageState createState() => _LoadEsewaPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _LoadEsewaPageState extends State<LoadEsewaPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _LoadEsewaPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  String? serviceinfoId;
  // String? genericMessage;
  String? OriginatingUniqueId;
  String? Status;

  postTransferData() async {
    String url = "${baseUrl}api/v1/esewa/load/paymentbookings";
    Map bodymap = {
      "transactionURI": "+977${esewaIdController.text.toString().trim()}",
      "amountInformation": {
        "currency": "NPR",
        "amount": amountController.text.toString().trim()
      },
      "senderInformation": {
        "identifier": login_username,
        "name": AccountName,
        "contactDetail": login_username,
        "address": Address
      },
      "Statement": statementController.text.toString().trim(),
      "requestFields": [
        {
          "key": "Remarks",
          "value": statementController.text.toString().trim(),
        }
      ]
    };
    var body = json.encode(bodymap);
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      'Content-Type': "application/json",
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status of paymentbooking of fonepay: ${jsonResponse}");
      _isLoading = true;
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        Status = jsonResponse["status"];
        if (Status.toString().toLowerCase() ==
            "Success".toString().toLowerCase()) {
          OriginatingUniqueId = jsonResponse["uniqueOriginatedId"];
          serviceinfoId = jsonResponse["serviceInfoId"];
          transactionPinMessage();
        } else {
          setState(() {
            _isLoading = false;
          });
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    AppLocalizations.of(context)!
                        .localizedString("process_error"),
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        }
      }
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the response from the api: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${amountController.text} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${esewaIdController.text.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: _obscureText,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postTopupData();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? paymentmessage;

  postTopupData() async {
    String url = "${baseUrl}api/v1/esewa/load/payments";
    Map mapbody = {
      "MPIN": mpincontroller.text.toString().trim(),
      "serviceinfoId": serviceinfoId.toString(),
      "OriginatingUniqueId": OriginatingUniqueId.toString(),
      "Amount": amountController.text.toString().trim(),
      "Statement": "QRCode. " + statementController.text.toString().trim(),
      "RequestFields": [
        {"Key": "Remarks", "Value": statementController.text.toString().trim()}
      ]
    };
    mpincontroller.clear();
    print(mapbody);
    var body = json.encode(mapbody);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      'Content-Type': "application/json",
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        isQRTransaction = false;
        _isLoading = true;
        if (jsonResponse != null) {
          paymentmessage = jsonResponse["responseMessage"];
          ReferenceId = jsonResponse["ReferenceId"];
          TransactionId = jsonResponse["transactionDetail"]["tcTransactionId"];
          ReceiptNo = jsonResponse["ReceiptNo"];
          Status = jsonResponse["status"];
          if (Status.toString().toLowerCase() ==
              "Success".toString().toLowerCase()) {
            Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
              setState(() {
                _isLoading = false;
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Row(
                  children: [
                    const Icon(
                      Icons.verified,
                      color: Colors.green,
                      size: 40,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      jsonResponse["responseMessage"],
                      style: const TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )));
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LoadEsewaPdfBillPage(
                              coopList: coopList,
                              userId: userId,
                              accesstoken: accesstoken,
                              balance: balance,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              Amount: amountController.text.toString(),
                              eSewaId: esewaIdController.text.toString(),
                              paymentmessage: paymentmessage,
                              ReferenceId: ReferenceId,
                              TransactionId: TransactionId,
                              ReceiptNo: ReceiptNo,
                              PaymentBillDate: PaymentBillDate,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                            )));
              });
            });
          } else {
            setState(() {
              _isLoading = false;
            });
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                        child: Column(
                      children: [
                        Text(
                          AppLocalizations.of(context)!
                              .localizedString("error_alert"),
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 50.0,
                        )
                      ],
                    )),
                    content: Text(
                      AppLocalizations.of(context)!
                          .localizedString("process_error"),
                      style: TextStyle(fontSize: 14),
                    ),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("okay"),
                          ))
                    ],
                  );
                });
          }
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16, color: Colors.black),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse}",
                    style: TextStyle(fontSize: 14, color: Colors.black),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.black),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
        jsonResponse = json.decode(res.body);
        print(jsonResponse);
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  @override
  void initState() {
    super.initState();
    if (ScannedEsewaId != null) {
      esewaIdController.text = ScannedEsewaId!;
    }
    ScannedEsewaLoad = false;
    ScannedEsewaId = null;
  }

  movetoLoadWalletpage() {
    isQRTransaction = false;
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => LoadWalletPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;
  bool _obscureText = true;
  bool showBalance = false;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  final TextEditingController esewaIdController = TextEditingController();
  final TextEditingController amountController = TextEditingController();
  final TextEditingController statementController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoLoadWalletpage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            top: 40,
                            child: IconButton(
                                onPressed: () {
                                  movetoLoadWalletpage();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                          Positioned(
                            left: 100,
                            top: 50,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sahakaari_pay"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 95,
                            child: Row(
                              children: [
                                const SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: Image(
                                      image: AssetImage(
                                          "assets/images/wallet_icon.png")),
                                ),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  showBalance
                                      ? AppLocalizations.of(context)!
                                              .localizedString("rs") +
                                          "${balance.toString()}"
                                      : "xxx.xx".toUpperCase(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 180.0),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          if (showBalance == false) {
                                            showBalance = true;
                                          } else {
                                            showBalance = false;
                                          }
                                        });
                                      },
                                      icon: Icon(
                                        showBalance
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        size: 25.0,
                                        color: Colors.white,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 30.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("load_money"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 24.0,
                                    // fontStyle: FontStyle.italic,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Center(
                              child: Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    image: const DecorationImage(
                                        image: AssetImage(
                                            "assets/images/esewa/esewa.png"),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            // SizedBox(
                            //   height: 20,
                            // ),
                            Form(
                              key: globalFormKey,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 40.0, top: 10),
                                    child: Container(
                                      width: 280,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                      ),
                                      child: TextFormField(
                                        controller: esewaIdController,
                                        maxLength: 10,
                                        keyboardType: TextInputType.number,
                                        validator: (value) => value!.isEmpty
                                            ? AppLocalizations.of(context)!
                                                .localizedString(
                                                    "enter_esewa_id")
                                            : null,
                                        onSaved: (value) => esewaIdController,
                                        decoration: InputDecoration(
                                          border: UnderlineInputBorder(),
                                          labelText:
                                              AppLocalizations.of(context)!
                                                  .localizedString("eSewa_Id"),
                                          labelStyle: const TextStyle(
                                              color: Colors.green),
                                          counterText: "",
                                          icon: const Icon(
                                            Icons.supervised_user_circle,
                                            color: Colors.green,
                                            size: 20.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 40.0, top: 30),
                                    child: Container(
                                      width: 280,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                      ),
                                      child: TextFormField(
                                        controller: amountController,
                                        maxLength: 30,
                                        keyboardType: TextInputType.number,
                                        validator: (value) => value!.isEmpty
                                            ? AppLocalizations.of(context)!
                                                .localizedString("amount")
                                            : null,
                                        onSaved: (value) => amountController,
                                        decoration: InputDecoration(
                                          border: UnderlineInputBorder(),
                                          labelText:
                                              AppLocalizations.of(context)!
                                                  .localizedString("amount"),
                                          labelStyle: const TextStyle(
                                              color: Colors.green),
                                          counterText: "",
                                          icon: const Icon(
                                            Icons.money,
                                            color: Colors.green,
                                            size: 20.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 40.0, top: 30),
                                    child: Container(
                                      width: 280,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                      ),
                                      child: TextFormField(
                                        controller: statementController,
                                        maxLength: 300,
                                        keyboardType: TextInputType.text,
                                        validator: (value) => value!.isEmpty
                                            ? AppLocalizations.of(context)!
                                                .localizedString("statement")
                                            : null,
                                        onSaved: (value) => statementController,
                                        decoration: InputDecoration(
                                          border: UnderlineInputBorder(),
                                          labelText:
                                              AppLocalizations.of(context)!
                                                  .localizedString("statement"),
                                          labelStyle: const TextStyle(
                                              color: Colors.green),
                                          counterText: "",
                                          icon: const Icon(
                                            Icons.book,
                                            color: Colors.green,
                                            size: 20.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 40),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: Color(int.parse(
                                            loginbuttonColor.toString())),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 5,
                                            blurRadius: 7,
                                            offset: const Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ]),
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width - 120,
                                    child: TextButton(
                                      onPressed: () async {
                                        if (validateAndSave() &&
                                            validateMobile(
                                                esewaIdController.text)) {
                                          setState(() {
                                            _isLoading = true;
                                          });
                                          postTransferData();
                                          // transactionPinMessage();
                                        } else {
                                          setState(() {
                                            _isLoading = false;
                                          });
                                        }
                                      },
                                      child: Text(
                                        AppLocalizations.of(context)!
                                            .localizedString("submit"),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMobile(String value) {
    String pattern = r'(^(9)?[0-9]{9}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }
}
