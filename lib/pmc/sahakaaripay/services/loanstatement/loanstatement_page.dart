import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:nepali_utils/nepali_utils.dart';
import 'package:intl/intl.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/utils/custom_scroll_behavior.dart';

import '../../../utils/localizations.dart';
import '../../models/loading.dart';
//import '../../pmc_homepage.dart';

class LoanStatementPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? loanaccountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  LoanStatementPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.loanaccountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _LoanStatementPageState createState() => _LoanStatementPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        loanaccountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _LoanStatementPageState extends State<LoanStatementPage> with TickerProviderStateMixin {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? loanaccountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _LoanStatementPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.loanaccountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });

    okAnimationController = AnimationController(
      duration: const Duration(milliseconds: 150),
      vsync: this,
    );

    selectedDateFrom = selectedDateTo = DateTime.now();

    selectedNepaliDateFrom = selectedNepaliDateTo = NepaliDateTime.now();

    getStatementData();
  }

  bool _isLoading = false;

  bool showBalance = false;

  Map<dynamic, dynamic>? statement;

  late AnimationController okAnimationController;

  TextEditingController textEditingControllerDateFrom = TextEditingController();
  TextEditingController textEditingControllerDateTo = TextEditingController();

  DateTime? selectedDateFrom;

  DateTime? selectedDateTo;

  NepaliDateTime? selectedNepaliDateFrom;

  NepaliDateTime? selectedNepaliDateTo;

  Future<String> getStatementData() async {
    String url = "${baseUrl}api/v1/pes/account/loan-statement?accountNumber=$loanaccountno&dateFrom=$selectedDateFrom&dateTo=$selectedDateTo";
    print(url);
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer $accesstoken"});
    setState(() {
      _isLoading = true;
    });
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            statement = jsonResponse;
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  movetohomepage() {
    Navigator.of(context).pop();
    /*Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);*/
  }

  showMessageLoan() {
    Alert(
      context: context,
      title: AppLocalizations.of(context)!.localizedString('loan_statements'),
      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
      onWillPopActive: false,
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter newState) {
          return SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                  child: TextFormField(
                    controller: textEditingControllerDateFrom,
                    validator: (value) => value!.isEmpty ? AppLocalizations.of(context)!.localizedString('from') : null,
                    onSaved: (value) {
                      textEditingControllerDateFrom;
                    },
                    readOnly: true,
                    onTap: () {
                      // selectDate(1);
                      selectNepaliDate(1);
                    },
                    style: const TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Arial',
                    ),
                    decoration: InputDecoration(
                      //isDense: true,
                      contentPadding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
                      hintText: AppLocalizations.of(context)!.localizedString('from'),
                      prefixIcon: const Icon(Icons.calendar_month_outlined),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      errorBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      focusedErrorBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                  child: TextFormField(
                    controller: textEditingControllerDateTo,
                    validator: (value) => value!.isEmpty ? AppLocalizations.of(context)!.localizedString('to') : null,
                    onSaved: (value) {
                      textEditingControllerDateTo;
                    },
                    readOnly: true,
                    onTap: () {
                      // selectDate(2);
                      selectNepaliDate(2);
                    },
                    style: const TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Arial',
                    ),
                    decoration: InputDecoration(
                      //isDense: true,
                      contentPadding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 8.0),
                      hintText: AppLocalizations.of(context)!.localizedString('to'),
                      prefixIcon: const Icon(Icons.calendar_month_outlined),
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      errorBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.grey,
                        ),
                      ),
                      focusedErrorBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.0,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    DialogButton(
                      radius: BorderRadius.circular(5.0),
                      color: Colors.transparent,
                      onPressed: () {
                        newState(() {
                          textEditingControllerDateFrom.text = '';
                          textEditingControllerDateTo.text = '';

                          selectedDateFrom = selectedDateTo = DateTime.now();

                          _isLoading = false;
                        });

                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString('cancel'),
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(int.parse('0XFF0C76AE')),
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Arial',
                        ),
                      ),
                    ),
                    ScaleTransition(
                      scale: Tween<double>(
                        begin: 1.0,
                        end: 0.7,
                      ).animate(okAnimationController),
                      child: DialogButton(
                        radius: BorderRadius.circular(5.0),
                        color: Colors.transparent,
                        onPressed: () {
                          if (_isLoading!) {
                            return;
                          }

                          okAnimationController.forward();
                          Future.delayed(const Duration(milliseconds: 150), ()
                          {
                            okAnimationController.reverse();

                            setState(() {
                              getStatementData();
                              Navigator.of(context).pop();
                            });
                          });
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString('okay'),
                          style: TextStyle(
                            fontSize: 18,
                            color: Color(int.parse('0XFF0C76AE')),
                            fontWeight: FontWeight.normal,
                            fontFamily: 'Arial',
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
      buttons: [],
      style: AlertStyle(
        animationType: AnimationType.grow,
        animationDuration: const Duration(milliseconds: 500),
        isCloseButton: false,
        isOverlayTapDismiss: true,
        overlayColor: Colors.black.withOpacity(0.5),
        alertPadding: const EdgeInsets.all(20.0),
        titlePadding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
        titleTextAlign: TextAlign.start,
        buttonAreaPadding: const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
        buttonsDirection: ButtonsDirection.row,
        alertBorder: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
      ),
    ).show();
  }

  /*selectDate(int type) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: type == 1 ? selectedDateFrom! : selectedDateTo!,
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
    );
    if (pickedDate != null) {
      setState(() {
        if (type == 1) {
          selectedDateFrom = pickedDate;
          textEditingControllerDateFrom.text = DateFormat('yyyy-MM-dd', 'en_US').format(selectedDateFrom!).toString();
        } else if (type == 2) {
          selectedDateTo = pickedDate;
          textEditingControllerDateTo.text = DateFormat('yyyy-MM-dd', 'en_US').format(selectedDateTo!).toString();
        }
      });
    }
  }*/

  selectNepaliDate(int type) async {
    final NepaliDateTime? pickedDate = await showMaterialDatePicker(
      context: context,
      initialDate: type == 1 ? selectedNepaliDateFrom! : selectedNepaliDateTo!,
      initialDatePickerMode: DatePickerMode.day,
      firstDate: NepaliDateTime(2000),
      lastDate: NepaliDateTime(2100),
    );
    if (pickedDate != null) {
      setState(() {
        if (type == 1) {
          selectedNepaliDateFrom = pickedDate;
          selectedDateFrom = pickedDate.toDateTime();
          textEditingControllerDateFrom.text = DateFormat('yyyy-MM-dd', 'en_US').format(selectedNepaliDateFrom!).toString();
        } else if (type == 2) {
          selectedNepaliDateTo = pickedDate;
          selectedDateTo = pickedDate.toDateTime();
          textEditingControllerDateTo.text = DateFormat('yyyy-MM-dd', 'en_US').format(selectedNepaliDateTo!).toString();
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(int.parse(primaryColor.toString())),
          toolbarHeight: 110,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 30,
              color: Colors.white,
            ),
            onPressed: () {
              return movetohomepage();
            },
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 40, top: 20),
                child: Text(
                  AppLocalizations.of(context)!
                      .localizedString("loan_statements"),
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.white),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Row(
                  children: [
                    const SizedBox(
                      height: 30,
                      width: 20,
                      child: Image(
                          image: AssetImage("assets/images/wallet_icon.png")),
                    ),
                    const SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      showBalance
                          ? AppLocalizations.of(context)!
                                  .localizedString("rs") +
                              "${balance.toString()}"
                          : "xxx.xx".toUpperCase(),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 150.0),
                      child: IconButton(
                          onPressed: () {
                            setState(() {
                              if (showBalance == false) {
                                showBalance = true;
                              } else {
                                showBalance = false;
                              }
                            });
                          },
                          icon: Icon(
                            showBalance
                                ? Icons.visibility
                                : Icons.visibility_off,
                            size: 25.0,
                            color: Colors.white,
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showMessageLoan();
          },
          backgroundColor: Color(int.parse(primaryColor.toString())),
          child: const Icon(
            Icons.filter_alt,
            size: 42,
          ),
        ),
        body: _isLoading
            ? Loading()
            : ScrollConfiguration(
                behavior: CustomScrollBehavior(
                androidSdkVersion: androidSdkVersion,
              ),
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: statement?['statementList'] == null ? 0 : statement?['statementList'].length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding:
                          const EdgeInsets.only(left: 30.0, right: 20, top: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Center(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(Icons.calendar_today,
                                      size: 16, color: Colors.green[900]),
                                  SizedBox(
                                    width: 6,
                                  ),
                                  Text(
                                    statement?['statementList'][index]["transactionDate"],
                                    style: const TextStyle(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    width: 50,
                                  ),
                                  Text(
                                    statement!['statementList'][index]["type"].toString(),
                                    style: const TextStyle(
                                        color: Colors.black87,
                                        // ministatement?[index]["Trans"]
                                        //             .toString() ==
                                        //         "Cr"
                                        //     ? Colors.red
                                        //     : Colors.green,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    width: 40,
                                  ),
                                  Text(
                                    "Rs. ${statement!['statementList'][index]["amount"].toString()}",
                                    style: TextStyle(
                                        color: statement!['statementList'][index]["type"]
                                                    .toString() ==
                                                "Cr"
                                            ? Colors.red
                                            : Colors.green,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Icon(
                                    statement!['statementList'][index]["type"].toString() ==
                                            "Cr"
                                        ? Icons.arrow_upward
                                        : Icons.arrow_downward,
                                    size: 22,
                                    color: statement!['statementList'][index]["type"]
                                                .toString() ==
                                            "Cr"
                                        ? Colors.red
                                        : Colors.green,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 25.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!.localizedString("loan_interest") + ": Rs. ${statement!['statementList'][index]["interest"].toString()}",
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  AppLocalizations.of(context)!.localizedString("loan_fine") + ": Rs. ${statement!['statementList'][index]["fine"].toString()}",
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 25.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!.localizedString("loan_principal") + ": Rs. ${statement!['statementList'][index]["principal"].toString()}",
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  AppLocalizations.of(context)!.localizedString("loan_discount") + ": Rs. ${statement!['statementList'][index]["discount"].toString()}",
                                  style: const TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            statement?['statementList'][index]["remarks"],
                            style: const TextStyle(
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.normal),
                          ),

                          const SizedBox(
                            height: 5,
                          ),
                          const Divider(
                            thickness: 1,
                            color: Colors.green,
                          ),
                        ],
                      ),
                    );
                  },
                ),
            ),
      ),
    );
  }
}
