import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/recharge/rechargepdf_billpage.dart';

import '../../../constants.dart';
import '../../../local_auth_api.dart';
import '../../../utils/localizations.dart';
import '../../../utils/utils.dart';
import '../../models/loading.dart';
import '../../pmc_homepage.dart';

class RechargePage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  RechargePage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<RechargePage> createState() => _RechargePageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _RechargePageState extends State<RechargePage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _RechargePageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;

  bool showBalance = false;

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  showMessage() {
    if (dropdownValue == null || dropdownValueamount == null) {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: const [
                    Text("Alert"),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content:
                  const Text("Please Select Vender and Amount to Recharge."),
              actions: [
                // const Spacer(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "OKAY",
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
    } else {
      if (showBiometricbuttonTransaction) {
        transactionPinMessageBiometric();
      } else {
        transactionPinMessage();
      }
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${dropdownValueamount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message6") +
                          " ${dropdownValue.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message9a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postTopupData();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: const [
                                          Text(
                                            "ALERT",
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: const Text(
                                        "Please enter 4 digit valid MPIN number"),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${dropdownValueamount.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message6") +
                          " ${dropdownValue.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message9a"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      postTopupData();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? paymentmessage;

  String? operatorCode;
  // String? UTLoperatorCode;
  // String? SmartCelloperatorCode;
  // String? DishHomeoperatorCode;
  // String? NetTvoperatorCode;
  // String? BroadLinkoperatorCode;
  // String? BroadTeloperatorCode;

  postTopupData() async {
    String url = "${baseUrl}api/v1/rechargepin";
    Map body = {
      "OperatorCode": operatorCode.toString(),
      "Quantity": "1",
      "Amount": dropdownValueamount.toString(),
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
    };
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          paymentmessage = jsonResponse["Message"];
          ReferenceId = jsonResponse["ReferenceId"];
          TransactionId = jsonResponse["TransactionId"];
          ReceiptNo = jsonResponse["ReceiptNo"];
          // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  const Icon(
                    Icons.verified,
                    color: Colors.green,
                    size: 40,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    jsonResponse["Message"],
                    style: const TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ],
              )));
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RechargePdfBillPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            balance: balance,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            vendor: dropdownValue,
                            Amount: dropdownValueamount.toString(),
                            paymentmessage: paymentmessage,
                            // renewalplans: renewalplans,
                            // planName: planName,
                            ReferenceId: ReferenceId,
                            TransactionId: TransactionId,
                            ReceiptNo: ReceiptNo,
                            PaymentBillDate: PaymentBillDate,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor: dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Center(
                  child: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              )),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    }
  }

  movetohomepage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  String? dropdownValue;
  String? dropdownValueamount;
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  List<String?> amount = [];
  List<String?> services = [
    'NTC',
    'UTL',
    'Smart Cell',
    'Dish Home',
    'Broadlink',
    'Net TV'
  ];
  List<String?> ntcamount = [
    'Select Amount',
    '100',
    '150',
    '200',
    '500',
    '1000'
  ];
  List<String?> utlamount = ['Select Amount', '100', '250', '500'];
  List<String?> smartCellamount = ['Select Amount', '50', '100', '200', '500'];
  List<String?> dishHomeamount = [
    'Select Amount',
    '1000',
    '2000',
    '3000',
    '4000',
    '5000',
    '6000',
    '7000'
  ];
  List<String?> broadlinkamount = ['Select Amount', '550', '1200', '1500'];
  List<String?> broadtelamount = ['Select Amount', '250', '500'];
  List<String?> netTvamount = ['Select Amount', '50', '100'];

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            top: 40,
                            child: IconButton(
                                onPressed: () {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomePage(
                                                coopList: coopList,
                                                userId: userId,
                                                accesstoken: accesstoken,
                                                baseUrl: baseUrl,
                                                accountno: accountno,
                                                primaryColor: primaryColor,
                                                loginButtonTitleColor:
                                                    loginButtonTitleColor,
                                                loginbuttonColor:
                                                    loginbuttonColor,
                                                loginTextFieldColor:
                                                    loginTextFieldColor,
                                                dasboardIconColor:
                                                    dasboardIconColor,
                                                dashboardTopTitleColor:
                                                    dashboardTopTitleColor,
                                                SecondaryColor: SecondaryColor,
                                              )));
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                          Positioned(
                            left: 100,
                            top: 50,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sahakaari_pay"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 95,
                            child: Row(
                              children: [
                                const SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: Image(
                                      image: AssetImage(
                                          "assets/images/wallet_icon.png")),
                                ),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  showBalance
                                      ? AppLocalizations.of(context)!
                                              .localizedString("rs") +
                                          "${balance.toString()}"
                                      : "xxx.xx".toUpperCase(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 180.0),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          if (showBalance == false) {
                                            showBalance = true;
                                          } else {
                                            showBalance = false;
                                          }
                                        });
                                      },
                                      icon: Icon(
                                        showBalance
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        size: 25.0,
                                        color: Colors.white,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 30.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("recharge_cards"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 20.0,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 40.0),
                              child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("select_vendors"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 14,
                                  )),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 30.0, top: 10.0),
                              child: Container(
                                width: 300,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12, vertical: 4),
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.1),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                    border: Border.all(
                                        color: Colors.black87, width: 2),
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    isExpanded: true,
                                    hint: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("select_vendors"),
                                    ),
                                    value: dropdownValue,
                                    alignment: AlignmentDirectional.center,
                                    // itemHeight: 90,
                                    borderRadius: BorderRadius.circular(20.0),
                                    icon: const Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.black87,
                                    ),
                                    iconSize: 34,
                                    elevation: 16,
                                    style: const TextStyle(
                                        color: Colors.black87,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                    onChanged: (services) {
                                      if (services == 'NTC') {
                                        amount = ntcamount;
                                        operatorCode = "5";
                                      } else if (services == 'UTL') {
                                        amount = utlamount;
                                        operatorCode = "7";
                                      } else if (services == 'Smart Cell') {
                                        amount = smartCellamount;
                                        operatorCode = "8";
                                      } else if (services == 'Dish Home') {
                                        amount = dishHomeamount;
                                        operatorCode = "11";
                                      } else if (services == 'Broadlink') {
                                        amount = broadlinkamount;
                                        operatorCode = "12";
                                      } else if (services == 'Net TV') {
                                        amount = netTvamount;
                                        operatorCode = "35";
                                      } else {
                                        amount = [];
                                        operatorCode = "";
                                      }
                                      setState(() {
                                        dropdownValueamount = 'Select Amount';
                                        dropdownValue = services.toString();
                                      });
                                    },
                                    items: services.map((String? newValue) {
                                      return DropdownMenuItem<String>(
                                          value: newValue,
                                          child: Text(newValue!));
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 40.0, top: 10),
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("amount"),
                                style: TextStyle(
                                    color: Colors.black87, fontSize: 16),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 30.0, top: 10.0),
                              child: Container(
                                width: 300,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12, vertical: 4),
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.1),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                    border: Border.all(
                                        color: Colors.black87, width: 2),
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                      hint: Text(
                                        AppLocalizations.of(context)!
                                            .localizedString("amount"),
                                      ),
                                      value: dropdownValueamount,
                                      onChanged: (amount) {
                                        setState(() {
                                          dropdownValueamount = amount;
                                        });
                                      },
                                      items: amount.map((String? newValue) {
                                        return DropdownMenuItem<String>(
                                            value: newValue,
                                            child: Text('${newValue}'));
                                      }).toList()),
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 220.0, top: 20),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    color: Color(
                                        int.parse(loginbuttonColor.toString())),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ]),
                                height: 50,
                                width: 100,
                                child: TextButton(
                                  onPressed: () async {
                                    showMessage();
                                    // if (validateAndSave()) {
                                    //   setState(() {
                                    //     _isLoading = true;
                                    //   });
                                    // } else {
                                    //   setState(() {
                                    //     _isLoading = false;
                                    //   });
                                    // }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("submit"),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  // bool validateAndSave() {
  //   final form = globalFormKey.currentState;
  //   if (form!.validate()) {
  //     form.save();
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }
}
