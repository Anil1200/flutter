import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loan_page.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';

class LoanScheduleDetailsPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List<dynamic>? loanSchedule;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  LoanScheduleDetailsPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.loanSchedule,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _LoanScheduleDetailsPageState createState() => _LoanScheduleDetailsPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        loanSchedule,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _LoanScheduleDetailsPageState extends State<LoanScheduleDetailsPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  List<dynamic>? loanSchedule;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _LoanScheduleDetailsPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.loanSchedule,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool showBalance = false;

  void initState() {
    super.initState();
  }

  movetoLoanSchedulePage() {
    Navigator.of(context).pop();
    /*Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => LoanPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);*/
  }

  DataTable createLoanSchedule() {
    return DataTable(
      columns: createLoanScheduleColumns(),
      rows: createLoanScheduleRows(),
    );
  }

  List<DataColumn> createLoanScheduleColumns() {
    return [
      const DataColumn(
        label: Text('Date'),
      ),
      const DataColumn(
        label: Text('Beginning Balance'),
      ),
      const DataColumn(
        label: Text('Scheduled Payment'),
      ),
      const DataColumn(
        label: Text('Interest Rate'),
      ),
      const DataColumn(
        label: Text('Principal Deduction'),
      ),
      const DataColumn(
        label: Text('Interest Deduction'),
      ),
      const DataColumn(
        label: Text('Ending Balance'),
      ),
      const DataColumn(
        label: Text('Is Paid'),
      ),
    ];
  }

  List<DataRow> createLoanScheduleRows() {
    return loanSchedule!.map((schedule) => DataRow(cells: [
      DataCell(Text(schedule['scheduleDate'].toString())),
      DataCell(Text(schedule['beginingBalance'].toString())),
      DataCell(Text(schedule['scheduledPayment'].toString())),
      DataCell(Text(schedule['interestRate'].toString())),
      DataCell(Text(schedule['principalDeduction'].toString())),
      DataCell(Text(schedule['interestDeduction'].toString())),
      DataCell(Text(schedule['endingBalnce'].toString())),
      DataCell(Text(schedule['isPaid'].toString())),
    ])).toList();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoLoanSchedulePage();
        },
        child: Scaffold(
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    color: Color(int.parse(primaryColor.toString())),
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        left: 10.0,
                        top: 40,
                        child: IconButton(
                            onPressed: () {
                              movetoLoanSchedulePage();
                            },
                            icon: const Icon(
                              Icons.arrow_back,
                              size: 30.0,
                              color: Colors.white,
                            )),
                      ),
                      Positioned(
                        left: 100,
                        top: 50,
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("sahakaari_pay"),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                      Positioned(
                        left: 20,
                        top: 95,
                        child: Row(
                          children: [
                            const SizedBox(
                              height: 30,
                              width: 30,
                              child: Image(
                                  image: AssetImage(
                                      "assets/images/wallet_icon.png")),
                            ),
                            const SizedBox(
                              width: 10.0,
                            ),
                            Text(
                              showBalance
                                  ? AppLocalizations.of(context)!
                                  .localizedString("rs") +
                                  "${balance.toString()}"
                                  : "xxx.xx".toUpperCase(),
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 180.0),
                              child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      if (showBalance == false) {
                                        showBalance = true;
                                      } else {
                                        showBalance = false;
                                      }
                                    });
                                  },
                                  icon: Icon(
                                    showBalance
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    size: 25.0,
                                    color: Colors.white,
                                  )),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Center(
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("loan_schedule"),
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: SingleChildScrollView(
                      child: createLoanSchedule(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
