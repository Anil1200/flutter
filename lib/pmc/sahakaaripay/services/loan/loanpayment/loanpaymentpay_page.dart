import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/pmc_homepage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loan_page.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';

import '../../../../local_auth_api.dart';

class LoanPaymentPayPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? loanaccountno;
  String? loanaccountname;
  String? loanmessage;
  String? loantotalamount;
  String? loanfine;
  String? loanpenalty;
  String? loaninterest;
  String? loandueprincipal;

  LoanPaymentPayPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.loanaccountno,
    this.loanaccountname,
    this.loanmessage,
    this.loantotalamount,
    this.loanfine,
    this.loanpenalty,
    this.loaninterest,
    this.loandueprincipal,
  }) : super(key: key);

  @override
  _LoanPaymentPayPageState createState() => _LoanPaymentPayPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
        loanaccountno,
        loanaccountname,
        loanmessage,
        loantotalamount,
        loanfine,
        loanpenalty,
        loaninterest,
        loandueprincipal,
  );
}

class _LoanPaymentPayPageState extends State<LoanPaymentPayPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? loanaccountno;
  String? loanaccountname;
  String? loanmessage;
  String? loantotalamount;
  String? loanfine;
  String? loanpenalty;
  String? loaninterest;
  String? loandueprincipal;
  _LoanPaymentPayPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
    this.loanaccountno,
    this.loanaccountname,
    this.loanmessage,
    this.loantotalamount,
    this.loanfine,
    this.loanpenalty,
    this.loaninterest,
    this.loandueprincipal,
  );

  bool showBalance = false;

  @override
  void initState() {
    super.initState();

    loanAccountNo = widget.loanaccountno;
    loanAccountName = widget.loanaccountname;
    loanMessage = widget.loanmessage;
    loanTotalAmount = widget.loantotalamount;
    loanFine = widget.loanfine;
    loanPenalty = widget.loanpenalty;
    loanInterest = widget.loaninterest;
    loanDuePrincipal = widget.loandueprincipal;
  }

  postLoanPaymentValidate() async {
    String url = "${baseUrl}api/v1/pes/account/validate-loan-account";
    Map body = {
      "AccountNumber": loanaccountno,
      "AccountName": loanaccountname,
    };
    print("Validate Request status: ${body}");
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });

          if (jsonResponse["message_code"] != null) {
            if (jsonResponse["message_code"] != "00") {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Center(
                          child: Column(
                            children: [
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("error_alert"),
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Icon(
                                Icons.add_alert,
                                color: Colors.red,
                                size: 50.0,
                              )
                            ],
                          )),
                      content: Text(
                        jsonResponse["message"],
                        style: TextStyle(fontSize: 14),
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              AppLocalizations.of(context)!.localizedString("okay"),
                            ))
                      ],
                    );
                  });
            } else if (jsonResponse["message_code"] == "00") {
              setState(() {
                _isLoading = true;
              });
              postLoanInformation(loanaccountno!);
            }
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("error_alert"),
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 50.0,
                            )
                          ],
                        )),
                    content: Text(
                      AppLocalizations.of(context)!
                          .localizedString("something_went_wrong"),
                      style: TextStyle(fontSize: 14),
                    ),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!.localizedString("okay"),
                          ))
                    ],
                  );
                });
          }
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error meesage: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  String? loanAccountNo;
  String? loanAccountName;
  String? loanMessage;
  String? loanTotalAmount;
  String? loanFine;
  String? loanPenalty;
  String? loanInterest;
  String? loanDuePrincipal;

  postLoanInformation(String loanAccountNumber) async {
    String url = "${baseUrl}api/v1/pes/account/get-loan-payment-information";
    Map body = {
      "AccountNumber": loanAccountNumber,
    };
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          loanMessage = jsonResponse["Message"];
          loanTotalAmount = jsonResponse["TotalAmount"].toString();
          loanFine = jsonResponse["Fine"].toString();
          loanPenalty = jsonResponse["Penalty"].toString();
          loanInterest = jsonResponse["Interest"].toString();
          loanDuePrincipal = jsonResponse["DuePrincipal"].toString();

          double loanMinimumPayAmount = double.parse(loanFine!) + double.parse(loanPenalty!);
          setState(() {
            _isLoading = false;
          });

          if (double.parse(loanAmountController.text.toString().trim()) >= loanMinimumPayAmount) {
            setState(() {
              _isLoading = true;
            });
            postLoanPayment();
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("error_alert"),
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 50.0,
                            )
                          ],
                        )),
                    content: Text(
                      "Amount should be more than Fine and Penalty.",
                      style: TextStyle(fontSize: 14),
                    ),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!.localizedString("okay"),
                          ))
                    ],
                  );
                });
          }
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error meesage: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  postLoanPayment() async {
    String url = "${baseUrl}api/v1/pes/account/loan-payment";
    Map body = {
      "LoanAccountNumber": loanaccountno,
      "DepositAccountNumber": AccountNumber,
      "Amount": loanAmountController.text.toString().trim(),
      "StatementReference": loanStatementReferenceController.text.toString().trim(),
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
    };
    print("Request body: ${body}");
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
          });

          if (jsonResponse["Message_code"] != null) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Row(
                  children: [
                    const Icon(
                      Icons.verified,
                      color: Colors.green,
                      size: 40,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width / 2,
                      child: Text(
                        jsonResponse["Message"],
                        style:
                        const TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ),
                  ],
                )));

            if (jsonResponse["Message_code"] == "00") {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomePage(
                        coopList: coopList,
                        userId: userId,
                        accesstoken: accesstoken,
                        baseUrl: baseUrl,
                        accountno: accountno,
                        primaryColor: primaryColor,
                        loginButtonTitleColor: loginButtonTitleColor,
                        loginbuttonColor: loginbuttonColor,
                        loginTextFieldColor: loginTextFieldColor,
                        dasboardIconColor: dasboardIconColor,
                        dashboardTopTitleColor: dashboardTopTitleColor,
                        SecondaryColor: SecondaryColor,
                      )));
            }
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                        child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("error_alert"),
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 50.0,
                            )
                          ],
                        )),
                    content: Text(
                      AppLocalizations.of(context)!
                          .localizedString("something_went_wrong"),
                      style: TextStyle(fontSize: 14),
                    ),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!.localizedString("okay"),
                          ))
                    ],
                  );
                });
          }
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      // jsonResponse = json.decode(res.body);
      // print("this is the error meesage: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 220,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 20),
                    child: Text(
                      "Rs.${loanAmountController.text.toString()} will be transferred to ${loanaccountno} ",
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        validator: (value) => value!.isEmpty || value.length < 3
                            ? AppLocalizations.of(context)!
                            .localizedString("enter_transaction_pin")
                            : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postLoanPaymentValidate();
                          } else {
                            _isLoading = false;
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessage_Biometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 20),
                    child: Text(
                      "Rs.${loanAmountController.text.toString()} will be transferred to ${loanaccountno} ",
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      postLoanPaymentValidate();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  movetoLoanpage() {
    Navigator.of(context).pop();
    /*Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => LoanPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);*/
  }

  bool _isLoading = false;

  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();

  final TextEditingController loanAmountController = TextEditingController();
  final TextEditingController loanStatementReferenceController = TextEditingController();
  final TextEditingController mpincontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoLoanpage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 150,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            top: 40,
                            child: IconButton(
                                onPressed: () {
                                  movetoLoanpage();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                          Positioned(
                            left: 100,
                            top: 50,
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("sahakaari_pay"),
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 20,
                            top: 95,
                            child: Row(
                              children: [
                                const SizedBox(
                                  height: 30,
                                  width: 30,
                                  child: Image(
                                      image: AssetImage(
                                          "assets/images/wallet_icon.png")),
                                ),
                                const SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  showBalance
                                      ? AppLocalizations.of(context)!
                                              .localizedString("rs") +
                                          "${balance.toString()}"
                                      : "xxx.xx".toUpperCase(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 180.0),
                                  child: IconButton(
                                      onPressed: () {
                                        setState(() {
                                          if (showBalance == false) {
                                            showBalance = true;
                                          } else {
                                            showBalance = false;
                                          }
                                        });
                                      },
                                      icon: Icon(
                                        showBalance
                                            ? Icons.visibility
                                            : Icons.visibility_off,
                                        size: 25.0,
                                        color: Colors.white,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 0.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("loan_payment_information"),
                                  style: TextStyle(
                                      color: Colors.green[900],
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0, top: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("account_number"),
                                      style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width / 2.5,
                                      child: Text("$loanaccountno")),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0, top: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("account_name"),
                                      style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width / 2.5,
                                      child: Text("$loanaccountname")),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0, top: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("loan_total_amount"),
                                      style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width / 2.5,
                                      child: Text("$loanTotalAmount")),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0, top: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("loan_fine"),
                                      style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width / 2.5,
                                      child: Text("$loanFine")),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0, top: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("loan_penalty"),
                                      style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width / 2.5,
                                      child: Text("$loanPenalty")),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0, top: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("loan_interest"),
                                      style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width / 2.5,
                                      child: Text("$loanInterest")),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0, top: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width / 3,
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("loan_due_principal"),
                                      style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  Container(
                                      width: MediaQuery.of(context).size.width / 2.5,
                                      child: Text("$loanDuePrincipal")),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 20.0,
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 30.0),
                              child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("loan_payment"),
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 24.0,
                                  ),
                                ),
                              ),
                            ),
                            _isLoading
                                ? const Center(
                                    child: Column(
                                    children: [
                                      Loading(),
                                    ],
                                  ))
                                : Form(
                                    key: globalFormKey,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 40.0, top: 10),
                                          child: Container(
                                            width: 280,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(20.0),
                                            ),
                                            child: TextFormField(
                                              controller: loanAmountController,
                                              maxLength: 30,
                                              keyboardType: TextInputType.number,
                                              validator: (value) =>
                                              value!.isEmpty
                                                  ? AppLocalizations.of(
                                                  context)!
                                                  .localizedString(
                                                  "enter_amount")
                                                  : null,
                                              onSaved: (value) =>
                                              loanAmountController,
                                              decoration: InputDecoration(
                                                border: UnderlineInputBorder(),
                                                labelText: AppLocalizations.of(
                                                    context)!
                                                    .localizedString(
                                                    "transaction_amount"),
                                                labelStyle: TextStyle(
                                                    color: Colors.green),
                                                counterText: "",
                                                icon: Icon(
                                                  Icons.supervised_user_circle,
                                                  color: Colors.green,
                                                  size: 20.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 40.0, top: 10),
                                          child: Container(
                                            width: 280,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(20.0),
                                            ),
                                            child: TextFormField(
                                              controller: loanStatementReferenceController,
                                              maxLength: 30,
                                              keyboardType: TextInputType.text,
                                              validator: (value) =>
                                              value!.isEmpty
                                                  ? AppLocalizations.of(
                                                  context)!
                                                  .localizedString(
                                                  "enter_statement")
                                                  : null,
                                              onSaved: (value) =>
                                              loanStatementReferenceController,
                                              decoration: InputDecoration(
                                                border: UnderlineInputBorder(),
                                                labelText: AppLocalizations.of(
                                                    context)!
                                                    .localizedString(
                                                    "statement"),
                                                labelStyle: TextStyle(
                                                    color: Colors.green),
                                                counterText: "",
                                                icon: Icon(
                                                  Icons.supervised_user_circle,
                                                  color: Colors.green,
                                                  size: 20.0,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 40, left: 40, right: 40),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: Color(int.parse(
                                            loginbuttonColor.toString())),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 5,
                                            blurRadius: 7,
                                            offset: const Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ]),
                                    height: 50,
                                    width:
                                        MediaQuery.of(context).size.width - 120,
                                    child: TextButton(
                                      onPressed: () async {
                                        if (validateAndSave()) {
                                          if (showBiometricbuttonTransaction) {
                                            transactionPinMessage_Biometric();
                                          } else {
                                            transactionPinMessage();
                                          }
                                        } else {
                                          setState(() {
                                            _isLoading = false;
                                          });
                                        }
                                      },
                                      child: Text(
                                        AppLocalizations.of(context)!
                                            .localizedString("submit"),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateMPINForm() {
    final form3 = globalFormKey2.currentState;
    if (form3!.validate()) {
      form3.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
