// import 'dart:convert';
//
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'package:sahakari_pay/pmc/sahakaaripay/models/loading.dart';
// import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loan_page.dart';
// import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loaninformation/loaninformation_detailspage.dart';
// import 'package:sahakari_pay/pmc/utils/localizations.dart';
//
// class LoanInformationPage extends StatefulWidget {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//
//   LoanInformationPage({
//     Key? key,
//     this.coopList,
//     this.userId,
//     this.accesstoken,
//     this.balance,
//     this.baseUrl,
//     this.accountno,
//     this.primaryColor,
//     this.loginButtonTitleColor,
//     this.loginbuttonColor,
//     this.loginTextFieldColor,
//     this.dasboardIconColor,
//     this.dashboardTopTitleColor,
//     this.SecondaryColor,
//   }) : super(key: key);
//
//   @override
//   _LoanInformationPageState createState() => _LoanInformationPageState(
//         coopList,
//         userId,
//         accesstoken,
//         balance,
//         baseUrl,
//         accountno,
//         primaryColor,
//         loginButtonTitleColor,
//         loginbuttonColor,
//         loginTextFieldColor,
//         dasboardIconColor,
//         dashboardTopTitleColor,
//         SecondaryColor,
//       );
// }
//
// class _LoanInformationPageState extends State<LoanInformationPage> {
//   List? coopList;
//   String? userId;
//   String? accesstoken;
//   double? balance;
//   String? baseUrl;
//   String? accountno;
//   String? primaryColor;
//   String? loginButtonTitleColor;
//   String? loginbuttonColor;
//   String? loginTextFieldColor;
//   String? dasboardIconColor;
//   String? dashboardTopTitleColor;
//   String? SecondaryColor;
//   _LoanInformationPageState(
//     this.coopList,
//     this.userId,
//     this.accesstoken,
//     this.balance,
//     this.baseUrl,
//     this.accountno,
//     this.primaryColor,
//     this.loginButtonTitleColor,
//     this.loginbuttonColor,
//     this.loginTextFieldColor,
//     this.dasboardIconColor,
//     this.dashboardTopTitleColor,
//     this.SecondaryColor,
//   );
//
//   String? loanMessage;
//   String? loanTotalAmount;
//   String? loanFine;
//   String? loanPenalty;
//   String? loanInterest;
//   String? loanDuePrincipal;
//
//   bool showBalance = false;
//
//   postLoanInformation() async {
//     String url = "${baseUrl}api/v1/pes/account/get-loan-payment-information";
//     Map body = {
//       "AccountNumber": loanAccountNumberController.text.toString().trim(),
//     };
//     var jsonResponse;
//     var res = await http.post(Uri.parse(url),
//         body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
//     if (res.statusCode == 200) {
//       jsonResponse = json.decode(res.body);
//       print("Response status: ${jsonResponse}");
//       setState(() {
//         _isLoading = true;
//         if (jsonResponse != null) {
//           loanMessage = jsonResponse["Message"];
//           loanTotalAmount = jsonResponse["TotalAmount"].toString();
//           loanFine = jsonResponse["Fine"].toString();
//           loanPenalty = jsonResponse["Penalty"].toString();
//           loanInterest = jsonResponse["Interest"].toString();
//           loanDuePrincipal = jsonResponse["DuePrincipal"].toString();
//
//           setState(() {
//             _isLoading = false;
//           });
//
//           Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (context) => LoanInformationDetailsPage(
//                     coopList: coopList,
//                     userId: userId,
//                     accesstoken: accesstoken,
//                     balance: balance,
//                     baseUrl: baseUrl,
//                     accountno: accountno,
//                     loanMessage: loanMessage,
//                     loanTotalAmount: loanTotalAmount,
//                     loanFine: loanFine,
//                     loanPenalty: loanPenalty,
//                     loanInterest: loanInterest,
//                     loanDuePrincipal: loanDuePrincipal,
//                     primaryColor: primaryColor,
//                     loginButtonTitleColor: loginButtonTitleColor,
//                     loginbuttonColor: loginbuttonColor,
//                     loginTextFieldColor: loginTextFieldColor,
//                     dasboardIconColor: dasboardIconColor,
//                     dashboardTopTitleColor: dashboardTopTitleColor,
//                     SecondaryColor: SecondaryColor,
//                   )));
//         }
//       });
//     } else if (res.statusCode == 400) {
//       setState(() {
//         _isLoading = true;
//       });
//       jsonResponse = json.decode(res.body);
//       print(jsonResponse);
//       if (jsonResponse != null) {
//         setState(() {
//           _isLoading = false;
//           showDialog(
//               context: context,
//               builder: (BuildContext context) {
//                 return AlertDialog(
//                   title: Center(
//                       child: Column(
//                     children: [
//                       Text(
//                         AppLocalizations.of(context)!
//                             .localizedString("error_alert"),
//                         style: TextStyle(fontSize: 16),
//                       ),
//                       SizedBox(
//                         height: 10,
//                       ),
//                       Icon(
//                         Icons.add_alert,
//                         color: Colors.red,
//                         size: 50.0,
//                       )
//                     ],
//                   )),
//                   content: Text(
//                     "${jsonResponse}",
//                     style: TextStyle(fontSize: 14),
//                   ),
//                   actions: [
//                     TextButton(
//                         onPressed: () {
//                           Navigator.of(context).pop();
//                         },
//                         child: Text(
//                           AppLocalizations.of(context)!.localizedString("okay"),
//                         ))
//                   ],
//                 );
//               });
//         });
//       }
//     } else if (res.statusCode == 401) {
//       setState(() {
//         _isLoading = false;
//       });
//       jsonResponse = json.decode(res.body);
//       print(jsonResponse);
//       return showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Center(
//                   child: Column(
//                 children: [
//                   Text(
//                     AppLocalizations.of(context)!
//                         .localizedString("error_alert"),
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Icon(
//                     Icons.add_alert,
//                     color: Colors.red,
//                     size: 50.0,
//                   )
//                 ],
//               )),
//               content: Text(
//                 "${jsonResponse}",
//                 style: TextStyle(fontSize: 14),
//               ),
//               actions: [
//                 TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Text(
//                       AppLocalizations.of(context)!.localizedString("okay"),
//                     ))
//               ],
//             );
//           });
//     } else {
//       setState(() {
//         _isLoading = false;
//       });
//       jsonResponse = json.decode(res.body);
//       print("this is the error meesage: ${jsonResponse}");
//       return showDialog(
//           context: context,
//           builder: (BuildContext context) {
//             return AlertDialog(
//               title: Center(
//                   child: Column(
//                 children: [
//                   Text(
//                     AppLocalizations.of(context)!
//                         .localizedString("error_alert"),
//                     style: TextStyle(fontSize: 16),
//                   ),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   Icon(
//                     Icons.add_alert,
//                     color: Colors.red,
//                     size: 50.0,
//                   )
//                 ],
//               )),
//               content: Text(
//                 AppLocalizations.of(context)!
//                     .localizedString("something_went_wrong"),
//                 style: TextStyle(fontSize: 14),
//               ),
//               actions: [
//                 TextButton(
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                     child: Text(
//                       AppLocalizations.of(context)!.localizedString("okay"),
//                     ))
//               ],
//             );
//           });
//     }
//   }
//
//   movetoLoanpage() {
//     Navigator.of(context).pushAndRemoveUntil(
//         MaterialPageRoute(
//             builder: (context) => LoanPage(
//                   coopList: coopList,
//                   userId: userId,
//                   accesstoken: accesstoken,
//                   balance: balance,
//                   baseUrl: baseUrl,
//                   accountno: accountno,
//                   primaryColor: primaryColor,
//                   loginButtonTitleColor: loginButtonTitleColor,
//                   loginbuttonColor: loginbuttonColor,
//                   loginTextFieldColor: loginTextFieldColor,
//                   dasboardIconColor: dasboardIconColor,
//                   dashboardTopTitleColor: dashboardTopTitleColor,
//                   SecondaryColor: SecondaryColor,
//                 )),
//         (Route<dynamic> route) => false);
//   }
//
//   bool _isLoading = false;
//
//   final GlobalKey<FormState> globalFormKey = GlobalKey();
//
//   final TextEditingController loanAccountNumberController = TextEditingController();
//
//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       onWillPop: () {
//         return movetoLoanpage();
//       },
//       child: Scaffold(
//         body: _isLoading
//             ? Loading()
//             : SingleChildScrollView(
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Container(
//                       height: 150,
//                       decoration: BoxDecoration(
//                         color: Color(int.parse(primaryColor.toString())),
//                       ),
//                       child: Stack(
//                         children: [
//                           Positioned(
//                             left: 10.0,
//                             top: 40,
//                             child: IconButton(
//                                 onPressed: () {
//                                   movetoLoanpage();
//                                 },
//                                 icon: const Icon(
//                                   Icons.arrow_back,
//                                   size: 30.0,
//                                   color: Colors.white,
//                                 )),
//                           ),
//                           Positioned(
//                             left: 100,
//                             top: 50,
//                             child: Text(
//                               AppLocalizations.of(context)!
//                                   .localizedString("sahakaari_pay"),
//                               style: TextStyle(
//                                 color: Colors.white,
//                                 fontSize: 20.0,
//                               ),
//                             ),
//                           ),
//                           Positioned(
//                             left: 20,
//                             top: 95,
//                             child: Row(
//                               children: [
//                                 const SizedBox(
//                                   height: 30,
//                                   width: 30,
//                                   child: Image(
//                                       image: AssetImage(
//                                           "assets/images/wallet_icon.png")),
//                                 ),
//                                 const SizedBox(
//                                   width: 10.0,
//                                 ),
//                                 Text(
//                                   showBalance
//                                       ? AppLocalizations.of(context)!
//                                               .localizedString("rs") +
//                                           "${balance.toString()}"
//                                       : "xxx.xx".toUpperCase(),
//                                   style: const TextStyle(
//                                     color: Colors.white,
//                                     fontSize: 16.0,
//                                   ),
//                                 ),
//                                 Padding(
//                                   padding: const EdgeInsets.only(left: 180.0),
//                                   child: IconButton(
//                                       onPressed: () {
//                                         setState(() {
//                                           if (showBalance == false) {
//                                             showBalance = true;
//                                           } else {
//                                             showBalance = false;
//                                           }
//                                         });
//                                       },
//                                       icon: Icon(
//                                         showBalance
//                                             ? Icons.visibility
//                                             : Icons.visibility_off,
//                                         size: 25.0,
//                                         color: Colors.white,
//                                       )),
//                                 )
//                               ],
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                     const SizedBox(
//                       height: 20.0,
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(left: 20.0, right: 20.0),
//                       child: Center(
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Padding(
//                               padding: EdgeInsets.only(top: 30.0),
//                               child: Center(
//                                 child: Text(
//                                   AppLocalizations.of(context)!
//                                       .localizedString("loan_payment_information"),
//                                   style: TextStyle(
//                                     color: Colors.black87,
//                                     fontSize: 24.0,
//                                   ),
//                                 ),
//                               ),
//                             ),
//                             _isLoading
//                                 ? const Center(
//                                     child: Column(
//                                     children: [
//                                       Loading(),
//                                     ],
//                                   ))
//                                 : Form(
//                                     key: globalFormKey,
//                                     child: Column(
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.start,
//                                       children: [
//                                         Padding(
//                                           padding: const EdgeInsets.only(
//                                               left: 40.0, top: 10),
//                                           child: Container(
//                                             width: 280,
//                                             decoration: BoxDecoration(
//                                               borderRadius:
//                                                   BorderRadius.circular(20.0),
//                                             ),
//                                             child: TextFormField(
//                                               controller: loanAccountNumberController,
//                                               maxLength: 30,
//                                               keyboardType: TextInputType.text,
//                                               validator: (value) =>
//                                                   value!.isEmpty
//                                                       ? AppLocalizations.of(
//                                                               context)!
//                                                           .localizedString(
//                                                               "account_number_message")
//                                                       : null,
//                                               onSaved: (value) =>
//                                                   loanAccountNumberController,
//                                               decoration: InputDecoration(
//                                                 border: UnderlineInputBorder(),
//                                                 labelText: AppLocalizations.of(
//                                                         context)!
//                                                     .localizedString(
//                                                         "account_number"),
//                                                 labelStyle: TextStyle(
//                                                     color: Colors.green),
//                                                 counterText: "",
//                                                 icon: Icon(
//                                                   Icons.supervised_user_circle,
//                                                   color: Colors.green,
//                                                   size: 20.0,
//                                                 ),
//                                               ),
//                                             ),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                             Padding(
//                               padding: const EdgeInsets.only(
//                                   top: 40, left: 40, right: 40),
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Container(
//                                     decoration: BoxDecoration(
//                                         borderRadius:
//                                             BorderRadius.circular(10.0),
//                                         color: Color(int.parse(
//                                             loginbuttonColor.toString())),
//                                         boxShadow: [
//                                           BoxShadow(
//                                             color: Colors.grey.withOpacity(0.5),
//                                             spreadRadius: 5,
//                                             blurRadius: 7,
//                                             offset: const Offset(0,
//                                                 3), // changes position of shadow
//                                           ),
//                                         ]),
//                                     height: 50,
//                                     width:
//                                         MediaQuery.of(context).size.width - 120,
//                                     child: TextButton(
//                                       onPressed: () async {
//                                         if (validateAndSave()) {
//                                           setState(() {
//                                             _isLoading = true;
//                                           });
//                                           postLoanInformation();
//                                         } else {
//                                           setState(() {
//                                             _isLoading = false;
//                                           });
//                                         }
//                                       },
//                                       child: Text(
//                                         AppLocalizations.of(context)!
//                                             .localizedString("submit"),
//                                         style: TextStyle(
//                                             color: Colors.white,
//                                             fontSize: 16,
//                                             fontWeight: FontWeight.bold),
//                                       ),
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//       ),
//     );
//   }
//
//   bool validateAndSave() {
//     final form = globalFormKey.currentState;
//     if (form!.validate()) {
//       form.save();
//       return true;
//     } else {
//       return false;
//     }
//   }
// }
