import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loan_page.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';

class LoanInformationDetailsPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? loanMessage;
  String? loanTotalAmount;
  String? loanFine;
  String? loanPenalty;
  String? loanInterest;
  String? loanDuePrincipal;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  LoanInformationDetailsPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.loanMessage,
    this.loanTotalAmount,
    this.loanFine,
    this.loanPenalty,
    this.loanInterest,
    this.loanDuePrincipal,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _LoanInformationDetailsPageState createState() => _LoanInformationDetailsPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        loanMessage,
        loanTotalAmount,
        loanFine,
        loanPenalty,
        loanInterest,
        loanDuePrincipal,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _LoanInformationDetailsPageState extends State<LoanInformationDetailsPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? loanMessage;
  String? loanTotalAmount;
  String? loanFine;
  String? loanPenalty;
  String? loanInterest;
  String? loanDuePrincipal;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _LoanInformationDetailsPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.loanMessage,
    this.loanTotalAmount,
    this.loanFine,
    this.loanPenalty,
    this.loanInterest,
    this.loanDuePrincipal,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool showBalance = false;

  void initState() {
    super.initState();
  }

  movetoLoanInformationPage() {
    Navigator.of(context).pop();
    /*Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => LoanPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);*/
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoLoanInformationPage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 150,
                    decoration: BoxDecoration(
                      color: Color(int.parse(primaryColor.toString())),
                    ),
                    child: Stack(
                      children: [
                        Positioned(
                          left: 10.0,
                          top: 40,
                          child: IconButton(
                              onPressed: () {
                                movetoLoanInformationPage();
                              },
                              icon: const Icon(
                                Icons.arrow_back,
                                size: 30.0,
                                color: Colors.white,
                              )),
                        ),
                        Positioned(
                          left: 100,
                          top: 50,
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("sahakaari_pay"),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                        Positioned(
                          left: 20,
                          top: 95,
                          child: Row(
                            children: [
                              const SizedBox(
                                height: 30,
                                width: 30,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/wallet_icon.png")),
                              ),
                              const SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                showBalance
                                    ? AppLocalizations.of(context)!
                                    .localizedString("rs") +
                                    "${balance.toString()}"
                                    : "xxx.xx".toUpperCase(),
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 180.0),
                                child: IconButton(
                                    onPressed: () {
                                      setState(() {
                                        if (showBalance == false) {
                                          showBalance = true;
                                        } else {
                                          showBalance = false;
                                        }
                                      });
                                    },
                                    icon: Icon(
                                      showBalance
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      size: 25.0,
                                      color: Colors.white,
                                    )),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("loan_payment_information"),
                        style: TextStyle(
                            color: Colors.green[900],
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("loan_total_amount"),
                            style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text("$loanTotalAmount")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("loan_fine"),
                            style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text("$loanFine")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("loan_penalty"),
                            style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text("$loanPenalty")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("loan_interest"),
                            style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text("$loanInterest")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 30),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 3,
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("loan_due_principal"),
                            style: TextStyle(color: Colors.black87, fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 2.5,
                            child: Text("$loanDuePrincipal")),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
