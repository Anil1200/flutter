import 'dart:core';

import 'package:flutter/material.dart';
//import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loaninformation/loaninformation_page.dart';
//import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loanschedule/loanschedule_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/loan/loanpayment/loanpayment_page.dart';

import '../../../utils/localizations.dart';
import '../../pmc_homepage.dart';

class LoanPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  LoanPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<LoanPage> createState() => _LoanPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _LoanPageState extends State<LoanPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _LoanPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 70,
          backgroundColor: Color(int.parse(primaryColor.toString())),
          leading: IconButton(
            onPressed: () {
              movetohomepage();
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
          ),
          title: Text(
              AppLocalizations.of(context)!.localizedString("loan")),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /*InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LoanInformationPage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                        loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                        loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                        dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage("assets/images/pes_logo_rbg.png"),
                                width: 60,
                                height: 60,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("loan_payment_information"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LoanSchedulePage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                        loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                        loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                        dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage("assets/images/pes_logo_rbg.png"),
                                width: 60,
                                height: 60,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("loan_schedule"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 20),*/
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LoanPaymentPage(
                                        coopList: coopList,
                                        userId: userId,
                                        accesstoken: accesstoken,
                                        balance: balance,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                        loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                        loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                        dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                      )));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width / 3 - 20,
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage("assets/images/transferimage.png"),
                                width: 60,
                                height: 60,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("loan_payment"),
                                style: TextStyle(
                                    fontSize: 12, color: Colors.green),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
