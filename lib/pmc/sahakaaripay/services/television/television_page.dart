import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/dishhome/dishhome_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/merotv/mero_tv_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/prabhutv/prabhutv_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/simtv/simtv_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/skytv/skytv_page.dart';

import '../../../utils/localizations.dart';
import '../../pmc_homepage.dart';

class TelevisionPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  TelevisionPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  State<TelevisionPage> createState() => _TelevisionPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _TelevisionPageState extends State<TelevisionPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _TelevisionPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  movetohomepage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    color: Color(int.parse(primaryColor.toString())),
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        left: 10.0,
                        top: 40,
                        child: IconButton(
                            onPressed: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomePage(
                                            coopList: coopList,
                                            userId: userId,
                                            accesstoken: accesstoken,
                                            baseUrl: baseUrl,
                                            accountno: accountno,
                                            primaryColor: primaryColor,
                                            loginButtonTitleColor:
                                                loginButtonTitleColor,
                                            loginbuttonColor: loginbuttonColor,
                                            loginTextFieldColor:
                                                loginTextFieldColor,
                                            dasboardIconColor:
                                                dasboardIconColor,
                                            dashboardTopTitleColor:
                                                dashboardTopTitleColor,
                                            SecondaryColor: SecondaryColor,
                                          )));
                            },
                            icon: const Icon(
                              Icons.arrow_back,
                              size: 30.0,
                              color: Colors.white,
                            )),
                      ),
                      Positioned(
                        left: 100,
                        top: 50,
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("television"),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 40.0, top: 10),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SimTvPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/tv/sim_tv_logo.png")),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("sim_tv"),
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 40.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MeroTvPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image:
                                        AssetImage("assets/images/tv/mtv.png")),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("mero_tv"),
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 40.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DishHomePage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/tv/dishhome_logo.png")),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("dishhome"),
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 40.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SkyTvPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/tv/skytv_logo.png")),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("sky_tv"),
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30.0, top: 10),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PrabhuTvPage(
                                    coopList: coopList,
                                    userId: userId,
                                    accesstoken: accesstoken,
                                    balance: balance,
                                    baseUrl: baseUrl,
                                    accountno: accountno,
                                    primaryColor: primaryColor,
                                    loginButtonTitleColor:
                                        loginButtonTitleColor,
                                    loginbuttonColor: loginbuttonColor,
                                    loginTextFieldColor: loginTextFieldColor,
                                    dasboardIconColor: dasboardIconColor,
                                    dashboardTopTitleColor:
                                        dashboardTopTitleColor,
                                    SecondaryColor: SecondaryColor,
                                  )));
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 50,
                          width: 50,
                          child: Image(
                              image: AssetImage(
                                  "assets/images/tv/prabhutv_logo.png")),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          AppLocalizations.of(context)!
                              .localizedString("prabhu_tv"),
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
