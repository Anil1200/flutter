import 'package:flutter/material.dart';

import '../../../../utils/localizations.dart';

class CasIDPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  CasIDPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _CasIDPageState createState() => _CasIDPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _CasIDPageState extends State<CasIDPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _CasIDPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  final TextEditingController CasIDController = TextEditingController();
  final TextEditingController amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cas_id"),
                          style: TextStyle(
                            color: Colors.black87,
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 50.0,
                      ),
                      child: Container(
                        width: 260,
                        child: TextFormField(
                          controller: CasIDController,
                          maxLength: 11,
                          keyboardType: TextInputType.number,
                          validator: (value) => value!.isEmpty
                              ? AppLocalizations.of(context)!
                                  .localizedString("cas_id")
                              : null,
                          onSaved: (value) => CasIDController,
                          decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: AppLocalizations.of(context)!
                                .localizedString("cas_id"),
                            labelStyle: TextStyle(color: Colors.green),
                            counterText: "",
                            icon: Icon(
                              Icons.confirmation_number,
                              color: Colors.green,
                              size: 20.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 50.0,
                      ),
                      child: Container(
                        width: 260,
                        child: TextFormField(
                          controller: amountController,
                          maxLength: 10,
                          keyboardType: TextInputType.number,
                          validator: (value) => value!.isEmpty
                              ? AppLocalizations.of(context)!
                                  .localizedString("amount")
                              : null,
                          onSaved: (value) => amountController,
                          decoration: InputDecoration(
                            border: UnderlineInputBorder(),
                            labelText: AppLocalizations.of(context)!
                                .localizedString("amount"),
                            labelStyle: TextStyle(color: Colors.green),
                            counterText: "",
                            icon: Icon(
                              Icons.confirmation_number,
                              color: Colors.green,
                              size: 20.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 220.0, top: 20),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color:
                                Color(int.parse(loginbuttonColor.toString())),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: const Offset(
                                    0, 3), // changes position of shadow
                              ),
                            ]),
                        height: 50,
                        width: 100,
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("submit"),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        "Customer ID is 10 digits ID provided by"
                        " SIM TV while installing the SET TOP BOX whereas Cas ID is 11 digits.",
                        style: TextStyle(fontSize: 14, color: Colors.black87),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
