import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/dishhome/dishhome_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/dishhome/dishhomepdf_billpage.dart';

import '../../../../constants.dart';
import '../../../../local_auth_api.dart';
import '../../../../utils/localizations.dart';
import '../../../../utils/utils.dart';
import '../../../models/loading.dart';

class DishHomeBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? CustomerID;
  String? PackageName;
  String? ExpiryDate;
  String? Quantity;
  String? Zone;
  String? District;
  String? CustomerType;
  String? CustomerStatus;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  DishHomeBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.CustomerID,
    this.PackageName,
    this.ExpiryDate,
    this.Quantity,
    this.Zone,
    this.District,
    this.CustomerType,
    this.CustomerStatus,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _DishHomeBillPageState createState() => _DishHomeBillPageState(
    coopList,
    userId,
    accesstoken,
    balance,
    baseUrl,
    accountno,
    CustomerID,
    PackageName,
    ExpiryDate,
    Quantity,
    Zone,
    District,
    CustomerType,
    CustomerStatus,
    primaryColor,
    loginButtonTitleColor,
    loginbuttonColor,
    loginTextFieldColor,
    dasboardIconColor,
    dashboardTopTitleColor,
    SecondaryColor,
  );
}

class _DishHomeBillPageState extends State<DishHomeBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? CustomerID;
  String? PackageName;
  String? ExpiryDate;
  String? Quantity;
  String? Zone;
  String? District;
  String? CustomerType;
  String? CustomerStatus;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _DishHomeBillPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.CustomerID,
      this.PackageName,
      this.ExpiryDate,
      this.Quantity,
      this.Zone,
      this.District,
      this.CustomerType,
      this.CustomerStatus,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      );

  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();

  List? dishHomeAmount;

  bool showBalance = false;

  bool _isLoading = false;

  String? dropdownValue;

  @override
  void initState() {
    super.initState();
    getJsonData();
  }

  Future<String> getJsonData() async {
    String url =
        "http://merchant.sahakaari.com/merchant/api/v1/getDishHomePaymentList";
    final response = await http.get(Uri.parse(url), headers: {
      'Token': 'q7ipClb7vUEMuoBVR0ERm6eRIgf626AnSzu961yTnb',
      'API_KEY': 'ahdfyasskkG6ldf4tkidsjf5jhdsk90jfhgbsdtUhgre2BiyrP',
      'Authorization': "Bearer ${accesstoken}"
    });
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        jsonResponse = json.decode(response.body);
        dishHomeAmount = jsonResponse["payload"];
        print(dishHomeAmount);
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
    return "success";
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  showMessage() {
    //if (dropdownValue == 'Select Amount') {
    if (dropdownValue == null) {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("select_top_up_amount"),
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });

      if (showBiometricbuttonTransaction) {
        transactionPinMessageBiometric();
      } else {
        transactionPinMessage();
      }
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${dropdownValue} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${CustomerID.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                            });
                            Navigator.of(context).pop();
                            postTopupData();
                          } else {
                            setState(() {
                              _isLoading = false;
                            });
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style:
                                            const TextStyle(fontSize: 18),
                                          ),
                                          const SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${dropdownValue} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${CustomerID.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      postTopupData();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;

  postTopupData() async {
    String url = "${baseUrl}api/v1/billpayments";
    Map body = {
      "OperatorCode": "19",
      "Amount": dropdownValue.toString(),
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
      "Subscriber": CustomerID.toString().trim()
    };
    print("This is dody ${body}");
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          ReferenceId = jsonResponse["ReferenceId"];
          TransactionId = jsonResponse["TransactionId"];
          ReceiptNo = jsonResponse["ReceiptNo"];
          // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                    children: [
                      const Icon(
                        Icons.verified,
                        color: Colors.green,
                        size: 40,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Text(
                        jsonResponse["Message"],
                        style: const TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ],
                  )));
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DishHomePdfBillPage(
                        coopList: coopList,
                        userId: userId,
                        accesstoken: accesstoken,
                        balance: balance,
                        baseUrl: baseUrl,
                        accountno: accountno,
                        DishHomeId: CustomerID,
                        Amount: dropdownValue,
                        ReferenceId: ReferenceId,
                        TransactionId: TransactionId,
                        ReceiptNo: ReceiptNo,
                        PaymentBillDate: "",
                        primaryColor: primaryColor,
                        loginButtonTitleColor: loginButtonTitleColor,
                        loginbuttonColor: loginbuttonColor,
                        loginTextFieldColor: loginTextFieldColor,
                        dasboardIconColor: dasboardIconColor,
                        dashboardTopTitleColor: dashboardTopTitleColor,
                        SecondaryColor: SecondaryColor,
                      )));
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("error_alert"),
                            style: TextStyle(fontSize: 16),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 50.0,
                          )
                        ],
                      )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  movetoDishHomepage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DishHomePage(
              coopList: coopList,
              userId: userId,
              accesstoken: accesstoken,
              balance: balance,
              baseUrl: baseUrl,
              accountno: accountno,
              primaryColor: primaryColor,
              loginButtonTitleColor: loginButtonTitleColor,
              loginbuttonColor: loginbuttonColor,
              loginTextFieldColor: loginTextFieldColor,
              dasboardIconColor: dasboardIconColor,
              dashboardTopTitleColor: dashboardTopTitleColor,
              SecondaryColor: SecondaryColor,
            )));
  }

  final GlobalKey<FormState> globalFormKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoDishHomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(
                  color: Color(int.parse(primaryColor.toString())),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: 10.0,
                      top: 40,
                      child: IconButton(
                        onPressed: () {
                          movetoDishHomepage();
                        },
                        icon: const Icon(
                          Icons.arrow_back,
                          size: 30.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 100,
                      top: 50,
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("sahakaari_pay"),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20,
                      top: 95,
                      child: Row(
                        children: [
                          const SizedBox(
                            height: 30,
                            width: 30,
                            child: Image(
                                image: AssetImage(
                                    "assets/images/wallet_icon.png")),
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            showBalance
                                ? AppLocalizations.of(context)!
                                .localizedString("rs") +
                                "${balance.toString()}"
                                : "xxx.xx".toUpperCase(),
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 180.0),
                            child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (showBalance == false) {
                                      showBalance = true;
                                    } else {
                                      showBalance = false;
                                    }
                                  });
                                },
                                icon: Icon(
                                  showBalance
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  size: 25.0,
                                  color: Colors.white,
                                )),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: EdgeInsets.only(top: 30.0),
                child: Center(
                  child: Text(
                    AppLocalizations.of(context)!
                        .localizedString("dishhome_top_up"),
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 30.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0, top: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("customer_id"),
                      style:
                      TextStyle(color: Colors.black87, fontSize: 14),
                    ),
                    const SizedBox(
                      width: 45,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width/2,
                      child: Text(
                        "${CustomerID.toString()}",
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0, top: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("package"),
                      style:
                      TextStyle(color: Colors.black87, fontSize: 14),
                    ),
                    const SizedBox(
                      width: 70,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width/2,
                      child: Text(
                        "${PackageName.toString()}",
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0, top: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("quantity"),
                      style:
                      TextStyle(color: Colors.black87, fontSize: 14),
                    ),
                    const SizedBox(
                      width: 70,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width/2,
                      child: Text(
                        "${Quantity.toString()}",
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0, top: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("customer_type"),
                      style:
                      TextStyle(color: Colors.black87, fontSize: 14),
                    ),
                    const SizedBox(
                      width: 30,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width/2,
                      child: Text(
                        "${CustomerType.toString()}",
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40.0, top: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      AppLocalizations.of(context)!
                          .localizedString("customer_status"),
                      style:
                      TextStyle(color: Colors.black87, fontSize: 14),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width/2,
                      child: Text(
                        "${CustomerStatus.toString()}",
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 50.0, top: 30.0
                ),
                child: Container(
                  width: 260,
                  child: Form(
                      key: globalFormKey,
                      child: Column(
                        children: [
                          Container(
                            height: 50,
                            width: 300,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 25, vertical: 4),
                            decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.circular(10),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey
                                        .withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: const Offset(0,
                                        3), // changes position of shadow
                                  ),
                                ]),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                isExpanded: true,
                                value: dropdownValue,
                                alignment:
                                AlignmentDirectional.center,
                                hint: Text(
                                  "Select Amount",
                                  style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 14),
                                ),
                                // itemHeight: 90,
                                borderRadius:
                                BorderRadius.circular(20.0),
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black87,
                                ),
                                iconSize: 34,
                                elevation: 16,
                                style: const TextStyle(
                                    color: Colors.black87,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    dropdownValue = newValue!;
                                    print(
                                        "this is new amount: ${dropdownValue}");
                                  });
                                },
                                items:
                                dishHomeAmount?.map((items) {
                                  return DropdownMenuItem<String>(
                                      value: items["amount"],
                                      child:
                                      Text(items["amount"]));
                                }).toList(),
                              ),
                            ),
                          ),
                          // TextFormField(
                          //   controller: amountController,
                          //   maxLength: 10,
                          //   keyboardType: TextInputType.number,
                          //   validator: (value) => value!.isEmpty
                          //       ? AppLocalizations.of(context)!
                          //           .localizedString("amount")
                          //       : null,
                          //   onSaved: (value) => amountController,
                          //   decoration: InputDecoration(
                          //     border: UnderlineInputBorder(),
                          //     labelText:
                          //         AppLocalizations.of(context)!
                          //             .localizedString("amount"),
                          //     labelStyle:
                          //         TextStyle(color: Colors.green),
                          //     counterText: "",
                          //     icon: Icon(
                          //       Icons.money,
                          //       color: Colors.green,
                          //       size: 20.0,
                          //     ),
                          //   ),
                          // ),
                        ],
                      )),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 0, top: 40),
                child: Center(
                  child: Container(
                    height: 60,
                    width: 150,
                    decoration: BoxDecoration(
                        color: Color(
                            int.parse(loginbuttonColor.toString())),
                        borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                      onPressed: () {
                        if (validateAndSave()) {
                          setState(() {
                            _isLoading = true;
                          });
                          showMessage();
                        } else {
                          setState(() {
                            _isLoading = false;
                          });
                        }
                      },
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("submit"),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 14),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}