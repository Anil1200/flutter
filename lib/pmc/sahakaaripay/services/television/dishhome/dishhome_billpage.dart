import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:syncfusion_flutter_pdf/pdf.dart';

import '../../../../utils/localizations.dart';
import '../../../pmc_homepage.dart';
import '../../water/ubs_water/mobile.dart'
    if (dart.library.html) '../water/ubs_water/web.dart';

class DishHomePdfBillPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? DishHomeId;
  String? Amount;
  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  DishHomePdfBillPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.DishHomeId,
    this.Amount,
    this.ReferenceId,
    this.TransactionId,
    this.ReceiptNo,
    this.PaymentBillDate,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _DishHomePdfBillPageState createState() => _DishHomePdfBillPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        DishHomeId,
        Amount,
        ReferenceId,
        TransactionId,
        ReceiptNo,
        PaymentBillDate,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _DishHomePdfBillPageState extends State<DishHomePdfBillPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? DishHomeId;
  String? Amount;
  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _DishHomePdfBillPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.DishHomeId,
    this.Amount,
    this.ReferenceId,
    this.TransactionId,
    this.ReceiptNo,
    this.PaymentBillDate,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  void initState() {
    super.initState();
    this.nullcheck();
  }

  String? Rid;
  String? Tid;
  String? Rno;
  String? PayBillDate;

  nullcheck() {
    if (ReferenceId == null) {
      Rid = "ReferenceID not available.";
    } else {
      Rid = ReferenceId;
    }
    if (TransactionId == null) {
      Tid = "TransactionId not available.";
    } else {
      Tid = TransactionId;
    }
    if (ReceiptNo == null) {
      Rno = "ReceiptNo not available.";
    } else {
      Rno = ReceiptNo.toString();
    }
    if (PaymentBillDate == null) {
      PayBillDate = "Date not available.";
    } else {
      PayBillDate = PaymentBillDate.toString();
    }
  }

  Future<void> createPDF() async {
    // Create a new PDF document.
    final PdfDocument document = PdfDocument();
    //Add page to the PDF
    final page = document.pages.add();
    final pageSize = page.getClientSize();

    page.graphics.drawImage(PdfBitmap(await _readImageData('pes_logo_rbg.png')),
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 100));

    Uint8List fontData = await _readFontData("arial-unicode-ms.ttf");
    final PdfFont font = PdfTrueTypeFont(fontData, 20);

    page.graphics.drawString(
      'DishHome Bill Details',
      PdfStandardFont(
        PdfFontFamily.helvetica,
        30,
      ),
    );

    final grid = PdfGrid();
    grid.columns.add(count: 2);
    var row = grid.rows.add();
    row.cells[0].value = "ReferenceId:";
    row.cells[1].value = Rid.toString();

    row = grid.rows.add();
    row.cells[0].value = "TransactionId:";
    row.cells[1].value = Tid.toString();

    row = grid.rows.add();
    row.cells[0].value = "ReceiptNo: ";
    row.cells[1].value = "${Rno.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "DishHome Id: ";
    row.cells[1].value = "${DishHomeId.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Amount: ";
    row.cells[1].value = "${Amount.toString()}";

    row = grid.rows.add();
    row.cells[0].value = "Date: ";
    row.cells[1].value = "${timeforBill.toString()}";

    grid.style = PdfGridStyle(
      font: font,
      textBrush: PdfBrushes.black,
      cellPadding: PdfPaddings(left: 5, right: 2, top: 2, bottom: 2),
    );

    grid.applyBuiltInStyle(PdfGridBuiltInStyle.listTable4Accent5);
    grid.draw(
        page: page,
        bounds: Rect.fromLTWH(0, 50, 0, 0),
        format: PdfLayoutFormat(layoutType: PdfLayoutType.paginate));

    List<int> bytes = document.save();
    document.dispose();

    saveAndLaunchFile(bytes, 'Bill.pdf');
  }

  Future<Uint8List> _readImageData(String name) async {
    final data = await rootBundle.load('assets/images/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  Future<Uint8List> _readFontData(String name) async {
    final data = await rootBundle.load('assets/fonts/$name');
    return data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  }

  String? timeforBill = DateTime.now().year.toString() +
      "-" +
      DateTime.now().month.toString() +
      "-" +
      DateTime.now().day.toString() +
      " " +
      DateTime.now().hour.toString() +
      ":" +
      DateTime.now().minute.toString();

  movetoHomePage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          return movetoHomePage();
        },
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Center(
                    child: Text(
                      "DishHome Bill Details",
                      style: TextStyle(
                          color: Colors.green[900],
                          fontSize: 16,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "ReferenceId:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 100,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Rid}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Tid:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 100,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Tid}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "ReceiptNo:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 100,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Rno}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "DishHome Id:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 80,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${DishHomeId.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Amount:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 108,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${Amount.toString()}")),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Date:",
                        style: TextStyle(color: Colors.black87, fontSize: 14),
                      ),
                      const SizedBox(
                        width: 134,
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text("${timeforBill.toString()}")),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Divider(
                  thickness: 1,
                  color: Colors.green[700],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      width: 100,
                    ),
                    TextButton(
                        onPressed: () {
                          movetoHomePage();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("home"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                    const SizedBox(
                      width: 60,
                    ),
                    TextButton(
                        onPressed: createPDF,
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("pdf"),
                          style:
                              TextStyle(fontSize: 16, color: Colors.green[900]),
                        )),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
