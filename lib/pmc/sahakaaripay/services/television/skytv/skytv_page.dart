import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/skytv/skytvbill_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/television/television_page.dart';

import '../../../../utils/localizations.dart';
import '../../../models/loading.dart';

class SkyTvPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  SkyTvPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _SkyTvPageState createState() => _SkyTvPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _SkyTvPageState extends State<SkyTvPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _SkyTvPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;

  bool showBalance = false;

  List? renewalplans;
  String? paymentmessage;
  String? SkyTvUserName;
  String? CustomerName;

  Future<String> getjsonData() async {
    String url =
        "${baseUrl}api/v1/billpayment/skyTv/${skytvIDController.text.toString()}/serviceType/${serviceTypeController.text.toString()}/inquiry";
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    setState(() {
      _isLoading = true;
    });
    print(response.body);
    var jsonResponse;
    if (response.statusCode == 200) {
      setState(() {
        _isLoading = true;
        var jsonResponse = json.decode(response.body);
        print(jsonResponse);
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            renewalplans = jsonResponse["RenewalPlans"];
            paymentmessage = jsonResponse["PaymentMessage"];
            SkyTvUserName = jsonResponse["SkyTvUserName"];
            CustomerName = jsonResponse["CustomerName"];

            print("this is the renewal plan: ${renewalplans}");
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                        child: Column(
                      children: [
                        Text(
                          AppLocalizations.of(context)!
                              .localizedString("alert"),
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Icon(
                          Icons.add_alert,
                          color: Colors.red,
                          size: 50.0,
                        )
                      ],
                    )),
                    content: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message8") +
                          " ${CustomerName.toString()} ${paymentmessage.toString()}",
                      style: TextStyle(fontSize: 14),
                    ),
                    actions: [
                      Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("cancel"),
                                style: TextStyle(color: Colors.green[900]),
                              ),
                            ),
                            SizedBox(
                              width: 100,
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SkyTvBillPage(
                                                coopList: coopList,
                                                userId: userId,
                                                accesstoken: accesstoken,
                                                balance: balance,
                                                baseUrl: baseUrl,
                                                accountno: accountno,
                                                renewalplans: renewalplans,
                                                CustomerName: CustomerName,
                                                paymentmessage: paymentmessage,
                                                SkyTvUserName: SkyTvUserName,
                                                primaryColor: primaryColor,
                                                loginButtonTitleColor:
                                                    loginButtonTitleColor,
                                                loginbuttonColor:
                                                    loginbuttonColor,
                                                loginTextFieldColor:
                                                    loginTextFieldColor,
                                                dasboardIconColor:
                                                    dasboardIconColor,
                                                dashboardTopTitleColor:
                                                    dashboardTopTitleColor,
                                                SecondaryColor: SecondaryColor,
                                              )));
                                },
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("okay"),
                                  style: TextStyle(color: Colors.green[900]),
                                )),
                          ],
                        ),
                      )
                    ],
                  );
                });
          });
        }
      });
      return "success";
    } else if (response.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (response.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(response.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                AppLocalizations.of(context)!.localizedString("alert"),
              ),
              content: Text(
                AppLocalizations.of(context)!.localizedString("server_down"),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
    return "success";
  }

  final GlobalKey<FormState> globalFormKey = GlobalKey();

  movetohomepage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => TelevisionPage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  final TextEditingController skytvIDController = TextEditingController();
  final TextEditingController serviceTypeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                decoration: BoxDecoration(
                  color: Color(int.parse(primaryColor.toString())),
                ),
                child: Stack(
                  children: [
                    Positioned(
                      left: 10.0,
                      top: 40,
                      child: IconButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TelevisionPage(
                                          coopList: coopList,
                                          userId: userId,
                                          accesstoken: accesstoken,
                                          balance: balance,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                        )));
                          },
                          icon: const Icon(
                            Icons.arrow_back,
                            size: 30.0,
                            color: Colors.white,
                          )),
                    ),
                    Positioned(
                      left: 100,
                      top: 50,
                      child: Text(
                        AppLocalizations.of(context)!
                            .localizedString("sahakaari_pay"),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20,
                      top: 95,
                      child: Row(
                        children: [
                          const SizedBox(
                            height: 30,
                            width: 30,
                            child: Image(
                                image: AssetImage(
                                    "assets/images/wallet_icon.png")),
                          ),
                          const SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            showBalance
                                ? AppLocalizations.of(context)!
                                        .localizedString("rs") +
                                    "${balance.toString()}"
                                : "xxx.xx".toUpperCase(),
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 180.0),
                            child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (showBalance == false) {
                                      showBalance = true;
                                    } else {
                                      showBalance = false;
                                    }
                                  });
                                },
                                icon: Icon(
                                  showBalance
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  size: 25.0,
                                  color: Colors.white,
                                )),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 30.0),
                        child: Center(
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("sky_tv_top_up"),
                            style: TextStyle(
                              color: Colors.black87,
                              fontSize: 20.0,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 40.0, top: 10),
                        child: Container(
                          width: 300,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: _isLoading
                              ? Center(
                                  child: Column(
                                    children: const [
                                      Loading(),
                                    ],
                                  ),
                                )
                              : Form(
                                  key: globalFormKey,
                                  child: Column(
                                    children: [
                                      TextFormField(
                                        controller: skytvIDController,
                                        maxLength: 30,
                                        keyboardType: TextInputType.text,
                                        validator: (value) => value!.isEmpty
                                            ? "Please Enter ID"
                                            : null,
                                        onSaved: (value) => skytvIDController,
                                        decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                          ),
                                          labelText:
                                              AppLocalizations.of(context)!
                                                  .localizedString("enter_id"),
                                          labelStyle: const TextStyle(
                                              color: Colors.green),
                                          counterText: "",
                                          icon: const Icon(
                                            Icons.supervised_user_circle,
                                            color: Colors.green,
                                            size: 20.0,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 30.0),
                                        child: TextFormField(
                                          controller: serviceTypeController,
                                          maxLength: 30,
                                          keyboardType: TextInputType.text,
                                          validator: (value) => value!.isEmpty
                                              ? "Please Enter Service Type"
                                              : null,
                                          onSaved: (value) =>
                                              serviceTypeController,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                            ),
                                            labelText: "Enter Service Type",
                                            labelStyle: const TextStyle(
                                                color: Colors.green),
                                            counterText: "",
                                            icon: const Icon(
                                              Icons.tv,
                                              color: Colors.green,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 220.0, top: 40),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color:
                                  Color(int.parse(loginbuttonColor.toString())),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 5,
                                  blurRadius: 7,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ]),
                          height: 50,
                          width: 140,
                          child: TextButton(
                            onPressed: () {
                              if (validateAndSave()) {
                                setState(() {
                                  _isLoading = true;
                                });
                                getjsonData();
                              } else {
                                setState(() {
                                  _isLoading = false;

                                  // showDialog(
                                  //     context: context,
                                  //     builder: (BuildContext context) {
                                  //       return AlertDialog(
                                  //         title: Center(
                                  //           child: Column(
                                  //             children: const [
                                  //               Text(AppLocalizations.of(context)!.localizedString("alert")),
                                  //               SizedBox(
                                  //                 height: 10,
                                  //               ),
                                  //               Icon(
                                  //                 Icons.add_alert,
                                  //                 color: Colors.red,
                                  //                 size: 50,
                                  //               )
                                  //             ],
                                  //           ),
                                  //         ),
                                  //         content: Text(
                                  //             AppLocalizations.of(context)!.localizedString("valid_mobile"),),
                                  //         actions: [
                                  //           const Spacer(),
                                  //           Container(
                                  //             height: 40,
                                  //             width: 80,
                                  //             decoration: BoxDecoration(
                                  //                 color: Colors.green,
                                  //                 borderRadius:
                                  //                     BorderRadius.circular(
                                  //                         30)),
                                  //             child: TextButton(
                                  //               onPressed: () {
                                  //                 Navigator.of(context).pop();
                                  //               },
                                  //               child: Text(
                                  //                 AppLocalizations.of(context)!.localizedString("okay"),
                                  //                 style: TextStyle(
                                  //                     color: Colors.white),
                                  //               ),
                                  //             ),
                                  //           )
                                  //         ],
                                  //       );
                                  //     });
                                });
                              }
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("inquire_balance"),
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
