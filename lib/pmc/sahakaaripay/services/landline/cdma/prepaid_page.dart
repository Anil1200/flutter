import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
<<<<<<< HEAD
<<<<<<< HEAD
=======
import 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart' as ncp;
=======
import 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart';
import 'package:flutter_native_contact_picker/model/contact.dart';
>>>>>>> sudeep
import 'package:permission_handler/permission_handler.dart';
>>>>>>> sudeep
import 'package:sahakari_pay/pmc/sahakaaripay/services/landline/cdma/cdmapdf_billpage.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/landline/landline_page.dart';

import '../../../../constants.dart';
import '../../../../local_auth_api.dart';
import '../../../../utils/localizations.dart';
import '../../../../utils/utils.dart';
import '../../../models/loading.dart';

class PrePaidPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  PrePaidPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _PrePaidPageState createState() => _PrePaidPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _PrePaidPageState extends State<PrePaidPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _PrePaidPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();
  List<String> recipient = ["37001"];

  void initState() {
    super.initState();
    print("My SMS Code: ${coopList?[0]["smsCode"]}");
    // setState(() {
    //   CooperativeSharedPreferences().getsmsCode();
    //   print("this is the code:  ${smsCode}");
    // });
  }

  Future<PermissionStatus> getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    print("this is contacts permission: $permission");
    if (permission != PermissionStatus.granted) {
      final Map<Permission, PermissionStatus> permissionStatus = await [Permission.contacts].request();
      return permissionStatus[Permission.contacts] ?? PermissionStatus.denied;
    } else {
      return permission;
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  showMessage() {
    //if (dropdownValue != 'Select Amount') {
    if (amountController.value.text == "") {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("select_top_up_amount"),
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });

      if (showBiometricbuttonTransaction) {
        transactionPinMessageBiometric();
      } else {
        transactionPinMessage();
      }
    }
  }

  transactionPinMessage() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 210,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                              .localizedString("transaction_message") +
                          "${amountController.text} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${phoneNoController.text.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                          const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 10, left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        // validator: (value) => value!.isEmpty
                        //     ? AppLocalizations.of(context)!
                        //         .localizedString("enter_transaction_pin")
                        //     : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            Navigator.of(context).pop();
                            postTopupData();
                          } else {
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        child: Text(
                                          AppLocalizations.of(context)!
                                              .localizedString("okay"),
                                          style: TextStyle(
                                              color: Colors.green[900]),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  transactionPinMessageBiometric() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("alert"),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                                  Icons.add_alert,
                                  color: Colors.white,
                                  size: 30,
                                )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 20),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("transaction_message") +
                          "${amountController.text} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1") +
                          " ${phoneNoController.text.toString()} " +
                          AppLocalizations.of(context)!
                              .localizedString("transaction_message1a"),
                      style:
                      const TextStyle(fontSize: 14, color: Colors.black87),
                      // textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          Navigator.of(context).pop();
                          showBiometricTransaction();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  showBiometricTransaction() async {
    final isAuthenticated = await LocalAuthApi.authenticate();
    if (isAuthenticated) {
      setState(() {
        _isLoading = true;
      });
      postTopupData();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Cancelled by user.")));
    }
  }

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? paymentmessage;

  postTopupData() async {
    String url = "${baseUrl}api/v1/topup";
    Map body = {
      "OperatorCode": "9",
      "Amount": amountController.text.toString().trim(),
      "MPIN": showBiometricbuttonTransaction ? transactionMPIN : mpincontroller.text.toString().trim(),
      "MobileNo": phoneNoController.text.toString().trim(),
    };
    mpincontroller.clear();
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;

        if (jsonResponse != null) {
          paymentmessage = jsonResponse["Message"];
          ReferenceId = jsonResponse["ReferenceId"];
          TransactionId = jsonResponse["TransactionId"];
          ReceiptNo = jsonResponse["ReceiptNo"];
          // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Row(
                children: [
                  const Icon(
                    Icons.verified,
                    color: Colors.green,
                    size: 40,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Text(
                    jsonResponse["Message"],
                    style: const TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ],
              )));
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CDMAPdfBillPage(
                            coopList: coopList,
                            userId: userId,
                            accesstoken: accesstoken,
                            balance: balance,
                            baseUrl: baseUrl,
                            accountno: accountno,
                            phoneNo: phoneNoController.text.toString(),
                            Amount: amountController.text.toString(),
                            paymentmessage: paymentmessage,
                            // renewalplans: renewalplans,
                            // planName: planName,
                            ReferenceId: ReferenceId,
                            TransactionId: TransactionId,
                            ReceiptNo: ReceiptNo,
                            PaymentBillDate: PaymentBillDate,
                            primaryColor: primaryColor,
                            loginButtonTitleColor: loginButtonTitleColor,
                            loginbuttonColor: loginbuttonColor,
                            loginTextFieldColor: loginTextFieldColor,
                            dasboardIconColor: dasboardIconColor,
                            dashboardTopTitleColor: dashboardTopTitleColor,
                            SecondaryColor: SecondaryColor,
                          )));
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .localizedString("error_alert"),
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                        ))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .localizedString("error_alert"),
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    }
  }

  movetoLandLinePage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LandLinePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  balance: balance,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )));
  }

  String dropdownValue = 'Select Amount';
  final TextEditingController phoneNoController = TextEditingController();
  final TextEditingController amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return movetoLandLinePage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 30.0),
                      child: Center(
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 30.0),
                                child: Center(
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("cdma_prepaid_top_up"),
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 20.0,
                              ),
                              Center(
                                child: Container(
                                  width: 260,
                                  child: Center(
                                    child: Form(
                                      key: globalFormKey,
                                      child: Column(
                                        children: [
<<<<<<< HEAD
                                          TextFormField(
                                            controller: phoneNoController,
                                            maxLength: 10,
                                            keyboardType: TextInputType.number,
                                            validator: (value) => value!.isEmpty
                                                ? AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "enter_phone_number")
                                                : null,
                                            onSaved: (value) =>
                                                phoneNoController,
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(),
                                              labelText:
                                                  AppLocalizations.of(context)!
                                                      .localizedString(
                                                          "phone_number"),
                                              labelStyle: TextStyle(
                                                  color: Colors.green),
                                              counterText: "",
                                              icon: Icon(
                                                Icons.confirmation_number,
                                                color: Colors.green,
                                                size: 20.0,
=======
                                          Row(
                                            children: [
                                              SizedBox(
                                                width: MediaQuery.of(context).size.width - 170,
                                                child: TextFormField(
                                                  controller: phoneNoController,
                                                  maxLength: 15,
                                                  keyboardType: TextInputType.number,
                                                  inputFormatters: <TextInputFormatter>[
                                                    FilteringTextInputFormatter.allow(RegExp("[0-9]")),
                                                  ],
                                                  validator: (value) => value!.isEmpty
                                                      ? AppLocalizations.of(context)!
                                                          .localizedString(
                                                              "enter_phone_number")
                                                      : null,
                                                  onSaved: (value) =>
                                                      phoneNoController,
                                                  decoration: InputDecoration(
                                                    border: UnderlineInputBorder(),
                                                    labelText:
                                                        AppLocalizations.of(context)!
                                                            .localizedString(
                                                                "phone_number"),
                                                    labelStyle: TextStyle(
                                                        color: Colors.green),
                                                    counterText: "",
                                                    icon: Icon(
                                                      Icons.confirmation_number,
                                                      color: Colors.green,
                                                      size: 20.0,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              IconButton(
                                                onPressed: () async {
                                                  if (Platform.isAndroid) {
                                                    final PermissionStatus permissionStatus = await getPermission();
                                                    if (permissionStatus == PermissionStatus.granted) {
                                                      Contact? contact = await FlutterNativeContactPicker().selectContact();
                                                      var selectedPhoneNumber = contact!.phoneNumbers!.first.toString();
                                                      selectedPhoneNumber = selectedPhoneNumber.replaceAll(RegExp('[^0-9]'), '');
                                                      phoneNoController.text = selectedPhoneNumber;
                                                    } else {
                                                      ScaffoldMessenger.of(context)
                                                          .showSnackBar(const SnackBar(content: Text("No Permission Granted. Grant Permission in Settings.")));
                                                    }
                                                  } else {
                                                    Contact? contact = await FlutterNativeContactPicker().selectContact();
                                                    var selectedPhoneNumber = contact!.phoneNumbers!.first.toString();
                                                    selectedPhoneNumber = selectedPhoneNumber.replaceAll(RegExp('[^0-9]'), '');
                                                    phoneNoController.text = selectedPhoneNumber;
                                                  }
                                                },
                                                icon: Image.asset('assets/images/phonebook.png'),
                                                iconSize: 32,
>>>>>>> sudeep
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          TextFormField(
                                            controller: amountController,
                                            maxLength: 10,
                                            keyboardType: TextInputType.number,
                                            validator: (value) => value!.isEmpty
                                                ? AppLocalizations.of(context)!
                                                    .localizedString("amount")
                                                : null,
                                            onSaved: (value) =>
                                                amountController,
                                            decoration: InputDecoration(
                                              border: UnderlineInputBorder(),
                                              labelText: AppLocalizations.of(
                                                      context)!
                                                  .localizedString("amount"),
                                              labelStyle: const TextStyle(
                                                  color: Colors.green),
                                              counterText: "",
                                              icon: const Icon(
                                                Icons.money,
                                                color: Colors.green,
                                                size: 20.0,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 220.0, top: 20),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      color: Color(int.parse(
                                          loginbuttonColor.toString())),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: const Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  height: 50,
                                  width: 100,
                                  child: TextButton(
                                    onPressed: () async {
                                      if (validateAndSave() &&
                                          validateMobile(
                                              phoneNoController.text) &&
                                          validateAmount(double.parse(
                                              amountController.text))) {
                                        setState(() {
                                          _isLoading = true;
                                        });
                                        showMessage();
                                      } else {
                                        setState(() {
                                          _isLoading = false;
                                          if (!validateMobile(
                                              phoneNoController.text)) {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Center(
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            AppLocalizations.of(
                                                                    context)!
                                                                .localizedString(
                                                                    "alert"),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          Icon(
                                                            Icons.add_alert,
                                                            color: Colors.red,
                                                            size: 50,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    content: Text(
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "valid_mobile"),
                                                    ),
                                                    actions: [
                                                      TextButton(
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: Text(
                                                          AppLocalizations.of(
                                                                  context)!
                                                              .localizedString(
                                                                  "okay"),
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .green[900]),
                                                        ),
                                                      )
                                                    ],
                                                  );
                                                });
                                          }
                                          if (!validateAmount(double.parse(
                                              amountController.text))) {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return AlertDialog(
                                                    title: Center(
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            AppLocalizations.of(
                                                                    context)!
                                                                .localizedString(
                                                                    "alert"),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          Icon(
                                                            Icons.add_alert,
                                                            color: Colors.red,
                                                            size: 50,
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    content: Text(
                                                      AppLocalizations.of(
                                                              context)!
                                                          .localizedString(
                                                              "minimum_amount"),
                                                    ),
                                                    actions: [
                                                      TextButton(
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: Text(
                                                          AppLocalizations.of(
                                                                  context)!
                                                              .localizedString(
                                                                  "okay"),
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .green[900]),
                                                        ),
                                                      )
                                                    ],
                                                  );
                                                });
                                          }
                                        });
                                      }
                                    },
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("submit"),
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMobile(String value) {
    String pattern = r'(^(974|976)?[0-9]{7}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateAmount(double value) {
    if (value < 10) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }
}
