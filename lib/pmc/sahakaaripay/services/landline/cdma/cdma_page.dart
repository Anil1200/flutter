import 'package:flutter/material.dart';
import 'package:sahakari_pay/pmc/constants.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/landline/cdma/postpaid_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/landline/cdma/prepaid_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/services/topup/topup_page.dart';

import '../../../../utils/localizations.dart';
import '../landline_page.dart';

class CDMAPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  CDMAPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _CDMAPageState createState() => _CDMAPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _CDMAPageState extends State<CDMAPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _CDMAPageState(
      this.coopList,
      this.userId,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor);

  bool showBalance = false;

  movetoLandLinePage() {
    if (checkCDMAtopup == true) {
      checkCDMAtopup = false;
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => TopUpPage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                  )));
    } else {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => LandLinePage(
                    coopList: coopList,
                    userId: userId,
                    accesstoken: accesstoken,
                    balance: balance,
                    baseUrl: baseUrl,
                    accountno: accountno,
                    primaryColor: primaryColor,
                    loginButtonTitleColor: loginButtonTitleColor,
                    loginbuttonColor: loginbuttonColor,
                    loginTextFieldColor: loginTextFieldColor,
                    dasboardIconColor: dasboardIconColor,
                    dashboardTopTitleColor: dashboardTopTitleColor,
                    SecondaryColor: SecondaryColor,
                  )));
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetoLandLinePage();
      },
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(
                Icons.arrow_back,
                size: 30,
                color: Colors.white,
              ),
              onPressed: () {
                movetoLandLinePage();
              },
            ),
            backgroundColor: Color(int.parse(primaryColor.toString())),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30))),
            toolbarHeight: 110,
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Text(
                    AppLocalizations.of(context)!
                        .localizedString("sahakaari_pay"),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.white),
                  ),
                ),
                Row(
                  children: [
                    const SizedBox(
                      height: 20,
                      width: 20,
                      child: Image(
                          image: AssetImage("assets/images/wallet_icon.png")),
                    ),
                    const SizedBox(
                      width: 10.0,
                    ),
                    Text(
                      showBalance
                          ? AppLocalizations.of(context)!
                                  .localizedString("rs") +
                              "${balance.toString()}"
                          : "xxx.xx".toUpperCase(),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 150.0),
                      child: IconButton(
                          onPressed: () {
                            setState(() {
                              if (showBalance == false) {
                                showBalance = true;
                              } else {
                                showBalance = false;
                              }
                            });
                          },
                          icon: Icon(
                            showBalance
                                ? Icons.visibility
                                : Icons.visibility_off,
                            size: 25.0,
                            color: Colors.white,
                          )),
                    ),
                  ],
                )
              ],
            ),
            bottom: PreferredSize(
              preferredSize: const Size(60, 60),
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 10.0, left: 10.0, right: 10),
                child: Container(
                  height: 60,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ]),
                  child: TabBar(
                      indicatorColor: Colors.red,
                      indicator: UnderlineTabIndicator(
                        borderSide: BorderSide(width: 3.0, color: Colors.red),
                        insets: EdgeInsets.symmetric(
                          horizontal: 30.0,
                        ),
                      ),
                      labelColor: Colors.black87,
                      tabs: [
                        Tab(
                          text: AppLocalizations.of(context)!
                              .localizedString("cdma_prepaid"),
                          icon: Icon(
                            Icons.payments_outlined,
                            color: Colors.red,
                          ),
                        ),
                        Tab(
                          text: AppLocalizations.of(context)!
                              .localizedString("cdma_postpaid"),
                          icon: Icon(
                            Icons.payments_outlined,
                            color: Colors.red,
                          ),
                        )
                      ]),
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: [
              PrePaidPage(
                coopList: coopList,
                userId: userId,
                accesstoken: accesstoken,
                balance: balance,
                baseUrl: baseUrl,
                accountno: accountno,
                primaryColor: primaryColor,
                loginButtonTitleColor: loginButtonTitleColor,
                loginbuttonColor: loginbuttonColor,
                loginTextFieldColor: loginTextFieldColor,
                dasboardIconColor: dasboardIconColor,
                dashboardTopTitleColor: dashboardTopTitleColor,
                SecondaryColor: SecondaryColor,
              ),
              PostPaidPage(
                coopList: coopList,
                userId: userId,
                accesstoken: accesstoken,
                balance: balance,
                baseUrl: baseUrl,
                accountno: accountno,
                primaryColor: primaryColor,
                loginButtonTitleColor: loginButtonTitleColor,
                loginbuttonColor: loginbuttonColor,
                loginTextFieldColor: loginTextFieldColor,
                dasboardIconColor: dasboardIconColor,
                dashboardTopTitleColor: dashboardTopTitleColor,
                SecondaryColor: SecondaryColor,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
