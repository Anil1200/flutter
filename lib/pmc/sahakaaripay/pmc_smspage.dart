import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:sahakari_pay/main.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/adsl/adsl_service.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/faq.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/landline/cdma/cdma_page.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/landline/telephone/offlineTelephone.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/landline/utl/offlineUtl.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/offlineNtc/postpaid_topup.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/offlineNtc/prepaid_topup.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/olaboutUs.dart';
// import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/olaboutUs/olsettings.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/recharge/offlineRecharge.dart';
import 'package:sahakari_pay/pmc/sahakaaripay/offlinenavScreens/television/simtv/offlineSimTv.dart';

import '../utils/localizations.dart';
import 'offlinenavScreens/ncell/ncell_topup.dart';

class PMCsmsPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  PMCsmsPage(
      {Key? key,
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode})
      : super(key: key);

  @override
  _PMCsmsPageState createState() => _PMCsmsPageState(
      coopList,
      accesstoken,
      balance,
      baseUrl,
      accountno,
      primaryColor,
      loginButtonTitleColor,
      loginbuttonColor,
      loginTextFieldColor,
      dasboardIconColor,
      dashboardTopTitleColor,
      SecondaryColor,
      smsCode);
}

class _PMCsmsPageState extends State<PMCsmsPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  String? smsCode;
  _PMCsmsPageState(
      this.coopList,
      this.accesstoken,
      this.balance,
      this.baseUrl,
      this.accountno,
      this.primaryColor,
      this.loginButtonTitleColor,
      this.loginbuttonColor,
      this.loginTextFieldColor,
      this.dasboardIconColor,
      this.dashboardTopTitleColor,
      this.SecondaryColor,
      this.smsCode);

  bool _isLoading = false;
  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();
  List<String> recipient = ["37001"];

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  void _sendSMS(String message, List<String> recipient) async {
    String _result = await sendSMS(message: message, recipients: recipient)
        .catchError((onError) {
      print(onError);
    });
    print(_result);
  }

  moveToPMCLoginPage() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SahakariScreenPage()),
        (Route<dynamic> route) => false);
  }

  DialogBox() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Column(
              children: [
                Center(
                    child: Text(
                  AppLocalizations.of(context)!.localizedString("alert"),
                )),
                SizedBox(
                  height: 10.0,
                ),
                Center(
                  child: Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 30,
                  ),
                )
              ],
            ),
            content: SizedBox(
              height: 150,
              width: 350,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("confirm_pin"),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(
                      AppLocalizations.of(context)!
                          .localizedString("enter_transaction_pin"),
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Form(
                    key: globalFormKey,
                    child: TextFormField(
                      controller: mpincontroller,
                      maxLength: 4,
                      keyboardType: TextInputType.number,
                      validator: (value) => value!.isEmpty
                          ? AppLocalizations.of(context)!
                              .localizedString("enter_transaction_pin")
                          : null,
                      onSaved: (value) => mpincontroller,
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: AppLocalizations.of(context)!
                            .localizedString("transaction_pin"),
                        labelStyle: TextStyle(color: Colors.green),
                        counterText: "",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(30)),
                    child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.white),
                        )),
                  ),
                  const Spacer(),
                  Container(
                    height: 40,
                    width: 80,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.circular(30)),
                    child: TextButton(
                        onPressed: () async {
                          if (validateAndSave() &&
                              validateMPIN(mpincontroller.text)) {
                            _sendSMS("${smsCode} ${mpincontroller.text} BE",
                                recipient);
                          } else {
                            return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Center(
                                      child: Column(
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("alert"),
                                            style: TextStyle(fontSize: 18),
                                          ),
                                          SizedBox(height: 10.0),
                                          Icon(
                                            Icons.add_alert,
                                            size: 40,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                    ),
                                    content: Text(
                                      AppLocalizations.of(context)!
                                          .localizedString("enter_valid_pin"),
                                    ),
                                    actions: [
                                      const Spacer(),
                                      Container(
                                        height: 40,
                                        width: 80,
                                        decoration: BoxDecoration(
                                            color: Colors.green,
                                            borderRadius:
                                                BorderRadius.circular(30)),
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Text(
                                            AppLocalizations.of(context)!
                                                .localizedString("okay"),
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      )
                                    ],
                                  );
                                });
                          }
                          Navigator.of(context).pop();
                          // Navigator.push(
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => PMCsmsPage()));
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.white),
                        )),
                  ),
                ],
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return moveToPMCLoginPage();
      },
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 50,
          centerTitle: true,
          backgroundColor: Color(int.parse(primaryColor.toString())),
          title: Text(
            AppLocalizations.of(context)!.localizedString("sahakaari_pay"),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.white,
                  image: DecorationImage(
                    image: AssetImage("assets/images/spb.jpg"),
                  ),
                ),
                child: null,
              ),
              ListTile(
                leading: const Icon(Icons.account_balance),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("account_balance"),
                ),
                onTap: () {
                  DialogBox();
                },
              ),
              // ListTile(
              //   leading: const Icon(Icons.settings),
              //   title: Text(
              //     AppLocalizations.of(context)!.localizedString("settings"),
              //   ),
              //   onTap: () {
              //     Navigator.push(
              //         context,
              //         MaterialPageRoute(
              //             builder: (context) => const OffLineSettingsPage()));
              //   },
              // ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage(
                          "assets/images/menu/ic_menu_about_us.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("about_us"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => OffLineAboutUsPage(
                              coopList: coopList,
                              accesstoken: accesstoken,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                              smsCode: smsCode)));
                },
              ),
              ListTile(
                leading: const SizedBox(
                  width: 30,
                  height: 25,
                  child: Image(
                      image: AssetImage("assets/images/menu/ic_menu_faq.png")),
                ),
                title: Text(
                  AppLocalizations.of(context)!.localizedString("faq"),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FAQPage(
                              coopList: coopList,
                              accesstoken: accesstoken,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                              smsCode: smsCode)));
                },
              ),
              ListTile(
                leading: const Icon(Icons.logout),
                title: Text(
                  AppLocalizations.of(context)!
                      .localizedString("switch_internet"),
                ),
                onTap: () {
                  moveToPMCLoginPage();
                },
              ),
            ],
          ),
        ),
        body: WillPopScope(
          onWillPop: () {
            return moveToPMCLoginPage();
          },
          child: SingleChildScrollView(
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0, top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineNTCPrePaid(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode)));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/topup/ntc_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 10.0),
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("prepaid_top_up"),
                                  style: const TextStyle(
                                    color: Colors.green,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 40.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineNcellPage(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode)));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/topup/ncell_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 10.0),
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("ncell_top_up"),
                                  style: const TextStyle(
                                    color: Colors.green,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 40.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineRechargePage(
                                          coopList: coopList,
                                          accesstoken: accesstoken,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                          smsCode: smsCode,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/recharge_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 10.0),
                                child: Text(
                                  AppLocalizations.of(context)!
                                      .localizedString("recharge_cards"),
                                  style: const TextStyle(
                                    color: Colors.green,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 35.0, top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineTelephonePage(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode)));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/landline/landline_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("landline"),
                                style: const TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 83.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineNTCPostPaid(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode)));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/topup/ntc_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("postpaid"),
                                style: const TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 80.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineADSLPage(
                                          coopList: coopList,
                                          accesstoken: accesstoken,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                          smsCode: smsCode,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/internet/ntc_adsl.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("adsl"),
                                style: const TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 35.0, top: 10),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineUTLPage(
                                          coopList: coopList,
                                          accesstoken: accesstoken,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                          smsCode: smsCode,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/landline/utl_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("utl"),
                                style: const TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 85.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineSimTvPage(
                                        coopList: coopList,
                                        accesstoken: accesstoken,
                                        baseUrl: baseUrl,
                                        accountno: accountno,
                                        primaryColor: primaryColor,
                                        loginButtonTitleColor:
                                            loginButtonTitleColor,
                                        loginbuttonColor: loginbuttonColor,
                                        loginTextFieldColor:
                                            loginTextFieldColor,
                                        dasboardIconColor: dasboardIconColor,
                                        dashboardTopTitleColor:
                                            dashboardTopTitleColor,
                                        SecondaryColor: SecondaryColor,
                                        smsCode: smsCode)));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/tv/sim_tv_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("sim_tv"),
                                style: const TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          width: 83.0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => OffLineCDMAPage(
                                          coopList: coopList,
                                          accesstoken: accesstoken,
                                          baseUrl: baseUrl,
                                          accountno: accountno,
                                          primaryColor: primaryColor,
                                          loginButtonTitleColor:
                                              loginButtonTitleColor,
                                          loginbuttonColor: loginbuttonColor,
                                          loginTextFieldColor:
                                              loginTextFieldColor,
                                          dasboardIconColor: dasboardIconColor,
                                          dashboardTopTitleColor:
                                              dashboardTopTitleColor,
                                          SecondaryColor: SecondaryColor,
                                          smsCode: smsCode,
                                        )));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(
                                height: 50,
                                width: 50,
                                child: Image(
                                    image: AssetImage(
                                        "assets/images/topup/ntc_logo.png")),
                              ),
                              const SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                AppLocalizations.of(context)!
                                    .localizedString("cdma"),
                                style: const TextStyle(
                                  color: Colors.green,
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
