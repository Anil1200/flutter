import 'dart:convert';

class HistoryModel {
  String? TransactionType = "";
  String? TransactionAmount = "";
  String? CreatedDate = "";
  String? Remarks = "";
  String? Status = "";
  String? TotalCount = "";

  HistoryModel(
      {this.TransactionType,
      this.TransactionAmount,
      this.CreatedDate,
      this.Remarks,
      this.Status,
      this.TotalCount});

  factory HistoryModel.fromJson(Map<String, dynamic> jsons) {
    var remarks = "";
    var statements;
    String? statement;
    // statements = json.decode(remarks);
    // print("this is the response statements:${statements}");
    try {
      if (jsons["Response"] != "null") {
        remarks = jsons["Response"];
        print("this is the request remarks :${remarks}");
        statements = json.decode(remarks);
        statement = statements["Message"];
        print("this is the required statement value: ${statement}");
      } else {
        statement = "No Remarks";
      }
    } on FormatException catch (e) {
      statement = "No Remarks";
    }
    // remarks = "{${remarks}}";
    // remarks = jsonDecode(remarks);

    // // print('this sis the message:${remarks["Message"]}');
    // if (jsons["Status"] == "Fail") {
    //   remarks = jsons["Response"];
    // } else {
    //   print("this is the remarks${remarks}");
    //   remarks = remarks.split("Message")[1].split("TransactionId")[0];
    //   remarks = remarks.substring(3, remarks.length - 4);
    //   remarks = remarks.replaceAll('\r\n', '');
    //   print("this is the split remarks:${remarks}");
    // }
    return HistoryModel(
        TransactionType: jsons['TransactionType'],
        TransactionAmount: jsons['TransactionAmount'].toString(),
        CreatedDate: jsons['CreatedDate'],
        Remarks: statement,
        Status: jsons['Status'],
        TotalCount: jsons['TotalCount'].toString());
  }

  // Map<String, dynamic> toJson() {
  //   final Map<String, dynamic> data = new Map<String, dynamic>();
  //   data['Title'] = this.TransactionType;
  //   data['FirstName'] = this.TransactionAmount;
  //   data['LastName'] = this.LastName;
  //   return data;
  // }
}
