/// success : true
/// status : "success"
/// payload : [{"id":1,"coopName":"PMC","coopShortName":"PES","logoUrl":"https://testmerchant.sahakaari.com/images/PMC.png","baseUrl":"http://pathway.sahakaari.com/","smsKeyword":"Pathways ","aboutUs":"This is about us","faq":"this is faq","primaryColor":"#41a124","secondaryColor":"#e8fcd7","loginPrimary":"#41a124","loginSemiTransparent":"#e8fcd7","loginTextField":"#ffffff","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#1a6404","kButtonTitle":"#000000","smsCode":"Pathways ","textFieldBackground":"#c7f8b8","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":2,"coopName":"Sudarshan","coopShortName":"Sudarshan","logoUrl":"https://testmerchant.sahakaari.com/images/sudarshan.png","baseUrl":"https://sudarshanpathway.sahakaari.com/","smsKeyword":"Sudarshan","aboutUs":"this is test about us","faq":"this is test faq","primaryColor":"#9e0d7c","secondaryColor":"#9e0d7c","loginPrimary":"#4aa147","loginSemiTransparent":"#e8fcd7","loginTextField":"#4aa147","loginButtonBackground":"#4aa147","loginButtonTitle":"#4aa147","dashboardTopIcon":"#4aa147","dashboardTopTitle":"#4aa147","primaryDark":"#4aa147","kButtonTitle":"#4aa147","smsCode":"Sudarshan","textFieldBackground":"#c7f8b8","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":3,"coopName":"Hamro Namuna","coopShortName":"Hamro","logoUrl":"https://testmerchant.sahakaari.com/images/namuna.png","baseUrl":"https://hamronamunapathway.sahakaari.com/","smsKeyword":"Hamro","aboutUs":"this is tes","faq":"THIS IS TEST","primaryColor":"#b92930","secondaryColor":"#f6e4d6","loginPrimary":"#b92930","loginSemiTransparent":"#f6e4d6","loginTextField":"#ffffff","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#b92930","dashboardTopTitle":"#000000","primaryDark":"#b92930","kButtonTitle":"#FFFFFF","smsCode":"Hamro","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":4,"coopName":"Friends","coopShortName":"Friends","logoUrl":"https://testmerchant.sahakaari.com/images/frnds.png","baseUrl":"https://friendspathway.sahakaari.com/","smsKeyword":"Friends ","aboutUs":"this is test","faq":"this is test","primaryColor":"#1000a5","secondaryColor":"#6657f3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"Friends ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":5,"coopName":"Darshan","coopShortName":"Darshan","logoUrl":"https://testmerchant.sahakaari.com/images/darshan.png","baseUrl":"https://darshanpes.sahakaari.com/","smsKeyword":"darshan ","aboutUs":"this is test","faq":"this is test","primaryColor":"#eeeeee","secondaryColor":"#1ab871","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"darshan ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":6,"coopName":"Gurukul","coopShortName":"Gurukul","logoUrl":"https://testmerchant.sahakaari.com/images/gurukul.png","baseUrl":"http://gurukulpes.sahakaari.com/","smsKeyword":"Gurukul ","aboutUs":"this is test","faq":"this is test","primaryColor":"#1ab871","secondaryColor":"#1ab871","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"Gurukul ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":7,"coopName":"Bidhutkarmi","coopShortName":"Bidhutkarmi","logoUrl":"https://testmerchant.sahakaari.com/images/bidhutkarmi.png","baseUrl":"https://bidhutpes.sahakaari.com/","smsKeyword":"BIDHUT ","aboutUs":"this is test","faq":"this is test","primaryColor":"#1ab871","secondaryColor":"#1c734c","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"BIDHUT ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":8,"coopName":"JivanJyoti","coopShortName":"JivanJyoti","logoUrl":"https://testmerchant.sahakaari.com/images/jivanjyoti.png","baseUrl":"https://jivanjyotipes.sahakaari.com/","smsKeyword":"InfoSms ","aboutUs":"this is test","faq":"this is test","primaryColor":"#1ab871","secondaryColor":"#1c734c","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"InfoSms ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":9,"coopName":"Lotus","coopShortName":"Lotus","logoUrl":"https://testmerchant.sahakaari.com/images/lotus.png","baseUrl":"https://lotuspathway.sahakaari.com/","smsKeyword":"lotus ","aboutUs":"this is test","faq":"this is test","primaryColor":"#9e0d7c","secondaryColor":"#1c734c","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"lotus ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":10,"coopName":"Aaradhya","coopShortName":"Aaradhya","logoUrl":"https://testmerchant.sahakaari.com/images/aradhya.png","baseUrl":"https://aaradhyapes.sahakaari.com/","smsKeyword":"InfoSms ","aboutUs":"this is test","faq":"this is test","primaryColor":"#9e0d7c","secondaryColor":"#1c734c","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"InfoSms ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":11,"coopName":"Alpine","coopShortName":"Alpine","logoUrl":"https://testmerchant.sahakaari.com/images/alpine.png","baseUrl":"https://alpinepes.sahakaari.com/","smsKeyword":"ALPINE ","aboutUs":"this is test","faq":"this is test","primaryColor":"#1c734c","secondaryColor":"#1c734c","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"ALPINE ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":12,"coopName":"Srinidhi","coopShortName":"Srinidhi","logoUrl":"https://testmerchant.sahakaari.com/images/srinidhi.png","baseUrl":"http://srinidhipes.sahakaari.com/","smsKeyword":"Srinidhi ","aboutUs":"this is test","faq":"this is test","primaryColor":"#2f3676","secondaryColor":"#dcdbe5","loginPrimary":"#2f3676","loginSemiTransparent":"#dcdbe5","loginTextField":"#ffffff","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#1d1e4b","kButtonTitle":"#FFFFFF","smsCode":"Srinidhi ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":13,"coopName":"Srijanatmak","coopShortName":"Srijanatmak","logoUrl":"https://testmerchant.sahakaari.com/images/srijanatmak.jpg","baseUrl":"http://pathway.sahakaari.com/","smsKeyword":"SRIJANATMAK ","aboutUs":"this is test","faq":"this is test","primaryColor":"#f1d33c","secondaryColor":"#fef9e3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"SRIJANATMAK ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":15,"coopName":"matapathivara","coopShortName":"matapathivara","logoUrl":"https://testmerchant.sahakaari.com/images/matapathivara.png","baseUrl":"http://matapathivara.sahakaari.com","smsKeyword":"Pathways ","aboutUs":"this is test","faq":"this is test","primaryColor":"#fef9e3","secondaryColor":"#fef9e3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"Pathways ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":16,"coopName":"sanakishan","coopShortName":"sanakishan","logoUrl":"https://testmerchant.sahakaari.com/images/sanakishan.png","baseUrl":"http://sanakishnapnagar.sahakaari.com/","smsKeyword":"InfoSms ","aboutUs":"this is test","faq":"this is test","primaryColor":"#fef9e3","secondaryColor":"#fef9e3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"InfoSms ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":17,"coopName":"sworajagar","coopShortName":"sworajagar","logoUrl":"https://testmerchant.sahakaari.com/images/sworajagar.png","baseUrl":"http://swarojagarbhawani.sahakaari.com","smsKeyword":"PMC","aboutUs":"this is test","faq":"this is test","primaryColor":"#fef9e3","secondaryColor":"#fef9e3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"Pathways ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":19,"coopName":"Kishan Kalyan","coopShortName":"Kishan Kalyan","logoUrl":"https://testmerchant.sahakaari.com/images/PMC.png","baseUrl":"http://kishankalyanpathway.sahakaari.com/","smsKeyword":"PMC","aboutUs":"this is test","faq":"this is test","primaryColor":"#fef9e3","secondaryColor":"#fef9e3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"Pathways ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":20,"coopName":"Manthali","coopShortName":"Manthali","logoUrl":"https://testmerchant.sahakaari.com/images/PMC.png","baseUrl":"http://manthalipathways.sahakaari.com/","smsKeyword":"Manthali ","aboutUs":"this is test","faq":"this is test","primaryColor":"#fef9e3","secondaryColor":"#fef9e3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"Manthali ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null},{"id":21,"coopName":"Gangajamuna","coopShortName":"Gangajamuna","logoUrl":"https://testmerchant.sahakaari.com/images/PMC.png","baseUrl":"https://gangajamunapes.sahakaari.com/","smsKeyword":"GANGAJAMUNA ","aboutUs":"this is test","faq":"this is test","primaryColor":"#fef9e3","secondaryColor":"#fef9e3","loginPrimary":"#f1d33c","loginSemiTransparent":"#fef9e3","loginTextField":"#000000","loginButtonBackground":"#ffffff","loginButtonTitle":"#000000","dashboardTopIcon":"#ff0000","dashboardTopTitle":"#000000","primaryDark":"#e2b13b","kButtonTitle":"#FFFFFF","smsCode":"GANGAJAMUNA ","textFieldBackground":"#FFFFFF","inputTitle":"#000000","inputHint":"#c7f8b8","inputText":"#41a124","faQs":null}]
/// message : "Data Fetched"
/// statusCode : 0

class CooperativeModel {
  CooperativeModel({
    bool? success,
    String? status,
    List<Payload>? payload,
    String? message,
    int? statusCode,
  }) {
    _success = success;
    _status = status;
    _payload = payload;
    _message = message;
    _statusCode = statusCode;
  }

  CooperativeModel.fromJson(dynamic json) {
    _success = json['success'];
    _status = json['status'];
    if (json['payload'] != null) {
      _payload = [];
      json['payload'].forEach((v) {
        _payload?.add(Payload.fromJson(v));
      });
    }
    _message = json['message'];
    _statusCode = json['statusCode'];
  }
  bool? _success;
  String? _status;
  List<Payload>? _payload;
  String? _message;
  int? _statusCode;

  bool? get success => _success;
  String? get status => _status;
  List<Payload>? get payload => _payload;
  String? get message => _message;
  int? get statusCode => _statusCode;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = _success;
    map['status'] = _status;
    if (_payload != null) {
      map['payload'] = _payload?.map((v) => v.toJson()).toList();
    }
    map['message'] = _message;
    map['statusCode'] = _statusCode;
    return map;
  }
}

/// id : 1
/// coopName : "PMC"
/// coopShortName : "PES"
/// logoUrl : "https://testmerchant.sahakaari.com/images/PMC.png"
/// baseUrl : "http://pathway.sahakaari.com/"
/// smsKeyword : "Pathways "
/// aboutUs : "This is about us"
/// faq : "this is faq"
/// primaryColor : "#41a124"
/// secondaryColor : "#e8fcd7"
/// loginPrimary : "#41a124"
/// loginSemiTransparent : "#e8fcd7"
/// loginTextField : "#ffffff"
/// loginButtonBackground : "#ffffff"
/// loginButtonTitle : "#000000"
/// dashboardTopIcon : "#ff0000"
/// dashboardTopTitle : "#000000"
/// primaryDark : "#1a6404"
/// kButtonTitle : "#000000"
/// smsCode : "Pathways "
/// textFieldBackground : "#c7f8b8"
/// inputTitle : "#000000"
/// inputHint : "#c7f8b8"
/// inputText : "#41a124"
/// faQs : null

class Payload {
  Payload({
    int? id,
    String? coopName,
    String? coopShortName,
    String? logoUrl,
    String? baseUrl,
    String? smsKeyword,
    String? aboutUs,
    String? faq,
    String? primaryColor,
    String? secondaryColor,
    String? loginPrimary,
    String? loginSemiTransparent,
    String? loginTextField,
    String? loginButtonBackground,
    String? loginButtonTitle,
    String? dashboardTopIcon,
    String? dashboardTopTitle,
    String? primaryDark,
    String? kButtonTitle,
    String? smsCode,
    String? textFieldBackground,
    String? inputTitle,
    String? inputHint,
    String? inputText,
    dynamic faQs,
  }) {
    _id = id;
    _coopName = coopName;
    _coopShortName = coopShortName;
    _logoUrl = logoUrl;
    _baseUrl = baseUrl;
    _smsKeyword = smsKeyword;
    _aboutUs = aboutUs;
    _faq = faq;
    _primaryColor = primaryColor;
    _secondaryColor = secondaryColor;
    _loginPrimary = loginPrimary;
    _loginSemiTransparent = loginSemiTransparent;
    _loginTextField = loginTextField;
    _loginButtonBackground = loginButtonBackground;
    _loginButtonTitle = loginButtonTitle;
    _dashboardTopIcon = dashboardTopIcon;
    _dashboardTopTitle = dashboardTopTitle;
    _primaryDark = primaryDark;
    _kButtonTitle = kButtonTitle;
    _smsCode = smsCode;
    _textFieldBackground = textFieldBackground;
    _inputTitle = inputTitle;
    _inputHint = inputHint;
    _inputText = inputText;
    _faQs = faQs;
  }

  Payload.fromJson(dynamic json) {
    _id = json['id'];
    _coopName = json['coopName'];
    _coopShortName = json['coopShortName'];
    _logoUrl = json['logoUrl'];
    _baseUrl = json['baseUrl'];
    _smsKeyword = json['smsKeyword'];
    _aboutUs = json['aboutUs'];
    _faq = json['faq'];
    _primaryColor = json['primaryColor'];
    _secondaryColor = json['secondaryColor'];
    _loginPrimary = json['loginPrimary'];
    _loginSemiTransparent = json['loginSemiTransparent'];
    _loginTextField = json['loginTextField'];
    _loginButtonBackground = json['loginButtonBackground'];
    _loginButtonTitle = json['loginButtonTitle'];
    _dashboardTopIcon = json['dashboardTopIcon'];
    _dashboardTopTitle = json['dashboardTopTitle'];
    _primaryDark = json['primaryDark'];
    _kButtonTitle = json['kButtonTitle'];
    _smsCode = json['smsCode'];
    _textFieldBackground = json['textFieldBackground'];
    _inputTitle = json['inputTitle'];
    _inputHint = json['inputHint'];
    _inputText = json['inputText'];
    _faQs = json['faQs'];
  }
  int? _id;
  String? _coopName;
  String? _coopShortName;
  String? _logoUrl;
  String? _baseUrl;
  String? _smsKeyword;
  String? _aboutUs;
  String? _faq;
  String? _primaryColor;
  String? _secondaryColor;
  String? _loginPrimary;
  String? _loginSemiTransparent;
  String? _loginTextField;
  String? _loginButtonBackground;
  String? _loginButtonTitle;
  String? _dashboardTopIcon;
  String? _dashboardTopTitle;
  String? _primaryDark;
  String? _kButtonTitle;
  String? _smsCode;
  String? _textFieldBackground;
  String? _inputTitle;
  String? _inputHint;
  String? _inputText;
  dynamic _faQs;

  int? get id => _id;
  String? get coopName => _coopName;
  String? get coopShortName => _coopShortName;
  String? get logoUrl => _logoUrl;
  String? get baseUrl => _baseUrl;
  String? get smsKeyword => _smsKeyword;
  String? get aboutUs => _aboutUs;
  String? get faq => _faq;
  String? get primaryColor => _primaryColor;
  String? get secondaryColor => _secondaryColor;
  String? get loginPrimary => _loginPrimary;
  String? get loginSemiTransparent => _loginSemiTransparent;
  String? get loginTextField => _loginTextField;
  String? get loginButtonBackground => _loginButtonBackground;
  String? get loginButtonTitle => _loginButtonTitle;
  String? get dashboardTopIcon => _dashboardTopIcon;
  String? get dashboardTopTitle => _dashboardTopTitle;
  String? get primaryDark => _primaryDark;
  String? get kButtonTitle => _kButtonTitle;
  String? get smsCode => _smsCode;
  String? get textFieldBackground => _textFieldBackground;
  String? get inputTitle => _inputTitle;
  String? get inputHint => _inputHint;
  String? get inputText => _inputText;
  dynamic get faQs => _faQs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['coopName'] = _coopName;
    map['coopShortName'] = _coopShortName;
    map['logoUrl'] = _logoUrl;
    map['baseUrl'] = _baseUrl;
    map['smsKeyword'] = _smsKeyword;
    map['aboutUs'] = _aboutUs;
    map['faq'] = _faq;
    map['primaryColor'] = _primaryColor;
    map['secondaryColor'] = _secondaryColor;
    map['loginPrimary'] = _loginPrimary;
    map['loginSemiTransparent'] = _loginSemiTransparent;
    map['loginTextField'] = _loginTextField;
    map['loginButtonBackground'] = _loginButtonBackground;
    map['loginButtonTitle'] = _loginButtonTitle;
    map['dashboardTopIcon'] = _dashboardTopIcon;
    map['dashboardTopTitle'] = _dashboardTopTitle;
    map['primaryDark'] = _primaryDark;
    map['kButtonTitle'] = _kButtonTitle;
    map['smsCode'] = _smsCode;
    map['textFieldBackground'] = _textFieldBackground;
    map['inputTitle'] = _inputTitle;
    map['inputHint'] = _inputHint;
    map['inputText'] = _inputText;
    map['faQs'] = _faQs;
    return map;
  }
}
