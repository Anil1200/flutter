class ManakamanaCableCarPassengersModel {
  String? TripType = "";
  String? PassengerType = "";
  int? NoOfPassenger = 0;
  double? Price = 0.0;

  ManakamanaCableCarPassengersModel(
      {this.TripType,
        this.PassengerType,
        this.NoOfPassenger,
        this.Price,}
      );

  /*factory ManakamanaCableCarPassengersModel.fromJson(Map<String, dynamic> json) {
    return ManakamanaCableCarPassengersModel(
      TripType: json['TripType'],
      PassengerType: json['PassengerType'],
      NoOfPassenger: json['NoOfPassenger'],
      Price: json['Price'],
    );
  }*/

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['TripType'] = this.TripType;
    data['PassengerType'] = this.PassengerType;
    data['NoOfPassenger'] = this.NoOfPassenger;
    data['Price'] = this.Price;
    return data;
  }
}