import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sahakari_pay/pmc/utils/localizations.dart';

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SpinKitChasingDots(
            color: Colors.green[900],
            size: 50.0,
          ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Center(
                child: Text(
              AppLocalizations.of(context)!.localizedString("loading"),
              style: TextStyle(color: Colors.green[900], fontSize: 14),
            )),
          )
        ],
      ),
    );
  }
}
