import 'dart:convert';

class ContactModel {
  String? id = "";
  String? displayName = "";
  String? phoneNumber = "";

  ContactModel(
      {this.id,
        this.displayName,
        this.phoneNumber});
}
