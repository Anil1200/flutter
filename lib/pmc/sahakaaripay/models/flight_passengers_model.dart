class FlightPassengersModel {
  String? Title = "";
  String? FirstName = "";
  String? LastName = "";

  FlightPassengersModel(
      {this.Title,
        this.FirstName,
        this.LastName,}
      );

  /*factory FlightPassengersModel.fromJson(Map<String, dynamic> json) {
    return FlightPassengersModel(
      Title: json['Title'],
      FirstName: json['FirstName'],
      LastName: json['LastName'],
    );
  }*/

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Title'] = this.Title;
    data['FirstName'] = this.FirstName;
    data['LastName'] = this.LastName;
    return data;
  }
}