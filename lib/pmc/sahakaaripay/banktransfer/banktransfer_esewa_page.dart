import 'dart:convert';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/pmc/constants.dart';

import '../../utils/localizations.dart';
import '../../utils/utils.dart';
import '../models/loading.dart';
import '../pmc_homepage.dart';
import 'pdf/banktransfer_esewa_pdfbillpage.dart';

class EsewaBankTransferPage extends StatefulWidget {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  // List? BankList;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  EsewaBankTransferPage({
    Key? key,
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    // this.BankList,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _EsewaBankTransferPageState createState() => _EsewaBankTransferPageState(
        coopList,
        userId,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        // BankList,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _EsewaBankTransferPageState extends State<EsewaBankTransferPage> {
  List? coopList;
  String? userId;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  // List? BankList;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;
  _EsewaBankTransferPageState(
    this.coopList,
    this.userId,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    // this.BankList,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  bool _isLoading = false;
  bool showBalance = false;

  List<String> dropBankName = [];

  void initState() {
    super.initState();
    _isLoading = true;
    print("bank transfer type: ${banktransferType}");
    // setState(() {
    //   this.BankList;
    //   print("this is the banklist that came from home page: ${BankList}");
    // });
    Get_BankList_Esewa().then((value) {
      if (value) {
<<<<<<< HEAD
        forEsewaSearch();
=======
        postforBankListQuickPayment_Esewa().then((value) {
          if (value) {
            forEsewaSearch();
          }
        });
        if(ScannedBankCode != null) {
          getSwiftCode().then((value) {
            locationId = ScannedBankCode;
            final index = BankList!.indexWhere((element) => element["swift_code"] == locationId);
            bankName = BankList![index]["bank_display_name"];
            dropdownValue = BankList![index]["bank_display_name"];

            nameController.text = ScannedBankAccountName!;
            accountNoController.text = ScannedBankAccountNumber!;

            ScannedBankDeposit = false;
            ScannedBankCode = null;
            ScannedBankAccountNumber = null;
            ScannedBankAccountName = null;
          });
        }
      } else {
        _isLoading = false;

        ScannedBankDeposit = false;
        ScannedBankCode = null;
        ScannedBankAccountNumber = null;
        ScannedBankAccountName = null;
>>>>>>> sudeep
      }
    });

    // this.postforBankList();
    // this.BankList;
  }

  List? BankList;
  double? minimum_Amount;
  double? maximum_Amount;

  Future<bool> Get_BankList_Esewa() async {
    String url = "https://ca.esewa.com.np/api/thirdparty/banks";
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status : ${jsonResponse}");
      setState(() {
        _isLoading = false;
        BankList = jsonResponse;
        minimum_Amount = jsonResponse[0]["min_amount"];
        maximum_Amount = jsonResponse[0]["max_amount"];
        print("the required list of banks: ${BankList}");
      });
      return true;
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error response:${jsonResponse}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(AppLocalizations.of(context)!
                  .localizedString("something_went_wrong")),
              actions: [
                // const Spacer(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
      return false;
    }
    return true;
  }

<<<<<<< HEAD
=======
  Future<bool> postforBankListQuickPayment_Esewa() async {
    String url = "${baseUrl}api/v1/${userId}/${banktransferType}/getquickpayment";
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (mounted) {
      if (res.statusCode == 200) {
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
        setState(() {
          _isLoading = false;
          BankListQuickPayment = jsonResponse["Payload"];
          print("the required list of banks: ${BankListQuickPayment}");
        });
        return true;
      } else if (res.statusCode == 400) {
        jsonResponse = json.decode(res.body);
        setState(() {
          _isLoading = false;
        });
        print(jsonResponse);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.localizedString("alert"),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 40,
                      )
                    ],
                  ),
                ),
                content: Text("${jsonResponse["Message"]}"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                      ))
                ],
              );
            });
      } else if (res.statusCode == 401) {
        jsonResponse = json.decode(res.body);
        setState(() {
          _isLoading = false;
        });
        print(jsonResponse);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.localizedString("alert"),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 40,
                      )
                    ],
                  ),
                ),
                content: Text("${jsonResponse["Message"]}"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        AppLocalizations.of(context)!.localizedString("okay"),
                      ))
                ],
              );
            });
      } else {
        setState(() {
          _isLoading = false;
        });
        jsonResponse = json.decode(res.body);
        print("this is the error response:${jsonResponse}");
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Center(
                  child: Column(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.localizedString("alert"),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50,
                      )
                    ],
                  ),
                ),
                content: Text(AppLocalizations.of(context)!
                    .localizedString("something_went_wrong")),
                actions: [
                  // const Spacer(),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                      style: TextStyle(color: Colors.green[900]),
                    ),
                  )
                ],
              );
            });
        return false;
      }
    }
    return true;
  }

  Future<bool> removeforBankListQuickPayment_Esewa(int id) async {
    String url = "${baseUrl}api/v1/removequickpayment/$id";
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      setState(() {
        //_isLoading = false;
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
      });
      postforBankListQuickPayment_Esewa();
      return true;
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error resposne: ${jsonResponse}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
              content: Text(
                "Something went Wrong. PLease try again.",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
      return false;
    }
  }

  showMessageRemove(int id) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(
            child: Column(
              children: [
                Text(
                  AppLocalizations.of(context)!.localizedString("alert"),
                ),
                SizedBox(
                  height: 10,
                ),
                Icon(
                  Icons.add_alert,
                  color: Colors.red,
                  size: 50,
                )
              ],
            ),
          ),
          content: Text(AppLocalizations.of(context)!
              .localizedString("transaction_message3")),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                AppLocalizations.of(context)!.localizedString("cancel"),
                style: TextStyle(color: Colors.green[900]),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                setState(() {
                  _isLoading = true;
                });
                removeforBankListQuickPayment_Esewa(id);
              },
              child: Text(
                AppLocalizations.of(context)!.localizedString("okay"),
                style: TextStyle(color: Colors.green[900]),
              ),
            ),
          ],
        );
      },
    );
  }

>>>>>>> sudeep
  forEsewaSearch() {
    for (var item in BankList!) {
      setState(() {
        _isLoading = false;
      });
      dropBankName.add(item["bank_display_name"]);
      print(dropBankName);
    }
  }

  Future<bool> getSwiftCode() async {
    String url = "${baseUrl}api/v1/Khalti/$ScannedBankCode/getswiftcode";
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status : ${jsonResponse}");
      setState(() {
        //_isLoading = false;
        ScannedBankCode = jsonResponse!["Payload"][0]["BankCode"];
        print("the required bank code: ${ScannedBankCode}");
      });
      return true;
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      /*setState(() {
        _isLoading = false;
      });*/
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      /*setState(() {
        _isLoading = false;
      });*/
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      AppLocalizations.of(context)!.localizedString("okay"),
                    ))
              ],
            );
          });
    } else {
      /*setState(() {
        _isLoading = false;
      });*/
      jsonResponse = json.decode(res.body);
      print("this is the error response:${jsonResponse}");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(AppLocalizations.of(context)!
                  .localizedString("something_went_wrong")),
              actions: [
                // const Spacer(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
      return false;
    }
    return true;
  }

  bool validateMPIN(String value) {
    String pattern = r'(^[0-9]{4}$)';
    // String pattern = r'(^(0|98)?[9][0-9]{9}$)';
    RegExp regExp = RegExp(pattern);
    if (!regExp.hasMatch(value)) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  String? bankName;
  String? locationId;
  String? Status;

  showMessage() {
    if (dropdownValue == null) {
      setState(() {
        _isLoading = false;
      });
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 50,
                    )
                  ],
                ),
              ),
              content: Text(AppLocalizations.of(context)!
                  .localizedString("select_bank_transfer")),
              actions: [
                // const Spacer(),
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    AppLocalizations.of(context)!.localizedString("okay"),
                    style: TextStyle(color: Colors.green[900]),
                  ),
                )
              ],
            );
          });
    } else {
      validateEsewa();
    }
  }

  validateEsewa() async {
    String url = "${baseUrl}api/v1/esewa/banktransfer/validateaccount";
    Map body = {
      "bankCode": locationId.toString(),
      "account": accountNoController.text.toString().trim(),
      "accountHolderName": nameController.text.toString().trim(),
      "amount": amountController.text.toString().trim()
    };
    print("this is map body: ${body}");
    var jsonResponse;
    var res = await http.post(Uri.parse(url),
        body: body, headers: {'Authorization': "Bearer ${accesstoken}"});
    // setState(() {
    //   _isLoading = true;
    // });
    if (res.statusCode == 200) {
      setState(() {
        _isLoading = true;
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
        if (jsonResponse != null) {
          Status = jsonResponse["status"];
          if (Status.toString().toLowerCase() ==
              "Success".toString().toLowerCase()) {
            Esewa_Booking();
          } else {
            _isLoading = false;
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 40,
                          )
                        ],
                      ),
                    ),
                    content: Text(AppLocalizations.of(context)!
                        .localizedString("process_error")),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(AppLocalizations.of(context)!
                              .localizedString("okay")))
                    ],
                  );
                });
          }
        }
        //
        // if (jsonResponse != null) {
        //   setState(() {
        //     _isLoading = true;
        //
        //   });
        // }
      });
      // BankList = jsonResponse["Locations"];
      // print("the required list of banks: ${BankList}");
      // BankList = SearchBankList;

      // print(SearchBankList);
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("This is the error response : ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text(
                  "${jsonResponse["Message"]},Please enter correct AccountNo and Name."),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error resposne: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                AppLocalizations.of(context)!
                    .localizedString("something_went_wrong"),
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
  }

  String? serviceCharges = "10";
  String? serviceInfoId;
  String? OriginatingUniqueId;

  Esewa_Booking() async {
    String url = "${baseUrl}api/v1/esewa/banktransfer/paymentbookings";
    Map bodymap = {
      "serviceCode": "BANK",
      "transactionURI":
          "${locationId.toString()};${accountNoController.text.toString().trim()}",
      "requestFields": [
        {"value": remarkController.text.toString().trim(), "key": "Remarks"},
      ],
      "amountInformation": {
        "amount": amountController.text.toString().trim(),
        "currency": "NPR"
      },
      "senderInformation": {
        "contactDetail": login_username,
        "address": Address,
        "identifier": accountno,
        "name": AccountName
      }
    };
    var body = json.encode(bodymap);
    print(body);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      'Content-Type': "application/json",
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      serviceInfoId = jsonResponse["serviceInfoId"].toString();
      OriginatingUniqueId = jsonResponse["uniqueOriginatedId"];
      // ServiceCharge = serviceCharges;
      print("this is serviceInfoId: ${serviceInfoId}");
      print("this is OriginatingUniqueId: ${OriginatingUniqueId}");
      print("Response status : ${jsonResponse}");
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          Status = jsonResponse["status"];
          if (Status.toString().toLowerCase() ==
              "Success".toString().toLowerCase()) {
            serviceCharge_esewa();
          } else {
            _isLoading = false;
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Center(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context)!
                                .localizedString("alert"),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Icon(
                            Icons.add_alert,
                            color: Colors.red,
                            size: 40,
                          )
                        ],
                      ),
                    ),
                    content: Text(AppLocalizations.of(context)!
                        .localizedString("process_error")),
                    actions: [
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(AppLocalizations.of(context)!
                              .localizedString("okay")))
                    ],
                  );
                });
          }
          // transactionPinMessage_Esewa();
        });
      }
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error: ${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "Something went Wrong. PLease try again.",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
  }

  serviceCharge_esewa() async {
    String url = "${baseUrl}esewa/banktransfer/serviceCharge";
    // Map body = {
    //   "LocationId": locationId.toString(),
    //   "Amount": amountController.text.toString(),
    // };
    // print(body);
    var jsonResponse;
    var res = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status : ${jsonResponse}");
      serviceCharges = jsonResponse.toString();
      ServiceCharge = serviceCharges;
      print("this is service Charge: ${serviceCharges}");
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          transactionPinMessage_Esewa();
        });
      }
    } else if (res.statusCode == 400) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      jsonResponse = json.decode(res.body);
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error:${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "Something went Wrong. PLease try again.",
                style: TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
  }

  double? sum;
  String? totalAmount;

  transactionPinMessage_Esewa() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            content: Container(
              height: 220,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 80,
                      decoration: BoxDecoration(color: Colors.green[800]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Center(
                                child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("alert"),
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Center(
                                child: Icon(
                              Icons.add_alert,
                              color: Colors.white,
                              size: 30,
                            )),
                          ),
                        ],
                      )),
                  Padding(
                    padding: EdgeInsets.only(left: 20.0, top: 20),
                    child: Text(
                      "Rs.${amountController.text.toString()} will be transferred to ${accountNoController.text.toString()} with extra Service Charge of Rs.${serviceCharges.toString()} ",
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 20),
                    child: Form(
                      key: globalFormKey2,
                      child: TextFormField(
                        controller: mpincontroller,
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        validator: (value) => value!.isEmpty || value.length < 3
                            ? AppLocalizations.of(context)!
                                .localizedString("enter_transaction_pin")
                            : null,
                        onSaved: (value) => mpincontroller,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: AppLocalizations.of(context)!
                              .localizedString("transaction_pin"),
                          labelStyle: TextStyle(color: Colors.green),
                          counterText: "",
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 10.0, left: 20, right: 20, top: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          mpincontroller.clear();
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .localizedString("cancel"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          if (validateMPINForm() &&
                              validateMPIN(mpincontroller.text)) {
                            setState(() {
                              _isLoading = true;
                              sum = double.parse(amountController.text) +
                                  double.parse(serviceCharges.toString());
                              // int.parse(amountController.text) + int.parse(serviceCharges.toString());
                              totalAmount = sum.toString();
                              print(
                                  "the required totalAmount is : ${totalAmount}");
                            });
                            Navigator.of(context).pop();
                            Esewa_Payment();
                          } else {
                            _isLoading = false;
                          }
                        },
                        child: Text(
                          AppLocalizations.of(context)!.localizedString("okay"),
                          style: TextStyle(color: Colors.green[900]),
                        )),
                  ],
                ),
              )
            ],
          );
        });
  }

  String? ReferenceId;
  String? TransactionId;
  String? ReceiptNo;
  String? PaymentBillDate;
  String? paymentmessage;
  String? status;
  String? tcTransactionId;
  String? genericMessage;

  Esewa_Payment() async {
    String url = "${baseUrl}api/v1/esewa/banktransfer/payment";
    Map mapbody = {
      "MPIN": mpincontroller.text.toString().trim(),
      "serviceinfoId": serviceInfoId.toString(),
      "OriginatingUniqueId": OriginatingUniqueId.toString(),
      "Amount": amountController.text.toString().trim(),
      "Statement": "QRCode. " + remarkController.text.toString().trim(),
      "RequestFields": [
        {"Key": "Remarks", "Value": remarkController.text.toString().trim()}
      ]
    };
    mpincontroller.clear();
    print(mapbody);
    var body = json.encode(mapbody);
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      'Content-Type': "application/json",
      'Authorization': "Bearer ${accesstoken}"
    });
    if (res.statusCode == 200) {
      setState(() {
        isQRTransaction = false;
        _isLoading = true;
        jsonResponse = json.decode(res.body);
        print("Response status : ${jsonResponse}");
        if (jsonResponse != null) {
          status = jsonResponse["status"];
          tcTransactionId =
              jsonResponse["transactionDetail"]["tcTransactionId"];
          genericMessage = jsonResponse["genericMessage"];
          // PaymentBillDate = jsonResponse["BillDetail"]["BillDate"];
          Utils.BalanceUpdate(baseUrl!, accesstoken).then((value) {
            setState(() {
              _isLoading = false;
              if (status.toString().toLowerCase() == "Success".toLowerCase()) {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Row(
                  children: [
                    const Icon(
                      Icons.verified,
                      color: Colors.green,
                      size: 40,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      jsonResponse["Message"],
                      style: const TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ],
                )));
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EsewaBankTransferPdfBillPage(
                              coopList: coopList,
                              userId: userId,
                              accesstoken: accesstoken,
                              balance: balance,
                              baseUrl: baseUrl,
                              accountno: accountno,
                              bankName: bankName,
                              Amount: amountController.text.toString(),
                              accountHolderName: nameController.text.toString(),
                              accountNotoTransfer:
                                  accountNoController.text.toString(),
                              remarks: remarkController.text.toString(),
                              paymentmessage: genericMessage,
                              // planName: planName,
                              TransactionId: tcTransactionId,
                              primaryColor: primaryColor,
                              loginButtonTitleColor: loginButtonTitleColor,
                              loginbuttonColor: loginbuttonColor,
                              loginTextFieldColor: loginTextFieldColor,
                              dasboardIconColor: dasboardIconColor,
                              dashboardTopTitleColor: dashboardTopTitleColor,
                              SecondaryColor: SecondaryColor,
                            )));
              } else {
                setState(() {
                  _isLoading = false;
                });
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Center(
                            child: Column(
                          children: [
                            Text(
                              AppLocalizations.of(context)!
                                  .localizedString("error_alert"),
                              style: TextStyle(fontSize: 16),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Icon(
                              Icons.add_alert,
                              color: Colors.red,
                              size: 50.0,
                            )
                          ],
                        )),
                        content: Text(
                          AppLocalizations.of(context)!
                              .localizedString("process_error"),
                          style: TextStyle(fontSize: 14),
                        ),
                        actions: [
                          TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("okay"),
                              ))
                        ],
                      );
                    });
              }
            });
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.localizedString("alert"),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Icon(
                      Icons.add_alert,
                      color: Colors.red,
                      size: 40,
                    )
                  ],
                ),
              ),
              content: Text("${jsonResponse["Message"]}"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print("this is the error:${jsonResponse}");
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: const Center(
                  child: Text(
                "Something went Wrong. PLease try again.",
                style: TextStyle(fontSize: 14),
              )),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.localizedString("okay")))
              ],
            );
          });
    }
  }

  movetohomepage() {
    setState(() {
      isQRTransaction = false;
    });
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (context) => HomePage(
                  coopList: coopList,
                  userId: userId,
                  accesstoken: accesstoken,
                  baseUrl: baseUrl,
                  accountno: accountno,
                  primaryColor: primaryColor,
                  loginButtonTitleColor: loginButtonTitleColor,
                  loginbuttonColor: loginbuttonColor,
                  loginTextFieldColor: loginTextFieldColor,
                  dasboardIconColor: dasboardIconColor,
                  dashboardTopTitleColor: dashboardTopTitleColor,
                  SecondaryColor: SecondaryColor,
                )),
        (Route<dynamic> route) => false);
  }

  final GlobalKey<FormState> globalFormKey = GlobalKey();
  final GlobalKey<FormState> globalFormKey3 = GlobalKey();
  final GlobalKey<FormState> globalFormKey2 = GlobalKey();
  final TextEditingController mpincontroller = TextEditingController();
  final TextEditingController mpincontroller3 = TextEditingController();
  final TextEditingController bankController = TextEditingController();

  final TextEditingController accountNoController = TextEditingController();
  final TextEditingController amountController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController remarkController = TextEditingController();

  String? dropdownValue;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohomepage();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                      child: Container(
                        height: 150,
                        decoration: BoxDecoration(
                          color: Color(int.parse(primaryColor.toString())),
                        ),
                        child: Stack(
                          children: [
                            Positioned(
                              left: 10.0,
                              top: 40,
                              child: IconButton(
                                  onPressed: () {
                                    movetohomepage();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back,
                                    size: 30.0,
                                    color: Colors.white,
                                  )),
                            ),
                            Positioned(
                              left: 100,
                              top: 50,
                              child: Text(
                                AppLocalizations.of(context)!
                                    .localizedString("bank_transfer"),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                            Positioned(
                              left: 20,
                              top: 95,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    height: 30,
                                    width: 30,
                                    child: Image(
                                        image: AssetImage(
                                            "assets/images/wallet_icon.png")),
                                  ),
                                  const SizedBox(
                                    width: 10.0,
                                  ),
                                  Text(
                                    showBalance
                                        ? AppLocalizations.of(context)!
                                                .localizedString("rs") +
                                            "${balance.toString()}"
                                        : "xxx.xx".toUpperCase(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 180.0),
                                    child: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            if (showBalance == false) {
                                              showBalance = true;
                                            } else {
                                              showBalance = false;
                                            }
                                          });
                                        },
                                        icon: Icon(
                                          showBalance
                                              ? Icons.visibility
                                              : Icons.visibility_off,
                                          size: 25.0,
                                          color: Colors.white,
                                        )),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 30.0, right: 10, top: 20),
                              child: DropdownSearch<String>(
                                mode: Mode.DIALOG,
                                maxHeight:
                                    MediaQuery.of(context).size.height - 100,
                                dropdownSearchDecoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)!
                                        .localizedString("select_bank"),
                                    hintStyle: TextStyle(
                                        fontSize: 16, color: Colors.black87)),
                                selectionListViewProps: SelectionListViewProps(
                                  padding: EdgeInsets.only(left: 15),
                                ),
                                searchFieldProps: TextFieldProps(
                                  // autofocus: true,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 24, vertical: 10),
                                  decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      hintText: AppLocalizations.of(context)!
                                          .localizedString("search"),
                                      hintStyle: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          color: Colors.grey[400]),
                                      suffixIcon: Icon(
                                        Icons.search,
                                        size: 30,
                                        color: Colors.grey[500],
                                      )),
                                ),
                                // showClearButton: true,
                                showSearchBox: true,
                                items: dropBankName,
                                selectedItem: dropdownValue,
                                // hint: AppLocalizations.of(context)!
                                //     .localizedString("search"),
                                onChanged: (String? newValue) {
                                  final index = BankList!.indexWhere(
                                      (element) =>
                                          element["bank_display_name"] ==
                                          newValue);
                                  setState(() {
                                    dropdownValue = newValue!;
                                    bankName = newValue!;

                                    newValue = BankList![index]["swift_code"];
                                    locationId = newValue!;
                                    print(
                                        "this is id : ${locationId} and ${bankName}");
                                  });
                                },
                              ),
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.only(
                            //       left: 30.0, right: 10.0, top: 10.0),
                            //   child: DropdownSearch<String>(
                            //     mode: Mode.BOTTOM_SHEET,
                            //     showSearchBox: true,
                            //     items: ,
                            //     onChanged: (String? newValue) {
                            //       final index = BankList!.indexWhere(
                            //           (element) =>
                            //               element["LocationName"] == newValue);
                            //       setState(() {
                            //         dropdownValue = newValue!;
                            //         bankName = newValue!;
                            //
                            //         newValue = BankList![index]["LocationId"];
                            //         locationId = newValue!;
                            //         print(
                            //             "this is locatioId : ${dropdownValue?.split(",")[0]}, AND THIS is BANK NAME: ${dropdownValue?.split(",")[1]}");
                            //       });
                            //     },
                            //   ),
                            // ),
                            Form(
                                key: globalFormKey,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 20),
                                      child: Container(
                                        width: 320,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: TextFormField(
                                          controller: amountController,
                                          maxLength: 5,
                                          keyboardType: TextInputType.number,
                                          validator: (value) => value!.isEmpty
                                              ? AppLocalizations.of(context)!
                                                  .localizedString("amount")
                                              : null,
                                          onSaved: (value) => amountController,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            labelText:
                                                AppLocalizations.of(context)!
                                                    .localizedString("amount"),
                                            labelStyle: const TextStyle(
                                                color: Colors.green),
                                            counterText: "",
                                            icon: const Icon(
                                              Icons.money,
                                              color: Colors.green,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
<<<<<<< HEAD
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 20),
                                      child: Container(
                                        width: 320,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
=======
                                        Container(
                                          width: MediaQuery.of(context).size.width - 158,
                                          child: HorizontalList(
                                            spacing: 15,
                                            itemCount: BankListQuickPayment == null ? 0 : BankListQuickPayment!.length,
                                            itemBuilder: (context, index) {
                                              return Column(
                                                children: [
                                                  Stack(
                                                    alignment: Alignment.topRight,
                                                    children: [
                                                      Container(
                                                        margin: const EdgeInsets.all(8.7),
                                                        child: InkWell(
                                                          onTap: () {
                                                            setState(() {
                                                              dropdownValue = BankListQuickPayment![index]["BankName"];
                                                              bankName = BankListQuickPayment![index]["BankName"];
                                                              locationId = BankListQuickPayment![index]["BankCode"];

                                                              accountNoController.text = BankListQuickPayment![index]["AccountNumber"];
                                                              nameController.text = BankListQuickPayment![index]["AccountName"];
                                                            });
                                                          },
                                                          child: Initicon(
                                                            text: BankListQuickPayment![index]["BankName"],
                                                            backgroundColor: Colors.green,
                                                            borderRadius: BorderRadius.circular(5),
                                                            size: 60,
                                                          ),
                                                        ),
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          showMessageRemove(BankListQuickPayment![index]["Id"]);
                                                        },
                                                        child: CircleAvatar(
                                                          radius: 10,
                                                          backgroundColor: Color(int.parse(loginbuttonColor.toString())),
                                                          child: const Icon(
                                                            Icons.close,
                                                            color: Colors.white,
                                                            size: 16,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    width: 60,
                                                    child: Text(
                                                      BankListQuickPayment![index]["AccountName"],
                                                      style: TextStyle(
                                                        fontSize: 10,
                                                        color: Color(int.parse(loginbuttonColor.toString())),
                                                      ),
                                                      textAlign: TextAlign.center,
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 30.0, right: 10, top: 20),
                                child: DropdownSearch<String>(
                                  popupProps: PopupProps.dialog(
                                    searchFieldProps: TextFieldProps(
                                      // autofocus: true,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 24, vertical: 10),
                                      decoration: InputDecoration(
                                          border: UnderlineInputBorder(),
                                          hintText: AppLocalizations.of(context)!
                                              .localizedString("search"),
                                          hintStyle: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                              color: Colors.grey[400]),
                                          suffixIcon: Icon(
                                            Icons.search,
                                            size: 30,
                                            color: Colors.grey[500],
                                          )),
                                    ),
                                    showSearchBox: true,
                                  ),
                                  dropdownDecoratorProps: DropDownDecoratorProps(
                                    dropdownSearchDecoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: AppLocalizations.of(context)!
                                            .localizedString("select_bank"),
                                        hintStyle: TextStyle(
                                            fontSize: 16, color: Colors.black87)),
                                  ),
                                  items: dropBankName,
                                  selectedItem: dropdownValue,
                                  // hint: AppLocalizations.of(context)!
                                  //     .localizedString("search"),
                                  onChanged: (String? newValue) {
                                    final index = BankList!.indexWhere(
                                        (element) =>
                                            element["bank_display_name"] ==
                                            newValue);
                                    setState(() {
                                      dropdownValue = newValue!;
                                      bankName = newValue!;

                                      newValue = BankList![index]["swift_code"];
                                      locationId = newValue!;
                                      print(
                                          "this is id : ${locationId} and ${bankName}");
                                    });
                                  },
                                ),
                              ),
                              // Padding(
                              //   padding: const EdgeInsets.only(
                              //       left: 30.0, right: 10.0, top: 10.0),
                              //   child: DropdownSearch<String>(
                              //     mode: Mode.BOTTOM_SHEET,
                              //     showSearchBox: true,
                              //     items: ,
                              //     onChanged: (String? newValue) {
                              //       final index = BankList!.indexWhere(
                              //           (element) =>
                              //               element["LocationName"] == newValue);
                              //       setState(() {
                              //         dropdownValue = newValue!;
                              //         bankName = newValue!;
                              //
                              //         newValue = BankList![index]["LocationId"];
                              //         locationId = newValue!;
                              //         print(
                              //             "this is locatioId : ${dropdownValue?.split(",")[0]}, AND THIS is BANK NAME: ${dropdownValue?.split(",")[1]}");
                              //       });
                              //     },
                              //   ),
                              // ),
                              Form(
                                  key: globalFormKey,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 20.0, top: 20),
                                        child: Container(
                                          width: 320,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          child: TextFormField(
                                            controller: amountController,
                                            maxLength: 6,
                                            keyboardType: TextInputType.number,
                                            validator: (value) => value!.isEmpty
                                                ? AppLocalizations.of(context)!
                                                    .localizedString("amount")
                                                : null,
                                            onSaved: (value) => amountController,
                                            decoration: InputDecoration(
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                              labelText:
                                                  AppLocalizations.of(context)!
                                                      .localizedString("amount"),
                                              labelStyle: const TextStyle(
                                                  color: Colors.green),
                                              counterText: "",
                                              icon: const Icon(
                                                Icons.money,
                                                color: Colors.green,
                                                size: 20.0,
                                              ),
                                            ),
                                          ),
>>>>>>> sudeep
                                        ),
                                        child: TextFormField(
                                          controller: accountNoController,
                                          maxLength: 50,
                                          keyboardType: TextInputType.text,
                                          validator: (value) => value!.isEmpty
                                              ? "Please Enter Destination Account Number"
                                              : null,
                                          onSaved: (value) =>
                                              accountNoController,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            labelText:
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "account_number"),
                                            labelStyle: const TextStyle(
                                                color: Colors.green),
                                            counterText: "",
                                            icon: const Icon(
                                              Icons.supervised_user_circle,
                                              color: Colors.green,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 20),
                                      child: Container(
                                        width: 320,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: TextFormField(
                                          controller: nameController,
                                          maxLength: 200,
                                          keyboardType: TextInputType.text,
                                          validator: (value) => value!.isEmpty
                                              ? "Please Enter Account Holder's Name"
                                              : null,
                                          onSaved: (value) => nameController,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            labelText:
                                                AppLocalizations.of(context)!
                                                    .localizedString(
                                                        "account_holders_name"),
                                            labelStyle: const TextStyle(
                                                color: Colors.green),
                                            counterText: "",
                                            icon: const Icon(
                                              Icons.person,
                                              color: Colors.green,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, top: 20),
                                      child: Container(
                                        width: 320,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: TextFormField(
                                          controller: remarkController,
                                          maxLength: 300,
                                          keyboardType: TextInputType.text,
                                          validator: (value) => value!.isEmpty
                                              ? "Please Enter Remarks."
                                              : null,
                                          onSaved: (value) => nameController,
                                          decoration: InputDecoration(
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            labelText:
                                                AppLocalizations.of(context)!
                                                    .localizedString("remarks"),
                                            labelStyle: const TextStyle(
                                                color: Colors.green),
                                            counterText: "",
                                            icon: const Icon(
                                              Icons.message,
                                              color: Colors.green,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 220.0, top: 20),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    color: Color(
                                        int.parse(loginbuttonColor.toString())),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.5),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ]),
                                height: 50,
                                width: 100,
                                child: TextButton(
                                  onPressed: () async {
                                    if (validateAndSave() &&
                                        validateAmount(double.parse(
                                            amountController.text))) {
                                      setState(() {
                                        _isLoading = true;
                                        // transactionPinMessage()
                                      });
                                      showMessage();
                                    } else {
                                      setState(() {
                                        _isLoading = false;
                                        if (!validateAmount(double.parse(
                                            amountController.text))) {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: Center(
                                                    child: Column(
                                                      children: [
                                                        Text(
                                                          AppLocalizations.of(
                                                                  context)!
                                                              .localizedString(
                                                                  "alert"),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        const Icon(
                                                          Icons.add_alert,
                                                          color: Colors.red,
                                                          size: 50,
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  content: Text(
                                                    AppLocalizations.of(
                                                            context)!
                                                        .localizedString(
                                                            "minimum_amount"),
                                                  ),
                                                  actions: [
                                                    TextButton(
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child: Text(
                                                        AppLocalizations.of(
                                                                context)!
                                                            .localizedString(
                                                                "okay"),
                                                        style: TextStyle(
                                                            color: Colors
                                                                .green[900]),
                                                      ),
                                                    )
                                                  ],
                                                );
                                              });
                                        }
                                      });
                                    }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .localizedString("submit"),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateMPINForm() {
    final form2 = globalFormKey2.currentState;
    if (form2!.validate()) {
      form2.save();
      return true;
    } else {
      return false;
    }
  }

  bool validateAmount(double value) {
    if (value < minimum_Amount!) {
      print("Does not match: ${value}");
      return false;
    }
    if (value > maximum_Amount!) {
      print("Does not match: ${value}");
      return false;
    }
    print("the value match: ${value}");
    return true;
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
