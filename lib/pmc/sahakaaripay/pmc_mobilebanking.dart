import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sahakari_pay/main.dart';

import '../utils/localizations.dart';
import 'models/loading.dart';

class PMCMobileBankingPage extends StatefulWidget {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? name;
  String? logoUrl;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  PMCMobileBankingPage({
    Key? key,
    this.coopList,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.name,
    this.logoUrl,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  }) : super(key: key);

  @override
  _PMCMobileBankingPageState createState() => _PMCMobileBankingPageState(
        coopList,
        accesstoken,
        balance,
        baseUrl,
        accountno,
        name,
        logoUrl,
        primaryColor,
        loginButtonTitleColor,
        loginbuttonColor,
        loginTextFieldColor,
        dasboardIconColor,
        dashboardTopTitleColor,
        SecondaryColor,
      );
}

class _PMCMobileBankingPageState extends State<PMCMobileBankingPage> {
  List? coopList;
  String? accesstoken;
  double? balance;
  String? baseUrl;
  String? accountno;
  String? name;
  String? logoUrl;
  String? primaryColor;
  String? loginButtonTitleColor;
  String? loginbuttonColor;
  String? loginTextFieldColor;
  String? dasboardIconColor;
  String? dashboardTopTitleColor;
  String? SecondaryColor;

  _PMCMobileBankingPageState(
    this.coopList,
    this.accesstoken,
    this.balance,
    this.baseUrl,
    this.accountno,
    this.name,
    this.logoUrl,
    this.primaryColor,
    this.loginButtonTitleColor,
    this.loginbuttonColor,
    this.loginTextFieldColor,
    this.dasboardIconColor,
    this.dashboardTopTitleColor,
    this.SecondaryColor,
  );

  final TextEditingController accountnumbercontroller = TextEditingController();
  final TextEditingController mobilenumbercontroller = TextEditingController();
  final GlobalKey<FormState> globalFormKey = GlobalKey();

  bool _isLoading = false;

  postToRegister() async {
    String url = "${baseUrl}api/v1/registeruser";
    Map body = {
      "AccountNo": accountnumbercontroller.text.toString(),
      "MobileNumber": mobilenumbercontroller.text.toString(),
    };
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body, headers: {
      // "Content-Type": application/json,
      "Authorization": "Bearer ${accesstoken}"
    });
    // print(body);

    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);
      print("Response status: ${jsonResponse}");
      setState(() {
        _isLoading = true;
        if (jsonResponse != null) {
          setState(() {
            _isLoading = false;
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => SahakariScreenPage()));
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: AlertDialog(
              title: Text(jsonResponse["Message"]),
            )));
          });
        }
      });
    } else if (res.statusCode == 400) {
      setState(() {
        _isLoading = true;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(
                      child: Column(
                    children: const [
                      Text(
                        "Error Alert",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Icon(
                        Icons.add_alert,
                        color: Colors.red,
                        size: 50.0,
                      )
                    ],
                  )),
                  content: Text(
                    "${jsonResponse["Message"]}",
                    style: const TextStyle(fontSize: 14),
                  ),
                  actions: [
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text("OKAY"))
                  ],
                );
              });
        });
      }
    } else if (res.statusCode == 401) {
      setState(() {
        _isLoading = false;
      });
      jsonResponse = json.decode(res.body);
      print(jsonResponse);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Center(
                  child: Column(
                children: const [
                  Text(
                    "Error Alert",
                    style: TextStyle(fontSize: 16),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Icon(
                    Icons.add_alert,
                    color: Colors.red,
                    size: 50.0,
                  )
                ],
              )),
              content: Text(
                "${jsonResponse["Message"]}",
                style: const TextStyle(fontSize: 14),
              ),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text("OKAY"))
              ],
            );
          });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  movetohome() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => SahakariScreenPage()),
        (Route<dynamic> route) => false);

    // Navigator.push(
    //     context, MaterialPageRoute(builder: (context) => SahakariScreenPage()));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return movetohome();
      },
      child: Scaffold(
        body: _isLoading
            ? Loading()
            : SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 100,
                      decoration: BoxDecoration(
                        color: Color(int.parse(primaryColor.toString())),
                      ),
                      child: Stack(
                        children: [
                          Positioned(
                            left: 10.0,
                            height: 140.0,
                            child: IconButton(
                                onPressed: () {
                                  movetohome();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 30.0,
                                  color: Colors.white,
                                )),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 30.0,
                    ),
                    const Center(
                      child: SizedBox(
                        height: 100,
                        width: 200,
                        child:
                            Image(image: AssetImage("assets/images/img.png")),
                      ),
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Center(
                      child: Form(
                          key: globalFormKey,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, top: 50, right: 10.0),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12.0, vertical: 10),
                                  child: TextFormField(
                                    controller: accountnumbercontroller,
                                    keyboardType: TextInputType.text,
                                    validator: (value) => value!.isEmpty
                                        ? AppLocalizations.of(context)!
                                            .localizedString(
                                                "account_number_message")
                                        : null,
                                    onSaved: (value) => accountnumbercontroller,
                                    decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText: AppLocalizations.of(context)!
                                          .localizedString(
                                              "account_number_title"),
                                      labelStyle:
                                          TextStyle(color: Colors.green),
                                      counterText: "",
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20.0, right: 10.0),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 12.0, vertical: 10),
                                  child: TextFormField(
                                    controller: mobilenumbercontroller,
                                    maxLength: 10,
                                    keyboardType: TextInputType.number,
                                    validator: (value) =>
                                        value!.isEmpty || value.length < 10
                                            ? AppLocalizations.of(context)!
                                                .localizedString(
                                                    "mobile_characters")
                                            : null,
                                    onSaved: (value) => mobilenumbercontroller,
                                    decoration: InputDecoration(
                                      border: UnderlineInputBorder(),
                                      labelText: AppLocalizations.of(context)!
                                          .localizedString(
                                              "enter_mobile_number"),
                                      labelStyle:
                                          TextStyle(color: Colors.green),
                                      counterText: "",
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )),
                    ),
                    const SizedBox(
                      height: 40.0,
                    ),
                    Center(
                      child: Container(
                        height: 50,
                        width: 200,
                        decoration: BoxDecoration(
                          color: Color(int.parse(loginbuttonColor.toString())),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: MaterialButton(
                          minWidth: 100,
                          onPressed: () async {
                            if (validateAndSave()) {
                              setState(() {
                                _isLoading = true;
                              });
                              postToRegister();
                            } else {
                              setState(() {
                                _isLoading = false;
                              });
                            }
                          },
                          child: Text(
                            AppLocalizations.of(context)!
                                .localizedString("register_now")
                                .toUpperCase(),
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          AppLocalizations.of(context)!
                              .localizedString("already_registered"),
                          style: TextStyle(
                            color: Colors.black87,
                            fontSize: 16.0,
                          ),
                        ),
                        const SizedBox(
                          width: 10.0,
                        ),
                        TextButton(
                            onPressed: () {
                              movetohome();
                            },
                            child: Text(
                              AppLocalizations.of(context)!
                                  .localizedString("login"),
                              style: const TextStyle(
                                color: Colors.black87,
                                fontSize: 16.0,
                              ),
                            ))
                      ],
                    )
                  ],
                ),
              ),
      ),
    );
  }

  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }
}
