import 'dart:convert';
import 'dart:typed_data';

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:encrypt/encrypt.dart' as enc;

import '../constants.dart';
import 'localizations.dart';

class Utils {
  static Future showTopSnackBar(
      BuildContext context, String message, Color color) {
    return Flushbar(
      icon: const Icon(
        Icons.wifi,
        size: 32,
        color: Colors.white,
      ),
      shouldIconPulse: false,
      mainButton: TextButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text(
          AppLocalizations.of(context)!.localizedString("okay"),
        ),
      ),
      messageColor: color,
      borderRadius: BorderRadius.circular(20),
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 40),
      message: message,
      duration: const Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.BOTTOM,
    ).show(context);
  }

  static Future<String> BalanceUpdate(String baseUrl, accesstoken) async {
    double? balance;
    String? accountName;
    String url = "${baseUrl}api/v1/pes/account/${AccountNumber}/info";
    print(url);
    print(accesstoken);
    final response = await http.get(Uri.parse(url),
        headers: {'Authorization': "Bearer ${accesstoken}"});
    print(response.body);
    if (response.statusCode == 200) {
      var convertDataToJson = json.decode(response.body);
      balance = double.parse(convertDataToJson["Balance"].toString());
      Balance = balance;
      accountName = convertDataToJson["AccountName"];
      AccountName = accountName;
      print("this is the account name: ${accountName}");
      print("this is balance: ${balance.toString()}");
      print(convertDataToJson);
      // sharedPreferences.setStringList(
      //     'Cooperatives', accountinfo!.map((e) => e.toString()).toList());
      return "success";
    }
    return "success";
  }

  static String encryptAES(value){
    final key = enc.Key.fromUtf8(encryptDecryptAES32LengthKey);
    final iv = enc.IV(Uint8List(16));
    final encrypter = enc.Encrypter(enc.AES(key));
    final encrypted = encrypter.encrypt(value, iv: iv);
    return encrypted.base64;
  }

  static String decryptAES(value){
    final key = enc.Key.fromUtf8(encryptDecryptAES32LengthKey);
    final iv = enc.IV(Uint8List(16));
    final encrypter = enc.Encrypter(enc.AES(key));
    final decrypted = encrypter.decrypt(enc.Encrypted.fromBase64(value), iv: iv);
    return decrypted;
  }
}
