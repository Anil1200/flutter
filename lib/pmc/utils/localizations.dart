import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppLocalizations {

  Locale? locale;
  static Map<dynamic, dynamic> _localizedValues = new Map<dynamic, dynamic>();

  AppLocalizations(Locale locale){
    _localizedValues;
    this.locale = locale;
  }

  static AppLocalizations? of(BuildContext context){
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get currentLanguage => locale!.languageCode;

  String localizedString(String key) {
    if (_localizedValues != null) {
      return _localizedValues[key] ?? key;
    }

    return key;
  }

  static Future<AppLocalizations> load(Locale locale) async {
    AppLocalizations translations = new AppLocalizations(locale);
    String jsonContent = await rootBundle.loadString("lib/pmc/locale/i18n_${locale.languageCode}.json");
    _localizedValues = json.decode(jsonContent);
    return translations;
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return locale.languageCode=='en' || locale.languageCode=='ne';
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}