import 'package:shared_preferences/shared_preferences.dart';

class CooperativeSharedPreferences {
  static SharedPreferences? _preferences;

  String? Cooperatives;
  // String? accesstoken;

  static Future init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  static Future setsmsCode(String smscode) async {
    await _preferences?.setString("Cooperatives", smscode);
  }

  getsmsCode() {
    Cooperatives = _preferences?.getString("Cooperatives");
    print(Cooperatives);
    // List? Cooperatives;
    // Cooperatives = _keySMS as List?;
  }

  static Future setAccessToken(String accesstoken) async {
    await _preferences?.setString("accesstoken", accesstoken);
  }

  static Future getAccessToken() async {
    String? accesstoken;
    accesstoken = await _preferences?.getString("accesstoken");
    print(accesstoken);
  }
}
